#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <vector>
#include <cstring>
#include <set>
#include <limits>
#include <fstream>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>
#include <memory>

#include <tga.h>

#include <stdlib.h>


#include "render/vulkanhelper.h"
#include "render/storagebuffer.h"
#include "render/vertexbuffer.h"
#include "render/indexbuffer.h"
#include "render/texture.h"
#include "render/model.h"
#include "render/shader.h"
#include "render/renderelement.h"
#include "render/viewport.h"

#include "util/meshhelper.h"
#include "util/simplexnoise.h"
#include "world/world.h"

const int WIDTH = 800;
const int HEIGHT = 600;


const int MAX_FRAMES_IN_FLIGHT = 2;

const std::vector<const char*> validationLayers = {
    "VK_LAYER_KHRONOS_validation"
};

const std::vector<const char*> deviceExtensions = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

RenderElement * rElem;
RenderElement * rElem2;

template <typename T> T randomValueInIntervall(T a, T b) {

    T val = a + (b - a) * ((T) rand() / (T) RAND_MAX);
    return val;

}

class HelloTriangleApplication {

    public:
        void run() {

            initWindow();
            initVulkan();
            mainLoop();
            cleanup();

        }

    private:

        std::vector<VkSemaphore> imageAvailableSemaphores;
        std::vector<VkSemaphore> renderFinishedSemaphores;
        unsigned int frameIndex;
        std::vector<VkFence> inFlightFences;

        VkDescriptorPool descriptorPool;
        std::vector<VkDescriptorSet> descriptorSets;

        bool frameBufferResized = false;

        GLFWwindow * window;

        std::shared_ptr<Model> model;

        std::shared_ptr<Texture> texture;
        VkSampler sampler;

        std::shared_ptr<Shader> shader;

        Viewport * viewport;
        RenderElement::Instance theInstance;

        std::shared_ptr<Camera> camera;

        std::shared_ptr<World> world;

        #define MAX_ANGLE_FACTOR 0.999
        #define MOUSE_SENSITIVITY 0.00125

        static void onKey(GLFWwindow * window, int key, int scancode, int action, int mods) {

            HelloTriangleApplication * app = (HelloTriangleApplication *) glfwGetWindowUserPointer(window);

            double dx = 0;
            double dy = 0;

            switch (key) {

                case GLFW_KEY_W:
                    app->camera->move(0.2f * app->camera->getFacing());
                    break;

                case GLFW_KEY_S:
                    app->camera->move(-0.2f * app->camera->getFacing());
                    break;

            }

        }

        static void onMouseMotion(GLFWwindow * window, double x, double y) {

            static double oldX, oldY;

            double dx = x - oldX;
            double dy = y - oldY;

            oldX = x;
            oldY = y;

            HelloTriangleApplication * app = (HelloTriangleApplication *) glfwGetWindowUserPointer(window);

            app->camera->rotate(-MOUSE_SENSITIVITY * dy, -MOUSE_SENSITIVITY * dx);

        }

        void initWindow() {

            glfwInit();

            glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
            window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan Triangle", nullptr, nullptr);
            glfwSetWindowUserPointer(window, this);

            glfwSetWindowSizeCallback(window, frameBufferResizeCallback);
            glfwSetKeyCallback(window, onKey);
            glfwSetCursorPosCallback(window, onMouseMotion);
            glfwSetInputMode((GLFWwindow*)window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        }


        void initVulkan() {

            camera = std::shared_ptr<Camera>(new Camera(M_PI / 4, 0.01, 100.0, (float) WIDTH / (float) HEIGHT, glm::vec3(0,0,0)));

            viewport = new Viewport(window, camera);

            camera->move(0,0,1);

            sampler = Texture::createSampler(VulkanHelper::getDevice(), 1);

            VulkanHelper::VertexInputDescriptions descs;
            descs.attributes = Model::Vertex::getAttributeDescriptions();
            descs.binding = Model::Vertex::getBindingDescription();

            shader = std::shared_ptr<Shader>(new Shader("shaders/vertex.vert.spirv", "shaders/terrain.frag.spirv"));
            shader->setupDescriptorSetLayout(sampler, 3);
            shader->setupGraphicsPipeline(descs, viewport->getRenderPass());

            texture = std::shared_ptr<Texture>(Texture::createTexture(viewport->getAllocator(), "ground_diffuse_lr.tga"));
            std::shared_ptr<Texture> normalMap = std::shared_ptr<Texture>(Texture::createTexture(viewport->getAllocator(), "ground_normals_lr.tga"));
            std::shared_ptr<Texture> createTexture = std::shared_ptr<Texture>(Texture::createTexture(viewport->getAllocator(), "stonebricks_diffuse.tga"));

            //model = std::shared_ptr<Model>(Model::loadFromFile(viewport->getAllocator(), "dragon_2.ply"));
            std::vector<Model::Vertex> verts;
            std::vector<uint16_t> indices;
            MeshHelper::ModelInfo modelData = MeshHelper::createHexagonPlane(16, 2.0);
            model = std::shared_ptr<Model>(new Model(viewport->getAllocator(), modelData.verts, modelData.indices));

            std::vector<std::shared_ptr<Texture>> textures = {texture, normalMap, createTexture};

            RenderElement::Transform trans;
            trans.position = glm::vec3(0,0,0);
            trans.qRot = Math::Quaternion<float>(1.0, 0.0, 0.0, 0.0);
            trans.scale = 0.0;

            rElem = new RenderElement(viewport, model, shader, textures, viewport->getSwapChainSize(), trans);

            std::array<float, 3> axis = {0.0, 0.0, 1.0};

            trans.position = glm::vec3(0,0,0);
            trans.qRot = Math::Quaternion<float>(1.0, 0.0, 0.0, 0.0);
            trans.scale = 1.0;
            theInstance = rElem->addInstance(trans);

            //viewport->addRenderElement(std::shared_ptr<RenderElement>(rElem));
            //viewport->addRenderElement(std::shared_ptr<RenderElement>(rElem2));

            viewport->addLight(glm::vec4(1.0, -0.5, 0.85, 1.0), glm::vec4(2.0, 2.0, 2.0, 0.0));
            viewport->addLight(glm::vec4(0.2, 0.0, 1.0, 2.0), glm::vec4(0.0, 0.0, 1.0, 0.0));
            viewport->addLight(glm::vec4(1.0, -1.0, 1.0, 2.0), glm::vec4(1.5, 1.5, 1.5, 1.2));

            viewport->addLight(glm::vec4(12.5, 12.5, 0.5, 2.0), glm::vec4(10, 10, 10, 0.0));

            std::shared_ptr<Shader> boxShader = std::shared_ptr<Shader>(new Shader("shaders/vertex.vert.spirv", "shaders/fragment.frag.spirv"));
            boxShader->setupDescriptorSetLayout(sampler, 2);
            boxShader->setupGraphicsPipeline(descs, viewport->getRenderPass());
            std::shared_ptr<Texture> boxAlbedo = std::shared_ptr<Texture>(Texture::createTexture(viewport->getAllocator(), "stonebricks_diffuse.tga"));
            std::shared_ptr<Texture> boxNormal = std::shared_ptr<Texture>(Texture::createTexture(viewport->getAllocator(), "stonebricks_normals.tga"));
            std::shared_ptr<Model> boxModel = std::shared_ptr<Model>(Model::loadFromFile(viewport->getAllocator(), "cube.ply"));

            std::vector<std::shared_ptr<Texture>> boxTex = {boxAlbedo, boxNormal};

            trans.scale = 0.0f;

            rElem2 = new RenderElement(viewport, boxModel, boxShader, boxTex, viewport->getSwapChainSize(), trans);

            std::array<float, 3> zAxis = {0,0,1};

            for (unsigned int i = 0; i < 2000; ++i) {

                float r = randomValueInIntervall<float>(0, sqrt(2) * 16);
                float phi = randomValueInIntervall(-M_PI, M_PI);

                std::cout << "r = " << r << " phi = " << phi << std::endl;

                float x = r * cos(phi);
                float y = r * sin(phi);

                float z = MeshHelper::noise(x, y);

                trans.position = glm::vec3(x,y,z);
                std::cout << "pos: " << trans.position.x << " " << trans.position.y << " " << trans.position.z << std::endl;
                trans.qRot = Math::Quaternion<float>(zAxis.data(), M_PI * SimplexNoise1234::noise(x, y, z));
                trans.scale = 0.1;

                rElem2->addInstance(trans);

            }

            //viewport->addRenderElement(std::shared_ptr<RenderElement>(rElem2));

            std::shared_ptr<ChunkGenerator> chunkGen(new ChunkGenerator());

            world = std::shared_ptr<World>(new World(2));
            std::cout << "Testing world coord transform" << std::endl;
            World::Coordinate cPos = {0, 0};
            world->getChunkFromChunkPos(cPos);

            World::Coordinate cPos2 = {1, 0};
            world->getChunkFromChunkPos(cPos2);

            World::Coordinate cPos3 = {0, 1};
            //world->getChunkFromChunkPos(cPos3);

            world->addChunkProvider(chunkGen);
            world->loadRequiredChunks();

            world->addChunksToView(viewport, shader, textures, viewport->getSwapChainSize());

            viewport->recordCommandBuffers();


        }

        void recreateSwapChain() {

            VkDevice device = VulkanHelper::getDevice();

            rElem->destroyUniformBuffers();
            rElem2->destroyUniformBuffers();

            int width = 0, height = 0;

            while (!width || !height) {
                glfwGetFramebufferSize(window, &width, &height);
                glfwWaitEvents();
            }

            vkDeviceWaitIdle(device);

            std::vector<Shader *> shaders = {shader.get()};

            viewport->destroySwapChain();

            viewport->recreateSwapChain();

        }

        void createSyncObjects() {

            VkDevice device = VulkanHelper::getDevice();

            imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
            renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
            inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

            VkSemaphoreCreateInfo semaphoreInfo = {};
            semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

            VkFenceCreateInfo fenceInfo = {};
            fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
            fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

            for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {

                if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS || vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS) {
                    throw std::runtime_error("Unable to create semaphores");
                }

                if (vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS)
                    throw std::runtime_error("Unable to create fence");

            }

        }

        void renderFunction(VkCommandBuffer & cmdBuffer, int i) {


            rElem->render(cmdBuffer, i);
            rElem2->render(cmdBuffer, i);

        }

        void mainLoop() {

            frameIndex = 0;

            std::array<float, 3> axisArray = {0, 0, 1};
            Math::Vector<3, float> axis = Math::Vector<3, float>(axisArray.data());
            auto startRenderTime = std::chrono::high_resolution_clock::now();

            int fCount = 0;


            while (!glfwWindowShouldClose(window)) {
                glfwPollEvents();

                if (this->frameBufferResized) {
                    viewport->setFramebufferResize();
                    frameBufferResized = false;
                }

                float time = std::chrono::duration<float, std::chrono::seconds::period>(std::chrono::high_resolution_clock::now() - startRenderTime).count();

                RenderElement::Transform trans;
                trans.position = glm::vec3(0,0,0);
                trans.qRot = Math::Quaternion<float>(axis, time * M_PI / 6);
                trans.scale = 0.5;
                //rElem->updateInstance(theInstance, trans);

                //camera->rotate(Math::Quaternion<float>(axis, 0.02 * cos(time * M_PI / 6)));

                viewport->drawFrame();
            }

            vkDeviceWaitIdle(viewport->getDevice());


        }

        static void frameBufferResizeCallback(GLFWwindow * window, int width, int height) {

            HelloTriangleApplication * app = (HelloTriangleApplication*) glfwGetWindowUserPointer(window);

            app->frameBufferResized = true;


        }

        void cleanup() {

            glfwSetInputMode((GLFWwindow*) window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

            VkDevice device = viewport->getDevice();

            std::vector<Shader *> shaders = {shader.get()};

            delete rElem2;
            delete rElem;

            /*model = nullptr;
            texture = nullptr;
            shader = nullptr;*/

            vkDestroySampler(device, sampler, nullptr);

            delete viewport;


            glfwDestroyWindow(window);
            glfwTerminate();

        }


};

int main (int arcg , char ** argv) {

    HelloTriangleApplication app;

    app.run();

    return 0;

}
