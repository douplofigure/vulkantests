#ifndef TEXTURE_H
#define TEXTURE_H

#include <vector>
#include <string>
#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

class Texture
{
    public:
        Texture(VmaAllocator & allocator, const std::vector<float> & data, int width, int height, int depth);
        virtual ~Texture();

        void transitionLayout(VkImageLayout layout);

        VkImageView & getView();
        VkImageLayout getLayout();

        VkSampler & getSampler();

        static Texture * createTexture(VmaAllocator & allocator, std::string fname);

        static void createImage(VmaAllocator & allocator, VkDevice device, int width, int height, int depth, int mipLevels, VkFormat format, VkImageUsageFlags usage, VkMemoryPropertyFlagBits memProps, VkImage & image, VmaAllocation & memory);
        static void copyBufferToImage(VkDevice & device, VkBuffer & buffer, VkImage & image, uint32_t width, uint32_t height, uint32_t depth);
        static VkImageView createImageView(VkDevice & device, VkImage & image, VkFormat format, VkImageAspectFlags aspect, int mipLevels);
        static VkSampler createSampler(VkDevice & device, int mipLevels);
        static void transitionImageLayout(VkImage & image, VkFormat format, VkImageLayout layout, VkImageLayout newLayout, int mipLevels);

    protected:

        VkImage image;
        VmaAllocation memory;
        VkImageLayout layout;
        VkFormat format;
        VkImageView view;

        int mipLevels;
        VkSampler sampler;

    private:

        VkDevice device;
        VmaAllocator & allocator;

        void generateMipmaps(int width, int height);

};

#endif // TEXTURE_H
