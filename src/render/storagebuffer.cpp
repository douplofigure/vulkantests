#include "storagebuffer.h"
#include <stdexcept>
#include "vulkanhelper.h"

StorageBuffer::StorageBuffer(VmaAllocator & allocator, size_t size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties) : allocator(allocator) {

    createBuffer(allocator, size, usage, properties, buffer, memory);
    this->device = VulkanHelper::getDevice();

}

StorageBuffer::~StorageBuffer() {

    vmaDestroyBuffer(allocator, buffer, memory);
}

uint32_t StorageBuffer::findMemoryType(uint32_t filter, VkMemoryPropertyFlags properties) {

    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(VulkanHelper::getPhysicalDevice(), &memProperties);

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i) {

        if ((filter & (1 << i)) && ((memProperties.memoryTypes[i].propertyFlags & properties) == properties))
            return i;

    }

    throw std::runtime_error("No suitable memory found");

}

VkBuffer & StorageBuffer::getBuffer() {
    return buffer;
}

VmaAllocation & StorageBuffer::getMemory() {
    return memory;
}

void StorageBuffer::createBuffer(VmaAllocator & allocator, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer & buffer, VmaAllocation & memory) {

    VkDevice device = VulkanHelper::getDevice();

    if (properties & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
        throw std::runtime_error("Bad buffer alloc");

    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
    //allocInfo.requiredFlags = properties;

    vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &buffer, &memory, nullptr);

    /*if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS)
        throw std::runtime_error("Unable to create vertex buffer");


    VkMemoryRequirements memRequirements = {};
    vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

    if (vkAllocateMemory(device, &allocInfo, nullptr, &memory) != VK_SUCCESS)
        throw std::runtime_error("Unable to allocate memory");

    vkBindBufferMemory(device, buffer, memory, 0);*/

}
