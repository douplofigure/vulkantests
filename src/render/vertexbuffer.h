#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include "storagebuffer.h"
#include <string.h>

template <typename T> class VertexBuffer : public StorageBuffer {

    public:
        VertexBuffer(VmaAllocator & allocator, const std::vector<T> & data) : StorageBuffer(allocator, sizeof(T) * data.size(), VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) {

            VkDeviceSize bufferSize = sizeof(T) * data.size();

            VkBuffer stagingBuffer;
            VmaAllocation stagingBufferMemory;

            VkBufferCreateInfo stBufferCreateInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
            stBufferCreateInfo.size = bufferSize;
            stBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
            stBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

            VmaAllocationCreateInfo stAllocCreateInfo = {};
            stAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
            stAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;

            VmaAllocationInfo stagingBufferAllocInfo = {};

            vmaCreateBuffer(allocator, &stBufferCreateInfo, &stAllocCreateInfo, &stagingBuffer, &stagingBufferMemory, &stagingBufferAllocInfo);

            void * tmp;
            //vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &tmp);
            //vmaMapMemory(allocator, stagingBufferMemory, &tmp);
            //memcpy(tmp, data.data(), bufferSize);
            memcpy(stagingBufferAllocInfo.pMappedData, data.data(), bufferSize);
            //vkUnmapMemory(device, stagingBufferMemory);
            //vmaUnmapMemory(allocator, stagingBufferMemory);

            VulkanHelper::copyBuffer(stagingBuffer, buffer, bufferSize);

            /*vkDestroyBuffer(device, stagingBuffer, nullptr);
            vkFreeMemory(device, stagingBufferMemory, nullptr);*/
            vmaDestroyBuffer(allocator, stagingBuffer, stagingBufferMemory);

        }
        virtual ~VertexBuffer(){

        }

        void bindForRender(VkCommandBuffer & cmdBuffer) {

            VkDeviceSize offsets[] = {0};
            vkCmdBindVertexBuffers(cmdBuffer, 0, 1, &buffer, offsets);

        }

    protected:

    private:

};

#endif // VERTEXBUFFER_H
