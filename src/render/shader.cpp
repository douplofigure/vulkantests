#include "shader.h"

#include <iostream>

Shader::Shader(std::string vertShader, std::string fragShader) {

    this->modules.resize(2);
    this->stages = {VK_SHADER_STAGE_VERTEX_BIT, VK_SHADER_STAGE_FRAGMENT_BIT};

    std::vector<uint8_t> vertShaderCode = readFile(vertShader);
    std::vector<uint8_t> fragShaderCode = readFile(fragShader);

    modules[0] = VulkanHelper::createShaderModule(vertShaderCode);
    modules[1] = VulkanHelper::createShaderModule(fragShaderCode);

    hasDescSets = false;

}

Shader::~Shader() {

    for (unsigned int i = 0; i < modules.size(); ++i)
        vkDestroyShaderModule(VulkanHelper::getDevice(), modules[i], nullptr);

}

std::vector<VkDescriptorSetLayoutBinding> Shader::getBindings(VkSampler & sampler, unsigned int textureCount) {

    std::vector<VkDescriptorSetLayoutBinding> bindings(1 + textureCount);

    VkDescriptorSetLayoutBinding uboLayoutBinding = {};
    uboLayoutBinding.binding = 0;
    uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBinding.descriptorCount = 1;
    uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    uboLayoutBinding.pImmutableSamplers = nullptr;

    bindings[0] = uboLayoutBinding;

    for (unsigned int i = 0; i < textureCount; ++i) {

        VkDescriptorSetLayoutBinding texLayoutBinding = {};
        texLayoutBinding.binding = i+1;
        texLayoutBinding.descriptorCount = 1;
        texLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        texLayoutBinding.pImmutableSamplers = &sampler;
        texLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

        bindings[i + 1] = texLayoutBinding;

    }

    return bindings;

}

void Shader::setupDescriptorSetLayout(VkSampler & sampler, unsigned int textures) {

    std::vector<VkDescriptorSetLayoutBinding> bindings = getBindings(sampler, textures);
    descSetLayout = VulkanHelper::createDescriptorSetLayout(bindings);

}

void Shader::bindForRender(VkCommandBuffer & cmdBuffer, VkDescriptorSet & descriptors) {

    vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptors, 0, nullptr);

}

void Shader::setupGraphicsPipeline(VulkanHelper::VertexInputDescriptions & descs, VkRenderPass & renderPass) {

    std::vector<VulkanHelper::ShaderInputDescription> inputShaders(modules.size());
    for (unsigned int i = 0; i < modules.size(); ++i) {

        inputShaders[i].entryName = "main";
        inputShaders[i].module = modules[i];
        inputShaders[i].usage = stages[i];

    }

    graphicsPipeline = VulkanHelper::createGraphicsPipeline(renderPass, inputShaders, descs, descSetLayout, pipelineLayout);


}

VkDescriptorPool Shader::setupDescriptorPool(VkDevice& device, int scSize, int textureCount) {

    VkDescriptorPoolSize poolSize = {};
    poolSize.type =  VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSize.descriptorCount = scSize;

    VkDescriptorPoolSize samplerSize = {};
    samplerSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerSize.descriptorCount = scSize;

    //VkDescriptorPoolSize sizes[] = {poolSize, samplerSize, samplerSize};
    std::vector<VkDescriptorPoolSize> sizes(1 + textureCount);
    sizes[0] = poolSize;
    for (int i = 1; i < textureCount+1; ++i)
        sizes[i] = samplerSize;

    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = 1 + textureCount;
    poolInfo.pPoolSizes = sizes.data();
    poolInfo.maxSets = scSize;

    VkDescriptorPool descriptorPool;

    if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS)
        throw std::runtime_error("Unable to create descriptor pool");

    return descriptorPool;

}

std::vector<VkDescriptorSet> Shader::createDescriptorSets(VkDevice & device, VkDescriptorPool & descPool, std::vector<VkBuffer> & uniformBuffers, size_t elementSize, std::vector<std::shared_ptr<Texture>> & tex, int scSize) {

    if (descPool == VK_NULL_HANDLE)
        throw std::runtime_error("Cannot create descriptor in NULL-pool!");

    std::cout << "Creating descriptor set with " << tex.size() << " textures" << std::endl;

    std::vector<VkDescriptorSetLayout> layouts(scSize, descSetLayout);

    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = descPool;
    allocInfo.descriptorSetCount = scSize;
    allocInfo.pSetLayouts = layouts.data();

    this->descSets.resize(scSize);
    if (vkAllocateDescriptorSets(device, &allocInfo, descSets.data()) != VK_SUCCESS)
        throw std::runtime_error("Unable to allocate descriptor sets");

    for (int i = 0; i < scSize; ++i) {

        int index = 0;

        std::vector<VkWriteDescriptorSet> descriptorWrites(1 + tex.size());

            VkDescriptorBufferInfo bufferInfo = {};
            bufferInfo.buffer = uniformBuffers[i];
            bufferInfo.offset = 0;
            bufferInfo.range = elementSize;

            descriptorWrites[index].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptorWrites[index].dstSet = descSets[i];
            descriptorWrites[index].dstBinding = 0;
            descriptorWrites[index].dstArrayElement = 0;
            descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            descriptorWrites[index].descriptorCount = 1;
            descriptorWrites[index].pBufferInfo = &bufferInfo;
            descriptorWrites[index].pImageInfo = nullptr;
            descriptorWrites[index].pTexelBufferView = nullptr;
            index++;

        std::vector<VkDescriptorImageInfo> imageInfos(tex.size());

        for (unsigned int j = 0; j < tex.size(); ++j) {

            imageInfos[j].imageView = tex[j]->getView();
            imageInfos[j].sampler = tex[j]->getSampler();
            imageInfos[j].imageLayout = tex[j]->getLayout();


            descriptorWrites[index].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptorWrites[index].dstSet = descSets[i];
            descriptorWrites[index].dstBinding = index;
            descriptorWrites[index].dstArrayElement = 0;
            descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            descriptorWrites[index].descriptorCount = 1;
            descriptorWrites[index].pBufferInfo = nullptr;
            descriptorWrites[index].pImageInfo = &imageInfos[j];
            descriptorWrites[index].pTexelBufferView = nullptr;

            index++;

        }

        vkUpdateDescriptorSets(device, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);

    }

    return descSets;

}

VkPipeline & Shader::getPipeline() {
    return graphicsPipeline;
}

VkPipelineLayout & Shader::getPipelineLayout() {
    return this->pipelineLayout;
}
