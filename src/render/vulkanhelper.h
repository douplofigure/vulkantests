#ifndef VULKANHELPER_H
#define VULKANHELPER_H

#include <string>
#include <functional>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>

class Texture;
class Shader;

class VulkanHelper
{
    public:

        struct VertexInputDescriptions {
            std::vector<VkVertexInputBindingDescription> binding;
            std::vector<VkVertexInputAttributeDescription> attributes;
        };

        struct ShaderInputDescription {
            VkShaderModule module;
            VkShaderStageFlagBits usage;
            char * entryName;
        };

        static VkInstance createInstance();
        static void setupDebugMessenger();
        static VkSurfaceKHR createSurface(GLFWwindow * window);
        static VkPhysicalDevice pickPhysicalDevice();
        static VkDevice createLogicalDevice();
        static VkSwapchainKHR createSwapChain();
        static std::vector<VkImageView> createImageViews();
        static VkRenderPass createRenderPass();
        static VkDescriptorSetLayout createDescriptorSetLayout(std::vector<VkDescriptorSetLayoutBinding> & bindings);

        static VkPipeline createGraphicsPipeline(VkRenderPass & renderPass, std::vector<ShaderInputDescription>& shaders, VertexInputDescriptions & descs, VkDescriptorSetLayout & descriptorSetLayout, VkPipelineLayout & retLayout);

        static VkShaderModule createShaderModule(const std::vector<uint8_t> & byteCode);
        static std::vector<VkFramebuffer> createFramebuffers();
        static VkCommandPool & createCommandPool();
        static std::vector<VkCommandBuffer> createCommandBuffers();
        static void recordCommandBuffers(VkPipeline graphicsPipeline, std::vector<VkDescriptorSet> descriptorSets, std::function<void(VkCommandBuffer &, int)> renderFunction);
        static VkDescriptorPool createDescriptorPool(int descCount);
        static std::vector<VkDescriptorSet> createDescriptorSets(VkDescriptorPool & descriptorPool, VkDescriptorSetLayout & descriptorSetLayout, std::vector<VkBuffer> & uniformBuffers, size_t elementSize, Texture * tex, VkSampler & sampler, int swapChainSize);

        static VkCommandBuffer beginSingleCommand();
        static void endSingleCommand(VkCommandBuffer & commandBuffer);

        static void createDepthResources();

        static void cleanup();
        static void cleanupSwapChain(std::vector<Shader *> shaders);

        static void copyBuffer(VkBuffer & src, VkBuffer & dst, VkDeviceSize & size);


        static VkDevice & getDevice();
        static VkSurfaceKHR & getSurface();
        static VkPhysicalDevice & getPhysicalDevice();
        static VkQueue & getGraphicsQueue();
        static VkQueue & getPresentQueue();
        static VkSwapchainKHR & getSwapChain();
        static VkExtent2D & getSwapChainExtent();
        static VkCommandBuffer & getCommandBufferByIndex(int i);
        static VkCommandBuffer * getCommandBufferPtr(int i);

        static int getSwapChainSize();

        static void getSwapChainComponents(VkSwapchainKHR & swapChain, VkExtent2D & extent, std::vector<VkImage> & images, std::vector<VkImageView> & imageViews, VkFormat & swapChainFormat);

    protected:

    private:

        ///Global state

        static VkInstance instance;
        static VkSurfaceKHR surface;
        static VkDevice device;
        static VkPhysicalDevice physicalDevice;
        static VkQueue graphicsQueue;
        static VkQueue presentQueue;
        static GLFWwindow * window;

        static VkSwapchainKHR swapChain;
        static VkFormat swapChainFormat;
        static VkExtent2D swapChainExtent;
        static std::vector<VkImage> swapChainImages;
        static std::vector<VkImageView> swapChainImageViews;
        static std::vector<VkFramebuffer> swapChainFramebuffers;

        static VkCommandPool commandPool;
        static std::vector<VkCommandBuffer> commandBuffers;
        static VkImage depthImage;
        static VkDeviceMemory depthImageMemory;
        static VkImageView depthImageView;
        static VkRenderPass renderPass;


        ///Local state
        /*static VkDescriptorSetLayout descriptorSetLayout;
        static VkPipelineLayout pipelineLayout;
        static VkPipeline graphicsPipeline;
        static VkDescriptorPool descriptorPool;
        static std::vector<VkDescriptorSet> descriptorSets;*/


};

std::vector<uint8_t> readFile(const std::string & fname);

#endif // VULKANHELPER_H
