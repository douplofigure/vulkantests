#ifndef INDEXBUFFER_H
#define INDEXBUFFER_H

#include "storagebuffer.h"
#include "vulkanhelper.h"
#include <type_traits>

#include <vector>
#include <string.h>

template <typename T = uint16_t> class IndexBuffer : public StorageBuffer {

    public:
        IndexBuffer(VmaAllocator & allocator, const std::vector<T> & indices) : StorageBuffer(allocator, sizeof(T) * indices.size(), VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) {

            VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

            VkBuffer stagingBuffer;
            VmaAllocation stagingBufferMemory;
            //StorageBuffer::createBuffer(allocator, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

            VkBufferCreateInfo stBufferCreateInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
            stBufferCreateInfo.size = bufferSize;
            stBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
            stBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

            VmaAllocationCreateInfo stAllocCreateInfo = {};
            stAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
            stAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;

            VmaAllocationInfo stagingBufferAllocInfo = {};

            vmaCreateBuffer(allocator, &stBufferCreateInfo, &stAllocCreateInfo, &stagingBuffer, &stagingBufferMemory, &stagingBufferAllocInfo);

            void * data;
            //vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
            //vmaMapMemory(allocator, stagingBufferMemory, &data);
            memcpy(stagingBufferAllocInfo.pMappedData, indices.data(), bufferSize);
            //vmaUnmapMemory(allocator, stagingBufferMemory);
            //vkUnmapMemory(device, stagingBufferMemory);

            VulkanHelper::copyBuffer(stagingBuffer, buffer, bufferSize);

            vmaDestroyBuffer(allocator, stagingBuffer, stagingBufferMemory);
            /*vkDestroyBuffer(device, stagingBuffer, nullptr);
            vkFreeMemory(device, stagingBufferMemory, nullptr);*/

        }
        virtual ~IndexBuffer() {

        }

        void bindForRender(VkCommandBuffer & cmdBuffer) {

            vkCmdBindIndexBuffer(cmdBuffer, buffer, 0, (VkIndexType) (sizeof(T) / 4));

        }

    protected:

    private:
};

#endif // INDEXBUFFER_H
