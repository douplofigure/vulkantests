#include "viewport.h"

#include <limits>
#include <chrono>
#include <iostream>

#include "vulkanhelper.h"
#include "texture.h"
#include "shader.h"
#include "renderelement.h"
#include "model.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

const int MAX_FRAMES_IN_FLIGHT = 2;

struct Viewport::CameraData {

    alignas(16) glm::vec3 position;

};

Viewport::Viewport(GLFWwindow * window, std::shared_ptr<Camera> cam) {

    this->window = window;
    this->camera = cam;

    this->instance = VulkanHelper::createInstance();
    this->surface = VulkanHelper::createSurface(window);
    this->physicalDevice = VulkanHelper::pickPhysicalDevice();
    this->device = VulkanHelper::createLogicalDevice();

    VmaAllocatorCreateInfo allocCreateInfo = {};
    allocCreateInfo.device = device;
    allocCreateInfo.physicalDevice = physicalDevice;
    allocCreateInfo.frameInUseCount = 1;

    vmaCreateAllocator(&allocCreateInfo, &vmaAllocator);

    this->graphicsQueue = VulkanHelper::getGraphicsQueue();
    this->presentQueue = VulkanHelper::getPresentQueue();

    this->swapChain.chain = VulkanHelper::createSwapChain();
    this->swapChain.imageViews = VulkanHelper::createImageViews();
    VulkanHelper::getSwapChainComponents(swapChain.chain, swapChain.extent, swapChain.images, swapChain.imageViews, swapChain.format);

    this->setupRenderPass();

    this->commandPool = VulkanHelper::createCommandPool();

    VulkanHelper::createDepthResources();
    VkFormat depthFormat = VK_FORMAT_D32_SFLOAT; /// <- this can be chosen by a function later

    Texture::createImage(vmaAllocator, device, swapChain.extent.width, swapChain.extent.height, 1, 1, depthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage, depthImageMemory);
    depthImageView = Texture::createImageView(device, depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1);

    Texture::transitionImageLayout(depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1);

    createPPObjects();

    this->createPpDescriptorSetLayout();
    this->setupPostProcessingPipeline();

    this->setupFramebuffers();

    this->createPpDescriptorPool();
    this->createPPDescriptorSets();

    this->setupCommandBuffers();

    createTransferCommandBuffer();

    frameIndex = 0;
    frameBufferResized = false;

    this->ppBufferModel = std::shared_ptr<Model>(Model::loadFromFile(vmaAllocator, "cube.ply"));

    this->createSyncObjects();
    lightIndex = 0;

    for (int i = 0; i < 32; ++i) {
        lights.color[i] = glm::vec4(0,0,0,0);
        lights.position[i] = glm::vec4(0,0,0,0);
    }

}

Viewport::~Viewport() {

    /*vkDestroyImage(device, depthImage, nullptr);
    vkDestroyImageView(device, depthImageView, nullptr);
    */
    vmaDestroyImage(vmaAllocator, depthImage, depthImageMemory);
    this->destroySwapChain();
    this->destroyPPObjects();

    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
        vkDestroySemaphore(device, renderFinishedSemaphores[i], nullptr);
        vkDestroySemaphore(device, imageAvailableSemaphores[i], nullptr);
        vkDestroyFence(device, inFlightFences[i], nullptr);
    }

    VulkanHelper::cleanup();

}

VmaAllocator & Viewport::getAllocator() {
    return vmaAllocator;
}

void Viewport::createPPObjects() {

    Texture::createImage(vmaAllocator, device, swapChain.extent.width, swapChain.extent.height, 1, 1, VK_FORMAT_R16G16B16A16_SFLOAT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, gBufferImage, gBufferImageMemory);
    gBufferImageView = Texture::createImageView(device, gBufferImage, VK_FORMAT_R16G16B16A16_SFLOAT, VK_IMAGE_ASPECT_COLOR_BIT, 1);

    Texture::createImage(vmaAllocator, device, swapChain.extent.width, swapChain.extent.height, 1, 1, VK_FORMAT_R16G16B16A16_SFLOAT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, nBufferImage, nBufferImageMemory);
    nBufferImageView = Texture::createImageView(device, nBufferImage, VK_FORMAT_R16G16B16A16_SFLOAT, VK_IMAGE_ASPECT_COLOR_BIT, 1);

    Texture::createImage(vmaAllocator, device, swapChain.extent.width, swapChain.extent.height, 1, 1, swapChain.format, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, aBufferImage, aBufferImageMemory);
    aBufferImageView = Texture::createImageView(device, aBufferImage, swapChain.format, VK_IMAGE_ASPECT_COLOR_BIT, 1);

    VkDeviceSize lightSize = sizeof(LightData);
    VkDeviceSize cameraSize = sizeof(CameraData);

    ppLightBuffers.resize(swapChain.imageViews.size());
    ppLightBuffersMemory.resize(swapChain.imageViews.size());

    ppCameraBuffers.resize(swapChain.imageViews.size());
    ppCameraBuffersMemory.resize(swapChain.imageViews.size());

    for (int i = 0; i < swapChain.imageViews.size(); ++i) {

        {
            VkBufferCreateInfo stBufferCreateInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
            stBufferCreateInfo.size = lightSize;
            stBufferCreateInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
            stBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

            VmaAllocationCreateInfo stAllocCreateInfo = {};
            stAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
            stAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;

            VmaAllocationInfo stagingBufferAllocInfo = {};

            vmaCreateBuffer(vmaAllocator, &stBufferCreateInfo, &stAllocCreateInfo, &ppLightBuffers[i], &ppLightBuffersMemory[i], &stagingBufferAllocInfo);
        }
        {
            VkBufferCreateInfo stBufferCreateInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
            stBufferCreateInfo.size = cameraSize;
            stBufferCreateInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
            stBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

            VmaAllocationCreateInfo stAllocCreateInfo = {};
            stAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
            stAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;

            VmaAllocationInfo stagingBufferAllocInfo = {};

            vmaCreateBuffer(vmaAllocator, &stBufferCreateInfo, &stAllocCreateInfo, &ppCameraBuffers[i], &ppCameraBuffersMemory[i], &stagingBufferAllocInfo);
        }
        //StorageBuffer::createBuffer(vmaAllocator, cameraSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, ppCameraBuffers[i], ppCameraBuffersMemory[i]);

    }

}

void Viewport::destroyPPObjects() {

    vmaDestroyImage(vmaAllocator, gBufferImage, gBufferImageMemory);
    vkDestroyImageView(device, gBufferImageView, nullptr);

    vmaDestroyImage(vmaAllocator, nBufferImage, nBufferImageMemory);
    vkDestroyImageView(device, nBufferImageView, nullptr);

    vmaDestroyImage(vmaAllocator, aBufferImage, aBufferImageMemory);
    vkDestroyImageView(device, aBufferImageView, nullptr);

    /*vkDestroyImage(device, gBufferImage, nullptr);
    vkFreeMemory(device, gBufferImageMemory, nullptr);

    vkDestroyImage(device, nBufferImage, nullptr);
    vkFreeMemory(device, nBufferImageMemory, nullptr);

    vkDestroyImage(device, aBufferImage, nullptr);
    vkFreeMemory(device, aBufferImageMemory, nullptr);*/

    for (int i = 0; i < swapChain.imageViews.size(); ++i) {

        vmaDestroyBuffer(vmaAllocator, ppLightBuffers[i], ppLightBuffersMemory[i]);
        vmaDestroyBuffer(vmaAllocator, ppCameraBuffers[i], ppCameraBuffersMemory[i]);

        /*vkDestroyBuffer(device, ppLightBuffers[i], nullptr);
        vkDestroyBuffer(device, ppCameraBuffers[i], nullptr);

        vkFreeMemory(device, ppLightBuffersMemory[i], nullptr);
        vkFreeMemory(device, ppCameraBuffersMemory[i], nullptr);*/

    }

}

void Viewport::setupRenderPass() {

    VkAttachmentDescription colorAttachment = {};
    colorAttachment.format = swapChain.format;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentDescription gAttachment = {};
    gAttachment.format = VK_FORMAT_R16G16B16A16_SFLOAT;
    gAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    gAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    gAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    gAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    gAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    gAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    gAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription nAttachment = {};
    nAttachment.format = VK_FORMAT_R16G16B16A16_SFLOAT;
    nAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    nAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    nAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    nAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    nAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    nAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    nAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription aAttachment = {};
    aAttachment.format = swapChain.format;
    aAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    aAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    aAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    aAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    aAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    aAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    aAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription depthAttachment = {};
    depthAttachment.format = VK_FORMAT_D32_SFLOAT;
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference colorAttachmentRef = {};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthAttachmentRef = {};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference gAttachmentRef = {};
    gAttachmentRef.attachment = 2;
    gAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference nAttachmentRef = {};
    nAttachmentRef.attachment = 3;
    nAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference aAttachmentRef = {};
    aAttachmentRef.attachment = 4;
    aAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference gInputRef = {};
    gInputRef.attachment = 2;
    gInputRef.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkAttachmentReference nInputRef = {};
    nInputRef.attachment = 3;
    nInputRef.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkAttachmentReference aInputRef = {};
    aInputRef.attachment = 4;
    aInputRef.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    std::array<VkAttachmentReference, 3> outputRefs = {gAttachmentRef, nAttachmentRef, aAttachmentRef};
    std::array<VkAttachmentReference, 3> inputRefs  = {gInputRef, nInputRef, aInputRef};

    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = outputRefs.size();
    subpass.pColorAttachments = outputRefs.data();
    subpass.pDepthStencilAttachment = &depthAttachmentRef;

    VkSubpassDescription subpass2 = {};
    subpass2.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass2.colorAttachmentCount = 1;
    subpass2.pColorAttachments = &colorAttachmentRef;
    subpass2.inputAttachmentCount = inputRefs.size();
    subpass2.pInputAttachments = inputRefs.data();

    VkSubpassDependency dependency = {};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    VkSubpassDependency dependency2 = {};
    dependency2.srcSubpass = 0;
    dependency2.dstSubpass = 1;
    dependency2.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency2.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependency2.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency2.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    std::array<VkAttachmentDescription, 5> attachments = {colorAttachment, depthAttachment, gAttachment, nAttachment, aAttachment};

    std::array<VkSubpassDescription, 2> subpasses = {subpass, subpass2};
    std::array<VkSubpassDependency, 2> dependencies = {dependency, dependency2};

    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = attachments.size();
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = subpasses.size();
    renderPassInfo.pSubpasses = subpasses.data();
    renderPassInfo.dependencyCount = dependencies.size();
    renderPassInfo.pDependencies = dependencies.data();

    if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS)
        throw std::runtime_error("Unable to create renderpass");

}

void Viewport::setupFramebuffers() {

    swapChain.framebuffers.resize(swapChain.imageViews.size());

    std::cout << "SwapChainSize " << swapChain.imageViews.size() << std::endl;

    for (int i = 0; i < swapChain.imageViews.size(); ++i) {

        std::array<VkImageView, 5> attachments = {swapChain.imageViews[i], depthImageView, gBufferImageView, nBufferImageView, aBufferImageView};

        VkFramebufferCreateInfo framebufferInfo = {};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = renderPass;
        framebufferInfo.attachmentCount = attachments.size();
        framebufferInfo.pAttachments = attachments.data();
        framebufferInfo.width = swapChain.extent.width;
        framebufferInfo.height = swapChain.extent.height;
        framebufferInfo.layers = 1;

        if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChain.framebuffers[i]) != VK_SUCCESS)
            throw std::runtime_error("Unable to create framebuffer");

    }

    if (this->camera)
        this->camera->updateProjection(70.0, 0.01, 100.0, (float) swapChain.extent.width / (float) swapChain.extent.height);

}

void Viewport::destroySwapChain() {

    destroyPPObjects();

    for (auto framebuffer : swapChain.framebuffers) {
        vkDestroyFramebuffer(device, framebuffer, nullptr);
    }

    vkDestroyImageView(device, depthImageView, nullptr);
    vmaDestroyImage(vmaAllocator, depthImage, depthImageMemory);

    vkFreeCommandBuffers(device, commandPool, commandBuffers.size(), commandBuffers.data());

    vkDestroyRenderPass(device, renderPass, nullptr);

    for (const VkImageView & v : swapChain.imageViews) {
        vkDestroyImageView(device, v, nullptr);
    }

    for (const VkImage & i : swapChain.images) {
        vkDestroyImage(device, i, nullptr);
    }

    vkDestroySwapchainKHR(device, swapChain.chain, nullptr);

    for (unsigned int i = 0; i < renderElements.size(); ++i) {
        this->renderElements[i]->destroyUniformBuffers();
    }

}

void Viewport::recreateSwapChain() {

    this->swapChain.chain = VulkanHelper::createSwapChain();
    this->swapChain.imageViews = VulkanHelper::createImageViews();
    VulkanHelper::getSwapChainComponents(swapChain.chain, swapChain.extent, swapChain.images, swapChain.imageViews, swapChain.format);

    this->setupRenderPass();

    VkFormat depthFormat = VK_FORMAT_D32_SFLOAT; /// <- this can be chosen by a function later

    Texture::createImage(vmaAllocator, device, swapChain.extent.width, swapChain.extent.height, 1, 1, depthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage, depthImageMemory);
    std::cout << "Creating depth image view" << std::endl;
    depthImageView = Texture::createImageView(device, depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1);

    Texture::transitionImageLayout(depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1);

    std::cout << "Creating PP objects" << std::endl;
    createPPObjects();

    //Texture::transitionImageLayout(gBufferImage, swapChain.format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

    this->setupFramebuffers();

    for (unsigned int i = 0; i < renderElements.size(); ++i) {
        renderElements[i]->recreateResources(device, renderPass, swapChain.imageViews.size());
    }

    this->setupPostProcessingPipeline();
    this->createPpDescriptorPool();
    this->createPPDescriptorSets();

    this->setupCommandBuffers();
    this->recordCommandBuffers();

}

void Viewport::recordCommandBuffers() {


    for (int i = 0; i < commandBuffers.size(); ++i) {

        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        beginInfo.pInheritanceInfo = nullptr;

        if (vkBeginCommandBuffer(commandBuffers[i], &beginInfo) != VK_SUCCESS)
            throw std::runtime_error("Unable to start recording to command buffer");

        VkRenderPassBeginInfo renderPassInfo = {};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = renderPass;
        renderPassInfo.framebuffer = swapChain.framebuffers[i];
        renderPassInfo.renderArea.offset = {0,0};
        renderPassInfo.renderArea.extent = swapChain.extent;

        std::array<VkClearValue, 5> clearValues;
        clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
        clearValues[1].depthStencil = {1.0f, 0};
        clearValues[2].color = {0.0f, 0.0f, 0.0f};
        clearValues[3].color = {0.0f, 0.0f, 0.0f};
        clearValues[4].color = {0.0f, 0.0f, 0.0f};
        renderPassInfo.clearValueCount = clearValues.size();
        renderPassInfo.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        for (unsigned int j = 0; j < renderElements.size(); ++j) {

            renderElements[j]->render(commandBuffers[i], i);

        }

        vkCmdNextSubpass(commandBuffers[i], VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, ppPipeline);

        ppBufferModel->bindForRender(commandBuffers[i]);

        vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, ppPipelineLayout, 0, 1, &ppDescSets[i], 0, nullptr);

        vkCmdDrawIndexed(commandBuffers[i], ppBufferModel->getIndexCount(), 1, 0, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS)
            throw std::runtime_error("Unable to record command buffer");

    }

}

void Viewport::createSyncObjects() {

    imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo = {};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {

        if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS || vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS) {
            throw std::runtime_error("Unable to create semaphores");
        }

        if (vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS)
            throw std::runtime_error("Unable to create fence");

    }

}

RenderElement::Instance Viewport::addRenderInstance(RenderElement * elem, RenderElement::Transform & trans) {

    //destroySwapChain();

    RenderElement::Instance tmp = elem->addInstance(trans);

    recreateSwapChain();

}

void Viewport::prepareRenderElements() {

    bool needsUpdate = false;

    for (unsigned int i = 0; i < renderElements.size(); ++i) {
        needsUpdate |= renderElements[i]->needsDrawCmdUpdate();
        if (needsUpdate) break;
    }

    if (needsUpdate) {


        vkFreeCommandBuffers(device, commandPool, commandBuffers.size(), commandBuffers.data());
        setupCommandBuffers();
        recordCommandBuffers();


    }

}

void Viewport::createTransferCommandBuffer() {

    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = 1;

    if (vkAllocateCommandBuffers(device, &allocInfo, &transferCmdBuffer) != VK_SUCCESS)
        throw std::runtime_error("Unable to allocate command buffer");

    VkFenceCreateInfo fenceInfo = {};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    vkCreateFence(device, &fenceInfo, nullptr, &transferFence);


}

void Viewport::drawFrame() {

    static auto startRenderTime = std::chrono::high_resolution_clock::now();

    prepareRenderElements();

    if (this->hasPendingTransfer()) {

        vkWaitForFences(device, 1, &transferFence, VK_TRUE, std::numeric_limits<uint64_t>::max());
        vkResetFences(device, 1, &transferFence);

        this->recordTranfer(transferCmdBuffer);

        VkSubmitInfo transferSubmit = {};
        transferSubmit.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        transferSubmit.commandBufferCount = 1;
        transferSubmit.pCommandBuffers = &transferCmdBuffer;
        transferSubmit.signalSemaphoreCount = 0;
        transferSubmit.waitSemaphoreCount = 0;

        vkQueueSubmit(graphicsQueue, 1, &transferSubmit, transferFence);

    }

    vkWaitForFences(device, 1, &inFlightFences[frameIndex], VK_TRUE, std::numeric_limits<uint64_t>::max());

    uint32_t imageIndex = 0;
    VkResult result = vkAcquireNextImageKHR(device, swapChain.chain, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphores[frameIndex], VK_NULL_HANDLE, &imageIndex);

    switch(result) {

    case VK_SUBOPTIMAL_KHR:
    case VK_ERROR_OUT_OF_DATE_KHR:
        frameBufferResized = false;
        recreateSwapChain();
        return;

    case VK_SUCCESS:
        break;

    default:
        throw std::runtime_error("Unable to fetch image from swap chain");

    }

    updateUniformBuffer(imageIndex);

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    VkSemaphore waitSemaphores[] = {imageAvailableSemaphores[frameIndex]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

    VkSemaphore signalSemaphores[] = {renderFinishedSemaphores[frameIndex]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    vkResetFences(device, 1, &inFlightFences[frameIndex]);

    if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, inFlightFences[frameIndex]) != VK_SUCCESS)
        throw std::runtime_error("Unable to submit command buffer");

    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    VkSwapchainKHR swapChains[] = {VulkanHelper::getSwapChain()};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr;

    vkQueuePresentKHR(presentQueue, &presentInfo);

    frameIndex = (frameIndex + 1) % MAX_FRAMES_IN_FLIGHT;

    if (!frameIndex) {
        double duration = std::chrono::duration<double, std::chrono::milliseconds::period>(std::chrono::high_resolution_clock::now() - startRenderTime).count();
        std::cout << "Frame time: " << duration << "ms => fps: " << (1000.0 / duration) << std::endl;
    }

    startRenderTime = std::chrono::high_resolution_clock::now();

}

void Viewport::updateUniformBuffer(uint32_t imageIndex) {

    static auto startTime = std::chrono::high_resolution_clock::now();
    auto currentTime = std::chrono::high_resolution_clock::now();

    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    UniformBufferObject ubo;

    float x = 50.0 * sin(time);

    ubo.view = this->camera->getView();//glm::lookAt(glm::vec3(0.0, -4.0f, 2.0f), glm::vec3(0,0,0), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.proj = this->camera->getProjection();//glm::perspective(glm::radians(45.0f), swapChain.extent.width / (float) swapChain.extent.height, 0.1f, 1000.0f);

    for (unsigned int i = 0; i < renderElements.size(); ++i) {

        renderElements[i]->updateUniformBuffer(ubo, device, imageIndex);

    }

    void * data;
    vmaMapMemory(vmaAllocator, ppLightBuffersMemory[imageIndex], &data);
    memcpy(data, &lights, sizeof(LightData));
    vmaUnmapMemory(vmaAllocator, ppLightBuffersMemory[imageIndex]);

    vmaMapMemory(vmaAllocator, ppCameraBuffersMemory[imageIndex], &data);
    CameraData * camData = (CameraData *) data;
    camData->position = glm::vec3(0,-4, 2);
    vmaUnmapMemory(vmaAllocator, ppCameraBuffersMemory[imageIndex]);

}

VkRenderPass & Viewport::getRenderPass() {
    return renderPass;
}

VkDevice & Viewport::getDevice() {
    return device;
}

void Viewport::setupCommandBuffers() {

    commandBuffers.resize(swapChain.framebuffers.size());

    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = swapChain.framebuffers.size();

    if (vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data()) != VK_SUCCESS)
        throw std::runtime_error("Unable to allocate command buffer");

}

int Viewport::getSwapChainSize() {
    return swapChain.imageViews.size();
}

void Viewport::addRenderElement(std::shared_ptr<RenderElement> elem) {
    this->renderElements.push_back(elem);
}

void Viewport::setupPostProcessingPipeline() {

    /** shaders **/

    std::cout << "Creating ppPipeline" << std::endl;

    std::vector<VkPipelineShaderStageCreateInfo> shaderStages(2);

    shaderStages[0].module = VulkanHelper::createShaderModule(readFile("shaders/id.vert.spirv"));
    shaderStages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    shaderStages[0].pName = "main";


    shaderStages[1].module = VulkanHelper::createShaderModule(readFile("shaders/pp.frag.spirv"));
    shaderStages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    shaderStages[1].pName = "main";

    /** fixed function **/

    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

    std::vector<VkVertexInputBindingDescription> inputDesc = Model::Vertex::getBindingDescription();
    std::vector<VkVertexInputAttributeDescription> attrDesc = Model::Vertex::getAttributeDescriptions();

    vertexInputInfo.vertexBindingDescriptionCount = inputDesc.size();
    vertexInputInfo.pVertexBindingDescriptions = inputDesc.data();
    vertexInputInfo.vertexAttributeDescriptionCount = attrDesc.size();
    vertexInputInfo.pVertexAttributeDescriptions = attrDesc.data();

    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    /* Viewport */

    VkViewport viewport = {};
    viewport.x = 0;
    viewport.y = 0;
    viewport.width = swapChain.extent.width;
    viewport.height = swapChain.extent.height;
    viewport.minDepth = 0.0;
    viewport.maxDepth = 1.0;

    VkRect2D scissor = {};
    scissor.offset = {0, 0};
    scissor.extent = swapChain.extent;

    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    /* Rasterizer */

    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0;
    rasterizer.cullMode = VK_CULL_MODE_NONE;
    //rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0;
    rasterizer.depthBiasClamp = 0.0;
    rasterizer.depthBiasSlopeFactor = 0.0;

    /* Multisampling */

    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f;
    multisampling.pSampleMask = nullptr;
    multisampling.alphaToCoverageEnable = VK_FALSE;
    multisampling.alphaToOneEnable = VK_FALSE;

    /* color blending */

    // For one framebuffer
    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

    std::array<VkPipelineColorBlendAttachmentState, 4> cbAttachments = {colorBlendAttachment, colorBlendAttachment, colorBlendAttachment, colorBlendAttachment};

    // global configuration
    VkPipelineColorBlendStateCreateInfo colorBlending = {};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY;
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f;
    colorBlending.blendConstants[1] = 0.0f;
    colorBlending.blendConstants[2] = 0.0f;
    colorBlending.blendConstants[3] = 0.0f;

    VkPipelineDepthStencilStateCreateInfo depthStencil = {};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.minDepthBounds = 0.0f;
    depthStencil.maxDepthBounds = 1.0f;
    depthStencil.stencilTestEnable = VK_FALSE;
    depthStencil.front = {};
    depthStencil.back = {};

    /* Uniform layout */

    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &ppDescLayout; ///Get from Shader object
    pipelineLayoutInfo.pushConstantRangeCount = 0;
    pipelineLayoutInfo.pPushConstantRanges = nullptr;

    if (vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &ppPipelineLayout) != VK_SUCCESS)
        throw std::runtime_error("Unable to create pipeline layout");


    /** Creating the pipeline **/

    VkGraphicsPipelineCreateInfo pipelineInfo = {};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = shaderStages.size();
    pipelineInfo.pStages = shaderStages.data();
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = &depthStencil;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = nullptr; // change for dynamic state
    pipelineInfo.layout = ppPipelineLayout;
    pipelineInfo.renderPass = renderPass;
    pipelineInfo.subpass = 1;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
    pipelineInfo.basePipelineIndex = -1;

    if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &ppPipeline) != VK_SUCCESS)
        throw std::runtime_error("Unable to create pipeline");

}

void Viewport::createPpDescriptorSetLayout() {

    std::array<VkDescriptorSetLayoutBinding, 5> bindings;
    bindings[0].binding = 0;
    bindings[0].descriptorCount = 1;
    bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
    bindings[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    bindings[1].binding = 1;
    bindings[1].descriptorCount = 1;
    bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
    bindings[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    bindings[2].binding = 2;
    bindings[2].descriptorCount = 1;
    bindings[2].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
    bindings[2].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    bindings[3].binding = 3;
    bindings[3].descriptorCount = 1;
    bindings[3].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    bindings[3].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    bindings[4].binding = 4;
    bindings[4].descriptorCount = 1;
    bindings[4].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    bindings[4].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.pBindings = bindings.data();
    layoutInfo.bindingCount = bindings.size();

    if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &ppDescLayout) != VK_SUCCESS)
        throw std::runtime_error("could not create descriptor set layout.");

}

void Viewport::createPpDescriptorPool() {

    VkDescriptorPoolSize samplerSize = {};
    samplerSize.type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
    samplerSize.descriptorCount = swapChain.images.size();

    VkDescriptorPoolSize lightSize = {};
    lightSize.descriptorCount = swapChain.images.size();
    lightSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

    VkDescriptorPoolSize cameraSize = {};
    cameraSize.descriptorCount = swapChain.images.size();
    cameraSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

    VkDescriptorPoolSize sizes[] = {samplerSize, samplerSize, samplerSize, lightSize, cameraSize};

    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = 5;
    poolInfo.pPoolSizes = sizes;
    poolInfo.maxSets = swapChain.images.size();

    if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &ppDescPool) != VK_SUCCESS)
        throw std::runtime_error("Unable to create descriptor pool");

}

void Viewport::createPPDescriptorSets() {

    std::vector<VkDescriptorSetLayout> layouts(swapChain.images.size(), ppDescLayout);
    this->ppDescSets.resize(swapChain.images.size());

    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = ppDescPool;
    allocInfo.descriptorSetCount = swapChain.images.size();
    allocInfo.pSetLayouts = layouts.data();

    if (vkAllocateDescriptorSets(device, &allocInfo, ppDescSets.data()) != VK_SUCCESS)
        throw std::runtime_error("Unable to allocate descriptor sets");

    for (int i = 0; i < swapChain.images.size(); ++i) {

        std::array<VkWriteDescriptorSet, 5> descriptorWrites = {};

        VkDescriptorImageInfo gInfo;
        gInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        gInfo.imageView = gBufferImageView;
        gInfo.sampler = VK_NULL_HANDLE;

        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = ppDescSets[i];
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].dstArrayElement = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo = nullptr;
        descriptorWrites[0].pImageInfo = &gInfo;
        descriptorWrites[0].pTexelBufferView = nullptr;

        VkDescriptorImageInfo nInfo;
        nInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        nInfo.imageView = nBufferImageView;
        nInfo.sampler = VK_NULL_HANDLE;

        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = ppDescSets[i];
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].dstArrayElement = 0;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pBufferInfo = nullptr;
        descriptorWrites[1].pImageInfo = &nInfo;
        descriptorWrites[1].pTexelBufferView = nullptr;

        VkDescriptorImageInfo aInfo;
        aInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        aInfo.imageView = aBufferImageView;
        aInfo.sampler = VK_NULL_HANDLE;

        descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[2].dstSet = ppDescSets[i];
        descriptorWrites[2].dstBinding = 2;
        descriptorWrites[2].dstArrayElement = 0;
        descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        descriptorWrites[2].descriptorCount = 1;
        descriptorWrites[2].pBufferInfo = nullptr;
        descriptorWrites[2].pImageInfo = &aInfo;
        descriptorWrites[2].pTexelBufferView = nullptr;

        VkDescriptorBufferInfo lightInfo = {};
        lightInfo.buffer = this->ppLightBuffers[i];
        lightInfo.offset = 0;
        lightInfo.range = sizeof(LightData);

        descriptorWrites[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[3].dstSet = ppDescSets[i];
        descriptorWrites[3].dstBinding = 3;
        descriptorWrites[3].dstArrayElement = 0;
        descriptorWrites[3].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[3].descriptorCount = 1;
        descriptorWrites[3].pBufferInfo = &lightInfo;
        descriptorWrites[3].pImageInfo = nullptr;
        descriptorWrites[3].pTexelBufferView = nullptr;

        VkDescriptorBufferInfo cameraInfo = {};
        cameraInfo.buffer = this->ppCameraBuffers[i];
        cameraInfo.offset = 0;
        cameraInfo.range = sizeof(CameraData);

        descriptorWrites[4].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[4].dstSet = ppDescSets[i];
        descriptorWrites[4].dstBinding = 4;
        descriptorWrites[4].dstArrayElement = 0;
        descriptorWrites[4].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[4].descriptorCount = 1;
        descriptorWrites[4].pBufferInfo = &cameraInfo;
        descriptorWrites[4].pImageInfo = nullptr;
        descriptorWrites[4].pTexelBufferView = nullptr;

        vkUpdateDescriptorSets(device, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);

    }

}

void Viewport::addLight(glm::vec4 pos, glm::vec4 color) {

    if (lightIndex > 31) throw std::runtime_error("To many lights in viewport");

    this->lights.position[lightIndex] = pos;
    this->lights.color[lightIndex] = color;

    this->lightIndex++;

}


void Viewport::setFramebufferResize() {
    this->frameBufferResized = true;
}

