#ifndef MODEL_H
#define MODEL_H

#include <glm/glm.hpp>

#include "indexbuffer.h"
#include "vertexbuffer.h"
#include <string>

class Model {

    public:

        struct Vertex {

            glm::vec3 pos;
            glm::vec3 normal;
            glm::vec3 tangent;
            glm::vec2 uv;

            static std::vector<VkVertexInputBindingDescription> getBindingDescription();

            static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();

        };

        Model(VmaAllocator & allocator, std::vector<Vertex> & verts, std::vector<uint16_t> & indices);
        virtual ~Model();

        void bindForRender(VkCommandBuffer & cmdBuffer);
        int getIndexCount();

        static Model * loadFromFile(VmaAllocator & allocator, std::string fname);

    protected:

    private:

        VertexBuffer<Vertex> * vBuffer;
        IndexBuffer<uint16_t> * iBuffer;

        int vCount;
        int iCount;

};

#endif // MODEL_H
