#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <vector>
#include <functional>
#include <memory>

#include <GLFW/glfw3.h>
#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "render/renderelement.h"
#include "render/camera.h"
#include "render/memorytransferhandler.h"

class Model;

class Viewport : public MemoryTransferHandler {

    public:

        struct SwapChainData {

            VkSwapchainKHR chain;
            VkFormat format;
            VkExtent2D extent;
            std::vector<VkImage> images;
            std::vector<VkImageView> imageViews;
            std::vector<VkFramebuffer> framebuffers;

        };

        struct LightData {

            alignas(16) glm::vec4 position[32];
            alignas(16) glm::vec4 color[32];

        };

        struct CameraData;

        Viewport(GLFWwindow * window, std::shared_ptr<Camera> cam);
        virtual ~Viewport();

        virtual void setupRenderPass();
        virtual void setupFramebuffers();
        virtual void setupCommandBuffers();
        virtual void setupPostProcessingPipeline();
        virtual void createPpDescriptorSetLayout();
        virtual void createPpDescriptorPool();
        virtual void createPPDescriptorSets();
        virtual void recordCommandBuffers();

        void destroySwapChain();
        void recreateSwapChain();

        void addRenderElement(std::shared_ptr<RenderElement> elem);
        RenderElement::Instance addRenderInstance(RenderElement * element, RenderElement::Transform & trans);

        int getSwapChainSize();

        void drawFrame();
        void updateUniformBuffer(uint32_t frameIndex);

        void setFramebufferResize();

        VkDevice & getDevice();
        VkRenderPass & getRenderPass();
        VmaAllocator & getAllocator();

        void addLight(glm::vec4 position, glm::vec4 color);

    protected:

        virtual void createSyncObjects();
        virtual void createPPObjects();
        virtual void destroyPPObjects();

        void prepareRenderElements();
        void createTransferCommandBuffer();

        std::vector<std::shared_ptr<RenderElement>> renderElements;

        std::shared_ptr<Camera> camera;

    private:

        VkInstance instance;
        VkSurfaceKHR surface;
        VkDevice device;
        VkPhysicalDevice physicalDevice;

        VmaAllocator vmaAllocator;

        VkQueue graphicsQueue;
        VkQueue presentQueue;
        GLFWwindow * window;

        VkCommandPool commandPool;
        std::vector<VkCommandBuffer> commandBuffers;
        VkImage depthImage;
        VmaAllocation depthImageMemory;
        VkImageView depthImageView;
        VkRenderPass renderPass;

        SwapChainData swapChain;

        std::vector<VkSemaphore> imageAvailableSemaphores;
        std::vector<VkSemaphore> renderFinishedSemaphores;
        unsigned int frameIndex;
        std::vector<VkFence> inFlightFences;

        bool frameBufferResized;


        VkImageView gBufferImageView;
        VmaAllocation gBufferImageMemory;
        VkImage gBufferImage;

        VkImageView nBufferImageView;
        VmaAllocation nBufferImageMemory;
        VkImage nBufferImage;

        VkImageView aBufferImageView;
        VmaAllocation aBufferImageMemory;
        VkImage aBufferImage;

        VkPipeline ppPipeline;
        VkPipelineLayout ppPipelineLayout;
        VkDescriptorPool ppDescPool;
        VkDescriptorSetLayout ppDescLayout;

        std::shared_ptr<Model> ppBufferModel;

        std::vector<VkDescriptorSet> ppDescSets;

        std::vector<VkBuffer> ppLightBuffers;
        std::vector<VmaAllocation> ppLightBuffersMemory;
        std::vector<VkBuffer> ppCameraBuffers;
        std::vector<VmaAllocation> ppCameraBuffersMemory;

        LightData lights;
        unsigned int lightIndex;

        VkCommandBuffer transferCmdBuffer;
        VkFence transferFence;


};

#endif // VIEWPORT_H
