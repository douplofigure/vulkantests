#ifndef STORAGEBUFFER_H
#define STORAGEBUFFER_H

#include <vk_mem_alloc.h>
#include <vulkan/vulkan.h>

class StorageBuffer
{
    public:
        StorageBuffer(VmaAllocator & allocator, size_t size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties);
        virtual ~StorageBuffer();

        static void createBuffer(VmaAllocator & allocator,VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer & buffer, VmaAllocation & memory);
        static uint32_t findMemoryType(uint32_t filter, VkMemoryPropertyFlags properties);

        VkBuffer & getBuffer();
        VmaAllocation & getMemory();

    protected:

        VkDevice device;

        VkBuffer buffer;
        VmaAllocation memory;

        VmaAllocator & allocator;

    private:

};

#endif // STORAGEBUFFER_H
