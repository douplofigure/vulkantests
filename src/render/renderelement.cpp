#include "renderelement.h"

#include "storagebuffer.h"

#define GLM_FORCE_RADIANS
#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <string.h>

RenderElement::RenderElement(VmaAllocator & allocator, VkDevice & device, std::shared_ptr<Model> model, std::shared_ptr<Shader> shader, std::vector<std::shared_ptr<Texture>> texture, VkSampler sampler, int scSize, Transform & initTransform) : allocator(allocator) {

    this->model = model;
    this->shader = shader;
    this->texture = texture;

    instanceTransforms = std::vector<glm::mat4>(1);
    instances = std::unordered_map<uint32_t, InstanceInfo>(1);
    transforms = std::vector<Transform>(1);

    std::array<float, 3> rAxis = {0.0, 0.0, 1.0};

    transforms[0] = initTransform;

    instanceTransforms[0] = getTransformationMatrix(transforms[0]);

    instanceCount = 1;

    this->instanceBuffer = new DynamicBuffer<glm::mat4>(allocator, instanceTransforms, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);

    this->createUniformBuffers(scSize);

    this->descPool = this->shader->setupDescriptorPool(view->getDevice(), scSize, texture.size());
    this->descriptorSets = shader->createDescriptorSets(view->getDevice(), descPool, uniformBuffers, sizeof(UniformBufferObject), texture, scSize);

}

RenderElement::~RenderElement() {

    destroyUniformBuffers();
    vkDestroyDescriptorPool(VulkanHelper::getDevice(), descPool, nullptr);

    delete this->instanceBuffer;

}

glm::mat4 RenderElement::getTransformationMatrix(Transform & i) {

    glm::mat4 smat = glm::scale(glm::vec3(i.scale, i.scale, i.scale));

    glm::mat4 trmat = i.qRot.toModelMatrix(i.position).toGlmMatrix();

    return trmat * smat;

}

void RenderElement::markBufferDirty() {
    this->instanceBufferDirty = true;
    this->handler.signalTransfer(this);
}

RenderElement::Instance RenderElement::addInstance(Transform & i) {

    uint32_t index = this->transforms.size();

    transforms.push_back(i);
    instanceTransforms.push_back(getTransformationMatrix(i));
    //instances.push_back((InstanceInfo){index, index});
    instances[index] = (InstanceInfo){index, index};
    this->markBufferDirty();

    this->instanceBuffer->recreate(instanceTransforms);

    instanceCount++;
    this->instanceCountUpdated = true;

    return (Instance) {index};

}

void RenderElement::updateInstance(Instance & instance, Transform & trans) {

    //Check for existance of instance.
    if (this->instances.find(instance.id) == instances.end())
        return;

    InstanceInfo info = this->instances[instance.id];

    this->transforms[info.pos] = trans;
    this->instanceTransforms[info.pos] = getTransformationMatrix(transforms[info.pos]);

    this->markBufferDirty();

}

void RenderElement::deleteInstance(Instance & instance) {

    ///No instances left
    if (!transforms.size()) {
        return;
    }

    //Info of instance to remove
    InstanceInfo info = this->instances[instance.id];
    uint32_t lastPos = instanceCount - 1;

    //remove instance from map
    this->instances.erase(this->instances.find(instance.id));

    //getting info for last element
    InstanceInfo * lastInfo;
    for (auto it : instances) {

        if (it.second.pos == lastPos) {
            lastInfo = &it.second;
            break;
        }

    }

    //Moving last element to override deleted one.
    transforms[info.pos] = transforms[lastPos];
    instanceTransforms[info.pos] = instanceTransforms[lastPos];
    lastInfo->pos = info.pos;

    this->instanceBuffer->recreate(this->instanceTransforms);
    this->markBufferDirty();

    instanceCount--;
    //this->instanceCountUpdated = true;

}

void RenderElement::createUniformBuffers(int swapChainSize) {

    VkDeviceSize bufferSize = sizeof(UniformBufferObject);

    uniformBuffers.resize(swapChainSize);
    uniformBuffersMemory.resize(swapChainSize);

    for (int i = 0; i < swapChainSize; ++i) {

        VkBufferCreateInfo stBufferCreateInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
        stBufferCreateInfo.size = bufferSize;
        stBufferCreateInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        stBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        VmaAllocationCreateInfo stAllocCreateInfo = {};
        stAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
        stAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;

        VmaAllocationInfo stagingBufferAllocInfo = {};

        vmaCreateBuffer(allocator, &stBufferCreateInfo, &stAllocCreateInfo, &uniformBuffers[i], &uniformBuffersMemory[i], &stagingBufferAllocInfo);

    }

}

void RenderElement::recreateResources(VkDevice & device, VkRenderPass & renderPass, int scSize) {

    VulkanHelper::VertexInputDescriptions descs;
    descs.attributes = Model::Vertex::getAttributeDescriptions();
    descs.binding = Model::Vertex::getBindingDescription();

    shader->setupGraphicsPipeline(descs, renderPass);

    descPool = shader->setupDescriptorPool(device, scSize, texture.size());
    createUniformBuffers(scSize);
    this->descriptorSets = shader->createDescriptorSets(device, descPool, uniformBuffers, sizeof(UniformBufferObject), texture, scSize);
}

void RenderElement::destroyUniformBuffers() {

    for (int i = 0; i < VulkanHelper::getSwapChainSize(); ++i) {

        vmaDestroyBuffer(allocator, uniformBuffers[i], uniformBuffersMemory[i]);

    }

}

void RenderElement::recordTransfer(VkCommandBuffer & cmdBuffer) {

    this->instanceBuffer->fill(instanceTransforms, cmdBuffer);

}

bool RenderElement::reusable() {
    return true;
}

void RenderElement::updateUniformBuffer(UniformBufferObject & obj, VkDevice & device, uint32_t imageIndex) {

    void * data;
    vmaMapMemory(allocator, uniformBuffersMemory[imageIndex], &data);
    memcpy(data, &obj, sizeof(UniformBufferObject));
    vmaUnmapMemory(allocator, uniformBuffersMemory[imageIndex]);

    if (this->instanceCountUpdated) {

        //this->instanceBuffer->recreate(this->instanceTransforms);
        this->instanceCountUpdated = false;
        //this->instanceBufferDirty = false;

    }
    if (this->instanceBufferDirty) {

        //this->instanceBuffer->fill(instanceTransforms);
        this->instanceBufferDirty = false;

    }

}

bool RenderElement::needsDrawCmdUpdate() {
    return instanceCountUpdated;// || instanceBufferDirty;
}

void RenderElement::render(VkCommandBuffer & cmdBuffer, uint32_t frameIndex) {

    vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->getPipeline());

    model->bindForRender(cmdBuffer);
    VkDeviceSize offsets[] = {0};
    vkCmdBindVertexBuffers(cmdBuffer, 1, 1, &instanceBuffer->getBuffer(), offsets);
    shader->bindForRender(cmdBuffer, descriptorSets[frameIndex]);

    vkCmdDrawIndexed(cmdBuffer, model->getIndexCount(), instanceCount, 0, 0, 0);

}

std::vector<VkDescriptorSet> & RenderElement::getDescriptorSets() {
    return this->descriptorSets;
}

std::vector<VmaAllocation> & RenderElement::getMemories() {
    return this->uniformBuffersMemory;
}
