<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="icon" href="images/vulkan_lunarg_icon.png">
<link rel="stylesheet" href="github-markdown.css">
<style>
    body {
        box-sizing: border-box;
        min-width: 200px;
        max-width: 980px;
        margin: 0 auto;
        padding: 45px;
    }
</style>
</head>
<body>
<article class="markdown-body">

<p><a href="https://www.LunarG.com/" TARGET="_blank" rel="nofollow"><img src="https://camo.githubusercontent.com/8284916f56546b1d1a9e4af0c8e308ef3b233e6d/68747470733a2f2f76756c6b616e2e6c756e6172672e636f6d2f696d672f4c756e6172474c6f676f2e706e67" alt="LunarG" title="www.LunarG.com" data-canonical-src="https://vulkan.lunarg.com/img/LunarGLogo.png" style="max-width:100%;"></a></p>
<p><a href="https://creativecommons.org/licenses/by-nd/4.0/" TARGET="_blank" rel="nofollow"><img src="https://camo.githubusercontent.com/769ad34d33c3c36b21baa4c4838b311433fcdcd3/68747470733a2f2f692e6372656174697665636f6d6d6f6e732e6f72672f6c2f62792d6e642f342e302f38387833312e706e67" alt="Creative Commons" title="Creative Commons License" data-canonical-src="https://i.creativecommons.org/l/by-nd/4.0/88x31.png" style="max-width:100%;"></a></p>
<p>Copyright © 2015-2019 LunarG, Inc.</p>
<h1>
<a id="user-content-vulkan-layer-factory" class="anchor" href="#user-content-vulkan-layer-factory" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Vulkan Layer Factory</h1>
<h2>
<a id="user-content-overview" class="anchor" href="#user-content-overview" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Overview</h2>
<p>The Vulkan Layer Factory is a framework based on the canonical Vulkan layer model that
facilitates the creation of Vulkan Layers. The layer factory hides the majority of the
loader-layer interface, layer boilerplate, setup and initialization, and complexities
of layer development.</p>
<p>A complete layer with the attendant support files can be produced by simply creating a
subdirectory in the <code>sources/layer_factory</code> directory and adding in a simple header file
and then running cmake. This layer can be used just as any other Vulkan layer.</p>
<p>The Vulkan Layer Factory framework produces 'Factory Layers' comprising one or more
'interceptor' objects. Interceptor objects override functions to be called before (<code>PreCallApiName</code>)
or after (<code>PostCallApiName</code>) each Vulkan entrypoint of interest. Each interceptor is independent
of all others within a single Factory layer.  If multiple interceptors attach to the same API call the calling
order is indeterminate.</p>
<h3>
<a id="user-content-layer-factory-sample-code" class="anchor" href="#user-content-layer-factory-sample-code" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Layer Factory sample code</h3>
<p>The base installation of the layer factory contains some sample layers, including the Vulkan
Assistant Layer, demo_layer, and the starter_layer. The Starter Layer in particular is meant to serve as
an example of a very simple layer implementation. The Demo Layer exhibits some new VLF ease-of-use features,
including global intercepts and various output options, while the Assistant Layer is a production layer
which functions to highlight performance issues and to enforce best-practices for applications.</p>
<h3>
<a id="user-content-create-a-factory-layer" class="anchor" href="#user-content-create-a-factory-layer" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Create a Factory Layer</h3>
<p><strong>Step 1:</strong> Create a subdirectory in the <code>source/layer_factory</code> directory using a name that will serve as the base
for the final layer</p>
<p>For example, a subdirectory named 'starter_layer' will produce <code>libVkLayer_starter_layer.so</code> and be loaded as <code>VK_LAYER_LUNARG_starter_layer</code></p>
<p><strong>Step 2:</strong> Add your interceptor file(s) to the subdirectory</p>
<p>This can be a single header file, or multiple header and source files. See the other VLF layers for examples.</p>
<p><strong>Step 3:</strong> Create or copy an <code>interceptor_objects.h</code> file into your new directory</p>
<p>This should include the header file for each of the included interceptors:</p>
<p><code>#include "whatever_you_called_your_layers_header_file.h"</code></p>
<p><strong>Step 4:</strong> Run CMake and build</p>
<div class="highlight highlight-source-shell"><pre>    <span class="pl-c1">cd</span> source/layer_factory
    mkdir build
    <span class="pl-c1">cd</span> build
    cmake -DCMAKE_BUILD_TYPE=Debug ..
    make</pre></div>
<p>CMake will discover all Factory Layer subdirectories in <code>layer_factory</code> each time it is run.
A *.json will also be created with your layer binary.
As part of the build process, these files will be copied to the appropriated SDK layer file locations,
and will be picked up by the usual <code>VK_LAYERS_PATH</code> environment variable set by running <code>setup-env.sh</code>.</p>
<p>Note that adding or removing a layer factory subdirectory requires re-running CMake in order to
properly recognize the additions/deletions.</p>
<h2>
<a id="user-content-using-layers" class="anchor" href="#user-content-using-layers" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Using Layers</h2>
<p><strong>1.</strong> Build VK loader using normal steps (cmake and make).</p>
<p>The library and json files will be automatically copied to the standard SDK locations, made available by running <code>source setup-env.sh</code>.</p>
<p><strong>2.</strong> Specify which layers to activate using environment variables.</p>
<p><code>export VK_INSTANCE_LAYERS=VK_LAYER_LUNARG_standard_validation:VK_LAYER_LUNARG_starter_layer</code></p>
<p><strong>3.</strong> Run your application. The layer path and layer choices will be picked up from your environment.</p>
<p>Alternatively, you can specify these variables on the command line, like so:</p>
<p><code>VK_INSTANCE_LAYERS=VK_LAYER_LUNARG_demo_layer VK_LAYER_PATH=/home/user/VulkanSDK/1.0.68.0/source/layer_factory/build ./vulkaninfo</code></p>
<h3>
<a id="user-content-layer-factory-features" class="anchor" href="#user-content-layer-factory-features" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Layer Factory Features</h3>
<p>The layer factory provides helper functions for layer authors to simplify layer tasks. These include some
simpler output functions, debug facilities, and simple global intercept functions.</p>
<p><em>Output Helpers:</em></p>
<p>Interceptors can use base-class defined output helpers for easy access to Debug Report Extension output.
These include Information(), Warning(), Performance_Warning(), and Error(), corresponding to the
VkDebugReportFlagBitsEXT enumerations. Alternatively, the standard layer-provided log_msg() call can be used
directly, as can printf for standard-out or OutputDebugString for Windows.</p>
<p><em>Debug Helpers:</em></p>
<p>A BreakPoint() helper can be used in an intercepted function which will generate a break in a Windows or Linux
debugger.</p>
<p><em>Global Intercept Helpers:</em></p>
<p>There are two global intercept helpers, PreCallApiFunction() and PostCallApiFunction(). Overriding these virtual
functions in your interceptor will result in them being called for EVERY API call.</p>
<h3>
<a id="user-content-details" class="anchor" href="#user-content-details" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Details</h3>
<p>By creating a child framework object, the factory will generate a full layer and call any overridden functions
in your interceptor.</p>
<p>Here is a simple, and complete, interceptor (the starter_layer). This layer intercepts the memory allocate and free
functions, tracking the number and total size of device memory allocations. The QueuePresent() function is also intercepted, and
results are outputted on every 60th frame.  Note that the function signatures are and must be identical to those in the specification.</p>
<p>In this example, there is a single interceptor in which the child object is named 'MemAllocLevel' and is instantiated as
'memory_allocation_stats'. A layer can contain many interceptors as long as the instantiated object names are unique within that layer.</p>
<div class="highlight highlight-source-c++"><pre>    #<span class="pl-k">pragma</span> once
    #<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&lt;</span>sstream<span class="pl-pds">&gt;</span></span>
    #<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&lt;</span>unordered_map<span class="pl-pds">&gt;</span></span>

    <span class="pl-k">static</span> <span class="pl-c1">uint32_t</span> display_rate = <span class="pl-c1">60</span>;

    <span class="pl-k">class</span> <span class="pl-en">MemAllocLevel</span> : <span class="pl-k">public</span> <span class="pl-en">layer_factory</span> {
        <span class="pl-k">public:</span>
            <span class="pl-c"><span class="pl-c">//</span> Constructor for interceptor</span>
            <span class="pl-en">MemAllocLevel</span>() : layer_factory(<span class="pl-c1">this</span>), number_mem_objects_(<span class="pl-c1">0</span>), total_memory_(<span class="pl-c1">0</span>), present_count_(<span class="pl-c1">0</span>) {};

            <span class="pl-c"><span class="pl-c">//</span> Intercept memory allocation calls and increment counter</span>
            VkResult <span class="pl-en">PostCallAllocateMemory</span>(VkDevice device, <span class="pl-k">const</span> VkMemoryAllocateInfo *pAllocateInfo,
                    <span class="pl-k">const</span> VkAllocationCallbacks *pAllocator, VkDeviceMemory *pMemory) {
                number_mem_objects_++;
                total_memory_ += pAllocateInfo-&gt;<span class="pl-smi">allocationSize</span>;
                mem_size_map_[*pMemory] = pAllocateInfo-&gt;<span class="pl-smi">allocationSize</span>;
                <span class="pl-k">return</span> VK_SUCCESS;
            };

            <span class="pl-c"><span class="pl-c">//</span> Intercept free memory calls and update totals</span>
            <span class="pl-k">void</span> <span class="pl-en">PreCallFreeMemory</span>(VkDevice device, VkDeviceMemory memory, <span class="pl-k">const</span> VkAllocationCallbacks *pAllocator) {
                <span class="pl-k">if</span> (memory != VK_NULL_HANDLE) {
                    number_mem_objects_--;
                    VkDeviceSize this_alloc = mem_size_map_[memory];
                    total_memory_ -= this_alloc;
                }
            }

            VkResult <span class="pl-en">PreCallQueuePresentKHR</span>(VkQueue queue, <span class="pl-k">const</span> VkPresentInfoKHR *pPresentInfo) {
                present_count_++;
                <span class="pl-k">if</span> (present_count_ &gt;= display_rate) {
                    present_count_ = <span class="pl-c1">0</span>;
                    std::stringstream message;
                    message &lt;&lt; <span class="pl-s"><span class="pl-pds">"</span>Memory Allocation Count: <span class="pl-pds">"</span></span> &lt;&lt; number_mem_objects_ &lt;&lt; <span class="pl-s"><span class="pl-pds">"</span><span class="pl-cce">\n</span><span class="pl-pds">"</span></span>;
                    message &lt;&lt; <span class="pl-s"><span class="pl-pds">"</span>Total Memory Allocation Size: <span class="pl-pds">"</span></span> &lt;&lt; total_memory_ &lt;&lt; <span class="pl-s"><span class="pl-pds">"</span><span class="pl-cce">\n\n</span><span class="pl-pds">"</span></span>;
                    <span class="pl-c1">Information</span>(message.<span class="pl-c1">str</span>());
                }
                <span class="pl-k">return</span> VK_SUCCESS;
            }

        <span class="pl-k">private:</span>
            <span class="pl-c"><span class="pl-c">//</span> Counter for the number of currently active memory allocations</span>
            <span class="pl-c1">uint32_t</span> number_mem_objects_;
            VkDeviceSize total_memory_;
            <span class="pl-c1">uint32_t</span> present_count_;
            std::unordered_map&lt;VkDeviceMemory, VkDeviceSize&gt; mem_size_map_;
    };

    MemAllocLevel memory_allocation_stats;</pre></div>
<h3>
<a id="user-content-current-known-issues" class="anchor" href="#user-content-current-known-issues" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Current known issues</h3>
<ul>
<li>CMake MUST be run to pick up and interpret new or deleted factory layers.</li>
</ul>
</article>
</body>
</html>
