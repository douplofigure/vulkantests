<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="icon" href="images/vulkan_lunarg_icon.png">
<link rel="stylesheet" href="github-markdown.css">
<style>
    body {
        box-sizing: border-box;
        min-width: 200px;
        max-width: 980px;
        margin: 0 auto;
        padding: 45px;
    }
</style>
</head>
<body>
<article class="markdown-body">


<p><a href="https://www.khronos.org/vulkan/" TARGET="_blank" rel="nofollow"><img src="https://camo.githubusercontent.com/16b56b2bf6d81f97a94bed0f0a4f89b24a2b8117/68747470733a2f2f76756c6b616e2e6c756e6172672e636f6d2f696d672f56756c6b616e5f31303070785f44656331362e706e67" alt="Khronos Vulkan" title="https://www.khronos.org/vulkan/" data-canonical-src="https://vulkan.lunarg.com/img/Vulkan_100px_Dec16.png" style="max-width:100%;"></a></p>
<h1>
<a id="user-content-layers-overview-and-configuration" class="anchor" href="#user-content-layers-overview-and-configuration" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Layers Overview and Configuration</h1>
<p><a href="https://creativecommons.org/licenses/by-nd/4.0/" TARGET="_blank" rel="nofollow"><img src="https://camo.githubusercontent.com/769ad34d33c3c36b21baa4c4838b311433fcdcd3/68747470733a2f2f692e6372656174697665636f6d6d6f6e732e6f72672f6c2f62792d6e642f342e302f38387833312e706e67" alt="Creative Commons" title="Creative Commons License" data-canonical-src="https://i.creativecommons.org/l/by-nd/4.0/88x31.png" style="max-width:100%;"></a></p>
<p>Vulkan supports intercepting or hooking API entry points via a layer framework.  A layer can intercept all or any subset of Vulkan API entry points.  Multiple layers can be chained together to cascade their functionality in the appearance of a single, larger layer.</p>
<p>Vulkan validation and utility layers give Vulkan application developers the ability to add additional functionality to applications without modifying the application itself, e.g., dumping API entry points or generating screenshots of specified frames.</p>
<h2>
<a id="user-content-layers-included-in-the-sdk" class="anchor" href="#user-content-layers-included-in-the-sdk" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Layers Included in the SDK</h2>
<p>The Vulkan SDK includes the following layers, including a group of deprecated validation layers that will reach end-of-life in the near future:</p>
<table>
<thead>
<tr>
<th>Layer Name</th>
<th>Layer Type</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="./api_dump_layer.html"><code>VK_LAYER_LUNARG_api_dump</code></a></td>
<td>utility</td>
<td>print API calls and their parameters and values</td>
</tr>
<tr>
<td><a href="./assistant_layer.html"><code>VK_LAYER_LUNARG_assistant_layer</code></a></td>
<td>utility</td>
<td>highlight potential application issues that are not specifically prohibited by the Vulkan spec, but which can still create problems</td>
</tr>
<tr>
<td><a href="./khronos_validation_layer.html"><code>VK_LAYER_KHRONOS_validation</code></a></td>
<td>validation</td>
<td>the main, comprehensive Khronos validation layer -- this layer encompasses the entire functionality of the deprecated layers listed below, and supercedes them. As the other layers are deprecated this layer should be used for all validation going forward.</td>
</tr>
<tr>
<td><a href="./device_simulation_layer.html"><code>VK_LAYER_LUNARG_device_simulation</code></a></td>
<td>utility</td>
<td>allows modification of an actual device's reported features, limits, and capabilities</td>
</tr>
<tr>
<td><a href="./monitor_layer.html"><code>VK_LAYER_LUNARG_monitor</code></a></td>
<td>utility</td>
<td>outputs the frames-per-second of the target application in the applications title bar</td>
</tr>
<tr>
<td><a href="./screenshot_layer.html"><code>VK_LAYER_LUNARG_screenshot</code></a></td>
<td>utility</td>
<td>outputs specified frames to an image file as they are presented</td>
</tr>
</tbody>
</table>
<table>
<thead>
<tr>
<th>Deprecated Layer Name</th>
<th>Layer Type</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="./core_validation_layer.html"><code>VK_LAYER_LUNARG_core_validation</code></a></td>
<td>validation</td>
<td>print and validate the descriptor set, pipeline state, and dynamic state; validate the interfaces between SPIR-V modules and the graphics pipeline; track and validate GPU memory and its binding to objects and command buffers; validate texture formats and render target formats. <em>This layer has been replaced by VK_LAYER_KHRONOS_validation.</em>
</td>
</tr>
<tr>
<td><a href="./object_tracker_layer.html"><code>VK_LAYER_LUNARG_object_tracker</code></a></td>
<td>validation</td>
<td>track all Vulkan objects and flag invalid objects and object memory leaks. <em>This layer has been replaced by VK_LAYER_KHRONOS_validation.</em>
</td>
</tr>
<tr>
<td><a href="./parameter_validation_layer.html"><code>VK_LAYER_LUNARG_parameter_validation</code></a></td>
<td>validation</td>
<td>validate API parameter values. <em>This layer has been replaced by VK_LAYER_KHRONOS_validation.</em>
</td>
</tr>
<tr>
<td><a href="./threading_layer.html"><code>VK_LAYER_GOOGLE_threading</code></a></td>
<td>validation</td>
<td>check validity of multi-threaded API usage. <em>This layer has been replaced by VK_LAYER_KHRONOS_validation.</em>
</td>
</tr>
<tr>
<td><a href="./unique_objects_layer.html"><code>VK_LAYER_GOOGLE_unique_objects</code></a></td>
<td>utility</td>
<td>wrap all Vulkan objects in a unique pointer at create time and unwrap them at use time. <em>This layer has been replaced by VK_LAYER_KHRONOS_validation.</em>
</td>
</tr>
</tbody>
</table>
<p>See the <a href="./validation_layers.html">Validation Layers</a> and <a href="./utility_layers.html">Utility Layers</a> sections for more information about validation and utility layers.</p>
<h2>
<a id="user-content-a-note-on-layer-message-types" class="anchor" href="#user-content-a-note-on-layer-message-types" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>A Note on Layer Message Types</h2>
<p>Vulkan layers will output messages of several types. It is important to note the meaning of the ERROR type as compared to the WARN type:</p>
<table>
<thead>
<tr>
<th>Type</th>
<th>Definitions</th>
</tr>
</thead>
<tbody>
<tr>
<td>Error</td>
<td>Errors are output when a layer detects some application behavior has violated the Vulkan Specification.  When an error is encountered it is recommended that the user callback function return 'true' for optimal validation results. Any validation error may result in undefined behavior and errors should be corrected as they are encountered for best results</td>
</tr>
<tr>
<td>Warn</td>
<td>
<strong>Warnings are output in cases where mistakes are commonly made and do NOT necessarily indicate that an app has violated the Vulkan specification.</strong> Warnings basically translate to 'Did you really mean to do this?'</td>
</tr>
<tr>
<td>Perf Warn</td>
<td>Performance Warnings are output in cases where a possible inefficiency has been detected.  These also do NOT imply that the specification was violated</td>
</tr>
<tr>
<td>Info</td>
<td>These log messages are for informational purposes only. For instance, the khronos validation layer can print out lists of memory objects and their bindings which may help with debugging or improving application efficiency</td>
</tr>
</tbody>
</table>
<h2>
<a id="user-content-configuring-layers-on-linux" class="anchor" href="#user-content-configuring-layers-on-linux" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Configuring Layers on Linux</h2>
<p>The Vulkan loader searches the <code>/usr/share/vulkan/implicit_layer.d</code>, <code>/usr/share/vulkan/explicit_layer.d</code>, <code>/etc/vulkan/implicit_layer.d</code>, <code>/etc/vulkan/explicit_layer.d</code> <code>$HOME/.local/share/vulkan/explicit_layer.d</code>, and <code>$HOME/.local/share/vulkan/implicit_layer.d</code>
directories for layer JSON manifest files.</p>
<p>Sample layer manifest file (<code>khronos_validation.json</code>):</p>
<div class="highlight highlight-source-json"><pre>{
    <span class="pl-s"><span class="pl-pds">"</span>file_format_version<span class="pl-pds">"</span></span> : <span class="pl-s"><span class="pl-pds">"</span>1.1.0<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>layer<span class="pl-pds">"</span></span> : {
        <span class="pl-s"><span class="pl-pds">"</span>name<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>VK_LAYER_KHRONOS_validation<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>type<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>GLOBAL<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>library_path<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>./libVkLayer_khronos_validation.so<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>api_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>1.1.102<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>implementation_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>1<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>description<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>LunarG Validation Layer<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>instance_extensions<span class="pl-pds">"</span></span>: [
             {
                 <span class="pl-s"><span class="pl-pds">"</span>name<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>VK_EXT_debug_report<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>spec_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>6<span class="pl-pds">"</span></span>
             }
         ],
        <span class="pl-s"><span class="pl-pds">"</span>device_extensions<span class="pl-pds">"</span></span>: [
             {
                 <span class="pl-s"><span class="pl-pds">"</span>name<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>VK_EXT_debug_marker<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>spec_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>4<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>entrypoints<span class="pl-pds">"</span></span>: [<span class="pl-s"><span class="pl-pds">"</span>vkDebugMarkerSetObjectTagEXT<span class="pl-pds">"</span></span>,
                        <span class="pl-s"><span class="pl-pds">"</span>vkDebugMarkerSetObjectNameEXT<span class="pl-pds">"</span></span>,
                        <span class="pl-s"><span class="pl-pds">"</span>vkCmdDebugMarkerBeginEXT<span class="pl-pds">"</span></span>,
                        <span class="pl-s"><span class="pl-pds">"</span>vkCmdDebugMarkerEndEXT<span class="pl-pds">"</span></span>,
                        <span class="pl-s"><span class="pl-pds">"</span>vkCmdDebugMarkerInsertEXT<span class="pl-pds">"</span></span>
                       ]
             },
             {
                 <span class="pl-s"><span class="pl-pds">"</span>name<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>VK_EXT_validation_cache<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>spec_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>1<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>entrypoints<span class="pl-pds">"</span></span>: [<span class="pl-s"><span class="pl-pds">"</span>vkCreateValidationCacheEXT<span class="pl-pds">"</span></span>,
                         <span class="pl-s"><span class="pl-pds">"</span>vkDestroyValidationCacheEXT<span class="pl-pds">"</span></span>,
                         <span class="pl-s"><span class="pl-pds">"</span>vkGetValidationCacheDataEXT<span class="pl-pds">"</span></span>,
                         <span class="pl-s"><span class="pl-pds">"</span>vkMergeValidationCachesEXT<span class="pl-pds">"</span></span>
                        ]
             }
         ]
    }
}</pre></div>
<p>Full and relative (to JSON manifest file) <code>library_path</code> names are supported, as are unqualified file names.  If just a filename is specified, the loader will search the default library directory (e.g., <code>/usr/lib/x86_64-linux-gnu/</code> on Ubuntu x64) for the layer shared library.</p>
<p>Setting the <code>VK_LAYER_PATH</code> environment variable overrides the default loader layer search mechanism.  When set, the loader will search only the directory(s) identified by <code>$VK_LAYER_PATH</code> for layer manifest files.</p>
<p>Applications can query available layers via the <code>vkEnumerateInstanceLayerProperties()</code> command.</p>
<h2>
<a id="user-content-configuring-layers-on-windows" class="anchor" href="#user-content-configuring-layers-on-windows" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Configuring Layers on Windows</h2>
<p>The Vulkan loader searches the following registry keys to find layers:</p>
<pre><code>HKEY_LOCAL_MACHINE\SOFTWARE\Khronos\Vulkan\ExplicitLayers
HKEY_LOCAL_MACHINE\SOFTWARE\Khronos\Vulkan\ImplicitLayers
</code></pre>
<p>For each value in each of the above two keys for which DWORD data is set to 0, the Vulkan loader opens the JSON text file specified by the value name. For example, if the <code>HKEY_LOCAL_MACHINE\SOFTWARE\Khronos\Vulkan\ExplicitLayers</code> key contains the following value:</p>
<pre><code>Name                                              Type         Data
C:\VulkanSDK\1.1.102\Bin\khronos_validation.json   REG_DWORD    0x00000000
</code></pre>
<p>The loader will open the file <code>C:\VulkanSDK\1.1.102\Bin\khronos_validation.json</code> to find the pathname to the layer library file. The <code>khronos_validation.json</code> file might contain:</p>
<div class="highlight highlight-source-json"><pre>{
    <span class="pl-s"><span class="pl-pds">"</span>file_format_version<span class="pl-pds">"</span></span> : <span class="pl-s"><span class="pl-pds">"</span>1.1.0<span class="pl-pds">"</span></span>,
    <span class="pl-s"><span class="pl-pds">"</span>layer<span class="pl-pds">"</span></span> : {
        <span class="pl-s"><span class="pl-pds">"</span>name<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>VK_LAYER_KHRONOS_validation<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>type<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>GLOBAL<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>library_path<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>.<span class="pl-cce">\\</span>VkLayer_khronos_validation.dll<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>api_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>1.1.102<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>implementation_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>1<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>description<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>LunarG Validation Layer<span class="pl-pds">"</span></span>,
        <span class="pl-s"><span class="pl-pds">"</span>instance_extensions<span class="pl-pds">"</span></span>: [
             {
                 <span class="pl-s"><span class="pl-pds">"</span>name<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>VK_EXT_debug_report<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>spec_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>6<span class="pl-pds">"</span></span>
             }
         ],
        <span class="pl-s"><span class="pl-pds">"</span>device_extensions<span class="pl-pds">"</span></span>: [
             {
                 <span class="pl-s"><span class="pl-pds">"</span>name<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>VK_EXT_debug_marker<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>spec_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>4<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>entrypoints<span class="pl-pds">"</span></span>: [<span class="pl-s"><span class="pl-pds">"</span>vkDebugMarkerSetObjectTagEXT<span class="pl-pds">"</span></span>,
                        <span class="pl-s"><span class="pl-pds">"</span>vkDebugMarkerSetObjectNameEXT<span class="pl-pds">"</span></span>,
                        <span class="pl-s"><span class="pl-pds">"</span>vkCmdDebugMarkerBeginEXT<span class="pl-pds">"</span></span>,
                        <span class="pl-s"><span class="pl-pds">"</span>vkCmdDebugMarkerEndEXT<span class="pl-pds">"</span></span>,
                        <span class="pl-s"><span class="pl-pds">"</span>vkCmdDebugMarkerInsertEXT<span class="pl-pds">"</span></span>
                       ]
             },
             {
                 <span class="pl-s"><span class="pl-pds">"</span>name<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>VK_EXT_validation_cache<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>spec_version<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>1<span class="pl-pds">"</span></span>,
                 <span class="pl-s"><span class="pl-pds">"</span>entrypoints<span class="pl-pds">"</span></span>: [<span class="pl-s"><span class="pl-pds">"</span>vkCreateValidationCacheEXT<span class="pl-pds">"</span></span>,
                         <span class="pl-s"><span class="pl-pds">"</span>vkDestroyValidationCacheEXT<span class="pl-pds">"</span></span>,
                         <span class="pl-s"><span class="pl-pds">"</span>vkGetValidationCacheDataEXT<span class="pl-pds">"</span></span>,
                         <span class="pl-s"><span class="pl-pds">"</span>vkMergeValidationCachesEXT<span class="pl-pds">"</span></span>
                        ]
             }
         ]
    }
}</pre></div>
<p>Full and relative (to JSON manifest file) <code>library_path</code> names are supported, as are unqualified file names.  If just a filename is specified, the loader will search the default library directory (typically <code>C:\Windows\System32</code>) for the layer shared library. For the above example, the layer library file for the <code>VK_LAYER_KHRONOS_validation</code> layer is <code>libVKLayer_khronos_validation.dll</code>, and that file will be loaded from <code>C:\VulkanSDK\1.1.102\Bin</code> and used by the loader if the <code>VK_LAYER_KHRONOS_validation</code> layer is activated.</p>
<h2>
<a id="user-content-activating-layers-on-linux" class="anchor" href="#user-content-activating-layers-on-linux" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Activating Layers on Linux</h2>
<p>Before or during execution of a Vulkan application, the loader must be informed of the layers to activate.  Applications can activate layers at runtime via the <code>vkCreateInstance()</code> entry point.</p>
<p>Layers configured in <code>/usr/share/vulkan/implicit_layer.d</code>, <code>$HOME/.local/share/vulkan/implicit_layer.d</code>,
and <code>/etc/vulkan/implicit_layer.d</code> are activated automatically by the loader.</p>
<p>Layers configured in <code>/usr/share/vulkan/explicit_layer.d</code>,  <code>$HOME/.local/share/vulkan/explicit_layer.d</code>, and <code>/etc/vulkan/explicit_layer.d</code> can be activated by applications at runtime.  These explicit layers can also be activated by the user by setting the <code>VK_INSTANCE_LAYERS</code> environment variable.  Set this variable to identify a colon separated list of layer names to activate.  Order is relevant with the first layer in the list being the topmost layer (closest to the application) and the last layer in the list being the bottom-most layer (closest to the driver).</p>
<p>For example, the list of explicit layers to activate can be specified with:</p>
<pre><code>$ export VK_INSTANCE_LAYERS=VK_LAYER_LUNARG_api_dump:VK_LAYER_KHRONOS_validation
</code></pre>
<p>To activate layers in a local SDK install, identify certain library paths and the layer JSON manifest file directory in addition to the layers to activate.  If the Vulkan SDK was locally installed to <code>/sdks</code>, <code>VULKAN_SDK=/sdks/VulkanSDK/1.1.102/x86_64</code>:</p>
<pre><code>$ export VK_LAYER_PATH=$VULKAN_SDK/lib/vulkan/layers
$ export LD_LIBRARY_PATH=$VULKAN_SDK/lib:$VULKAN_SDK/lib/vulkan/layers
$ export VK_INSTANCE_LAYERS=VK_LAYER_LUNARG_api_dump:VK_LAYER_KHRONOS_validation
$ ./vkcube
</code></pre>
<h2>
<a id="user-content-activating-layers-on-windows" class="anchor" href="#user-content-activating-layers-on-windows" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Activating Layers on Windows</h2>
<p>Before or during execution of a Vulkan application, the loader must be informed of the layers to activate.  Applications can activate layers at runtime via the <code>vkCreateInstance()</code> entry point.</p>
<p>Layers configured in registry key <code>HKEY_LOCAL_MACHINE\SOFTWARE\Khronos\Vulkan\ImplicitLayers</code> are activated automatically by the loader.</p>
<p>Layers configured in registry key <code>HKEY_LOCAL_MACHINE\SOFTWARE\Khronos\Vulkan\ExplicitLayers</code> can be activated by applications at runtime. These explicit layers can also be activated by the user by setting the <code>VK_INSTANCE_LAYERS</code> environment variable.  Set this variable to identify a  semi-colon separated list of layer names to activate.  Order is relevant with the first layer in the list being the topmost layer (closest to the application) and the last layer in the list being the bottom-most layer (closest to the driver).</p>
<p>In a Command Window, the list of explicit layers to activate can be specified with:</p>
<pre><code>C:\&gt; set VK_INSTANCE_LAYERS=VK_LAYER_LUNARG_api_dump;VK_LAYER_KHRONOS_validation
</code></pre>
<p><code>VK_INSTANCE_LAYERS</code> can also be set in the system environment variables.</p>
<p>To activate layers located in a particular SDK install, identify the layer JSON manifest file directory using the <code>VK_LAYER_PATH</code> environment variable.  For example, if a Vulkan SDK is locally installed to <code>C:\VulkanSDK\1.1.102</code>:</p>
<pre><code>C:\&gt; set VK_LAYER_PATH=C:\VulkanSDK\1.1.102\Bin
C:\&gt; set VK_INSTANCE_LAYERS=VK_LAYER_LUNARG_api_dump;VK_LAYER_KHRONOS_validation
C:\&gt; vkcube
</code></pre>
<h2>
<a id="user-content-layer-controls" class="anchor" href="#user-content-layer-controls" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Layer Controls</h2>
<p>Most layers support one or both of the available methods for controlling layer behavior: through a layer settings file or an extension.
The layer settings file allows a user to control various layer features and behaviors by providing easily modifiable settings.
Various Vulkan extensions also provide layer controls:</p>
<table>
<thead>
<tr>
<th>Extension</th>
<th>Description</th>
<th>Status</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="#user-content-debugutils">VK_EXT_debug_utils</a></td>
<td>allows applications control and capture of expanded debug reporting information</td>
<td>Active</td>
</tr>
<tr>
<td><a href="#user-content-validationfeatures">VK_EXT_validation_features</a></td>
<td>allows applications expanded control of various layer features</td>
<td>Active</td>
</tr>
<tr>
<td><a href="#user-content-debugreport">VK_EXT_debug_report</a></td>
<td>allowed applications control and capture of debug reporting information</td>
<td>Deprecated</td>
</tr>
<tr>
<td><a href="#user-content-validationflags">VK_EXT_validation_flags</a></td>
<td>allows applications minimal control of layer features</td>
<td>Deprecated</td>
</tr>
</tbody>
</table>
<h3>
<a id="user-content-layer-settings-file" class="anchor" href="#user-content-layer-settings-file" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Layer Settings File</h3>
<p>In addition to activating the layers, specific reporting levels can be set for each layer programmatically or via the <code>vk_layer_settings.txt</code> settings file.  If no environment variable variable <code>VK_LAYER_SETTINGS_PATH</code> is set, then this must be a file called <code>vk_layer_settings.txt</code> in the working directory of the application. If <code>VK_LAYER_SETTINGS_PATH</code> is set and is a directory, then the settings file must be a file called <code>vk_layer_settings.txt</code> in the directory given by <code>VK_LAYER_SETTINGS_PATH</code>. If <code>VK_LAYER_SETTINGS_PATH</code> is set and is not a directory, then it must point to a file (with any name) which is the layer settings file.</p>
<p>Note:  To control layer reporting output, a layer settings file must be provided that identifies specific reporting levels for the layers enabled via the <code>VK_INSTANCE_LAYER</code> environment variable.</p>
<p>The settings file consists of comment lines and settings lines.  Comment lines begin with the <code>#</code> character.  Settings lines have the following format:</p>
<p><code>&lt;</code><em><code>LayerName</code></em><code>&gt;&lt;</code><em><code>setting_name</code></em><code>&gt; = &lt;</code><em><code>setting_value</code></em><code>&gt;</code></p>
<p>Three settings are common to all layers:</p>
<table>
<thead>
<tr>
<th>Setting</th>
<th>Values</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<em><code>LayerName</code></em><code>.report_flags</code>
</td>
<td><code>info</code></td>
<td>Report information level messages</td>
</tr>
<tr>
<td></td>
<td><code>warn</code></td>
<td>Report warning level messages</td>
</tr>
<tr>
<td></td>
<td><code>perf</code></td>
<td>Report performance level warning messages</td>
</tr>
<tr>
<td></td>
<td><code>error</code></td>
<td>Report error level messages</td>
</tr>
<tr>
<td></td>
<td><code>debug</code></td>
<td>No output messages</td>
</tr>
<tr>
<td>
<em><code>LayerName</code></em><code>.debug_action</code>
</td>
<td><code>VK_DBG_LAYER_ACTION_IGNORE</code></td>
<td>Ignore message reporting</td>
</tr>
<tr>
<td></td>
<td><code>VK_DBG_LAYER_ACTION_LOG_MSG</code></td>
<td>Report messages to log</td>
</tr>
<tr>
<td></td>
<td><code>VK_DBG_LAYER_ACTION_DEBUG_OUTPUT</code></td>
<td>(Windows) Report messages to debug console of Microsoft Visual Studio</td>
</tr>
<tr>
<td></td>
<td><code>VK_DBG_LAYER_ACTION_BREAK</code></td>
<td>Break on messages (not currently used)</td>
</tr>
<tr>
<td>
<em><code>LayerName</code></em><code>.log_filename</code>
</td>
<td>
<em><code>filename</code></em><code>.txt</code>
</td>
<td>Name of file to log <code>report_flags</code> level messages; default is <code>stdout</code>
</td>
</tr>
<tr>
<td></td>
<td><code>debug</code></td>
<td>No output messages</td>
</tr>
<tr>
<td>
<em><code>LayerName</code></em><code>.disables</code>
</td>
<td>comma separated list of <code>VkValidationFeatureDisableEXT</code> enum values as defined in the Vulkan Specification</td>
<td>Disables the specified validation features</td>
</tr>
</tbody>
</table>
<p>Layer-specific settings are also supported in the layer settings file.</p>
<p>Sample layer settings file contents:</p>
<pre><code>khronos_validation.report_flags = info,error
khronos_validation.debug_action = VK_DBG_LAYER_ACTION_LOG_MSG
khronos_validation.disable = VK_VALIDATION_FEATURE_DISABLE_UNIQUE_HANDLES_EXT 
# VK_LAYER_LUNARG_api_dump custom settings
lunarg_api_dump.no_addr = TRUE
lunarg_api_dump.file = FALSE
</code></pre>
<p>The Vulkan SDK includes a sample layer settings file identifying the available and supported settings for each layer.  On Linux,   you can find the sample layer settings file in <code>config/vk_layer_settings.txt</code> of your local Vulkan SDK install. On Windows, you can find the sample layer settings file in <code>Config\vk_layer_settings.txt</code> of your local Vulkan SDK install.</p>
<p>Note: If layers are activated via <code>VK_INSTANCE_LAYER</code> environment variable and if neither an application-defined callback is defined nor a layer settings file is present, the loader/layers will provide default callbacks enabling output of error-level messages to standard out (and via <code>OutputDebugString</code> on Windows).</p>
<h3>
<a id="user-content-vk_ext_debug_utils" class="anchor" href="#user-content-vk_ext_debug_utils" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a><a name="user-content-debugutils"></a>VK_EXT_debug_utils</h3>
<p>The preferred method for an app to control layer logging is via <code>VK_EXT_debug_utils</code> extension.
Using <code>VK_EXT_debug_utils</code> extension allows an application to register multiple messengers with the layers.
Each messenger can trigger a message callback when a log message occurs.
Some messenger callbacks may log the information to a file, others may cause a debug break point or other application defined behavior.
An application can create a messenger even when no layers are enabled, but they will only be called for loader and, if implemented, driver events.
Each message is identified by both a severity level and a message type.
Severity levels indicate the severity of the message that should be logged including: error, warning, etc.
Message types indicate the specific type of message including: validation, performance, etc.
Some layers return a unique message ID string per message as well.
Using the severity, type, and message ID, an application can easily filter the messages received by their messenger callback.</p>
<h4>
<a id="user-content-message-types-as-reported-by-vk_ext_debug_utils-flags" class="anchor" href="#user-content-message-types-as-reported-by-vk_ext_debug_utils-flags" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Message Types As Reported By VK_EXT_debug_utils flags:</h4>
<table>
<thead>
<tr>
<th>Type</th>
<th>Debug Utils Severity</th>
<th>Debug Utils Type</th>
</tr>
</thead>
<tbody>
<tr>
<td>Error</td>
<td><code>VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT</code></td>
<td><code>VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT</code></td>
</tr>
<tr>
<td>Warn</td>
<td><code>VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT</code></td>
<td><code>VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT</code></td>
</tr>
<tr>
<td>Perf Warn</td>
<td><code>VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT</code></td>
<td><code>VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT</code></td>
</tr>
<tr>
<td>Info</td>
<td><code>VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT</code></td>
<td>
<code>VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT</code> or <code>VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT</code>
</td>
</tr>
</tbody>
</table>
<p><strong>Refer to the Validation Features section of the full</strong> <a href="./vkspec.html#debugging-debug-messengers">Vulkan Spec</a> <strong>for details on this extension.</strong></p>
<h3>
<a id="user-content-vk_ext_validation_features" class="anchor" href="#user-content-vk_ext_validation_features" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a><a name="user-content-validationfeatures"></a>VK_EXT_validation_features</h3>
<p>The preferred method for an app to control layer features is through the <code>VK_EXT_validation_features</code> extension.
Using <code>VK_EXT_validation_features</code> extension allows an application to enable or disable specific validation features.
Note that this extension provides low-level control to an application, and that some combinations of enable/disable settings may produce undefined behavior.</p>
<p>Previous to the introduction of the unified Khronos validation layer, individual layers could be disabled by the expedient of not loading them.
Similar functionality is available with the Khronos validation layer through this extension -- these <code>VK_EXT_validation_features</code> flags correspond to
the following deprecated layers:</p>
<table>
<thead>
<tr>
<th>Setting this <code>VK_EXT_validation_features</code> disable flag</th>
<th>Corresponds to not loading this deprecated layer</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>VK_VALIDATION_FEATURE_DISABLE_THREAD_SAFETY_EXT</code></td>
<td><code>VK_LAYER_GOOGLE_threading</code></td>
</tr>
<tr>
<td><code>VK_VALIDATION_FEATURE_DISABLE_API_PARAMETERS_EXT</code></td>
<td><code>VK_LAYER_LUNARG_parameter_validation</code></td>
</tr>
<tr>
<td><code>VK_VALIDATION_FEATURE_DISABLE_OBJECT_LIFETIMES_EXT</code></td>
<td><code>VK_LAYER_LUNARG_object_tracker</code></td>
</tr>
<tr>
<td><code>VK_VALIDATION_FEATURE_DISABLE_CORE_CHECKS_EXT</code></td>
<td><code>VK_LAYER_LUNARG_core_validation</code></td>
</tr>
<tr>
<td><code>VK_VALIDATION_FEATURE_DISABLE_UNIQUE_HANDLES_EXT</code></td>
<td><code>VK_LAYER_GOOGLE_unqiue_objects</code></td>
</tr>
</tbody>
</table>
<p><strong>Refer to the Validation Features extension section of the full</strong> <a href="./vkspec.html##VkValidationFeaturesEXT">Vulkan Spec</a> <strong>for additional details.</strong></p>
<h3>
<a id="user-content-vk_ext_debug_report-deprecated" class="anchor" href="#user-content-vk_ext_debug_report-deprecated" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a><a name="user-content-debugreport"></a>VK_EXT_debug_report [DEPRECATED]</h3>
<p>The <code>VK_EXT_debug_report</code> extension provides a subset of the functionality in the <code>VK_EXT_debug_utils</code> extension.
<strong>It has been deprecated and should not be used for new applications.</strong>
Instead, use the new <code>VK_EXT_debug_utils</code> extension for your applications.</p>
<h4>
<a id="user-content-message-types-as-reported-by-vk_ext_debug_report-flags" class="anchor" href="#user-content-message-types-as-reported-by-vk_ext_debug_report-flags" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a>Message Types As Reported By VK_EXT_debug_report flags:</h4>
<table>
<thead>
<tr>
<th>Type</th>
<th>Debug Report Flag</th>
</tr>
</thead>
<tbody>
<tr>
<td>Error</td>
<td><code>VK_DEBUG_REPORT_ERROR_BIT_EXT</code></td>
</tr>
<tr>
<td>Warn</td>
<td><code>VK_DEBUG_REPORT_WARNING_BIT_EXT</code></td>
</tr>
<tr>
<td>Perf Warn</td>
<td><code>VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT</code></td>
</tr>
<tr>
<td>Info</td>
<td><code>VK_DEBUG_REPORT_INFORMATION_BIT_EXT</code></td>
</tr>
</tbody>
</table>
<p>If you are still interested in learning more about this extension,
refer to the "Debug Report Callbacks" section of the full <a href="./vkspec.html#debugging-debug-report-callbacks">Vulkan Spec</a> for more details.</p>
<h3>
<a id="user-content-vk_ext_validation_flags-deprecated" class="anchor" href="#user-content-vk_ext_validation_flags-deprecated" aria-hidden="true"><span aria-hidden="true" class="octicon octicon-link"></span></a><a name="user-content-validationfeatures"></a>VK_EXT_validation_flags [DEPRECATED]</h3>
<p>One method for an app to control layer features is through the <code>VK_EXT_validation_flags</code> extension.
<strong>This extension has been deprecated and should not be used for new applications.</strong></p>
<p>Refer to the Validation Flags extension section of the full <a href="./vkspec.html##VkValidationFeaturesEXT">Vulkan Spec</a> for additional details.</p>
</article>
</body>
</html>
