// *** THIS FILE IS GENERATED - DO NOT EDIT ***
// See vktrace_file_generator.py for modifications


/***************************************************************************
 *
 * Copyright (c) 2015-2017 The Khronos Group Inc.
 * Copyright (c) 2015-2017 Valve Corporation
 * Copyright (c) 2015-2017 LunarG, Inc.
 * Copyright (c) 2015-2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: Mark Lobodzinski <mark@lunarg.com>
 * Author: Jon Ashburn <jon@lunarg.com>
 * Author: Tobin Ehlis <tobin@lunarg.com>
 * Author: Peter Lohrmann <peterl@valvesoftware.com>
 * Author: David Pinedo <david@lunarg.com>
 *
 ****************************************************************************/

#include "vktrace_platform.h"
#include "vktrace_common.h"
#include "vktrace_lib_helpers.h"
#include "vktrace_lib_trim.h"
#include "vktrace_vk_vk.h"
#include "vktrace_interconnect.h"
#include "vktrace_filelike.h"
#include "vk_struct_size_helper.h"
#ifdef PLATFORM_LINUX
#include <pthread.h>
#endif
#include "vktrace_trace_packet_utils.h"
#include <stdio.h>
#include <string.h>

#ifdef WIN32
INIT_ONCE gInitOnce = INIT_ONCE_STATIC_INIT;
#elif defined(PLATFORM_LINUX)
pthread_once_t gInitOnce = PTHREAD_ONCE_INIT;
#endif

extern VKTRACE_CRITICAL_SECTION g_memInfoLock;

#ifdef WIN32
BOOL CALLBACK InitTracer(_Inout_ PINIT_ONCE initOnce, _Inout_opt_ PVOID param, _Out_opt_ PVOID *lpContext) {
#elif defined(PLATFORM_LINUX)
void InitTracer(void) {
#endif

#if defined(ANDROID)
    // On Android, we can use an abstract socket to fit permissions model
    const char *ipAddr = "localabstract";
    const char *ipPort = "vktrace";
    gMessageStream = vktrace_MessageStream_create_port_string(FALSE, ipAddr, ipPort);
#else
    const char *ipAddr = vktrace_get_global_var("VKTRACE_LIB_IPADDR");
    if (ipAddr == NULL)
        ipAddr = "127.0.0.1";
    gMessageStream = vktrace_MessageStream_create(FALSE, ipAddr, VKTRACE_BASE_PORT + VKTRACE_TID_VULKAN);
#endif
    vktrace_trace_set_trace_file(vktrace_FileLike_create_msg(gMessageStream));
    vktrace_tracelog_set_tracer_id(VKTRACE_TID_VULKAN);
    trim::initialize();
    vktrace_initialize_trace_packet_utils();
    vktrace_create_critical_section(&g_memInfoLock);
#ifdef WIN32
    return true;
}
#elif defined(PLATFORM_LINUX)
    return;
}
#endif

// __HOOKED_vkCreateInstance is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateInstance(
    const VkInstanceCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkInstance* pInstance);
// __HOOKED_vkDestroyInstance is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyInstance(
    VkInstance instance,
    const VkAllocationCallbacks* pAllocator);
// __HOOKED_vkEnumeratePhysicalDevices is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkEnumeratePhysicalDevices(
    VkInstance instance,
    uint32_t* pPhysicalDeviceCount,
    VkPhysicalDevice* pPhysicalDevices);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceFeatures(
    VkPhysicalDevice physicalDevice,
    VkPhysicalDeviceFeatures* pFeatures) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceFeatures* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceFeatures, sizeof(VkPhysicalDeviceFeatures));
    mid(physicalDevice)->instTable.GetPhysicalDeviceFeatures(physicalDevice, pFeatures);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceFeatures(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFeatures), sizeof(VkPhysicalDeviceFeatures), pFeatures);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFeatures));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceFormatProperties(
    VkPhysicalDevice physicalDevice,
    VkFormat format,
    VkFormatProperties* pFormatProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceFormatProperties* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceFormatProperties, sizeof(VkFormatProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceFormatProperties(physicalDevice, format, pFormatProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceFormatProperties(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->format = format;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFormatProperties), sizeof(VkFormatProperties), pFormatProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFormatProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceImageFormatProperties(
    VkPhysicalDevice physicalDevice,
    VkFormat format,
    VkImageType type,
    VkImageTiling tiling,
    VkImageUsageFlags usage,
    VkImageCreateFlags flags,
    VkImageFormatProperties* pImageFormatProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceImageFormatProperties* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceImageFormatProperties, sizeof(VkImageFormatProperties));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceImageFormatProperties(physicalDevice, format, type, tiling, usage, flags, pImageFormatProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceImageFormatProperties(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->format = format;
    pPacket->type = type;
    pPacket->tiling = tiling;
    pPacket->usage = usage;
    pPacket->flags = flags;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImageFormatProperties), sizeof(VkImageFormatProperties), pImageFormatProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImageFormatProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// __HOOKED_vkGetPhysicalDeviceProperties is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceProperties(
    VkPhysicalDevice physicalDevice,
    VkPhysicalDeviceProperties* pProperties);
// __HOOKED_vkGetPhysicalDeviceQueueFamilyProperties is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceQueueFamilyProperties(
    VkPhysicalDevice physicalDevice,
    uint32_t* pQueueFamilyPropertyCount,
    VkQueueFamilyProperties* pQueueFamilyProperties);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceMemoryProperties(
    VkPhysicalDevice physicalDevice,
    VkPhysicalDeviceMemoryProperties* pMemoryProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceMemoryProperties* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceMemoryProperties, sizeof(VkPhysicalDeviceMemoryProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceMemoryProperties(physicalDevice, pMemoryProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceMemoryProperties(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryProperties), sizeof(VkPhysicalDeviceMemoryProperties), pMemoryProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsPreTrim) {
            trim::ObjectInfo* pInfo = trim::get_PhysicalDevice_objectInfo(physicalDevice);
            if (pInfo != NULL) {
                pInfo->ObjectInfo.PhysicalDevice.pGetPhysicalDeviceMemoryPropertiesPacket = trim::copy_packet(pHeader);
            }
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkGetInstanceProcAddr is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL __HOOKED_vkGetInstanceProcAddr(
    VkInstance instance,
    const char* pName);
// __HOOKED_vkGetDeviceProcAddr is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL __HOOKED_vkGetDeviceProcAddr(
    VkDevice device,
    const char* pName);
// __HOOKED_vkCreateDevice is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateDevice(
    VkPhysicalDevice physicalDevice,
    const VkDeviceCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDevice* pDevice);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyDevice(
    VkDevice device,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyDevice* pPacket = NULL;
    dispatch_key key = get_dispatch_key(device);
    CREATE_TRACE_PACKET(vkDestroyDevice, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyDevice(device, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyDevice(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_Device_object(device);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    g_deviceDataMap.erase(key);
}
// __HOOKED_vkEnumerateInstanceExtensionProperties is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkEnumerateInstanceExtensionProperties(
    const char* pLayerName,
    uint32_t* pPropertyCount,
    VkExtensionProperties* pProperties);
// __HOOKED_vkEnumerateDeviceExtensionProperties is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkEnumerateDeviceExtensionProperties(
    VkPhysicalDevice physicalDevice,
    const char* pLayerName,
    uint32_t* pPropertyCount,
    VkExtensionProperties* pProperties);
// __HOOKED_vkEnumerateInstanceLayerProperties is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkEnumerateInstanceLayerProperties(
    uint32_t* pPropertyCount,
    VkLayerProperties* pProperties);
// __HOOKED_vkEnumerateDeviceLayerProperties is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkEnumerateDeviceLayerProperties(
    VkPhysicalDevice physicalDevice,
    uint32_t* pPropertyCount,
    VkLayerProperties* pProperties);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetDeviceQueue(
    VkDevice device,
    uint32_t queueFamilyIndex,
    uint32_t queueIndex,
    VkQueue* pQueue) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDeviceQueue* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDeviceQueue, sizeof(VkQueue));
    mdd(device)->devTable.GetDeviceQueue(device, queueFamilyIndex, queueIndex, pQueue);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDeviceQueue(pHeader);
    pPacket->device = device;
    pPacket->queueFamilyIndex = queueFamilyIndex;
    pPacket->queueIndex = queueIndex;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pQueue), sizeof(VkQueue), pQueue);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pQueue));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_Queue_object(*pQueue);
        info.belongsToDevice = device;
        info.ObjectInfo.Queue.pCreatePacket = trim::copy_packet(pHeader);
        info.ObjectInfo.Queue.queueFamilyIndex = queueFamilyIndex;
        info.ObjectInfo.Queue.queueIndex = queueIndex;
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkQueueSubmit is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkQueueSubmit(
    VkQueue queue,
    uint32_t submitCount,
    const VkSubmitInfo* pSubmits,
    VkFence fence);
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkQueueWaitIdle(
    VkQueue queue) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkQueueWaitIdle* pPacket = NULL;
    CREATE_TRACE_PACKET(vkQueueWaitIdle, 0);
    result = mdd(queue)->devTable.QueueWaitIdle(queue);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkQueueWaitIdle(pHeader);
    pPacket->queue = queue;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkDeviceWaitIdle(
    VkDevice device) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkDeviceWaitIdle* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDeviceWaitIdle, 0);
    result = mdd(device)->devTable.DeviceWaitIdle(device);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDeviceWaitIdle(pHeader);
    pPacket->device = device;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// __HOOKED_vkAllocateMemory is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkAllocateMemory(
    VkDevice device,
    const VkMemoryAllocateInfo* pAllocateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDeviceMemory* pMemory);
// __HOOKED_vkFreeMemory is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkFreeMemory(
    VkDevice device,
    VkDeviceMemory memory,
    const VkAllocationCallbacks* pAllocator);
// __HOOKED_vkMapMemory is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkMapMemory(
    VkDevice device,
    VkDeviceMemory memory,
    VkDeviceSize offset,
    VkDeviceSize size,
    VkMemoryMapFlags flags,
    void** ppData);
// __HOOKED_vkUnmapMemory is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkUnmapMemory(
    VkDevice device,
    VkDeviceMemory memory);
// __HOOKED_vkFlushMappedMemoryRanges is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkFlushMappedMemoryRanges(
    VkDevice device,
    uint32_t memoryRangeCount,
    const VkMappedMemoryRange* pMemoryRanges);
// __HOOKED_vkInvalidateMappedMemoryRanges is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkInvalidateMappedMemoryRanges(
    VkDevice device,
    uint32_t memoryRangeCount,
    const VkMappedMemoryRange* pMemoryRanges);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetDeviceMemoryCommitment(
    VkDevice device,
    VkDeviceMemory memory,
    VkDeviceSize* pCommittedMemoryInBytes) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDeviceMemoryCommitment* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDeviceMemoryCommitment, sizeof(VkDeviceSize));
    mdd(device)->devTable.GetDeviceMemoryCommitment(device, memory, pCommittedMemoryInBytes);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDeviceMemoryCommitment(pHeader);
    pPacket->device = device;
    pPacket->memory = memory;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCommittedMemoryInBytes), sizeof(VkDeviceSize), pCommittedMemoryInBytes);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCommittedMemoryInBytes));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// __HOOKED_vkBindBufferMemory is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkBindBufferMemory(
    VkDevice device,
    VkBuffer buffer,
    VkDeviceMemory memory,
    VkDeviceSize memoryOffset);
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkBindImageMemory(
    VkDevice device,
    VkImage image,
    VkDeviceMemory memory,
    VkDeviceSize memoryOffset) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkBindImageMemory* pPacket = NULL;
    CREATE_TRACE_PACKET(vkBindImageMemory, 0);
    result = mdd(device)->devTable.BindImageMemory(device, image, memory, memoryOffset);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkBindImageMemory(pHeader);
    pPacket->device = device;
    pPacket->image = image;
    pPacket->memory = memory;
    pPacket->memoryOffset = memoryOffset;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_Image_objectInfo(image);
        if (pInfo != NULL) {
            if (pInfo->ObjectInfo.Image.memorySize == 0) {
                // trim get image memory size through target title call
                // vkGetImageMemoryRequirements for the image, but so
                // far the title doesn't call vkGetImageMemoryRequirements,
                // so here we call it for the image.
                VkMemoryRequirements MemoryRequirements;
                mdd(device)->devTable.GetImageMemoryRequirements(device,image,&MemoryRequirements);
                pInfo->ObjectInfo.Image.memorySize = MemoryRequirements.size;
            }

            pInfo->ObjectInfo.Image.pBindImageMemoryPacket = trim::copy_packet(pHeader);
            pInfo->ObjectInfo.Image.memory = memory;
            pInfo->ObjectInfo.Image.memoryOffset = memoryOffset;
            pInfo->ObjectInfo.Image.needsStagingBuffer = pInfo->ObjectInfo.Image.needsStagingBuffer || trim::IsMemoryDeviceOnly(memory);
        }
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetBufferMemoryRequirements(
    VkDevice device,
    VkBuffer buffer,
    VkMemoryRequirements* pMemoryRequirements) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetBufferMemoryRequirements* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetBufferMemoryRequirements, sizeof(VkMemoryRequirements));
    mdd(device)->devTable.GetBufferMemoryRequirements(device, buffer, pMemoryRequirements);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetBufferMemoryRequirements(pHeader);
    pPacket->device = device;
    pPacket->buffer = buffer;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryRequirements), sizeof(VkMemoryRequirements), pMemoryRequirements);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryRequirements));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(buffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetImageMemoryRequirements(
    VkDevice device,
    VkImage image,
    VkMemoryRequirements* pMemoryRequirements) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetImageMemoryRequirements* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetImageMemoryRequirements, sizeof(VkMemoryRequirements));
    mdd(device)->devTable.GetImageMemoryRequirements(device, image, pMemoryRequirements);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetImageMemoryRequirements(pHeader);
    pPacket->device = device;
    pPacket->image = image;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryRequirements), sizeof(VkMemoryRequirements), pMemoryRequirements);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryRequirements));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pTrimObjectInfo = trim::get_Image_objectInfo(image);
        if (pTrimObjectInfo != NULL) {
            pTrimObjectInfo->ObjectInfo.Image.memorySize = pMemoryRequirements->size;
        }
#if TRIM_USE_ORDERED_IMAGE_CREATION
        trim::add_Image_call(trim::copy_packet(pHeader));
#else
        if (pTrimObjectInfo != NULL) {
            pTrimObjectInfo->ObjectInfo.Image.pGetImageMemoryRequirementsPacket = trim::copy_packet(pHeader);
        }
#endif //TRIM_USE_ORDERED_IMAGE_CREATION
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetImageSparseMemoryRequirements(
    VkDevice device,
    VkImage image,
    uint32_t* pSparseMemoryRequirementCount,
    VkSparseImageMemoryRequirements* pSparseMemoryRequirements) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetImageSparseMemoryRequirements* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetImageSparseMemoryRequirements, sizeof(uint32_t) + (*pSparseMemoryRequirementCount) * sizeof(VkSparseImageMemoryRequirements));
    mdd(device)->devTable.GetImageSparseMemoryRequirements(device, image, pSparseMemoryRequirementCount, pSparseMemoryRequirements);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetImageSparseMemoryRequirements(pHeader);
    pPacket->device = device;
    pPacket->image = image;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSparseMemoryRequirementCount), sizeof(uint32_t), pSparseMemoryRequirementCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSparseMemoryRequirements), (*pSparseMemoryRequirementCount) * sizeof(VkSparseImageMemoryRequirements), pSparseMemoryRequirements);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSparseMemoryRequirementCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSparseMemoryRequirements));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSparseImageFormatProperties(
    VkPhysicalDevice physicalDevice,
    VkFormat format,
    VkImageType type,
    VkSampleCountFlagBits samples,
    VkImageUsageFlags usage,
    VkImageTiling tiling,
    uint32_t* pPropertyCount,
    VkSparseImageFormatProperties* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceSparseImageFormatProperties* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceSparseImageFormatProperties, sizeof(uint32_t) + (*pPropertyCount) * sizeof(VkSparseImageFormatProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceSparseImageFormatProperties(physicalDevice, format, type, samples, usage, tiling, pPropertyCount, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceSparseImageFormatProperties(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->format = format;
    pPacket->type = type;
    pPacket->samples = samples;
    pPacket->usage = usage;
    pPacket->tiling = tiling;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPropertyCount), sizeof(uint32_t), pPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), (*pPropertyCount) * sizeof(VkSparseImageFormatProperties), pProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// __HOOKED_vkQueueBindSparse is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkQueueBindSparse(
    VkQueue queue,
    uint32_t bindInfoCount,
    const VkBindSparseInfo* pBindInfo,
    VkFence fence);
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateFence(
    VkDevice device,
    const VkFenceCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkFence* pFence) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateFence* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateFence, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkFence));
    result = mdd(device)->devTable.CreateFence(device, pCreateInfo, pAllocator, pFence);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateFence(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkFenceCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFence), sizeof(VkFence), pFence);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFence));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_Fence_object(*pFence);
        info.belongsToDevice = device;
        info.ObjectInfo.Fence.signaled = ((pCreateInfo->flags & VK_FENCE_CREATE_SIGNALED_BIT) == VK_FENCE_CREATE_SIGNALED_BIT);
        if (pAllocator != NULL) {
            info.ObjectInfo.Fence.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyFence(
    VkDevice device,
    VkFence fence,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyFence* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyFence, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyFence(device, fence, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyFence(pHeader);
    pPacket->device = device;
    pPacket->fence = fence;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_Fence_object(fence);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkResetFences(
    VkDevice device,
    uint32_t fenceCount,
    const VkFence* pFences) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkResetFences* pPacket = NULL;
    CREATE_TRACE_PACKET(vkResetFences, fenceCount * sizeof(VkFence));
    result = mdd(device)->devTable.ResetFences(device, fenceCount, pFences);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkResetFences(pHeader);
    pPacket->device = device;
    pPacket->fenceCount = fenceCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFences), fenceCount * sizeof(VkFence), pFences);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFences));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        for (uint32_t i = 0; i < fenceCount; i++) {
            trim::ObjectInfo* pFenceInfo = trim::get_Fence_objectInfo(pFences[i]);
            if (pFenceInfo != NULL && result == VK_SUCCESS) {
                // clear the fence
                pFenceInfo->ObjectInfo.Fence.signaled = false;
            }
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetFenceStatus(
    VkDevice device,
    VkFence fence) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetFenceStatus* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetFenceStatus, 0);
    result = mdd(device)->devTable.GetFenceStatus(device, fence);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetFenceStatus(pHeader);
    pPacket->device = device;
    pPacket->fence = fence;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// __HOOKED_vkWaitForFences is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkWaitForFences(
    VkDevice device,
    uint32_t fenceCount,
    const VkFence* pFences,
    VkBool32 waitAll,
    uint64_t timeout);
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateSemaphore(
    VkDevice device,
    const VkSemaphoreCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSemaphore* pSemaphore) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateSemaphore* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateSemaphore, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkSemaphore));
    result = mdd(device)->devTable.CreateSemaphore(device, pCreateInfo, pAllocator, pSemaphore);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateSemaphore(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkSemaphoreCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSemaphore), sizeof(VkSemaphore), pSemaphore);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSemaphore));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_Semaphore_object(*pSemaphore);
        info.belongsToDevice = device;
        info.ObjectInfo.Semaphore.pCreatePacket = trim::copy_packet(pHeader);
        info.ObjectInfo.Semaphore.signaledOnQueue = VK_NULL_HANDLE;
        info.ObjectInfo.Semaphore.signaledOnSwapChain = VK_NULL_HANDLE;
        if (pAllocator != NULL) {
            info.ObjectInfo.Semaphore.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroySemaphore(
    VkDevice device,
    VkSemaphore semaphore,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroySemaphore* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroySemaphore, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroySemaphore(device, semaphore, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroySemaphore(pHeader);
    pPacket->device = device;
    pPacket->semaphore = semaphore;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_Semaphore_object(semaphore);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateEvent(
    VkDevice device,
    const VkEventCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkEvent* pEvent) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateEvent* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateEvent, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkEvent));
    result = mdd(device)->devTable.CreateEvent(device, pCreateInfo, pAllocator, pEvent);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateEvent(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkEventCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pEvent), sizeof(VkEvent), pEvent);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pEvent));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_Event_object(*pEvent);
        info.belongsToDevice = device;
        info.ObjectInfo.Event.pCreatePacket = trim::copy_packet(pHeader);
        if (pAllocator != NULL) {
            info.ObjectInfo.Event.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyEvent(
    VkDevice device,
    VkEvent event,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyEvent* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyEvent, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyEvent(device, event, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyEvent(pHeader);
    pPacket->device = device;
    pPacket->event = event;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_Event_object(event);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetEventStatus(
    VkDevice device,
    VkEvent event) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetEventStatus* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetEventStatus, 0);
    result = mdd(device)->devTable.GetEventStatus(device, event);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetEventStatus(pHeader);
    pPacket->device = device;
    pPacket->event = event;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkSetEvent(
    VkDevice device,
    VkEvent event) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkSetEvent* pPacket = NULL;
    CREATE_TRACE_PACKET(vkSetEvent, 0);
    result = mdd(device)->devTable.SetEvent(device, event);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkSetEvent(pHeader);
    pPacket->device = device;
    pPacket->event = event;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkResetEvent(
    VkDevice device,
    VkEvent event) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkResetEvent* pPacket = NULL;
    CREATE_TRACE_PACKET(vkResetEvent, 0);
    result = mdd(device)->devTable.ResetEvent(device, event);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkResetEvent(pHeader);
    pPacket->device = device;
    pPacket->event = event;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateQueryPool(
    VkDevice device,
    const VkQueryPoolCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkQueryPool* pQueryPool) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateQueryPool* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateQueryPool, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkQueryPool));
    result = mdd(device)->devTable.CreateQueryPool(device, pCreateInfo, pAllocator, pQueryPool);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateQueryPool(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkQueryPoolCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pQueryPool), sizeof(VkQueryPool), pQueryPool);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pQueryPool));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_QueryPool_object(*pQueryPool);
        info.belongsToDevice = device;
        info.ObjectInfo.QueryPool.pCreatePacket = trim::copy_packet(pHeader);
        if (pCreateInfo != nullptr) {
            info.ObjectInfo.QueryPool.queryType = pCreateInfo->queryType;
            info.ObjectInfo.QueryPool.size = pCreateInfo->queryCount;
            info.ObjectInfo.QueryPool.pResultsAvailable = new bool[pCreateInfo->queryCount];
            for (uint32_t i = 0; i < pCreateInfo->queryCount; i++) {
                info.ObjectInfo.QueryPool.pResultsAvailable[i] = false;
            }
        }
        if (pAllocator != NULL) {
            info.ObjectInfo.QueryPool.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyQueryPool(
    VkDevice device,
    VkQueryPool queryPool,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyQueryPool* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyQueryPool, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyQueryPool(device, queryPool, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyQueryPool(pHeader);
    pPacket->device = device;
    pPacket->queryPool = queryPool;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_QueryPool_object(queryPool);
        if (g_trimIsInTrim) {
            trim::mark_QueryPool_reference(queryPool);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkGetQueryPoolResults is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetQueryPoolResults(
    VkDevice device,
    VkQueryPool queryPool,
    uint32_t firstQuery,
    uint32_t queryCount,
    size_t dataSize,
    void* pData,
    VkDeviceSize stride,
    VkQueryResultFlags flags);
// __HOOKED_vkCreateBuffer is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateBuffer(
    VkDevice device,
    const VkBufferCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkBuffer* pBuffer);
// __HOOKED_vkDestroyBuffer is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyBuffer(
    VkDevice device,
    VkBuffer buffer,
    const VkAllocationCallbacks* pAllocator);
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateBufferView(
    VkDevice device,
    const VkBufferViewCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkBufferView* pView) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateBufferView* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateBufferView, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkBufferView));
    result = mdd(device)->devTable.CreateBufferView(device, pCreateInfo, pAllocator, pView);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateBufferView(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkBufferViewCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pView), sizeof(VkBufferView), pView);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pView));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_BufferView_object(*pView);
        info.belongsToDevice = device;
        info.ObjectInfo.BufferView.pCreatePacket = trim::copy_packet(pHeader);
        info.ObjectInfo.BufferView.buffer = pCreateInfo->buffer;
        if (pAllocator != NULL) {
            info.ObjectInfo.BufferView.pAllocator = pAllocator;
        }
        if (pAllocator != NULL) {
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(pCreateInfo->buffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyBufferView(
    VkDevice device,
    VkBufferView bufferView,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyBufferView* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyBufferView, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyBufferView(device, bufferView, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyBufferView(pHeader);
    pPacket->device = device;
    pPacket->bufferView = bufferView;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_BufferView_object(bufferView);
        if (g_trimIsInTrim) {
            trim::mark_BufferView_reference(bufferView);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkCreateImage is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateImage(
    VkDevice device,
    const VkImageCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkImage* pImage);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyImage(
    VkDevice device,
    VkImage image,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyImage* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyImage, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyImage(device, image, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyImage(pHeader);
    pPacket->device = device;
    pPacket->image = image;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::deleteImageSubResourceSizes(image);
#if TRIM_USE_ORDERED_IMAGE_CREATION
        trim::add_Image_call(trim::copy_packet(pHeader));
#endif //TRIM_USE_ORDERED_IMAGE_CREATION
        trim::remove_Image_object(image);
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetImageSubresourceLayout(
    VkDevice device,
    VkImage image,
    const VkImageSubresource* pSubresource,
    VkSubresourceLayout* pLayout) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetImageSubresourceLayout* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetImageSubresourceLayout, sizeof(VkImageSubresource) + sizeof(VkSubresourceLayout));
    mdd(device)->devTable.GetImageSubresourceLayout(device, image, pSubresource, pLayout);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetImageSubresourceLayout(pHeader);
    pPacket->device = device;
    pPacket->image = image;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSubresource), sizeof(VkImageSubresource), pSubresource);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pLayout), sizeof(VkSubresourceLayout), pLayout);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSubresource));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pLayout));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateImageView(
    VkDevice device,
    const VkImageViewCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkImageView* pView) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateImageView* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateImageView, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkImageView));
    result = mdd(device)->devTable.CreateImageView(device, pCreateInfo, pAllocator, pView);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateImageView(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkImageViewCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pView), sizeof(VkImageView), pView);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pView));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_ImageView_object(*pView);
        info.belongsToDevice = device;
        info.ObjectInfo.ImageView.pCreatePacket = trim::copy_packet(pHeader);
        info.ObjectInfo.ImageView.image = pCreateInfo->image;
        if (pAllocator != NULL) {
            info.ObjectInfo.ImageView.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(pCreateInfo->image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyImageView(
    VkDevice device,
    VkImageView imageView,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyImageView* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyImageView, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyImageView(device, imageView, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyImageView(pHeader);
    pPacket->device = device;
    pPacket->imageView = imageView;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_ImageView_object(imageView);
        if (g_trimIsInTrim) {
            trim::mark_ImageView_reference(imageView);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateShaderModule(
    VkDevice device,
    const VkShaderModuleCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkShaderModule* pShaderModule) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateShaderModule* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateShaderModule, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkShaderModule));
    result = mdd(device)->devTable.CreateShaderModule(device, pCreateInfo, pAllocator, pShaderModule);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateShaderModule(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkShaderModuleCreateInfo), pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo->pCode), pPacket->pCreateInfo->codeSize, pCreateInfo->pCode);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pShaderModule), sizeof(VkShaderModule), pShaderModule);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo->pCode));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pShaderModule));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_ShaderModule_object(*pShaderModule);
        info.belongsToDevice = device;
        trim::StateTracker::copy_VkShaderModuleCreateInfo(&info.ObjectInfo.ShaderModule.createInfo, *pCreateInfo);
        if (pAllocator != NULL) {
            info.ObjectInfo.ShaderModule.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyShaderModule(
    VkDevice device,
    VkShaderModule shaderModule,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyShaderModule* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyShaderModule, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyShaderModule(device, shaderModule, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyShaderModule(pHeader);
    pPacket->device = device;
    pPacket->shaderModule = shaderModule;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_ShaderModule_object(shaderModule);
        if (g_trimIsInTrim) {
            trim::mark_ShaderModule_reference(shaderModule);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkCreatePipelineCache is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreatePipelineCache(
    VkDevice device,
    const VkPipelineCacheCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkPipelineCache* pPipelineCache);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyPipelineCache(
    VkDevice device,
    VkPipelineCache pipelineCache,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyPipelineCache* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyPipelineCache, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyPipelineCache(device, pipelineCache, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyPipelineCache(pHeader);
    pPacket->device = device;
    pPacket->pipelineCache = pipelineCache;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_PipelineCache_object(pipelineCache);
        if (g_trimIsInTrim) {
            trim::mark_PipelineCache_reference(pipelineCache);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkGetPipelineCacheData is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPipelineCacheData(
    VkDevice device,
    VkPipelineCache pipelineCache,
    size_t* pDataSize,
    void* pData);
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkMergePipelineCaches(
    VkDevice device,
    VkPipelineCache dstCache,
    uint32_t srcCacheCount,
    const VkPipelineCache* pSrcCaches) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkMergePipelineCaches* pPacket = NULL;
    CREATE_TRACE_PACKET(vkMergePipelineCaches, srcCacheCount * sizeof(VkPipelineCache));
    result = mdd(device)->devTable.MergePipelineCaches(device, dstCache, srcCacheCount, pSrcCaches);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkMergePipelineCaches(pHeader);
    pPacket->device = device;
    pPacket->dstCache = dstCache;
    pPacket->srcCacheCount = srcCacheCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSrcCaches), srcCacheCount * sizeof(VkPipelineCache), pSrcCaches);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSrcCaches));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            for (uint32_t i = 0; i < srcCacheCount; i++) {
                trim::mark_PipelineCache_reference(pSrcCaches[i]);
            }
            trim::mark_PipelineCache_reference(dstCache);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
// __HOOKED_vkCreateGraphicsPipelines is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateGraphicsPipelines(
    VkDevice device,
    VkPipelineCache pipelineCache,
    uint32_t createInfoCount,
    const VkGraphicsPipelineCreateInfo* pCreateInfos,
    const VkAllocationCallbacks* pAllocator,
    VkPipeline* pPipelines);
// __HOOKED_vkCreateComputePipelines is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateComputePipelines(
    VkDevice device,
    VkPipelineCache pipelineCache,
    uint32_t createInfoCount,
    const VkComputePipelineCreateInfo* pCreateInfos,
    const VkAllocationCallbacks* pAllocator,
    VkPipeline* pPipelines);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyPipeline(
    VkDevice device,
    VkPipeline pipeline,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyPipeline* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyPipeline, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyPipeline(device, pipeline, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyPipeline(pHeader);
    pPacket->device = device;
    pPacket->pipeline = pipeline;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_Pipeline_object(pipeline);
        trim::clear_CommandBuffer_calls_by_binding_Pipeline(pipeline);
        if (g_trimIsInTrim) {
            trim::mark_Pipeline_reference(pipeline);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreatePipelineLayout(
    VkDevice device,
    const VkPipelineLayoutCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkPipelineLayout* pPipelineLayout) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreatePipelineLayout* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreatePipelineLayout, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkPipelineLayout));
    result = mdd(device)->devTable.CreatePipelineLayout(device, pCreateInfo, pAllocator, pPipelineLayout);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreatePipelineLayout(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkPipelineLayoutCreateInfo), pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo->pSetLayouts), pCreateInfo->setLayoutCount * sizeof(VkDescriptorSetLayout), pCreateInfo->pSetLayouts);    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo->pPushConstantRanges), pCreateInfo->pushConstantRangeCount * sizeof(VkPushConstantRange), pCreateInfo->pPushConstantRanges);;
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPipelineLayout), sizeof(VkPipelineLayout), pPipelineLayout);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo->pSetLayouts));
vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo->pPushConstantRanges));
vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPipelineLayout));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_PipelineLayout_object(*pPipelineLayout);
        info.belongsToDevice = device;
        info.ObjectInfo.PipelineLayout.pCreatePacket = trim::copy_packet(pHeader);
        info.ObjectInfo.PipelineLayout.descriptorSetLayoutCount = pCreateInfo->setLayoutCount;
        info.ObjectInfo.PipelineLayout.pDescriptorSetLayouts = (pCreateInfo->setLayoutCount == 0) ? nullptr : new VkDescriptorSetLayout[pCreateInfo->setLayoutCount];
        memcpy(info.ObjectInfo.PipelineLayout.pDescriptorSetLayouts, pCreateInfo->pSetLayouts, pCreateInfo->setLayoutCount * sizeof( VkDescriptorSetLayout ));
        if (pAllocator != NULL) {
            info.ObjectInfo.PipelineLayout.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyPipelineLayout(
    VkDevice device,
    VkPipelineLayout pipelineLayout,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyPipelineLayout* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyPipelineLayout, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyPipelineLayout(device, pipelineLayout, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyPipelineLayout(pHeader);
    pPacket->device = device;
    pPacket->pipelineLayout = pipelineLayout;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_PipelineLayout_object(pipelineLayout);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateSampler(
    VkDevice device,
    const VkSamplerCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSampler* pSampler) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateSampler* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateSampler, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkSampler));
    result = mdd(device)->devTable.CreateSampler(device, pCreateInfo, pAllocator, pSampler);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateSampler(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkSamplerCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSampler), sizeof(VkSampler), pSampler);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSampler));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_Sampler_object(*pSampler);
        info.belongsToDevice = device;
        info.ObjectInfo.Sampler.pCreatePacket = trim::copy_packet(pHeader);
        if (pAllocator != NULL) {
            info.ObjectInfo.Sampler.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroySampler(
    VkDevice device,
    VkSampler sampler,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroySampler* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroySampler, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroySampler(device, sampler, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroySampler(pHeader);
    pPacket->device = device;
    pPacket->sampler = sampler;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_Sampler_object(sampler);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateDescriptorSetLayout(
    VkDevice device,
    const VkDescriptorSetLayoutCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDescriptorSetLayout* pSetLayout) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateDescriptorSetLayout* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateDescriptorSetLayout, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkDescriptorSetLayout));
    result = mdd(device)->devTable.CreateDescriptorSetLayout(device, pCreateInfo, pAllocator, pSetLayout);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateDescriptorSetLayout(pHeader);
    pPacket->device = device;
    add_create_ds_layout_to_trace_packet(pHeader, &pPacket->pCreateInfo, pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSetLayout), sizeof(VkDescriptorSetLayout), pSetLayout);
    pPacket->result = result;
    // pCreateInfo finalized in add_create_ds_layout_to_trace_packet;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSetLayout));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_DescriptorSetLayout_object(*pSetLayout);
        info.belongsToDevice = device;
        info.ObjectInfo.DescriptorSetLayout.pCreatePacket = trim::copy_packet(pHeader);
        info.ObjectInfo.DescriptorSetLayout.bindingCount = pCreateInfo->bindingCount;
        info.ObjectInfo.DescriptorSetLayout.pBindings = (pCreateInfo->bindingCount == 0) ? nullptr : new VkDescriptorSetLayoutBinding[pCreateInfo->bindingCount];
        for (uint32_t i = 0; i < pCreateInfo->bindingCount; i++ ) {
            info.ObjectInfo.DescriptorSetLayout.pBindings[i] = pCreateInfo->pBindings[i];
            if (pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_SAMPLER ||
                pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER ||
                pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE ||
                pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_STORAGE_IMAGE ||
                pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT) {
                    info.ObjectInfo.DescriptorSetLayout.numImages++;
            }
            if (pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER ||
                pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_STORAGE_BUFFER ||
                pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC ||
                pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC) {
                    info.ObjectInfo.DescriptorSetLayout.numBuffers++;
            }
            if (pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER ||
                pCreateInfo->pBindings[i].descriptorType == VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER) {
                    info.ObjectInfo.DescriptorSetLayout.numTexelBufferViews++;
            }
        }
        if (pAllocator != NULL) {
            info.ObjectInfo.DescriptorSetLayout.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout descriptorSetLayout,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyDescriptorSetLayout* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyDescriptorSetLayout, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyDescriptorSetLayout(device, descriptorSetLayout, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyDescriptorSetLayout(pHeader);
    pPacket->device = device;
    pPacket->descriptorSetLayout = descriptorSetLayout;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_DescriptorSetLayout_object(descriptorSetLayout);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkCreateDescriptorPool is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateDescriptorPool(
    VkDevice device,
    const VkDescriptorPoolCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDescriptorPool* pDescriptorPool);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyDescriptorPool(
    VkDevice device,
    VkDescriptorPool descriptorPool,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyDescriptorPool* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyDescriptorPool, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyDescriptorPool(device, descriptorPool, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyDescriptorPool(pHeader);
    pPacket->device = device;
    pPacket->descriptorPool = descriptorPool;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_DescriptorPool_object(descriptorPool);
        if (g_trimIsInTrim) {
            trim::mark_DescriptorPool_reference(descriptorPool);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkResetDescriptorPool(
    VkDevice device,
    VkDescriptorPool descriptorPool,
    VkDescriptorPoolResetFlags flags) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkResetDescriptorPool* pPacket = NULL;
    CREATE_TRACE_PACKET(vkResetDescriptorPool, 0);
    result = mdd(device)->devTable.ResetDescriptorPool(device, descriptorPool, flags);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkResetDescriptorPool(pHeader);
    pPacket->device = device;
    pPacket->descriptorPool = descriptorPool;
    pPacket->flags = flags;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pPoolInfo = trim::get_DescriptorPool_objectInfo(descriptorPool);
        if (pPoolInfo != NULL) {
            pPoolInfo->ObjectInfo.DescriptorPool.numSets = 0;
        }
        trim::reset_DescriptorPool(descriptorPool);
        if (g_trimIsInTrim) {
            trim::mark_DescriptorPool_reference(descriptorPool);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
// __HOOKED_vkAllocateDescriptorSets is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkAllocateDescriptorSets(
    VkDevice device,
    const VkDescriptorSetAllocateInfo* pAllocateInfo,
    VkDescriptorSet* pDescriptorSets);
// __HOOKED_vkFreeDescriptorSets is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkFreeDescriptorSets(
    VkDevice device,
    VkDescriptorPool descriptorPool,
    uint32_t descriptorSetCount,
    const VkDescriptorSet* pDescriptorSets);
// __HOOKED_vkUpdateDescriptorSets is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkUpdateDescriptorSets(
    VkDevice device,
    uint32_t descriptorWriteCount,
    const VkWriteDescriptorSet* pDescriptorWrites,
    uint32_t descriptorCopyCount,
    const VkCopyDescriptorSet* pDescriptorCopies);
// __HOOKED_vkCreateFramebuffer is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateFramebuffer(
    VkDevice device,
    const VkFramebufferCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkFramebuffer* pFramebuffer);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyFramebuffer(
    VkDevice device,
    VkFramebuffer framebuffer,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyFramebuffer* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyFramebuffer, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyFramebuffer(device, framebuffer, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyFramebuffer(pHeader);
    pPacket->device = device;
    pPacket->framebuffer = framebuffer;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_Framebuffer_object(framebuffer);
        if (g_trimIsInTrim) {
            trim::mark_Framebuffer_reference(framebuffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkCreateRenderPass is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateRenderPass(
    VkDevice device,
    const VkRenderPassCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkRenderPass* pRenderPass);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyRenderPass(
    VkDevice device,
    VkRenderPass renderPass,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyRenderPass* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyRenderPass, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyRenderPass(device, renderPass, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyRenderPass(pHeader);
    pPacket->device = device;
    pPacket->renderPass = renderPass;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_RenderPass_object(renderPass);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetRenderAreaGranularity(
    VkDevice device,
    VkRenderPass renderPass,
    VkExtent2D* pGranularity) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetRenderAreaGranularity* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetRenderAreaGranularity, sizeof(VkExtent2D));
    mdd(device)->devTable.GetRenderAreaGranularity(device, renderPass, pGranularity);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetRenderAreaGranularity(pHeader);
    pPacket->device = device;
    pPacket->renderPass = renderPass;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pGranularity), sizeof(VkExtent2D), pGranularity);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pGranularity));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateCommandPool(
    VkDevice device,
    const VkCommandPoolCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkCommandPool* pCommandPool) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateCommandPool* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateCommandPool, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkCommandPool));
    result = mdd(device)->devTable.CreateCommandPool(device, pCreateInfo, pAllocator, pCommandPool);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateCommandPool(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkCommandPoolCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCommandPool), sizeof(VkCommandPool), pCommandPool);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCommandPool));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo &info = trim::add_CommandPool_object(*pCommandPool);
        info.belongsToDevice = device;
        info.ObjectInfo.CommandPool.pCreatePacket = trim::copy_packet(pHeader);
        info.ObjectInfo.CommandPool.queueFamilyIndex = pCreateInfo->queueFamilyIndex;
        if (pAllocator != NULL) {
            info.ObjectInfo.CommandPool.pAllocator = pAllocator;
            trim::add_Allocator(pAllocator);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyCommandPool(
    VkDevice device,
    VkCommandPool commandPool,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyCommandPool* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyCommandPool, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyCommandPool(device, commandPool, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyCommandPool(pHeader);
    pPacket->device = device;
    pPacket->commandPool = commandPool;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_CommandPool_object(commandPool);
        if (g_trimIsInTrim) {
            trim::mark_CommandPool_reference(commandPool);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkResetCommandPool(
    VkDevice device,
    VkCommandPool commandPool,
    VkCommandPoolResetFlags flags) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkResetCommandPool* pPacket = NULL;
    CREATE_TRACE_PACKET(vkResetCommandPool, 0);
    result = mdd(device)->devTable.ResetCommandPool(device, commandPool, flags);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkResetCommandPool(pHeader);
    pPacket->device = device;
    pPacket->commandPool = commandPool;
    pPacket->flags = flags;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::mark_CommandPool_reference(commandPool);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
// __HOOKED_vkAllocateCommandBuffers is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkAllocateCommandBuffers(
    VkDevice device,
    const VkCommandBufferAllocateInfo* pAllocateInfo,
    VkCommandBuffer* pCommandBuffers);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkFreeCommandBuffers(
    VkDevice device,
    VkCommandPool commandPool,
    uint32_t commandBufferCount,
    const VkCommandBuffer* pCommandBuffers) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkFreeCommandBuffers* pPacket = NULL;
    CREATE_TRACE_PACKET(vkFreeCommandBuffers, commandBufferCount * sizeof(VkCommandBuffer));
    mdd(device)->devTable.FreeCommandBuffers(device, commandPool, commandBufferCount, pCommandBuffers);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkFreeCommandBuffers(pHeader);
    pPacket->device = device;
    pPacket->commandPool = commandPool;
    pPacket->commandBufferCount = commandBufferCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCommandBuffers), commandBufferCount * sizeof(VkCommandBuffer), pCommandBuffers);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCommandBuffers));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pCBInfo = trim::get_CommandBuffer_objectInfo(pCommandBuffers[0]);
        VkCommandBufferLevel level = (pCBInfo == NULL) ? VK_COMMAND_BUFFER_LEVEL_PRIMARY : pCBInfo->ObjectInfo.CommandBuffer.level;
        trim::ObjectInfo* pInfo = trim::get_CommandPool_objectInfo(commandPool);
        if (pInfo != NULL ) { pInfo->ObjectInfo.CommandPool.numCommandBuffersAllocated[level] -= commandBufferCount; }
        for (uint32_t i = 0; i < commandBufferCount; i++) {
            trim::remove_CommandBuffer_object(pCommandBuffers[i]);
            trim::remove_CommandBuffer_calls(pCommandBuffers[i]);
            trim::ClearImageTransitions(pCommandBuffers[i]);
            trim::ClearBufferTransitions(pCommandBuffers[i]);
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkBeginCommandBuffer is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkBeginCommandBuffer(
    VkCommandBuffer commandBuffer,
    const VkCommandBufferBeginInfo* pBeginInfo);
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkEndCommandBuffer(
    VkCommandBuffer commandBuffer) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkEndCommandBuffer* pPacket = NULL;
    CREATE_TRACE_PACKET(vkEndCommandBuffer, 0);
    result = mdd(commandBuffer)->devTable.EndCommandBuffer(commandBuffer);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkEndCommandBuffer(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkResetCommandBuffer(
    VkCommandBuffer commandBuffer,
    VkCommandBufferResetFlags flags) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkResetCommandBuffer* pPacket = NULL;
    CREATE_TRACE_PACKET(vkResetCommandBuffer, 0);
    result = mdd(commandBuffer)->devTable.ResetCommandBuffer(commandBuffer, flags);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkResetCommandBuffer(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->flags = flags;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_CommandBuffer_calls(commandBuffer);
        trim::ClearImageTransitions(commandBuffer);
        trim::ClearBufferTransitions(commandBuffer);
        trim::clear_binding_Pipelines_from_CommandBuffer(commandBuffer);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdBindPipeline(
    VkCommandBuffer commandBuffer,
    VkPipelineBindPoint pipelineBindPoint,
    VkPipeline pipeline) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdBindPipeline* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdBindPipeline, 0);
    mdd(commandBuffer)->devTable.CmdBindPipeline(commandBuffer, pipelineBindPoint, pipeline);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdBindPipeline(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->pipelineBindPoint = pipelineBindPoint;
    pPacket->pipeline = pipeline;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        trim::add_CommandBuffer_to_binding_Pipeline(commandBuffer, pipeline);
        trim::add_binding_Pipeline_to_CommandBuffer(commandBuffer, pipeline);
        if (g_trimIsInTrim) {
            trim::mark_Pipeline_reference(pipeline);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetViewport(
    VkCommandBuffer commandBuffer,
    uint32_t firstViewport,
    uint32_t viewportCount,
    const VkViewport* pViewports) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetViewport* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetViewport, viewportCount * sizeof(VkViewport));
    mdd(commandBuffer)->devTable.CmdSetViewport(commandBuffer, firstViewport, viewportCount, pViewports);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetViewport(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->firstViewport = firstViewport;
    pPacket->viewportCount = viewportCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pViewports), viewportCount * sizeof(VkViewport), pViewports);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pViewports));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetScissor(
    VkCommandBuffer commandBuffer,
    uint32_t firstScissor,
    uint32_t scissorCount,
    const VkRect2D* pScissors) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetScissor* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetScissor, scissorCount * sizeof(VkRect2D));
    mdd(commandBuffer)->devTable.CmdSetScissor(commandBuffer, firstScissor, scissorCount, pScissors);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetScissor(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->firstScissor = firstScissor;
    pPacket->scissorCount = scissorCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pScissors), scissorCount * sizeof(VkRect2D), pScissors);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pScissors));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetLineWidth(
    VkCommandBuffer commandBuffer,
    float lineWidth) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetLineWidth* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetLineWidth, 0);
    mdd(commandBuffer)->devTable.CmdSetLineWidth(commandBuffer, lineWidth);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetLineWidth(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->lineWidth = lineWidth;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetDepthBias(
    VkCommandBuffer commandBuffer,
    float depthBiasConstantFactor,
    float depthBiasClamp,
    float depthBiasSlopeFactor) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetDepthBias* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetDepthBias, 0);
    mdd(commandBuffer)->devTable.CmdSetDepthBias(commandBuffer, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetDepthBias(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->depthBiasConstantFactor = depthBiasConstantFactor;
    pPacket->depthBiasClamp = depthBiasClamp;
    pPacket->depthBiasSlopeFactor = depthBiasSlopeFactor;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetBlendConstants(
    VkCommandBuffer commandBuffer,
    const float blendConstants[4]) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetBlendConstants* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetBlendConstants, 0);
    mdd(commandBuffer)->devTable.CmdSetBlendConstants(commandBuffer, blendConstants);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetBlendConstants(pHeader);
    pPacket->commandBuffer = commandBuffer;
    memcpy((void *) pPacket->blendConstants, blendConstants, sizeof(pPacket->blendConstants));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetDepthBounds(
    VkCommandBuffer commandBuffer,
    float minDepthBounds,
    float maxDepthBounds) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetDepthBounds* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetDepthBounds, 0);
    mdd(commandBuffer)->devTable.CmdSetDepthBounds(commandBuffer, minDepthBounds, maxDepthBounds);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetDepthBounds(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->minDepthBounds = minDepthBounds;
    pPacket->maxDepthBounds = maxDepthBounds;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetStencilCompareMask(
    VkCommandBuffer commandBuffer,
    VkStencilFaceFlags faceMask,
    uint32_t compareMask) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetStencilCompareMask* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetStencilCompareMask, 0);
    mdd(commandBuffer)->devTable.CmdSetStencilCompareMask(commandBuffer, faceMask, compareMask);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetStencilCompareMask(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->faceMask = faceMask;
    pPacket->compareMask = compareMask;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetStencilWriteMask(
    VkCommandBuffer commandBuffer,
    VkStencilFaceFlags faceMask,
    uint32_t writeMask) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetStencilWriteMask* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetStencilWriteMask, 0);
    mdd(commandBuffer)->devTable.CmdSetStencilWriteMask(commandBuffer, faceMask, writeMask);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetStencilWriteMask(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->faceMask = faceMask;
    pPacket->writeMask = writeMask;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetStencilReference(
    VkCommandBuffer commandBuffer,
    VkStencilFaceFlags faceMask,
    uint32_t reference) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetStencilReference* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetStencilReference, 0);
    mdd(commandBuffer)->devTable.CmdSetStencilReference(commandBuffer, faceMask, reference);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetStencilReference(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->faceMask = faceMask;
    pPacket->reference = reference;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdBindDescriptorSets(
    VkCommandBuffer commandBuffer,
    VkPipelineBindPoint pipelineBindPoint,
    VkPipelineLayout layout,
    uint32_t firstSet,
    uint32_t descriptorSetCount,
    const VkDescriptorSet* pDescriptorSets,
    uint32_t dynamicOffsetCount,
    const uint32_t* pDynamicOffsets) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdBindDescriptorSets* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdBindDescriptorSets, descriptorSetCount * sizeof(VkDescriptorSet) + dynamicOffsetCount * sizeof(uint32_t));
    mdd(commandBuffer)->devTable.CmdBindDescriptorSets(commandBuffer, pipelineBindPoint, layout, firstSet, descriptorSetCount, pDescriptorSets, dynamicOffsetCount, pDynamicOffsets);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdBindDescriptorSets(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->pipelineBindPoint = pipelineBindPoint;
    pPacket->layout = layout;
    pPacket->firstSet = firstSet;
    pPacket->descriptorSetCount = descriptorSetCount;
    pPacket->dynamicOffsetCount = dynamicOffsetCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDescriptorSets), descriptorSetCount * sizeof(VkDescriptorSet), pDescriptorSets);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDynamicOffsets), dynamicOffsetCount * sizeof(uint32_t), pDynamicOffsets);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDescriptorSets));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDynamicOffsets));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            for (uint32_t i = 0; i < descriptorSetCount; i++) {
                trim::mark_DescriptorSet_reference(pDescriptorSets[i]);
            }
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdBindIndexBuffer(
    VkCommandBuffer commandBuffer,
    VkBuffer buffer,
    VkDeviceSize offset,
    VkIndexType indexType) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdBindIndexBuffer* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdBindIndexBuffer, 0);
    mdd(commandBuffer)->devTable.CmdBindIndexBuffer(commandBuffer, buffer, offset, indexType);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdBindIndexBuffer(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->buffer = buffer;
    pPacket->offset = offset;
    pPacket->indexType = indexType;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(buffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdBindVertexBuffers(
    VkCommandBuffer commandBuffer,
    uint32_t firstBinding,
    uint32_t bindingCount,
    const VkBuffer* pBuffers,
    const VkDeviceSize* pOffsets) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdBindVertexBuffers* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdBindVertexBuffers, bindingCount * sizeof(VkBuffer) + bindingCount * sizeof(VkDeviceSize));
    mdd(commandBuffer)->devTable.CmdBindVertexBuffers(commandBuffer, firstBinding, bindingCount, pBuffers, pOffsets);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdBindVertexBuffers(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->firstBinding = firstBinding;
    pPacket->bindingCount = bindingCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pBuffers), bindingCount * sizeof(VkBuffer), pBuffers);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pOffsets), bindingCount * sizeof(VkDeviceSize), pOffsets);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pBuffers));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pOffsets));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            for (uint32_t i = 0; i < bindingCount; i++) {
                trim::mark_Buffer_reference(pBuffers[i]);
            }
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDraw(
    VkCommandBuffer commandBuffer,
    uint32_t vertexCount,
    uint32_t instanceCount,
    uint32_t firstVertex,
    uint32_t firstInstance) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDraw* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDraw, 0);
    mdd(commandBuffer)->devTable.CmdDraw(commandBuffer, vertexCount, instanceCount, firstVertex, firstInstance);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDraw(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->vertexCount = vertexCount;
    pPacket->instanceCount = instanceCount;
    pPacket->firstVertex = firstVertex;
    pPacket->firstInstance = firstInstance;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDrawIndexed(
    VkCommandBuffer commandBuffer,
    uint32_t indexCount,
    uint32_t instanceCount,
    uint32_t firstIndex,
    int32_t vertexOffset,
    uint32_t firstInstance) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDrawIndexed* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDrawIndexed, 0);
    mdd(commandBuffer)->devTable.CmdDrawIndexed(commandBuffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDrawIndexed(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->indexCount = indexCount;
    pPacket->instanceCount = instanceCount;
    pPacket->firstIndex = firstIndex;
    pPacket->vertexOffset = vertexOffset;
    pPacket->firstInstance = firstInstance;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDrawIndirect(
    VkCommandBuffer commandBuffer,
    VkBuffer buffer,
    VkDeviceSize offset,
    uint32_t drawCount,
    uint32_t stride) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDrawIndirect* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDrawIndirect, 0);
    mdd(commandBuffer)->devTable.CmdDrawIndirect(commandBuffer, buffer, offset, drawCount, stride);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDrawIndirect(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->buffer = buffer;
    pPacket->offset = offset;
    pPacket->drawCount = drawCount;
    pPacket->stride = stride;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(buffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDrawIndexedIndirect(
    VkCommandBuffer commandBuffer,
    VkBuffer buffer,
    VkDeviceSize offset,
    uint32_t drawCount,
    uint32_t stride) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDrawIndexedIndirect* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDrawIndexedIndirect, 0);
    mdd(commandBuffer)->devTable.CmdDrawIndexedIndirect(commandBuffer, buffer, offset, drawCount, stride);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDrawIndexedIndirect(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->buffer = buffer;
    pPacket->offset = offset;
    pPacket->drawCount = drawCount;
    pPacket->stride = stride;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(buffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDispatch(
    VkCommandBuffer commandBuffer,
    uint32_t groupCountX,
    uint32_t groupCountY,
    uint32_t groupCountZ) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDispatch* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDispatch, 0);
    mdd(commandBuffer)->devTable.CmdDispatch(commandBuffer, groupCountX, groupCountY, groupCountZ);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDispatch(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->groupCountX = groupCountX;
    pPacket->groupCountY = groupCountY;
    pPacket->groupCountZ = groupCountZ;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDispatchIndirect(
    VkCommandBuffer commandBuffer,
    VkBuffer buffer,
    VkDeviceSize offset) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDispatchIndirect* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDispatchIndirect, 0);
    mdd(commandBuffer)->devTable.CmdDispatchIndirect(commandBuffer, buffer, offset);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDispatchIndirect(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->buffer = buffer;
    pPacket->offset = offset;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(buffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdCopyBuffer(
    VkCommandBuffer commandBuffer,
    VkBuffer srcBuffer,
    VkBuffer dstBuffer,
    uint32_t regionCount,
    const VkBufferCopy* pRegions) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdCopyBuffer* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdCopyBuffer, regionCount * sizeof(VkBufferCopy));
    mdd(commandBuffer)->devTable.CmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, regionCount, pRegions);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdCopyBuffer(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->srcBuffer = srcBuffer;
    pPacket->dstBuffer = dstBuffer;
    pPacket->regionCount = regionCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRegions), regionCount * sizeof(VkBufferCopy), pRegions);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRegions));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(srcBuffer);
            trim::mark_Buffer_reference(dstBuffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdCopyImage(
    VkCommandBuffer commandBuffer,
    VkImage srcImage,
    VkImageLayout srcImageLayout,
    VkImage dstImage,
    VkImageLayout dstImageLayout,
    uint32_t regionCount,
    const VkImageCopy* pRegions) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdCopyImage* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdCopyImage, regionCount * sizeof(VkImageCopy));
    mdd(commandBuffer)->devTable.CmdCopyImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdCopyImage(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->srcImage = srcImage;
    pPacket->srcImageLayout = srcImageLayout;
    pPacket->dstImage = dstImage;
    pPacket->dstImageLayout = dstImageLayout;
    pPacket->regionCount = regionCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRegions), regionCount * sizeof(VkImageCopy), pRegions);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRegions));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_Image_objectInfo(dstImage);
        if (pInfo != NULL) {
            pInfo->ObjectInfo.Image.mostRecentLayout = dstImageLayout;
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(srcImage);
            trim::mark_Image_reference(dstImage);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdBlitImage(
    VkCommandBuffer commandBuffer,
    VkImage srcImage,
    VkImageLayout srcImageLayout,
    VkImage dstImage,
    VkImageLayout dstImageLayout,
    uint32_t regionCount,
    const VkImageBlit* pRegions,
    VkFilter filter) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdBlitImage* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdBlitImage, regionCount * sizeof(VkImageBlit));
    mdd(commandBuffer)->devTable.CmdBlitImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions, filter);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdBlitImage(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->srcImage = srcImage;
    pPacket->srcImageLayout = srcImageLayout;
    pPacket->dstImage = dstImage;
    pPacket->dstImageLayout = dstImageLayout;
    pPacket->regionCount = regionCount;
    pPacket->filter = filter;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRegions), regionCount * sizeof(VkImageBlit), pRegions);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRegions));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_Image_objectInfo(dstImage);
        if (pInfo != NULL) {
            pInfo->ObjectInfo.Image.mostRecentLayout = dstImageLayout;
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(srcImage);
            trim::mark_Image_reference(dstImage);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdCopyBufferToImage(
    VkCommandBuffer commandBuffer,
    VkBuffer srcBuffer,
    VkImage dstImage,
    VkImageLayout dstImageLayout,
    uint32_t regionCount,
    const VkBufferImageCopy* pRegions) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdCopyBufferToImage* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdCopyBufferToImage, regionCount * sizeof(VkBufferImageCopy));
    mdd(commandBuffer)->devTable.CmdCopyBufferToImage(commandBuffer, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdCopyBufferToImage(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->srcBuffer = srcBuffer;
    pPacket->dstImage = dstImage;
    pPacket->dstImageLayout = dstImageLayout;
    pPacket->regionCount = regionCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRegions), regionCount * sizeof(VkBufferImageCopy), pRegions);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRegions));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_Image_objectInfo(dstImage);
        if (pInfo != NULL) {
            pInfo->ObjectInfo.Image.mostRecentLayout = dstImageLayout;
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(srcBuffer);
            trim::mark_Image_reference(dstImage);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkCmdCopyImageToBuffer is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdCopyImageToBuffer(
    VkCommandBuffer commandBuffer,
    VkImage srcImage,
    VkImageLayout srcImageLayout,
    VkBuffer dstBuffer,
    uint32_t regionCount,
    const VkBufferImageCopy* pRegions);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdUpdateBuffer(
    VkCommandBuffer commandBuffer,
    VkBuffer dstBuffer,
    VkDeviceSize dstOffset,
    VkDeviceSize dataSize,
    const void* pData) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdUpdateBuffer* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdUpdateBuffer, dataSize);
    mdd(commandBuffer)->devTable.CmdUpdateBuffer(commandBuffer, dstBuffer, dstOffset, dataSize, pData);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdUpdateBuffer(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->dstBuffer = dstBuffer;
    pPacket->dstOffset = dstOffset;
    pPacket->dataSize = dataSize;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pData), dataSize, pData);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pData));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(dstBuffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdFillBuffer(
    VkCommandBuffer commandBuffer,
    VkBuffer dstBuffer,
    VkDeviceSize dstOffset,
    VkDeviceSize size,
    uint32_t data) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdFillBuffer* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdFillBuffer, 0);
    mdd(commandBuffer)->devTable.CmdFillBuffer(commandBuffer, dstBuffer, dstOffset, size, data);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdFillBuffer(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->dstBuffer = dstBuffer;
    pPacket->dstOffset = dstOffset;
    pPacket->size = size;
    pPacket->data = data;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(dstBuffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdClearColorImage(
    VkCommandBuffer commandBuffer,
    VkImage image,
    VkImageLayout imageLayout,
    const VkClearColorValue* pColor,
    uint32_t rangeCount,
    const VkImageSubresourceRange* pRanges) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdClearColorImage* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdClearColorImage, sizeof(VkClearColorValue) + rangeCount * sizeof(VkImageSubresourceRange));
    mdd(commandBuffer)->devTable.CmdClearColorImage(commandBuffer, image, imageLayout, pColor, rangeCount, pRanges);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdClearColorImage(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->image = image;
    pPacket->imageLayout = imageLayout;
    pPacket->rangeCount = rangeCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pColor), sizeof(VkClearColorValue), pColor);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRanges), rangeCount * sizeof(VkImageSubresourceRange), pRanges);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pColor));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRanges));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_Image_objectInfo(image);
        if (pInfo != NULL) {
            pInfo->ObjectInfo.Image.mostRecentLayout = imageLayout;
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdClearDepthStencilImage(
    VkCommandBuffer commandBuffer,
    VkImage image,
    VkImageLayout imageLayout,
    const VkClearDepthStencilValue* pDepthStencil,
    uint32_t rangeCount,
    const VkImageSubresourceRange* pRanges) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdClearDepthStencilImage* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdClearDepthStencilImage, sizeof(VkClearDepthStencilValue) + rangeCount * sizeof(VkImageSubresourceRange));
    mdd(commandBuffer)->devTable.CmdClearDepthStencilImage(commandBuffer, image, imageLayout, pDepthStencil, rangeCount, pRanges);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdClearDepthStencilImage(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->image = image;
    pPacket->imageLayout = imageLayout;
    pPacket->rangeCount = rangeCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDepthStencil), sizeof(VkClearDepthStencilValue), pDepthStencil);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRanges), rangeCount * sizeof(VkImageSubresourceRange), pRanges);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDepthStencil));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRanges));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_Image_objectInfo(image);
        if (pInfo != NULL) {
            pInfo->ObjectInfo.Image.mostRecentLayout = imageLayout;
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdClearAttachments(
    VkCommandBuffer commandBuffer,
    uint32_t attachmentCount,
    const VkClearAttachment* pAttachments,
    uint32_t rectCount,
    const VkClearRect* pRects) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdClearAttachments* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdClearAttachments, attachmentCount * sizeof(VkClearAttachment) + rectCount * sizeof(VkClearRect));
    mdd(commandBuffer)->devTable.CmdClearAttachments(commandBuffer, attachmentCount, pAttachments, rectCount, pRects);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdClearAttachments(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->attachmentCount = attachmentCount;
    pPacket->rectCount = rectCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAttachments), attachmentCount * sizeof(VkClearAttachment), pAttachments);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRects), rectCount * sizeof(VkClearRect), pRects);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAttachments));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRects));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdResolveImage(
    VkCommandBuffer commandBuffer,
    VkImage srcImage,
    VkImageLayout srcImageLayout,
    VkImage dstImage,
    VkImageLayout dstImageLayout,
    uint32_t regionCount,
    const VkImageResolve* pRegions) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdResolveImage* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdResolveImage, regionCount * sizeof(VkImageResolve));
    mdd(commandBuffer)->devTable.CmdResolveImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdResolveImage(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->srcImage = srcImage;
    pPacket->srcImageLayout = srcImageLayout;
    pPacket->dstImage = dstImage;
    pPacket->dstImageLayout = dstImageLayout;
    pPacket->regionCount = regionCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRegions), regionCount * sizeof(VkImageResolve), pRegions);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRegions));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_Image_objectInfo(dstImage);
        if (pInfo != NULL) {
            pInfo->ObjectInfo.Image.mostRecentLayout = dstImageLayout;
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(srcImage);
            trim::mark_Image_reference(dstImage);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetEvent(
    VkCommandBuffer commandBuffer,
    VkEvent event,
    VkPipelineStageFlags stageMask) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetEvent* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetEvent, 0);
    mdd(commandBuffer)->devTable.CmdSetEvent(commandBuffer, event, stageMask);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetEvent(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->event = event;
    pPacket->stageMask = stageMask;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdResetEvent(
    VkCommandBuffer commandBuffer,
    VkEvent event,
    VkPipelineStageFlags stageMask) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdResetEvent* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdResetEvent, 0);
    mdd(commandBuffer)->devTable.CmdResetEvent(commandBuffer, event, stageMask);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdResetEvent(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->event = event;
    pPacket->stageMask = stageMask;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkCmdWaitEvents is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdWaitEvents(
    VkCommandBuffer commandBuffer,
    uint32_t eventCount,
    const VkEvent* pEvents,
    VkPipelineStageFlags srcStageMask,
    VkPipelineStageFlags dstStageMask,
    uint32_t memoryBarrierCount,
    const VkMemoryBarrier* pMemoryBarriers,
    uint32_t bufferMemoryBarrierCount,
    const VkBufferMemoryBarrier* pBufferMemoryBarriers,
    uint32_t imageMemoryBarrierCount,
    const VkImageMemoryBarrier* pImageMemoryBarriers);
// __HOOKED_vkCmdPipelineBarrier is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdPipelineBarrier(
    VkCommandBuffer commandBuffer,
    VkPipelineStageFlags srcStageMask,
    VkPipelineStageFlags dstStageMask,
    VkDependencyFlags dependencyFlags,
    uint32_t memoryBarrierCount,
    const VkMemoryBarrier* pMemoryBarriers,
    uint32_t bufferMemoryBarrierCount,
    const VkBufferMemoryBarrier* pBufferMemoryBarriers,
    uint32_t imageMemoryBarrierCount,
    const VkImageMemoryBarrier* pImageMemoryBarriers);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdBeginQuery(
    VkCommandBuffer commandBuffer,
    VkQueryPool queryPool,
    uint32_t query,
    VkQueryControlFlags flags) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdBeginQuery* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdBeginQuery, 0);
    mdd(commandBuffer)->devTable.CmdBeginQuery(commandBuffer, queryPool, query, flags);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdBeginQuery(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->queryPool = queryPool;
    pPacket->query = query;
    pPacket->flags = flags;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_QueryPool_reference(queryPool);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdEndQuery(
    VkCommandBuffer commandBuffer,
    VkQueryPool queryPool,
    uint32_t query) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdEndQuery* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdEndQuery, 0);
    mdd(commandBuffer)->devTable.CmdEndQuery(commandBuffer, queryPool, query);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdEndQuery(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->queryPool = queryPool;
    pPacket->query = query;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_QueryPool_objectInfo(queryPool);
        if (pInfo != NULL) {
            pInfo->ObjectInfo.QueryPool.commandBuffer = commandBuffer;
            pInfo->ObjectInfo.QueryPool.pResultsAvailable[query] = true;
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_QueryPool_reference(queryPool);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdResetQueryPool(
    VkCommandBuffer commandBuffer,
    VkQueryPool queryPool,
    uint32_t firstQuery,
    uint32_t queryCount) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdResetQueryPool* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdResetQueryPool, 0);
    mdd(commandBuffer)->devTable.CmdResetQueryPool(commandBuffer, queryPool, firstQuery, queryCount);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdResetQueryPool(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->queryPool = queryPool;
    pPacket->firstQuery = firstQuery;
    pPacket->queryCount = queryCount;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_QueryPool_objectInfo(queryPool);
        if (pInfo != NULL) {
            for (uint32_t i = firstQuery; (i < pInfo->ObjectInfo.QueryPool.size) && (i < firstQuery + queryCount); i++) {
                pInfo->ObjectInfo.QueryPool.pResultsAvailable[i] = false;
            }
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_QueryPool_reference(queryPool);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdWriteTimestamp(
    VkCommandBuffer commandBuffer,
    VkPipelineStageFlagBits pipelineStage,
    VkQueryPool queryPool,
    uint32_t query) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdWriteTimestamp* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdWriteTimestamp, 0);
    mdd(commandBuffer)->devTable.CmdWriteTimestamp(commandBuffer, pipelineStage, queryPool, query);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdWriteTimestamp(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->pipelineStage = pipelineStage;
    pPacket->queryPool = queryPool;
    pPacket->query = query;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pInfo = trim::get_QueryPool_objectInfo(queryPool);
        if (pInfo != NULL) {
            pInfo->ObjectInfo.QueryPool.commandBuffer = commandBuffer;
            pInfo->ObjectInfo.QueryPool.pResultsAvailable[query] = true;
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::mark_QueryPool_reference(queryPool);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdCopyQueryPoolResults(
    VkCommandBuffer commandBuffer,
    VkQueryPool queryPool,
    uint32_t firstQuery,
    uint32_t queryCount,
    VkBuffer dstBuffer,
    VkDeviceSize dstOffset,
    VkDeviceSize stride,
    VkQueryResultFlags flags) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdCopyQueryPoolResults* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdCopyQueryPoolResults, 0);
    mdd(commandBuffer)->devTable.CmdCopyQueryPoolResults(commandBuffer, queryPool, firstQuery, queryCount, dstBuffer, dstOffset, stride, flags);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdCopyQueryPoolResults(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->queryPool = queryPool;
    pPacket->firstQuery = firstQuery;
    pPacket->queryCount = queryCount;
    pPacket->dstBuffer = dstBuffer;
    pPacket->dstOffset = dstOffset;
    pPacket->stride = stride;
    pPacket->flags = flags;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::mark_QueryPool_reference(queryPool);
            trim::mark_Buffer_reference(dstBuffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkCmdPushConstants is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdPushConstants(
    VkCommandBuffer commandBuffer,
    VkPipelineLayout layout,
    VkShaderStageFlags stageFlags,
    uint32_t offset,
    uint32_t size,
    const void* pValues);
// __HOOKED_vkCmdBeginRenderPass is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdBeginRenderPass(
    VkCommandBuffer commandBuffer,
    const VkRenderPassBeginInfo* pRenderPassBegin,
    VkSubpassContents contents);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdNextSubpass(
    VkCommandBuffer commandBuffer,
    VkSubpassContents contents) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdNextSubpass* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdNextSubpass, 0);
    mdd(commandBuffer)->devTable.CmdNextSubpass(commandBuffer, contents);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdNextSubpass(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->contents = contents;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdEndRenderPass(
    VkCommandBuffer commandBuffer) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdEndRenderPass* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdEndRenderPass, 0);
    mdd(commandBuffer)->devTable.CmdEndRenderPass(commandBuffer);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdEndRenderPass(pHeader);
    pPacket->commandBuffer = commandBuffer;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pCommandBuffer = trim::get_CommandBuffer_objectInfo(commandBuffer);
        if (pCommandBuffer != nullptr) {
            trim::ObjectInfo* pRenderPass = trim::get_RenderPass_objectInfo(pCommandBuffer->ObjectInfo.CommandBuffer.activeRenderPass);
            if (pRenderPass != nullptr) {
                for (uint32_t i = 0; i < pRenderPass->ObjectInfo.RenderPass.attachmentCount; i++) {
                    trim::AddImageTransition(commandBuffer, pRenderPass->ObjectInfo.RenderPass.pAttachments[i]);
                }
            }
        }
        trim::add_CommandBuffer_call(commandBuffer, trim::copy_packet(pHeader));
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkCmdExecuteCommands is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdExecuteCommands(
    VkCommandBuffer commandBuffer,
    uint32_t commandBufferCount,
    const VkCommandBuffer* pCommandBuffers);
// TODO: Add support for __HOOKED_vkEnumerateInstanceVersion: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkBindBufferMemory2(
    VkDevice device,
    uint32_t bindInfoCount,
    const VkBindBufferMemoryInfo* pBindInfos) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkBindBufferMemory2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkBindBufferMemory2, bindInfoCount * sizeof(VkBindBufferMemoryInfo));
    result = mdd(device)->devTable.BindBufferMemory2(device, bindInfoCount, pBindInfos);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkBindBufferMemory2(pHeader);
    pPacket->device = device;
    pPacket->bindInfoCount = bindInfoCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pBindInfos), bindInfoCount * sizeof(VkBindBufferMemoryInfo), pBindInfos);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pBindInfos));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            for (uint32_t i = 0; i < bindInfoCount; i++) {
                trim::mark_Buffer_reference(pBindInfos[i].buffer);
            }
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkBindImageMemory2(
    VkDevice device,
    uint32_t bindInfoCount,
    const VkBindImageMemoryInfo* pBindInfos) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkBindImageMemory2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkBindImageMemory2, bindInfoCount * sizeof(VkBindImageMemoryInfo));
    result = mdd(device)->devTable.BindImageMemory2(device, bindInfoCount, pBindInfos);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkBindImageMemory2(pHeader);
    pPacket->device = device;
    pPacket->bindInfoCount = bindInfoCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pBindInfos), bindInfoCount * sizeof(VkBindImageMemoryInfo), pBindInfos);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pBindInfos));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            for (uint32_t i = 0; i < bindInfoCount; i++) {
                trim::mark_Image_reference(pBindInfos[i].image);
            }
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetDeviceGroupPeerMemoryFeatures(
    VkDevice device,
    uint32_t heapIndex,
    uint32_t localDeviceIndex,
    uint32_t remoteDeviceIndex,
    VkPeerMemoryFeatureFlags* pPeerMemoryFeatures) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDeviceGroupPeerMemoryFeatures* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDeviceGroupPeerMemoryFeatures, sizeof(VkPeerMemoryFeatureFlags));
    mdd(device)->devTable.GetDeviceGroupPeerMemoryFeatures(device, heapIndex, localDeviceIndex, remoteDeviceIndex, pPeerMemoryFeatures);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDeviceGroupPeerMemoryFeatures(pHeader);
    pPacket->device = device;
    pPacket->heapIndex = heapIndex;
    pPacket->localDeviceIndex = localDeviceIndex;
    pPacket->remoteDeviceIndex = remoteDeviceIndex;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPeerMemoryFeatures), sizeof(VkPeerMemoryFeatureFlags), pPeerMemoryFeatures);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPeerMemoryFeatures));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetDeviceMask(
    VkCommandBuffer commandBuffer,
    uint32_t deviceMask) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetDeviceMask* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetDeviceMask, 0);
    mdd(commandBuffer)->devTable.CmdSetDeviceMask(commandBuffer, deviceMask);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetDeviceMask(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->deviceMask = deviceMask;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDispatchBase(
    VkCommandBuffer commandBuffer,
    uint32_t baseGroupX,
    uint32_t baseGroupY,
    uint32_t baseGroupZ,
    uint32_t groupCountX,
    uint32_t groupCountY,
    uint32_t groupCountZ) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDispatchBase* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDispatchBase, 0);
    mdd(commandBuffer)->devTable.CmdDispatchBase(commandBuffer, baseGroupX, baseGroupY, baseGroupZ, groupCountX, groupCountY, groupCountZ);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDispatchBase(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->baseGroupX = baseGroupX;
    pPacket->baseGroupY = baseGroupY;
    pPacket->baseGroupZ = baseGroupZ;
    pPacket->groupCountX = groupCountX;
    pPacket->groupCountY = groupCountY;
    pPacket->groupCountZ = groupCountZ;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkEnumeratePhysicalDeviceGroups(
    VkInstance instance,
    uint32_t* pPhysicalDeviceGroupCount,
    VkPhysicalDeviceGroupProperties* pPhysicalDeviceGroupProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkEnumeratePhysicalDeviceGroups* pPacket = NULL;
    CREATE_TRACE_PACKET(vkEnumeratePhysicalDeviceGroups, sizeof(uint32_t) + get_struct_chain_size((void*)pPhysicalDeviceGroupProperties));
    result = mid(instance)->instTable.EnumeratePhysicalDeviceGroups(instance, pPhysicalDeviceGroupCount, pPhysicalDeviceGroupProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkEnumeratePhysicalDeviceGroups(pHeader);
    pPacket->instance = instance;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPhysicalDeviceGroupCount), sizeof(uint32_t), pPhysicalDeviceGroupCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPhysicalDeviceGroupProperties), (*pPhysicalDeviceGroupCount) * sizeof(VkPhysicalDeviceGroupProperties), pPhysicalDeviceGroupProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPhysicalDeviceGroupCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPhysicalDeviceGroupProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetImageMemoryRequirements2(
    VkDevice device,
    const VkImageMemoryRequirementsInfo2* pInfo,
    VkMemoryRequirements2* pMemoryRequirements) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetImageMemoryRequirements2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetImageMemoryRequirements2, get_struct_chain_size((void*)pInfo) + get_struct_chain_size((void*)pMemoryRequirements));
    mdd(device)->devTable.GetImageMemoryRequirements2(device, pInfo, pMemoryRequirements);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetImageMemoryRequirements2(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pInfo), sizeof(VkImageMemoryRequirementsInfo2), pInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pInfo, (void *)pInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryRequirements), sizeof(VkMemoryRequirements2), pMemoryRequirements);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pMemoryRequirements, (void *)pMemoryRequirements);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryRequirements));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pTrimObjectInfo = trim::get_Image_objectInfo(pInfo->image);
        if (pTrimObjectInfo != NULL) {
            pTrimObjectInfo->ObjectInfo.Image.memorySize = pMemoryRequirements->memoryRequirements.size;
        }
#if TRIM_USE_ORDERED_IMAGE_CREATION
        trim::add_Image_call(trim::copy_packet(pHeader));
#else
        if (pTrimObjectInfo != NULL) {
            pTrimObjectInfo->ObjectInfo.Image.pGetImageMemoryRequirementsPacket = trim::copy_packet(pHeader);
        }
#endif //TRIM_USE_ORDERED_IMAGE_CREATION
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(pInfo->image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetBufferMemoryRequirements2(
    VkDevice device,
    const VkBufferMemoryRequirementsInfo2* pInfo,
    VkMemoryRequirements2* pMemoryRequirements) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetBufferMemoryRequirements2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetBufferMemoryRequirements2, get_struct_chain_size((void*)pInfo) + get_struct_chain_size((void*)pMemoryRequirements));
    mdd(device)->devTable.GetBufferMemoryRequirements2(device, pInfo, pMemoryRequirements);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetBufferMemoryRequirements2(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pInfo), sizeof(VkBufferMemoryRequirementsInfo2), pInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pInfo, (void *)pInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryRequirements), sizeof(VkMemoryRequirements2), pMemoryRequirements);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pMemoryRequirements, (void *)pMemoryRequirements);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryRequirements));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(pInfo->buffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetImageSparseMemoryRequirements2(
    VkDevice device,
    const VkImageSparseMemoryRequirementsInfo2* pInfo,
    uint32_t* pSparseMemoryRequirementCount,
    VkSparseImageMemoryRequirements2* pSparseMemoryRequirements) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetImageSparseMemoryRequirements2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetImageSparseMemoryRequirements2, get_struct_chain_size((void*)pInfo) + sizeof(uint32_t) + get_struct_chain_size((void*)pSparseMemoryRequirements));
    mdd(device)->devTable.GetImageSparseMemoryRequirements2(device, pInfo, pSparseMemoryRequirementCount, pSparseMemoryRequirements);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetImageSparseMemoryRequirements2(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pInfo), sizeof(VkImageSparseMemoryRequirementsInfo2), pInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pInfo, (void *)pInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSparseMemoryRequirementCount), sizeof(uint32_t), pSparseMemoryRequirementCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSparseMemoryRequirements), (*pSparseMemoryRequirementCount) * sizeof(VkSparseImageMemoryRequirements2), pSparseMemoryRequirements);
    for (uint32_t i=0; i< *pPacket->pSparseMemoryRequirementCount; i++)
        vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)(&pPacket->pSparseMemoryRequirements[i]), (void *)(&pSparseMemoryRequirements[i]));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSparseMemoryRequirementCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSparseMemoryRequirements));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(pInfo->image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceFeatures2(
    VkPhysicalDevice physicalDevice,
    VkPhysicalDeviceFeatures2* pFeatures) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceFeatures2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceFeatures2, get_struct_chain_size((void*)pFeatures));
    mid(physicalDevice)->instTable.GetPhysicalDeviceFeatures2(physicalDevice, pFeatures);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceFeatures2(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFeatures), sizeof(VkPhysicalDeviceFeatures2), pFeatures);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pFeatures, (void *)pFeatures);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFeatures));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceProperties2(
    VkPhysicalDevice physicalDevice,
    VkPhysicalDeviceProperties2* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceProperties2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceProperties2, get_struct_chain_size((void*)pProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceProperties2(physicalDevice, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceProperties2(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), sizeof(VkPhysicalDeviceProperties2), pProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pProperties, (void *)pProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceFormatProperties2(
    VkPhysicalDevice physicalDevice,
    VkFormat format,
    VkFormatProperties2* pFormatProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceFormatProperties2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceFormatProperties2, get_struct_chain_size((void*)pFormatProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceFormatProperties2(physicalDevice, format, pFormatProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceFormatProperties2(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->format = format;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFormatProperties), sizeof(VkFormatProperties2), pFormatProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pFormatProperties, (void *)pFormatProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFormatProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceImageFormatProperties2(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceImageFormatInfo2* pImageFormatInfo,
    VkImageFormatProperties2* pImageFormatProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceImageFormatProperties2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceImageFormatProperties2, get_struct_chain_size((void*)pImageFormatInfo) + get_struct_chain_size((void*)pImageFormatProperties));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceImageFormatProperties2(physicalDevice, pImageFormatInfo, pImageFormatProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceImageFormatProperties2(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImageFormatInfo), sizeof(VkPhysicalDeviceImageFormatInfo2), pImageFormatInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pImageFormatInfo, (void *)pImageFormatInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImageFormatProperties), sizeof(VkImageFormatProperties2), pImageFormatProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pImageFormatProperties, (void *)pImageFormatProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImageFormatInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImageFormatProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceQueueFamilyProperties2(
    VkPhysicalDevice physicalDevice,
    uint32_t* pQueueFamilyPropertyCount,
    VkQueueFamilyProperties2* pQueueFamilyProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceQueueFamilyProperties2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceQueueFamilyProperties2, sizeof(uint32_t) + get_struct_chain_size((void*)pQueueFamilyProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceQueueFamilyProperties2(physicalDevice, pQueueFamilyPropertyCount, pQueueFamilyProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceQueueFamilyProperties2(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pQueueFamilyPropertyCount), sizeof(uint32_t), pQueueFamilyPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pQueueFamilyProperties), (*pQueueFamilyPropertyCount) * sizeof(VkQueueFamilyProperties2), pQueueFamilyProperties);
    for (uint32_t i=0; i< *pPacket->pQueueFamilyPropertyCount; i++)
        vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)(&pPacket->pQueueFamilyProperties[i]), (void *)(&pQueueFamilyProperties[i]));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pQueueFamilyPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pQueueFamilyProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceMemoryProperties2(
    VkPhysicalDevice physicalDevice,
    VkPhysicalDeviceMemoryProperties2* pMemoryProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceMemoryProperties2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceMemoryProperties2, get_struct_chain_size((void*)pMemoryProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceMemoryProperties2(physicalDevice, pMemoryProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceMemoryProperties2(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryProperties), sizeof(VkPhysicalDeviceMemoryProperties2), pMemoryProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pMemoryProperties, (void *)pMemoryProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSparseImageFormatProperties2(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceSparseImageFormatInfo2* pFormatInfo,
    uint32_t* pPropertyCount,
    VkSparseImageFormatProperties2* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceSparseImageFormatProperties2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceSparseImageFormatProperties2, get_struct_chain_size((void*)pFormatInfo) + sizeof(uint32_t) + get_struct_chain_size((void*)pProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceSparseImageFormatProperties2(physicalDevice, pFormatInfo, pPropertyCount, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceSparseImageFormatProperties2(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFormatInfo), sizeof(VkPhysicalDeviceSparseImageFormatInfo2), pFormatInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pFormatInfo, (void *)pFormatInfo);
    for (uint32_t i=0; i< *pPacket->pPropertyCount; i++)
        vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)(&pPacket->pProperties[i]), (void *)(&pProperties[i]));
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPropertyCount), sizeof(uint32_t), pPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), (*pPropertyCount) * sizeof(VkSparseImageFormatProperties2), pProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pProperties, (void *)pProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFormatInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkTrimCommandPool(
    VkDevice device,
    VkCommandPool commandPool,
    VkCommandPoolTrimFlags flags) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkTrimCommandPool* pPacket = NULL;
    CREATE_TRACE_PACKET(vkTrimCommandPool, 0);
    mdd(device)->devTable.TrimCommandPool(device, commandPool, flags);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkTrimCommandPool(pHeader);
    pPacket->device = device;
    pPacket->commandPool = commandPool;
    pPacket->flags = flags;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetDeviceQueue2(
    VkDevice device,
    const VkDeviceQueueInfo2* pQueueInfo,
    VkQueue* pQueue) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDeviceQueue2* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDeviceQueue2, sizeof(VkDeviceQueueInfo2) + sizeof(VkQueue));
    mdd(device)->devTable.GetDeviceQueue2(device, pQueueInfo, pQueue);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDeviceQueue2(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pQueueInfo), sizeof(VkDeviceQueueInfo2), pQueueInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pQueueInfo, (void *)pQueueInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pQueue), sizeof(VkQueue), pQueue);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pQueueInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pQueue));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateSamplerYcbcrConversion(
    VkDevice device,
    const VkSamplerYcbcrConversionCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSamplerYcbcrConversion* pYcbcrConversion) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateSamplerYcbcrConversion* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateSamplerYcbcrConversion, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkSamplerYcbcrConversion));
    result = mdd(device)->devTable.CreateSamplerYcbcrConversion(device, pCreateInfo, pAllocator, pYcbcrConversion);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateSamplerYcbcrConversion(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkSamplerYcbcrConversionCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pYcbcrConversion), sizeof(VkSamplerYcbcrConversion), pYcbcrConversion);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pYcbcrConversion));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroySamplerYcbcrConversion(
    VkDevice device,
    VkSamplerYcbcrConversion ycbcrConversion,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroySamplerYcbcrConversion* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroySamplerYcbcrConversion, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroySamplerYcbcrConversion(device, ycbcrConversion, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroySamplerYcbcrConversion(pHeader);
    pPacket->device = device;
    pPacket->ycbcrConversion = ycbcrConversion;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// __HOOKED_vkCreateDescriptorUpdateTemplate is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateDescriptorUpdateTemplate(
    VkDevice device,
    const VkDescriptorUpdateTemplateCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDescriptorUpdateTemplate* pDescriptorUpdateTemplate);
// __HOOKED_vkDestroyDescriptorUpdateTemplate is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyDescriptorUpdateTemplate(
    VkDevice device,
    VkDescriptorUpdateTemplate descriptorUpdateTemplate,
    const VkAllocationCallbacks* pAllocator);
// __HOOKED_vkUpdateDescriptorSetWithTemplate is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkUpdateDescriptorSetWithTemplate(
    VkDevice device,
    VkDescriptorSet descriptorSet,
    VkDescriptorUpdateTemplate descriptorUpdateTemplate,
    const void* pData);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceExternalBufferProperties(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceExternalBufferInfo* pExternalBufferInfo,
    VkExternalBufferProperties* pExternalBufferProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceExternalBufferProperties* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceExternalBufferProperties, get_struct_chain_size((void*)pExternalBufferInfo) + get_struct_chain_size((void*)pExternalBufferProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceExternalBufferProperties(physicalDevice, pExternalBufferInfo, pExternalBufferProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceExternalBufferProperties(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalBufferInfo), sizeof(VkPhysicalDeviceExternalBufferInfo), pExternalBufferInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalBufferProperties), sizeof(VkExternalBufferProperties), pExternalBufferProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalBufferInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalBufferProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceExternalFenceProperties(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceExternalFenceInfo* pExternalFenceInfo,
    VkExternalFenceProperties* pExternalFenceProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceExternalFenceProperties* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceExternalFenceProperties, get_struct_chain_size((void*)pExternalFenceInfo) + get_struct_chain_size((void*)pExternalFenceProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceExternalFenceProperties(physicalDevice, pExternalFenceInfo, pExternalFenceProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceExternalFenceProperties(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalFenceInfo), sizeof(VkPhysicalDeviceExternalFenceInfo), pExternalFenceInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalFenceProperties), sizeof(VkExternalFenceProperties), pExternalFenceProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalFenceInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalFenceProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceExternalSemaphoreProperties(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceExternalSemaphoreInfo* pExternalSemaphoreInfo,
    VkExternalSemaphoreProperties* pExternalSemaphoreProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceExternalSemaphoreProperties* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceExternalSemaphoreProperties, get_struct_chain_size((void*)pExternalSemaphoreInfo) + get_struct_chain_size((void*)pExternalSemaphoreProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceExternalSemaphoreProperties(physicalDevice, pExternalSemaphoreInfo, pExternalSemaphoreProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceExternalSemaphoreProperties(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalSemaphoreInfo), sizeof(VkPhysicalDeviceExternalSemaphoreInfo), pExternalSemaphoreInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalSemaphoreProperties), sizeof(VkExternalSemaphoreProperties), pExternalSemaphoreProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalSemaphoreInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalSemaphoreProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetDescriptorSetLayoutSupport(
    VkDevice device,
    const VkDescriptorSetLayoutCreateInfo* pCreateInfo,
    VkDescriptorSetLayoutSupport* pSupport) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDescriptorSetLayoutSupport* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDescriptorSetLayoutSupport, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkDescriptorSetLayoutSupport));
    mdd(device)->devTable.GetDescriptorSetLayoutSupport(device, pCreateInfo, pSupport);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDescriptorSetLayoutSupport(pHeader);
    pPacket->device = device;
    add_create_ds_layout_to_trace_packet(pHeader, &pPacket->pCreateInfo, pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSupport), sizeof(VkDescriptorSetLayoutSupport), pSupport);
    // pCreateInfo finalized in add_create_ds_layout_to_trace_packet;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSupport));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroySurfaceKHR(
    VkInstance instance,
    VkSurfaceKHR surface,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroySurfaceKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroySurfaceKHR, sizeof(VkAllocationCallbacks));
    mid(instance)->instTable.DestroySurfaceKHR(instance, surface, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroySurfaceKHR(pHeader);
    pPacket->instance = instance;
    pPacket->surface = surface;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_SurfaceKHR_object(surface);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSurfaceSupportKHR(
    VkPhysicalDevice physicalDevice,
    uint32_t queueFamilyIndex,
    VkSurfaceKHR surface,
    VkBool32* pSupported) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceSurfaceSupportKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceSurfaceSupportKHR, sizeof(VkBool32));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamilyIndex, surface, pSupported);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceSurfaceSupportKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->queueFamilyIndex = queueFamilyIndex;
    pPacket->surface = surface;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSupported), sizeof(VkBool32), pSupported);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSupported));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// __HOOKED_vkGetPhysicalDeviceSurfaceCapabilitiesKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
    VkPhysicalDevice physicalDevice,
    VkSurfaceKHR surface,
    VkSurfaceCapabilitiesKHR* pSurfaceCapabilities);
// __HOOKED_vkGetPhysicalDeviceSurfaceFormatsKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSurfaceFormatsKHR(
    VkPhysicalDevice physicalDevice,
    VkSurfaceKHR surface,
    uint32_t* pSurfaceFormatCount,
    VkSurfaceFormatKHR* pSurfaceFormats);
// __HOOKED_vkGetPhysicalDeviceSurfacePresentModesKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSurfacePresentModesKHR(
    VkPhysicalDevice physicalDevice,
    VkSurfaceKHR surface,
    uint32_t* pPresentModeCount,
    VkPresentModeKHR* pPresentModes);
// __HOOKED_vkCreateSwapchainKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateSwapchainKHR(
    VkDevice device,
    const VkSwapchainCreateInfoKHR* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSwapchainKHR* pSwapchain);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroySwapchainKHR(
    VkDevice device,
    VkSwapchainKHR swapchain,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroySwapchainKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroySwapchainKHR, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroySwapchainKHR(device, swapchain, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroySwapchainKHR(pHeader);
    pPacket->device = device;
    pPacket->swapchain = swapchain;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::remove_SwapchainKHR_object(swapchain);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
// __HOOKED_vkGetSwapchainImagesKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetSwapchainImagesKHR(
    VkDevice device,
    VkSwapchainKHR swapchain,
    uint32_t* pSwapchainImageCount,
    VkImage* pSwapchainImages);
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkAcquireNextImageKHR(
    VkDevice device,
    VkSwapchainKHR swapchain,
    uint64_t timeout,
    VkSemaphore semaphore,
    VkFence fence,
    uint32_t* pImageIndex) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkAcquireNextImageKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkAcquireNextImageKHR, sizeof(uint32_t));
    result = mdd(device)->devTable.AcquireNextImageKHR(device, swapchain, timeout, semaphore, fence, pImageIndex);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkAcquireNextImageKHR(pHeader);
    pPacket->device = device;
    pPacket->swapchain = swapchain;
    pPacket->timeout = timeout;
    pPacket->semaphore = semaphore;
    pPacket->fence = fence;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImageIndex), sizeof(uint32_t), pImageIndex);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImageIndex));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR ) {
            if (semaphore != VK_NULL_HANDLE) {
                trim::ObjectInfo* pInfo = trim::get_Semaphore_objectInfo(semaphore);
                if (pInfo != NULL) {
                    // We need to signal the semaphore because other command may wait on it.
                    pInfo->ObjectInfo.Semaphore.signaledOnQueue = VK_NULL_HANDLE;
                    pInfo->ObjectInfo.Semaphore.signaledOnSwapChain = swapchain;
                }
            }
            if (fence != VK_NULL_HANDLE) {
                trim::ObjectInfo* pFenceInfo = trim::get_Fence_objectInfo(fence);
                if (pFenceInfo != NULL) {
                    pFenceInfo->ObjectInfo.Fence.signaled = true;
                }
            }
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
// __HOOKED_vkQueuePresentKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkQueuePresentKHR(
    VkQueue queue,
    const VkPresentInfoKHR* pPresentInfo);
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetDeviceGroupPresentCapabilitiesKHR(
    VkDevice device,
    VkDeviceGroupPresentCapabilitiesKHR* pDeviceGroupPresentCapabilities) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDeviceGroupPresentCapabilitiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDeviceGroupPresentCapabilitiesKHR, get_struct_chain_size((void*)pDeviceGroupPresentCapabilities));
    result = mdd(device)->devTable.GetDeviceGroupPresentCapabilitiesKHR(device, pDeviceGroupPresentCapabilities);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDeviceGroupPresentCapabilitiesKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDeviceGroupPresentCapabilities), sizeof(VkDeviceGroupPresentCapabilitiesKHR), pDeviceGroupPresentCapabilities);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDeviceGroupPresentCapabilities));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetDeviceGroupSurfacePresentModesKHR(
    VkDevice device,
    VkSurfaceKHR surface,
    VkDeviceGroupPresentModeFlagsKHR* pModes) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDeviceGroupSurfacePresentModesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDeviceGroupSurfacePresentModesKHR, sizeof(VkDeviceGroupPresentModeFlagsKHR));
    result = mdd(device)->devTable.GetDeviceGroupSurfacePresentModesKHR(device, surface, pModes);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDeviceGroupSurfacePresentModesKHR(pHeader);
    pPacket->device = device;
    pPacket->surface = surface;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pModes), sizeof(VkDeviceGroupPresentModeFlagsKHR), pModes);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pModes));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDevicePresentRectanglesKHR(
    VkPhysicalDevice physicalDevice,
    VkSurfaceKHR surface,
    uint32_t* pRectCount,
    VkRect2D* pRects) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDevicePresentRectanglesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDevicePresentRectanglesKHR, sizeof(uint32_t) + (*pRectCount) * sizeof(VkRect2D));
    result = mid(physicalDevice)->instTable.GetPhysicalDevicePresentRectanglesKHR(physicalDevice, surface, pRectCount, pRects);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDevicePresentRectanglesKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->surface = surface;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRectCount), sizeof(uint32_t), pRectCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pRects), (*pRectCount) * sizeof(VkRect2D), pRects);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRectCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pRects));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkAcquireNextImage2KHR(
    VkDevice device,
    const VkAcquireNextImageInfoKHR* pAcquireInfo,
    uint32_t* pImageIndex) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkAcquireNextImage2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkAcquireNextImage2KHR, get_struct_chain_size((void*)pAcquireInfo) + sizeof(uint32_t));
    result = mdd(device)->devTable.AcquireNextImage2KHR(device, pAcquireInfo, pImageIndex);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkAcquireNextImage2KHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAcquireInfo), sizeof(VkAcquireNextImageInfoKHR), pAcquireInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImageIndex), sizeof(uint32_t), pImageIndex);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAcquireInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImageIndex));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR ) {
            if (pAcquireInfo->semaphore != VK_NULL_HANDLE) {
                trim::ObjectInfo* pInfo = trim::get_Semaphore_objectInfo(pAcquireInfo->semaphore);
                if (pInfo != NULL) {
                    // We need to signal the semaphore because other command may wait on it.
                    pInfo->ObjectInfo.Semaphore.signaledOnQueue = VK_NULL_HANDLE;
                    pInfo->ObjectInfo.Semaphore.signaledOnSwapChain = pAcquireInfo->swapchain;
                }
            }
            if (pAcquireInfo->fence != VK_NULL_HANDLE) {
                trim::ObjectInfo* pFenceInfo = trim::get_Fence_objectInfo(pAcquireInfo->fence);
                if (pFenceInfo != NULL) {
                    pFenceInfo->ObjectInfo.Fence.signaled = true;
                }
            }
        }
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceDisplayPropertiesKHR(
    VkPhysicalDevice physicalDevice,
    uint32_t* pPropertyCount,
    VkDisplayPropertiesKHR* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceDisplayPropertiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceDisplayPropertiesKHR, sizeof(uint32_t) + (*pPropertyCount) * sizeof(VkDisplayPropertiesKHR));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceDisplayPropertiesKHR(physicalDevice, pPropertyCount, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceDisplayPropertiesKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPropertyCount), sizeof(uint32_t), pPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), (*pPropertyCount) * sizeof(VkDisplayPropertiesKHR), pProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceDisplayPlanePropertiesKHR(
    VkPhysicalDevice physicalDevice,
    uint32_t* pPropertyCount,
    VkDisplayPlanePropertiesKHR* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceDisplayPlanePropertiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceDisplayPlanePropertiesKHR, sizeof(uint32_t) + (*pPropertyCount) * sizeof(VkDisplayPlanePropertiesKHR));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceDisplayPlanePropertiesKHR(physicalDevice, pPropertyCount, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceDisplayPlanePropertiesKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPropertyCount), sizeof(uint32_t), pPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), (*pPropertyCount) * sizeof(VkDisplayPlanePropertiesKHR), pProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetDisplayPlaneSupportedDisplaysKHR(
    VkPhysicalDevice physicalDevice,
    uint32_t planeIndex,
    uint32_t* pDisplayCount,
    VkDisplayKHR* pDisplays) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDisplayPlaneSupportedDisplaysKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDisplayPlaneSupportedDisplaysKHR, sizeof(uint32_t) + (*pDisplayCount) * sizeof(VkDisplayKHR));
    result = mid(physicalDevice)->instTable.GetDisplayPlaneSupportedDisplaysKHR(physicalDevice, planeIndex, pDisplayCount, pDisplays);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDisplayPlaneSupportedDisplaysKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->planeIndex = planeIndex;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDisplayCount), sizeof(uint32_t), pDisplayCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDisplays), (*pDisplayCount) * sizeof(VkDisplayKHR), pDisplays);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDisplayCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDisplays));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetDisplayModePropertiesKHR(
    VkPhysicalDevice physicalDevice,
    VkDisplayKHR display,
    uint32_t* pPropertyCount,
    VkDisplayModePropertiesKHR* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDisplayModePropertiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDisplayModePropertiesKHR, sizeof(uint32_t) + (*pPropertyCount) * sizeof(VkDisplayModePropertiesKHR));
    result = mid(physicalDevice)->instTable.GetDisplayModePropertiesKHR(physicalDevice, display, pPropertyCount, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDisplayModePropertiesKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->display = display;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPropertyCount), sizeof(uint32_t), pPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), (*pPropertyCount) * sizeof(VkDisplayModePropertiesKHR), pProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateDisplayModeKHR(
    VkPhysicalDevice physicalDevice,
    VkDisplayKHR display,
    const VkDisplayModeCreateInfoKHR* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDisplayModeKHR* pMode) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateDisplayModeKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateDisplayModeKHR, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkDisplayModeKHR));
    result = mid(physicalDevice)->instTable.CreateDisplayModeKHR(physicalDevice, display, pCreateInfo, pAllocator, pMode);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateDisplayModeKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->display = display;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkDisplayModeCreateInfoKHR), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMode), sizeof(VkDisplayModeKHR), pMode);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMode));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetDisplayPlaneCapabilitiesKHR(
    VkPhysicalDevice physicalDevice,
    VkDisplayModeKHR mode,
    uint32_t planeIndex,
    VkDisplayPlaneCapabilitiesKHR* pCapabilities) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDisplayPlaneCapabilitiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDisplayPlaneCapabilitiesKHR, sizeof(VkDisplayPlaneCapabilitiesKHR));
    result = mid(physicalDevice)->instTable.GetDisplayPlaneCapabilitiesKHR(physicalDevice, mode, planeIndex, pCapabilities);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDisplayPlaneCapabilitiesKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->mode = mode;
    pPacket->planeIndex = planeIndex;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCapabilities), sizeof(VkDisplayPlaneCapabilitiesKHR), pCapabilities);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCapabilities));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateDisplayPlaneSurfaceKHR(
    VkInstance instance,
    const VkDisplaySurfaceCreateInfoKHR* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSurfaceKHR* pSurface) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateDisplayPlaneSurfaceKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateDisplayPlaneSurfaceKHR, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkSurfaceKHR));
    result = mid(instance)->instTable.CreateDisplayPlaneSurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateDisplayPlaneSurfaceKHR(pHeader);
    pPacket->instance = instance;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkDisplaySurfaceCreateInfoKHR), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSurface), sizeof(VkSurfaceKHR), pSurface);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSurface));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateSharedSwapchainsKHR(
    VkDevice device,
    uint32_t swapchainCount,
    const VkSwapchainCreateInfoKHR* pCreateInfos,
    const VkAllocationCallbacks* pAllocator,
    VkSwapchainKHR* pSwapchains) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateSharedSwapchainsKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateSharedSwapchainsKHR, vk_size_vkswapchaincreateinfokhr(pCreateInfos) + sizeof(VkAllocationCallbacks) + swapchainCount * sizeof(VkSwapchainKHR));
    result = mdd(device)->devTable.CreateSharedSwapchainsKHR(device, swapchainCount, pCreateInfos, pAllocator, pSwapchains);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateSharedSwapchainsKHR(pHeader);
    pPacket->device = device;
    pPacket->swapchainCount = swapchainCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfos), swapchainCount * sizeof(VkSwapchainCreateInfoKHR), pCreateInfos);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSwapchains), swapchainCount * sizeof(VkSwapchainKHR), pSwapchains);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfos));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSwapchains));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// __HOOKED_vkCreateXlibSurfaceKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_XLIB_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateXlibSurfaceKHR(
    VkInstance instance,
    const VkXlibSurfaceCreateInfoKHR* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSurfaceKHR* pSurface);
#endif // VK_USE_PLATFORM_XLIB_KHR
// __HOOKED_vkGetPhysicalDeviceXlibPresentationSupportKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_XLIB_KHR
VKTRACER_EXPORT VKAPI_ATTR VkBool32 VKAPI_CALL __HOOKED_vkGetPhysicalDeviceXlibPresentationSupportKHR(
    VkPhysicalDevice physicalDevice,
    uint32_t queueFamilyIndex,
    Display* dpy,
    VisualID visualID);
#endif // VK_USE_PLATFORM_XLIB_KHR
// __HOOKED_vkCreateXcbSurfaceKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_XCB_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateXcbSurfaceKHR(
    VkInstance instance,
    const VkXcbSurfaceCreateInfoKHR* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSurfaceKHR* pSurface);
#endif // VK_USE_PLATFORM_XCB_KHR
// __HOOKED_vkGetPhysicalDeviceXcbPresentationSupportKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_XCB_KHR
VKTRACER_EXPORT VKAPI_ATTR VkBool32 VKAPI_CALL __HOOKED_vkGetPhysicalDeviceXcbPresentationSupportKHR(
    VkPhysicalDevice physicalDevice,
    uint32_t queueFamilyIndex,
    xcb_connection_t* connection,
    xcb_visualid_t visual_id);
#endif // VK_USE_PLATFORM_XCB_KHR
// __HOOKED_vkCreateWaylandSurfaceKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_WAYLAND_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateWaylandSurfaceKHR(
    VkInstance instance,
    const VkWaylandSurfaceCreateInfoKHR* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSurfaceKHR* pSurface);
#endif // VK_USE_PLATFORM_WAYLAND_KHR
// __HOOKED_vkGetPhysicalDeviceWaylandPresentationSupportKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_WAYLAND_KHR
VKTRACER_EXPORT VKAPI_ATTR VkBool32 VKAPI_CALL __HOOKED_vkGetPhysicalDeviceWaylandPresentationSupportKHR(
    VkPhysicalDevice physicalDevice,
    uint32_t queueFamilyIndex,
    struct wl_display* display);
#endif // VK_USE_PLATFORM_WAYLAND_KHR
// __HOOKED_vkCreateAndroidSurfaceKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_ANDROID_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateAndroidSurfaceKHR(
    VkInstance instance,
    const VkAndroidSurfaceCreateInfoKHR* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSurfaceKHR* pSurface);
#endif // VK_USE_PLATFORM_ANDROID_KHR
// __HOOKED_vkCreateWin32SurfaceKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateWin32SurfaceKHR(
    VkInstance instance,
    const VkWin32SurfaceCreateInfoKHR* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSurfaceKHR* pSurface);
#endif // VK_USE_PLATFORM_WIN32_KHR
// __HOOKED_vkGetPhysicalDeviceWin32PresentationSupportKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkBool32 VKAPI_CALL __HOOKED_vkGetPhysicalDeviceWin32PresentationSupportKHR(
    VkPhysicalDevice physicalDevice,
    uint32_t queueFamilyIndex);
#endif // VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceFeatures2KHR(
    VkPhysicalDevice physicalDevice,
    VkPhysicalDeviceFeatures2* pFeatures) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceFeatures2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceFeatures2KHR, get_struct_chain_size((void*)pFeatures));
    mid(physicalDevice)->instTable.GetPhysicalDeviceFeatures2KHR(physicalDevice, pFeatures);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceFeatures2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFeatures), sizeof(VkPhysicalDeviceFeatures2), pFeatures);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pFeatures, (void *)pFeatures);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFeatures));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// __HOOKED_vkGetPhysicalDeviceProperties2KHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceProperties2KHR(
    VkPhysicalDevice physicalDevice,
    VkPhysicalDeviceProperties2* pProperties);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceFormatProperties2KHR(
    VkPhysicalDevice physicalDevice,
    VkFormat format,
    VkFormatProperties2* pFormatProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceFormatProperties2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceFormatProperties2KHR, get_struct_chain_size((void*)pFormatProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceFormatProperties2KHR(physicalDevice, format, pFormatProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceFormatProperties2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->format = format;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFormatProperties), sizeof(VkFormatProperties2), pFormatProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pFormatProperties, (void *)pFormatProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFormatProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceImageFormatProperties2KHR(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceImageFormatInfo2* pImageFormatInfo,
    VkImageFormatProperties2* pImageFormatProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceImageFormatProperties2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceImageFormatProperties2KHR, get_struct_chain_size((void*)pImageFormatInfo) + get_struct_chain_size((void*)pImageFormatProperties));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceImageFormatProperties2KHR(physicalDevice, pImageFormatInfo, pImageFormatProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceImageFormatProperties2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImageFormatInfo), sizeof(VkPhysicalDeviceImageFormatInfo2), pImageFormatInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pImageFormatInfo, (void *)pImageFormatInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImageFormatProperties), sizeof(VkImageFormatProperties2), pImageFormatProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pImageFormatProperties, (void *)pImageFormatProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImageFormatInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImageFormatProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// __HOOKED_vkGetPhysicalDeviceQueueFamilyProperties2KHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceQueueFamilyProperties2KHR(
    VkPhysicalDevice physicalDevice,
    uint32_t* pQueueFamilyPropertyCount,
    VkQueueFamilyProperties2* pQueueFamilyProperties);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceMemoryProperties2KHR(
    VkPhysicalDevice physicalDevice,
    VkPhysicalDeviceMemoryProperties2* pMemoryProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceMemoryProperties2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceMemoryProperties2KHR, get_struct_chain_size((void*)pMemoryProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceMemoryProperties2KHR(physicalDevice, pMemoryProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceMemoryProperties2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryProperties), sizeof(VkPhysicalDeviceMemoryProperties2), pMemoryProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pMemoryProperties, (void *)pMemoryProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSparseImageFormatProperties2KHR(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceSparseImageFormatInfo2* pFormatInfo,
    uint32_t* pPropertyCount,
    VkSparseImageFormatProperties2* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceSparseImageFormatProperties2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceSparseImageFormatProperties2KHR, get_struct_chain_size((void*)pFormatInfo) + sizeof(uint32_t) + get_struct_chain_size((void*)pProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceSparseImageFormatProperties2KHR(physicalDevice, pFormatInfo, pPropertyCount, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceSparseImageFormatProperties2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFormatInfo), sizeof(VkPhysicalDeviceSparseImageFormatInfo2), pFormatInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPropertyCount), sizeof(uint32_t), pPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), (*pPropertyCount) * sizeof(VkSparseImageFormatProperties2), pProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pProperties, (void *)pProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFormatInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// TODO: Add support for __HOOKED_vkGetDeviceGroupPeerMemoryFeaturesKHR: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdSetDeviceMaskKHR: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdDispatchBaseKHR: Skipping for now.
// TODO: Add support for __HOOKED_vkGetDeviceGroupSurfacePresentModes2EXT: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkTrimCommandPoolKHR(
    VkDevice device,
    VkCommandPool commandPool,
    VkCommandPoolTrimFlags flags) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkTrimCommandPoolKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkTrimCommandPoolKHR, 0);
    mdd(device)->devTable.TrimCommandPoolKHR(device, commandPool, flags);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkTrimCommandPoolKHR(pHeader);
    pPacket->device = device;
    pPacket->commandPool = commandPool;
    pPacket->flags = flags;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// TODO: Add support for __HOOKED_vkEnumeratePhysicalDeviceGroupsKHR: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceExternalBufferPropertiesKHR(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceExternalBufferInfo* pExternalBufferInfo,
    VkExternalBufferProperties* pExternalBufferProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceExternalBufferPropertiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceExternalBufferPropertiesKHR, get_struct_chain_size((void*)pExternalBufferInfo) + get_struct_chain_size((void*)pExternalBufferProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceExternalBufferPropertiesKHR(physicalDevice, pExternalBufferInfo, pExternalBufferProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceExternalBufferPropertiesKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalBufferInfo), sizeof(VkPhysicalDeviceExternalBufferInfo), pExternalBufferInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalBufferProperties), sizeof(VkExternalBufferProperties), pExternalBufferProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalBufferInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalBufferProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
#ifdef VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetMemoryWin32HandleKHR(
    VkDevice device,
    const VkMemoryGetWin32HandleInfoKHR* pGetWin32HandleInfo,
    HANDLE* pHandle) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetMemoryWin32HandleKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetMemoryWin32HandleKHR, sizeof(VkMemoryGetWin32HandleInfoKHR) + sizeof(HANDLE));
    result = mdd(device)->devTable.GetMemoryWin32HandleKHR(device, pGetWin32HandleInfo, pHandle);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetMemoryWin32HandleKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pGetWin32HandleInfo), sizeof(VkMemoryGetWin32HandleInfoKHR), pGetWin32HandleInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pHandle), sizeof(HANDLE), pHandle);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pGetWin32HandleInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pHandle));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetMemoryWin32HandlePropertiesKHR(
    VkDevice device,
    VkExternalMemoryHandleTypeFlagBits handleType,
    HANDLE handle,
    VkMemoryWin32HandlePropertiesKHR* pMemoryWin32HandleProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetMemoryWin32HandlePropertiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetMemoryWin32HandlePropertiesKHR, sizeof(VkMemoryWin32HandlePropertiesKHR));
    result = mdd(device)->devTable.GetMemoryWin32HandlePropertiesKHR(device, handleType, handle, pMemoryWin32HandleProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetMemoryWin32HandlePropertiesKHR(pHeader);
    pPacket->device = device;
    pPacket->handleType = handleType;
    pPacket->handle = handle;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryWin32HandleProperties), sizeof(VkMemoryWin32HandlePropertiesKHR), pMemoryWin32HandleProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryWin32HandleProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
#endif // VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetMemoryFdKHR(
    VkDevice device,
    const VkMemoryGetFdInfoKHR* pGetFdInfo,
    int* pFd) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetMemoryFdKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetMemoryFdKHR, get_struct_chain_size((void*)pGetFdInfo) + sizeof(int));
    result = mdd(device)->devTable.GetMemoryFdKHR(device, pGetFdInfo, pFd);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetMemoryFdKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pGetFdInfo), sizeof(VkMemoryGetFdInfoKHR), pGetFdInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFd), sizeof(int), pFd);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pGetFdInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFd));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetMemoryFdPropertiesKHR(
    VkDevice device,
    VkExternalMemoryHandleTypeFlagBits handleType,
    int fd,
    VkMemoryFdPropertiesKHR* pMemoryFdProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetMemoryFdPropertiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetMemoryFdPropertiesKHR, get_struct_chain_size((void*)pMemoryFdProperties));
    result = mdd(device)->devTable.GetMemoryFdPropertiesKHR(device, handleType, fd, pMemoryFdProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetMemoryFdPropertiesKHR(pHeader);
    pPacket->device = device;
    pPacket->handleType = handleType;
    pPacket->fd = fd;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryFdProperties), sizeof(VkMemoryFdPropertiesKHR), pMemoryFdProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryFdProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceExternalSemaphoreInfo* pExternalSemaphoreInfo,
    VkExternalSemaphoreProperties* pExternalSemaphoreProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceExternalSemaphorePropertiesKHR, get_struct_chain_size((void*)pExternalSemaphoreInfo) + get_struct_chain_size((void*)pExternalSemaphoreProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceExternalSemaphorePropertiesKHR(physicalDevice, pExternalSemaphoreInfo, pExternalSemaphoreProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalSemaphoreInfo), sizeof(VkPhysicalDeviceExternalSemaphoreInfo), pExternalSemaphoreInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalSemaphoreProperties), sizeof(VkExternalSemaphoreProperties), pExternalSemaphoreProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalSemaphoreInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalSemaphoreProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
#ifdef VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkImportSemaphoreWin32HandleKHR(
    VkDevice device,
    const VkImportSemaphoreWin32HandleInfoKHR* pImportSemaphoreWin32HandleInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkImportSemaphoreWin32HandleKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkImportSemaphoreWin32HandleKHR, sizeof(VkImportSemaphoreWin32HandleInfoKHR));
    result = mdd(device)->devTable.ImportSemaphoreWin32HandleKHR(device, pImportSemaphoreWin32HandleInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkImportSemaphoreWin32HandleKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImportSemaphoreWin32HandleInfo), sizeof(VkImportSemaphoreWin32HandleInfoKHR), pImportSemaphoreWin32HandleInfo);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImportSemaphoreWin32HandleInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetSemaphoreWin32HandleKHR(
    VkDevice device,
    const VkSemaphoreGetWin32HandleInfoKHR* pGetWin32HandleInfo,
    HANDLE* pHandle) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetSemaphoreWin32HandleKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetSemaphoreWin32HandleKHR, sizeof(VkSemaphoreGetWin32HandleInfoKHR) + sizeof(HANDLE));
    result = mdd(device)->devTable.GetSemaphoreWin32HandleKHR(device, pGetWin32HandleInfo, pHandle);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetSemaphoreWin32HandleKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pGetWin32HandleInfo), sizeof(VkSemaphoreGetWin32HandleInfoKHR), pGetWin32HandleInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pHandle), sizeof(HANDLE), pHandle);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pGetWin32HandleInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pHandle));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
#endif // VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkImportSemaphoreFdKHR(
    VkDevice device,
    const VkImportSemaphoreFdInfoKHR* pImportSemaphoreFdInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkImportSemaphoreFdKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkImportSemaphoreFdKHR, get_struct_chain_size((void*)pImportSemaphoreFdInfo));
    result = mdd(device)->devTable.ImportSemaphoreFdKHR(device, pImportSemaphoreFdInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkImportSemaphoreFdKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImportSemaphoreFdInfo), sizeof(VkImportSemaphoreFdInfoKHR), pImportSemaphoreFdInfo);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImportSemaphoreFdInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetSemaphoreFdKHR(
    VkDevice device,
    const VkSemaphoreGetFdInfoKHR* pGetFdInfo,
    int* pFd) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetSemaphoreFdKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetSemaphoreFdKHR, get_struct_chain_size((void*)pGetFdInfo) + sizeof(int));
    result = mdd(device)->devTable.GetSemaphoreFdKHR(device, pGetFdInfo, pFd);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetSemaphoreFdKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pGetFdInfo), sizeof(VkSemaphoreGetFdInfoKHR), pGetFdInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFd), sizeof(int), pFd);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pGetFdInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFd));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// __HOOKED_vkCmdPushDescriptorSetKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdPushDescriptorSetKHR(
    VkCommandBuffer commandBuffer,
    VkPipelineBindPoint pipelineBindPoint,
    VkPipelineLayout layout,
    uint32_t set,
    uint32_t descriptorWriteCount,
    const VkWriteDescriptorSet* pDescriptorWrites);
// __HOOKED_vkCmdPushDescriptorSetWithTemplateKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdPushDescriptorSetWithTemplateKHR(
    VkCommandBuffer commandBuffer,
    VkDescriptorUpdateTemplate descriptorUpdateTemplate,
    VkPipelineLayout layout,
    uint32_t set,
    const void* pData);
// __HOOKED_vkCreateDescriptorUpdateTemplateKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateDescriptorUpdateTemplateKHR(
    VkDevice device,
    const VkDescriptorUpdateTemplateCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDescriptorUpdateTemplate* pDescriptorUpdateTemplate);
// __HOOKED_vkDestroyDescriptorUpdateTemplateKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyDescriptorUpdateTemplateKHR(
    VkDevice device,
    VkDescriptorUpdateTemplate descriptorUpdateTemplate,
    const VkAllocationCallbacks* pAllocator);
// __HOOKED_vkUpdateDescriptorSetWithTemplateKHR is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkUpdateDescriptorSetWithTemplateKHR(
    VkDevice device,
    VkDescriptorSet descriptorSet,
    VkDescriptorUpdateTemplate descriptorUpdateTemplate,
    const void* pData);
// TODO: Add support for __HOOKED_vkCreateRenderPass2KHR: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdBeginRenderPass2KHR: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdNextSubpass2KHR: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdEndRenderPass2KHR: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetSwapchainStatusKHR(
    VkDevice device,
    VkSwapchainKHR swapchain) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetSwapchainStatusKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetSwapchainStatusKHR, 0);
    result = mdd(device)->devTable.GetSwapchainStatusKHR(device, swapchain);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetSwapchainStatusKHR(pHeader);
    pPacket->device = device;
    pPacket->swapchain = swapchain;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceExternalFencePropertiesKHR(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceExternalFenceInfo* pExternalFenceInfo,
    VkExternalFenceProperties* pExternalFenceProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceExternalFencePropertiesKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceExternalFencePropertiesKHR, get_struct_chain_size((void*)pExternalFenceInfo) + get_struct_chain_size((void*)pExternalFenceProperties));
    mid(physicalDevice)->instTable.GetPhysicalDeviceExternalFencePropertiesKHR(physicalDevice, pExternalFenceInfo, pExternalFenceProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceExternalFencePropertiesKHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalFenceInfo), sizeof(VkPhysicalDeviceExternalFenceInfo), pExternalFenceInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalFenceProperties), sizeof(VkExternalFenceProperties), pExternalFenceProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalFenceInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalFenceProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
#ifdef VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkImportFenceWin32HandleKHR(
    VkDevice device,
    const VkImportFenceWin32HandleInfoKHR* pImportFenceWin32HandleInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkImportFenceWin32HandleKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkImportFenceWin32HandleKHR, sizeof(VkImportFenceWin32HandleInfoKHR));
    result = mdd(device)->devTable.ImportFenceWin32HandleKHR(device, pImportFenceWin32HandleInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkImportFenceWin32HandleKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImportFenceWin32HandleInfo), sizeof(VkImportFenceWin32HandleInfoKHR), pImportFenceWin32HandleInfo);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImportFenceWin32HandleInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetFenceWin32HandleKHR(
    VkDevice device,
    const VkFenceGetWin32HandleInfoKHR* pGetWin32HandleInfo,
    HANDLE* pHandle) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetFenceWin32HandleKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetFenceWin32HandleKHR, sizeof(VkFenceGetWin32HandleInfoKHR) + sizeof(HANDLE));
    result = mdd(device)->devTable.GetFenceWin32HandleKHR(device, pGetWin32HandleInfo, pHandle);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetFenceWin32HandleKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pGetWin32HandleInfo), sizeof(VkFenceGetWin32HandleInfoKHR), pGetWin32HandleInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pHandle), sizeof(HANDLE), pHandle);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pGetWin32HandleInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pHandle));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
#endif // VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkImportFenceFdKHR(
    VkDevice device,
    const VkImportFenceFdInfoKHR* pImportFenceFdInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkImportFenceFdKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkImportFenceFdKHR, sizeof(VkImportFenceFdInfoKHR));
    result = mdd(device)->devTable.ImportFenceFdKHR(device, pImportFenceFdInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkImportFenceFdKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pImportFenceFdInfo), sizeof(VkImportFenceFdInfoKHR), pImportFenceFdInfo);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pImportFenceFdInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetFenceFdKHR(
    VkDevice device,
    const VkFenceGetFdInfoKHR* pGetFdInfo,
    int* pFd) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetFenceFdKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetFenceFdKHR, get_struct_chain_size((void*)pGetFdInfo) + sizeof(int));
    result = mdd(device)->devTable.GetFenceFdKHR(device, pGetFdInfo, pFd);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetFenceFdKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pGetFdInfo), sizeof(VkFenceGetFdInfoKHR), pGetFdInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFd), sizeof(int), pFd);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pGetFdInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFd));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSurfaceCapabilities2KHR(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceSurfaceInfo2KHR* pSurfaceInfo,
    VkSurfaceCapabilities2KHR* pSurfaceCapabilities) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceSurfaceCapabilities2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceSurfaceCapabilities2KHR, get_struct_chain_size((void*)pSurfaceInfo) + get_struct_chain_size((void*)pSurfaceCapabilities));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceSurfaceCapabilities2KHR(physicalDevice, pSurfaceInfo, pSurfaceCapabilities);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceSurfaceCapabilities2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSurfaceInfo), sizeof(VkPhysicalDeviceSurfaceInfo2KHR), pSurfaceInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pSurfaceInfo, (void *)pSurfaceInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSurfaceCapabilities), sizeof(VkSurfaceCapabilities2KHR), pSurfaceCapabilities);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSurfaceInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSurfaceCapabilities));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSurfaceFormats2KHR(
    VkPhysicalDevice physicalDevice,
    const VkPhysicalDeviceSurfaceInfo2KHR* pSurfaceInfo,
    uint32_t* pSurfaceFormatCount,
    VkSurfaceFormat2KHR* pSurfaceFormats) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceSurfaceFormats2KHR* pPacket = NULL;
    size_t byteCount=0;
    uint32_t surfaceFormatCount = *pSurfaceFormatCount;
    for (uint32_t i=0; i<surfaceFormatCount; i++) {
        byteCount += get_struct_chain_size(&pSurfaceFormats[i]);
    }
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceSurfaceFormats2KHR, get_struct_chain_size((void*)pSurfaceInfo) + sizeof(uint32_t) + byteCount);
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceSurfaceFormats2KHR(physicalDevice, pSurfaceInfo, pSurfaceFormatCount, pSurfaceFormats);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceSurfaceFormats2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSurfaceInfo), sizeof(VkPhysicalDeviceSurfaceInfo2KHR), pSurfaceInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSurfaceFormatCount), sizeof(uint32_t), pSurfaceFormatCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSurfaceFormats), surfaceFormatCount*sizeof(VkSurfaceFormat2KHR), pSurfaceFormats);
    for (uint32_t i = 0; i < surfaceFormatCount; i++) {
        vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)(&pPacket->pSurfaceFormats[i]), (void *)(&pSurfaceFormats[i]));
    }
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSurfaceInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSurfaceFormatCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSurfaceFormats));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceDisplayProperties2KHR(
    VkPhysicalDevice physicalDevice,
    uint32_t* pPropertyCount,
    VkDisplayProperties2KHR* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceDisplayProperties2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceDisplayProperties2KHR, sizeof(uint32_t) + get_struct_chain_size((void*)pProperties));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceDisplayProperties2KHR(physicalDevice, pPropertyCount, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceDisplayProperties2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPropertyCount), sizeof(uint32_t), pPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), (*pPropertyCount) * sizeof(VkDisplayProperties2KHR), pProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pProperties, (void *)pProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceDisplayPlaneProperties2KHR(
    VkPhysicalDevice physicalDevice,
    uint32_t* pPropertyCount,
    VkDisplayPlaneProperties2KHR* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceDisplayPlaneProperties2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceDisplayPlaneProperties2KHR, sizeof(uint32_t) + get_struct_chain_size((void*)pProperties));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceDisplayPlaneProperties2KHR(physicalDevice, pPropertyCount, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceDisplayPlaneProperties2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPropertyCount), sizeof(uint32_t), pPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), (*pPropertyCount) * sizeof(VkDisplayPlaneProperties2KHR), pProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pProperties, (void *)pProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetDisplayModeProperties2KHR(
    VkPhysicalDevice physicalDevice,
    VkDisplayKHR display,
    uint32_t* pPropertyCount,
    VkDisplayModeProperties2KHR* pProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDisplayModeProperties2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDisplayModeProperties2KHR, sizeof(uint32_t) + get_struct_chain_size((void*)pProperties));
    result = mid(physicalDevice)->instTable.GetDisplayModeProperties2KHR(physicalDevice, display, pPropertyCount, pProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDisplayModeProperties2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->display = display;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPropertyCount), sizeof(uint32_t), pPropertyCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pProperties), (*pPropertyCount) * sizeof(VkDisplayModeProperties2KHR), pProperties);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pProperties, (void *)pProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPropertyCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetDisplayPlaneCapabilities2KHR(
    VkPhysicalDevice physicalDevice,
    const VkDisplayPlaneInfo2KHR* pDisplayPlaneInfo,
    VkDisplayPlaneCapabilities2KHR* pCapabilities) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetDisplayPlaneCapabilities2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetDisplayPlaneCapabilities2KHR, sizeof(VkDisplayPlaneInfo2KHR) + sizeof(VkDisplayPlaneCapabilities2KHR));
    result = mid(physicalDevice)->instTable.GetDisplayPlaneCapabilities2KHR(physicalDevice, pDisplayPlaneInfo, pCapabilities);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetDisplayPlaneCapabilities2KHR(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDisplayPlaneInfo), sizeof(VkDisplayPlaneInfo2KHR), pDisplayPlaneInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCapabilities), sizeof(VkDisplayPlaneCapabilities2KHR), pCapabilities);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDisplayPlaneInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCapabilities));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetImageMemoryRequirements2KHR(
    VkDevice device,
    const VkImageMemoryRequirementsInfo2* pInfo,
    VkMemoryRequirements2* pMemoryRequirements) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetImageMemoryRequirements2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetImageMemoryRequirements2KHR, get_struct_chain_size((void*)pInfo) + get_struct_chain_size((void*)pMemoryRequirements));
    mdd(device)->devTable.GetImageMemoryRequirements2KHR(device, pInfo, pMemoryRequirements);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetImageMemoryRequirements2KHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pInfo), sizeof(VkImageMemoryRequirementsInfo2), pInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pInfo, (void *)pInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryRequirements), sizeof(VkMemoryRequirements2), pMemoryRequirements);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pMemoryRequirements, (void *)pMemoryRequirements);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryRequirements));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        trim::ObjectInfo* pTrimObjectInfo = trim::get_Image_objectInfo(pInfo->image);
        if (pTrimObjectInfo != NULL) {
            pTrimObjectInfo->ObjectInfo.Image.memorySize = pMemoryRequirements->memoryRequirements.size;
        }
#if TRIM_USE_ORDERED_IMAGE_CREATION
        trim::add_Image_call(trim::copy_packet(pHeader));
#else
        if (pTrimObjectInfo != NULL) {
            pTrimObjectInfo->ObjectInfo.Image.pGetImageMemoryRequirementsPacket = trim::copy_packet(pHeader);
        }
#endif //TRIM_USE_ORDERED_IMAGE_CREATION
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(pInfo->image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetBufferMemoryRequirements2KHR(
    VkDevice device,
    const VkBufferMemoryRequirementsInfo2* pInfo,
    VkMemoryRequirements2* pMemoryRequirements) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetBufferMemoryRequirements2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetBufferMemoryRequirements2KHR, get_struct_chain_size((void*)pInfo) + get_struct_chain_size((void*)pMemoryRequirements));
    mdd(device)->devTable.GetBufferMemoryRequirements2KHR(device, pInfo, pMemoryRequirements);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetBufferMemoryRequirements2KHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pInfo), sizeof(VkBufferMemoryRequirementsInfo2), pInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pInfo, (void *)pInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryRequirements), sizeof(VkMemoryRequirements2), pMemoryRequirements);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pMemoryRequirements, (void *)pMemoryRequirements);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryRequirements));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::mark_Buffer_reference(pInfo->buffer);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetImageSparseMemoryRequirements2KHR(
    VkDevice device,
    const VkImageSparseMemoryRequirementsInfo2* pInfo,
    uint32_t* pSparseMemoryRequirementCount,
    VkSparseImageMemoryRequirements2* pSparseMemoryRequirements) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetImageSparseMemoryRequirements2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetImageSparseMemoryRequirements2KHR, get_struct_chain_size((void*)pInfo) + sizeof(uint32_t) + get_struct_chain_size((void*)pSparseMemoryRequirements));
    mdd(device)->devTable.GetImageSparseMemoryRequirements2KHR(device, pInfo, pSparseMemoryRequirementCount, pSparseMemoryRequirements);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetImageSparseMemoryRequirements2KHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pInfo), sizeof(VkImageSparseMemoryRequirementsInfo2), pInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pInfo, (void *)pInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSparseMemoryRequirementCount), sizeof(uint32_t), pSparseMemoryRequirementCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSparseMemoryRequirements), (*pSparseMemoryRequirementCount) * sizeof(VkSparseImageMemoryRequirements2), pSparseMemoryRequirements);
    for (uint32_t i=0; i< *pPacket->pSparseMemoryRequirementCount; i++)
        vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)(&pPacket->pSparseMemoryRequirements[i]), (void *)(&pSparseMemoryRequirements[i]));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSparseMemoryRequirementCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSparseMemoryRequirements));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::mark_Image_reference(pInfo->image);
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateSamplerYcbcrConversionKHR(
    VkDevice device,
    const VkSamplerYcbcrConversionCreateInfo* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSamplerYcbcrConversion* pYcbcrConversion) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateSamplerYcbcrConversionKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateSamplerYcbcrConversionKHR, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkSamplerYcbcrConversion));
    result = mdd(device)->devTable.CreateSamplerYcbcrConversionKHR(device, pCreateInfo, pAllocator, pYcbcrConversion);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateSamplerYcbcrConversionKHR(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkSamplerYcbcrConversionCreateInfo), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pYcbcrConversion), sizeof(VkSamplerYcbcrConversion), pYcbcrConversion);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pYcbcrConversion));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroySamplerYcbcrConversionKHR(
    VkDevice device,
    VkSamplerYcbcrConversion ycbcrConversion,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroySamplerYcbcrConversionKHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroySamplerYcbcrConversionKHR, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroySamplerYcbcrConversionKHR(device, ycbcrConversion, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroySamplerYcbcrConversionKHR(pHeader);
    pPacket->device = device;
    pPacket->ycbcrConversion = ycbcrConversion;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkBindBufferMemory2KHR(
    VkDevice device,
    uint32_t bindInfoCount,
    const VkBindBufferMemoryInfo* pBindInfos) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkBindBufferMemory2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkBindBufferMemory2KHR, bindInfoCount * sizeof(VkBindBufferMemoryInfo));
    result = mdd(device)->devTable.BindBufferMemory2KHR(device, bindInfoCount, pBindInfos);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkBindBufferMemory2KHR(pHeader);
    pPacket->device = device;
    pPacket->bindInfoCount = bindInfoCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pBindInfos), bindInfoCount * sizeof(VkBindBufferMemoryInfo), pBindInfos);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pBindInfos));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            for (uint32_t i = 0; i < bindInfoCount; i++) {
                trim::mark_Buffer_reference(pBindInfos[i].buffer);
            }
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkBindImageMemory2KHR(
    VkDevice device,
    uint32_t bindInfoCount,
    const VkBindImageMemoryInfo* pBindInfos) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkBindImageMemory2KHR* pPacket = NULL;
    CREATE_TRACE_PACKET(vkBindImageMemory2KHR, bindInfoCount * sizeof(VkBindImageMemoryInfo));
    result = mdd(device)->devTable.BindImageMemory2KHR(device, bindInfoCount, pBindInfos);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkBindImageMemory2KHR(pHeader);
    pPacket->device = device;
    pPacket->bindInfoCount = bindInfoCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pBindInfos), bindInfoCount * sizeof(VkBindImageMemoryInfo), pBindInfos);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pBindInfos));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            for (uint32_t i = 0; i < bindInfoCount; i++) {
                trim::mark_Image_reference(pBindInfos[i].image);
            }
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }    }
    return result;
}
// TODO: Add support for __HOOKED_vkGetDescriptorSetLayoutSupportKHR: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdDrawIndirectCountKHR: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdDrawIndexedIndirectCountKHR: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateDebugReportCallbackEXT(
    VkInstance instance,
    const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkDebugReportCallbackEXT* pCallback) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateDebugReportCallbackEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateDebugReportCallbackEXT, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkDebugReportCallbackEXT));
    result = mid(instance)->instTable.CreateDebugReportCallbackEXT(instance, pCreateInfo, pAllocator, pCallback);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateDebugReportCallbackEXT(pHeader);
    pPacket->instance = instance;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkDebugReportCallbackCreateInfoEXT), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCallback), sizeof(VkDebugReportCallbackEXT), pCallback);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCallback));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyDebugReportCallbackEXT(
    VkInstance instance,
    VkDebugReportCallbackEXT callback,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyDebugReportCallbackEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyDebugReportCallbackEXT, sizeof(VkAllocationCallbacks));
    mid(instance)->instTable.DestroyDebugReportCallbackEXT(instance, callback, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyDebugReportCallbackEXT(pHeader);
    pPacket->instance = instance;
    pPacket->callback = callback;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDebugReportMessageEXT(
    VkInstance instance,
    VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t object,
    size_t location,
    int32_t messageCode,
    const char* pLayerPrefix,
    const char* pMessage) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDebugReportMessageEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDebugReportMessageEXT, ((pLayerPrefix != NULL) ? ROUNDUP_TO_4(strlen(pLayerPrefix) + 1) : 0) + ((pMessage != NULL) ? ROUNDUP_TO_4(strlen(pMessage) + 1) : 0));
    mid(instance)->instTable.DebugReportMessageEXT(instance, flags, objectType, object, location, messageCode, pLayerPrefix, pMessage);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDebugReportMessageEXT(pHeader);
    pPacket->instance = instance;
    pPacket->flags = flags;
    pPacket->objectType = objectType;
    pPacket->object = object;
    pPacket->location = location;
    pPacket->messageCode = messageCode;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pLayerPrefix), sizeof(char), pLayerPrefix);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMessage), sizeof(char), pMessage);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pLayerPrefix));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMessage));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkDebugMarkerSetObjectTagEXT(
    VkDevice device,
    const VkDebugMarkerObjectTagInfoEXT* pTagInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkDebugMarkerSetObjectTagEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDebugMarkerSetObjectTagEXT, get_struct_chain_size((void*)pTagInfo));
    result = mdd(device)->devTable.DebugMarkerSetObjectTagEXT(device, pTagInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDebugMarkerSetObjectTagEXT(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pTagInfo), sizeof(VkDebugMarkerObjectTagInfoEXT), pTagInfo);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pTagInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkDebugMarkerSetObjectNameEXT(
    VkDevice device,
    const VkDebugMarkerObjectNameInfoEXT* pNameInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkDebugMarkerSetObjectNameEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDebugMarkerSetObjectNameEXT, get_struct_chain_size((void*)pNameInfo));
    result = mdd(device)->devTable.DebugMarkerSetObjectNameEXT(device, pNameInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDebugMarkerSetObjectNameEXT(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pNameInfo), sizeof(VkDebugMarkerObjectNameInfoEXT), pNameInfo);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pNameInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDebugMarkerBeginEXT(
    VkCommandBuffer commandBuffer,
    const VkDebugMarkerMarkerInfoEXT* pMarkerInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDebugMarkerBeginEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDebugMarkerBeginEXT, get_struct_chain_size((void*)pMarkerInfo));
    mdd(commandBuffer)->devTable.CmdDebugMarkerBeginEXT(commandBuffer, pMarkerInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDebugMarkerBeginEXT(pHeader);
    pPacket->commandBuffer = commandBuffer;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMarkerInfo), sizeof(VkDebugMarkerMarkerInfoEXT), pMarkerInfo);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMarkerInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDebugMarkerEndEXT(
    VkCommandBuffer commandBuffer) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDebugMarkerEndEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDebugMarkerEndEXT, 0);
    mdd(commandBuffer)->devTable.CmdDebugMarkerEndEXT(commandBuffer);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDebugMarkerEndEXT(pHeader);
    pPacket->commandBuffer = commandBuffer;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDebugMarkerInsertEXT(
    VkCommandBuffer commandBuffer,
    const VkDebugMarkerMarkerInfoEXT* pMarkerInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDebugMarkerInsertEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDebugMarkerInsertEXT, get_struct_chain_size((void*)pMarkerInfo));
    mdd(commandBuffer)->devTable.CmdDebugMarkerInsertEXT(commandBuffer, pMarkerInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDebugMarkerInsertEXT(pHeader);
    pPacket->commandBuffer = commandBuffer;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMarkerInfo), sizeof(VkDebugMarkerMarkerInfoEXT), pMarkerInfo);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMarkerInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// TODO: Add support for __HOOKED_vkCmdBindTransformFeedbackBuffersEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdBeginTransformFeedbackEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdEndTransformFeedbackEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdBeginQueryIndexedEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdEndQueryIndexedEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdDrawIndirectByteCountEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkGetImageViewHandleNVX: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDrawIndirectCountAMD(
    VkCommandBuffer commandBuffer,
    VkBuffer buffer,
    VkDeviceSize offset,
    VkBuffer countBuffer,
    VkDeviceSize countBufferOffset,
    uint32_t maxDrawCount,
    uint32_t stride) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDrawIndirectCountAMD* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDrawIndirectCountAMD, 0);
    mdd(commandBuffer)->devTable.CmdDrawIndirectCountAMD(commandBuffer, buffer, offset, countBuffer, countBufferOffset, maxDrawCount, stride);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDrawIndirectCountAMD(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->buffer = buffer;
    pPacket->offset = offset;
    pPacket->countBuffer = countBuffer;
    pPacket->countBufferOffset = countBufferOffset;
    pPacket->maxDrawCount = maxDrawCount;
    pPacket->stride = stride;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdDrawIndexedIndirectCountAMD(
    VkCommandBuffer commandBuffer,
    VkBuffer buffer,
    VkDeviceSize offset,
    VkBuffer countBuffer,
    VkDeviceSize countBufferOffset,
    uint32_t maxDrawCount,
    uint32_t stride) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdDrawIndexedIndirectCountAMD* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdDrawIndexedIndirectCountAMD, 0);
    mdd(commandBuffer)->devTable.CmdDrawIndexedIndirectCountAMD(commandBuffer, buffer, offset, countBuffer, countBufferOffset, maxDrawCount, stride);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdDrawIndexedIndirectCountAMD(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->buffer = buffer;
    pPacket->offset = offset;
    pPacket->countBuffer = countBuffer;
    pPacket->countBufferOffset = countBufferOffset;
    pPacket->maxDrawCount = maxDrawCount;
    pPacket->stride = stride;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetShaderInfoAMD(
    VkDevice device,
    VkPipeline pipeline,
    VkShaderStageFlagBits shaderStage,
    VkShaderInfoTypeAMD infoType,
    size_t* pInfoSize,
    void* pInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetShaderInfoAMD* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetShaderInfoAMD, sizeof(size_t) + (*pInfoSize));
    result = mdd(device)->devTable.GetShaderInfoAMD(device, pipeline, shaderStage, infoType, pInfoSize, pInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetShaderInfoAMD(pHeader);
    pPacket->device = device;
    pPacket->pipeline = pipeline;
    pPacket->shaderStage = shaderStage;
    pPacket->infoType = infoType;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pInfoSize), sizeof(size_t), pInfoSize);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pInfo), (*pInfoSize), pInfo);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pInfoSize));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// TODO: Add support for __HOOKED_vkCreateStreamDescriptorSurfaceGGP: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceExternalImageFormatPropertiesNV(
    VkPhysicalDevice physicalDevice,
    VkFormat format,
    VkImageType type,
    VkImageTiling tiling,
    VkImageUsageFlags usage,
    VkImageCreateFlags flags,
    VkExternalMemoryHandleTypeFlagsNV externalHandleType,
    VkExternalImageFormatPropertiesNV* pExternalImageFormatProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceExternalImageFormatPropertiesNV* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceExternalImageFormatPropertiesNV, sizeof(VkExternalImageFormatPropertiesNV));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceExternalImageFormatPropertiesNV(physicalDevice, format, type, tiling, usage, flags, externalHandleType, pExternalImageFormatProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceExternalImageFormatPropertiesNV(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->format = format;
    pPacket->type = type;
    pPacket->tiling = tiling;
    pPacket->usage = usage;
    pPacket->flags = flags;
    pPacket->externalHandleType = externalHandleType;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pExternalImageFormatProperties), sizeof(VkExternalImageFormatPropertiesNV), pExternalImageFormatProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pExternalImageFormatProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
#ifdef VK_USE_PLATFORM_WIN32_KHR
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetMemoryWin32HandleNV(
    VkDevice device,
    VkDeviceMemory memory,
    VkExternalMemoryHandleTypeFlagsNV handleType,
    HANDLE* pHandle) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetMemoryWin32HandleNV* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetMemoryWin32HandleNV, sizeof(HANDLE));
    result = mdd(device)->devTable.GetMemoryWin32HandleNV(device, memory, handleType, pHandle);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetMemoryWin32HandleNV(pHeader);
    pPacket->device = device;
    pPacket->memory = memory;
    pPacket->handleType = handleType;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pHandle), sizeof(HANDLE), pHandle);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pHandle));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
#endif // VK_USE_PLATFORM_WIN32_KHR
// TODO: Add support for __HOOKED_vkCreateViSurfaceNN: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdBeginConditionalRenderingEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdEndConditionalRenderingEXT: Skipping for now.
// __HOOKED_vkCmdProcessCommandsNVX is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdProcessCommandsNVX(
    VkCommandBuffer commandBuffer,
    const VkCmdProcessCommandsInfoNVX* pProcessCommandsInfo);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdReserveSpaceForCommandsNVX(
    VkCommandBuffer commandBuffer,
    const VkCmdReserveSpaceForCommandsInfoNVX* pReserveSpaceInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdReserveSpaceForCommandsNVX* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdReserveSpaceForCommandsNVX, get_struct_chain_size((void*)pReserveSpaceInfo));
    mdd(commandBuffer)->devTable.CmdReserveSpaceForCommandsNVX(commandBuffer, pReserveSpaceInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdReserveSpaceForCommandsNVX(pHeader);
    pPacket->commandBuffer = commandBuffer;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pReserveSpaceInfo), sizeof(VkCmdReserveSpaceForCommandsInfoNVX), pReserveSpaceInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pReserveSpaceInfo, (void *)pReserveSpaceInfo);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pReserveSpaceInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// __HOOKED_vkCreateIndirectCommandsLayoutNVX is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateIndirectCommandsLayoutNVX(
    VkDevice device,
    const VkIndirectCommandsLayoutCreateInfoNVX* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkIndirectCommandsLayoutNVX* pIndirectCommandsLayout);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyIndirectCommandsLayoutNVX(
    VkDevice device,
    VkIndirectCommandsLayoutNVX indirectCommandsLayout,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyIndirectCommandsLayoutNVX* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyIndirectCommandsLayoutNVX, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyIndirectCommandsLayoutNVX(device, indirectCommandsLayout, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyIndirectCommandsLayoutNVX(pHeader);
    pPacket->device = device;
    pPacket->indirectCommandsLayout = indirectCommandsLayout;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// __HOOKED_vkCreateObjectTableNVX is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateObjectTableNVX(
    VkDevice device,
    const VkObjectTableCreateInfoNVX* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkObjectTableNVX* pObjectTable);
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyObjectTableNVX(
    VkDevice device,
    VkObjectTableNVX objectTable,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyObjectTableNVX* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyObjectTableNVX, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyObjectTableNVX(device, objectTable, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyObjectTableNVX(pHeader);
    pPacket->device = device;
    pPacket->objectTable = objectTable;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkRegisterObjectsNVX(
    VkDevice device,
    VkObjectTableNVX objectTable,
    uint32_t objectCount,
    const VkObjectTableEntryNVX* const*    ppObjectTableEntries,
    const uint32_t* pObjectIndices) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkRegisterObjectsNVX* pPacket = NULL;
    CREATE_TRACE_PACKET(vkRegisterObjectsNVX, objectCount * sizeof(VkObjectTableEntryNVX) + objectCount * sizeof(uint32_t));
    result = mdd(device)->devTable.RegisterObjectsNVX(device, objectTable, objectCount, ppObjectTableEntries, pObjectIndices);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkRegisterObjectsNVX(pHeader);
    pPacket->device = device;
    pPacket->objectTable = objectTable;
    pPacket->objectCount = objectCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->ppObjectTableEntries), objectCount * sizeof(VkObjectTableEntryNVX), ppObjectTableEntries);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pObjectIndices), objectCount * sizeof(uint32_t), pObjectIndices);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->ppObjectTableEntries));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pObjectIndices));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkUnregisterObjectsNVX(
    VkDevice device,
    VkObjectTableNVX objectTable,
    uint32_t objectCount,
    const VkObjectEntryTypeNVX* pObjectEntryTypes,
    const uint32_t* pObjectIndices) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkUnregisterObjectsNVX* pPacket = NULL;
    CREATE_TRACE_PACKET(vkUnregisterObjectsNVX, objectCount * sizeof(VkObjectEntryTypeNVX) + objectCount * sizeof(uint32_t));
    result = mdd(device)->devTable.UnregisterObjectsNVX(device, objectTable, objectCount, pObjectEntryTypes, pObjectIndices);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkUnregisterObjectsNVX(pHeader);
    pPacket->device = device;
    pPacket->objectTable = objectTable;
    pPacket->objectCount = objectCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pObjectEntryTypes), objectCount * sizeof(VkObjectEntryTypeNVX), pObjectEntryTypes);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pObjectIndices), objectCount * sizeof(uint32_t), pObjectIndices);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pObjectEntryTypes));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pObjectIndices));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX(
    VkPhysicalDevice physicalDevice,
    VkDeviceGeneratedCommandsFeaturesNVX* pFeatures,
    VkDeviceGeneratedCommandsLimitsNVX* pLimits) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX, get_struct_chain_size((void*)pFeatures) + get_struct_chain_size((void*)pLimits));
    mid(physicalDevice)->instTable.GetPhysicalDeviceGeneratedCommandsPropertiesNVX(physicalDevice, pFeatures, pLimits);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX(pHeader);
    pPacket->physicalDevice = physicalDevice;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFeatures), sizeof(VkDeviceGeneratedCommandsFeaturesNVX), pFeatures);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pFeatures, (void *)pFeatures);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pLimits), sizeof(VkDeviceGeneratedCommandsLimitsNVX), pLimits);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pLimits, (void *)pLimits);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFeatures));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pLimits));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetViewportWScalingNV(
    VkCommandBuffer commandBuffer,
    uint32_t firstViewport,
    uint32_t viewportCount,
    const VkViewportWScalingNV* pViewportWScalings) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetViewportWScalingNV* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetViewportWScalingNV, viewportCount * sizeof(VkViewportWScalingNV));
    mdd(commandBuffer)->devTable.CmdSetViewportWScalingNV(commandBuffer, firstViewport, viewportCount, pViewportWScalings);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetViewportWScalingNV(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->firstViewport = firstViewport;
    pPacket->viewportCount = viewportCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pViewportWScalings), viewportCount * sizeof(VkViewportWScalingNV), pViewportWScalings);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pViewportWScalings));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkReleaseDisplayEXT(
    VkPhysicalDevice physicalDevice,
    VkDisplayKHR display) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkReleaseDisplayEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkReleaseDisplayEXT, 0);
    result = mid(physicalDevice)->instTable.ReleaseDisplayEXT(physicalDevice, display);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkReleaseDisplayEXT(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->display = display;
    pPacket->result = result;
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// __HOOKED_vkAcquireXlibDisplayEXT is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_XLIB_XRANDR_EXT
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkAcquireXlibDisplayEXT(
    VkPhysicalDevice physicalDevice,
    Display* dpy,
    VkDisplayKHR display);
#endif // VK_USE_PLATFORM_XLIB_XRANDR_EXT
// __HOOKED_vkGetRandROutputDisplayEXT is manually written. Look in vktrace_lib_trace.cpp. Stub for proc mapping function.
#ifdef VK_USE_PLATFORM_XLIB_XRANDR_EXT
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetRandROutputDisplayEXT(
    VkPhysicalDevice physicalDevice,
    Display* dpy,
    RROutput rrOutput,
    VkDisplayKHR* pDisplay);
#endif // VK_USE_PLATFORM_XLIB_XRANDR_EXT
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPhysicalDeviceSurfaceCapabilities2EXT(
    VkPhysicalDevice physicalDevice,
    VkSurfaceKHR surface,
    VkSurfaceCapabilities2EXT* pSurfaceCapabilities) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceSurfaceCapabilities2EXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceSurfaceCapabilities2EXT, get_struct_chain_size((void*)pSurfaceCapabilities));
    result = mid(physicalDevice)->instTable.GetPhysicalDeviceSurfaceCapabilities2EXT(physicalDevice, surface, pSurfaceCapabilities);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceSurfaceCapabilities2EXT(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->surface = surface;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSurfaceCapabilities), sizeof(VkSurfaceCapabilities2EXT), pSurfaceCapabilities);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSurfaceCapabilities));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkDisplayPowerControlEXT(
    VkDevice device,
    VkDisplayKHR display,
    const VkDisplayPowerInfoEXT* pDisplayPowerInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkDisplayPowerControlEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDisplayPowerControlEXT, get_struct_chain_size((void*)pDisplayPowerInfo));
    result = mdd(device)->devTable.DisplayPowerControlEXT(device, display, pDisplayPowerInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDisplayPowerControlEXT(pHeader);
    pPacket->device = device;
    pPacket->display = display;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDisplayPowerInfo), sizeof(VkDisplayPowerInfoEXT), pDisplayPowerInfo);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDisplayPowerInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkRegisterDeviceEventEXT(
    VkDevice device,
    const VkDeviceEventInfoEXT* pDeviceEventInfo,
    const VkAllocationCallbacks* pAllocator,
    VkFence* pFence) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkRegisterDeviceEventEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkRegisterDeviceEventEXT, get_struct_chain_size((void*)pDeviceEventInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkFence));
    result = mdd(device)->devTable.RegisterDeviceEventEXT(device, pDeviceEventInfo, pAllocator, pFence);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkRegisterDeviceEventEXT(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDeviceEventInfo), sizeof(VkDeviceEventInfoEXT), pDeviceEventInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFence), sizeof(VkFence), pFence);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDeviceEventInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFence));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkRegisterDisplayEventEXT(
    VkDevice device,
    VkDisplayKHR display,
    const VkDisplayEventInfoEXT* pDisplayEventInfo,
    const VkAllocationCallbacks* pAllocator,
    VkFence* pFence) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkRegisterDisplayEventEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkRegisterDisplayEventEXT, get_struct_chain_size((void*)pDisplayEventInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkFence));
    result = mdd(device)->devTable.RegisterDisplayEventEXT(device, display, pDisplayEventInfo, pAllocator, pFence);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkRegisterDisplayEventEXT(pHeader);
    pPacket->device = device;
    pPacket->display = display;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDisplayEventInfo), sizeof(VkDisplayEventInfoEXT), pDisplayEventInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pFence), sizeof(VkFence), pFence);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDisplayEventInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pFence));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetSwapchainCounterEXT(
    VkDevice device,
    VkSwapchainKHR swapchain,
    VkSurfaceCounterFlagBitsEXT counter,
    uint64_t* pCounterValue) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetSwapchainCounterEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetSwapchainCounterEXT, sizeof(uint64_t));
    result = mdd(device)->devTable.GetSwapchainCounterEXT(device, swapchain, counter, pCounterValue);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetSwapchainCounterEXT(pHeader);
    pPacket->device = device;
    pPacket->swapchain = swapchain;
    pPacket->counter = counter;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCounterValue), sizeof(uint64_t), pCounterValue);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCounterValue));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetRefreshCycleDurationGOOGLE(
    VkDevice device,
    VkSwapchainKHR swapchain,
    VkRefreshCycleDurationGOOGLE* pDisplayTimingProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetRefreshCycleDurationGOOGLE* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetRefreshCycleDurationGOOGLE, sizeof(VkRefreshCycleDurationGOOGLE));
    result = mdd(device)->devTable.GetRefreshCycleDurationGOOGLE(device, swapchain, pDisplayTimingProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetRefreshCycleDurationGOOGLE(pHeader);
    pPacket->device = device;
    pPacket->swapchain = swapchain;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDisplayTimingProperties), sizeof(VkRefreshCycleDurationGOOGLE), pDisplayTimingProperties);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDisplayTimingProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetPastPresentationTimingGOOGLE(
    VkDevice device,
    VkSwapchainKHR swapchain,
    uint32_t* pPresentationTimingCount,
    VkPastPresentationTimingGOOGLE* pPresentationTimings) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPastPresentationTimingGOOGLE* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPastPresentationTimingGOOGLE, sizeof(uint32_t) + (*pPresentationTimingCount) * sizeof(VkPastPresentationTimingGOOGLE));
    result = mdd(device)->devTable.GetPastPresentationTimingGOOGLE(device, swapchain, pPresentationTimingCount, pPresentationTimings);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPastPresentationTimingGOOGLE(pHeader);
    pPacket->device = device;
    pPacket->swapchain = swapchain;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPresentationTimingCount), sizeof(uint32_t), pPresentationTimingCount);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pPresentationTimings), (*pPresentationTimingCount) * sizeof(VkPastPresentationTimingGOOGLE), pPresentationTimings);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPresentationTimingCount));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pPresentationTimings));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetDiscardRectangleEXT(
    VkCommandBuffer commandBuffer,
    uint32_t firstDiscardRectangle,
    uint32_t discardRectangleCount,
    const VkRect2D* pDiscardRectangles) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetDiscardRectangleEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetDiscardRectangleEXT, discardRectangleCount * sizeof(VkRect2D));
    mdd(commandBuffer)->devTable.CmdSetDiscardRectangleEXT(commandBuffer, firstDiscardRectangle, discardRectangleCount, pDiscardRectangles);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetDiscardRectangleEXT(pHeader);
    pPacket->commandBuffer = commandBuffer;
    pPacket->firstDiscardRectangle = firstDiscardRectangle;
    pPacket->discardRectangleCount = discardRectangleCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDiscardRectangles), discardRectangleCount * sizeof(VkRect2D), pDiscardRectangles);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDiscardRectangles));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkSetHdrMetadataEXT(
    VkDevice device,
    uint32_t swapchainCount,
    const VkSwapchainKHR* pSwapchains,
    const VkHdrMetadataEXT* pMetadata) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkSetHdrMetadataEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkSetHdrMetadataEXT, swapchainCount * sizeof(VkSwapchainKHR) + get_struct_chain_size((void*)pMetadata));
    mdd(device)->devTable.SetHdrMetadataEXT(device, swapchainCount, pSwapchains, pMetadata);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkSetHdrMetadataEXT(pHeader);
    pPacket->device = device;
    pPacket->swapchainCount = swapchainCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSwapchains), swapchainCount * sizeof(VkSwapchainKHR), pSwapchains);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMetadata), swapchainCount * sizeof(VkHdrMetadataEXT), pMetadata);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSwapchains));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMetadata));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// TODO: Add support for __HOOKED_vkCreateIOSSurfaceMVK: Skipping for now.
// TODO: Add support for __HOOKED_vkCreateMacOSSurfaceMVK: Skipping for now.
// TODO: Add support for __HOOKED_vkSetDebugUtilsObjectNameEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkSetDebugUtilsObjectTagEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkQueueBeginDebugUtilsLabelEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkQueueEndDebugUtilsLabelEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkQueueInsertDebugUtilsLabelEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdBeginDebugUtilsLabelEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdEndDebugUtilsLabelEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdInsertDebugUtilsLabelEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCreateDebugUtilsMessengerEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkDestroyDebugUtilsMessengerEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkSubmitDebugUtilsMessageEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkGetAndroidHardwareBufferPropertiesANDROID: Skipping for now.
// TODO: Add support for __HOOKED_vkGetMemoryAndroidHardwareBufferANDROID: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkCmdSetSampleLocationsEXT(
    VkCommandBuffer commandBuffer,
    const VkSampleLocationsInfoEXT* pSampleLocationsInfo) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkCmdSetSampleLocationsEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCmdSetSampleLocationsEXT, sizeof(VkSampleLocationsInfoEXT));
    mdd(commandBuffer)->devTable.CmdSetSampleLocationsEXT(commandBuffer, pSampleLocationsInfo);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCmdSetSampleLocationsEXT(pHeader);
    pPacket->commandBuffer = commandBuffer;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSampleLocationsInfo), sizeof(VkSampleLocationsInfoEXT), pSampleLocationsInfo);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSampleLocationsInfo));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkGetPhysicalDeviceMultisamplePropertiesEXT(
    VkPhysicalDevice physicalDevice,
    VkSampleCountFlagBits samples,
    VkMultisamplePropertiesEXT* pMultisampleProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkGetPhysicalDeviceMultisamplePropertiesEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetPhysicalDeviceMultisamplePropertiesEXT, sizeof(VkMultisamplePropertiesEXT));
    mid(physicalDevice)->instTable.GetPhysicalDeviceMultisamplePropertiesEXT(physicalDevice, samples, pMultisampleProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetPhysicalDeviceMultisamplePropertiesEXT(pHeader);
    pPacket->physicalDevice = physicalDevice;
    pPacket->samples = samples;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMultisampleProperties), sizeof(VkMultisamplePropertiesEXT), pMultisampleProperties);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMultisampleProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
// TODO: Add support for __HOOKED_vkGetImageDrmFormatModifierPropertiesEXT: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkCreateValidationCacheEXT(
    VkDevice device,
    const VkValidationCacheCreateInfoEXT* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkValidationCacheEXT* pValidationCache) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkCreateValidationCacheEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkCreateValidationCacheEXT, get_struct_chain_size((void*)pCreateInfo) + sizeof(VkAllocationCallbacks) + sizeof(VkValidationCacheEXT));
    result = mdd(device)->devTable.CreateValidationCacheEXT(device, pCreateInfo, pAllocator, pValidationCache);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkCreateValidationCacheEXT(pHeader);
    pPacket->device = device;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pCreateInfo), sizeof(VkValidationCacheCreateInfoEXT), pCreateInfo);
    vktrace_add_pnext_structs_to_trace_packet(pHeader, (void *)pPacket->pCreateInfo, (void *)pCreateInfo);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pValidationCache), sizeof(VkValidationCacheEXT), pValidationCache);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pCreateInfo));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pValidationCache));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR void VKAPI_CALL __HOOKED_vkDestroyValidationCacheEXT(
    VkDevice device,
    VkValidationCacheEXT validationCache,
    const VkAllocationCallbacks* pAllocator) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    vktrace_trace_packet_header* pHeader;
    packet_vkDestroyValidationCacheEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkDestroyValidationCacheEXT, sizeof(VkAllocationCallbacks));
    mdd(device)->devTable.DestroyValidationCacheEXT(device, validationCache, pAllocator);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkDestroyValidationCacheEXT(pHeader);
    pPacket->device = device;
    pPacket->validationCache = validationCache;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pAllocator), sizeof(VkAllocationCallbacks), NULL);
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pAllocator));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkMergeValidationCachesEXT(
    VkDevice device,
    VkValidationCacheEXT dstCache,
    uint32_t srcCacheCount,
    const VkValidationCacheEXT* pSrcCaches) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkMergeValidationCachesEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkMergeValidationCachesEXT, srcCacheCount * sizeof(VkValidationCacheEXT));
    result = mdd(device)->devTable.MergeValidationCachesEXT(device, dstCache, srcCacheCount, pSrcCaches);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkMergeValidationCachesEXT(pHeader);
    pPacket->device = device;
    pPacket->dstCache = dstCache;
    pPacket->srcCacheCount = srcCacheCount;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pSrcCaches), srcCacheCount * sizeof(VkValidationCacheEXT), pSrcCaches);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pSrcCaches));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetValidationCacheDataEXT(
    VkDevice device,
    VkValidationCacheEXT validationCache,
    size_t* pDataSize,
    void* pData) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    size_t _dataSize;
    packet_vkGetValidationCacheDataEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetValidationCacheDataEXT, ((pDataSize != NULL) ? sizeof(size_t) : 0) + (*pDataSize));
    result = mdd(device)->devTable.GetValidationCacheDataEXT(device, validationCache, pDataSize, pData);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    _dataSize = (pDataSize == NULL || pData == NULL) ? 0 : *pDataSize;
    pPacket = interpret_body_as_vkGetValidationCacheDataEXT(pHeader);
    pPacket->device = device;
    pPacket->validationCache = validationCache;
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pDataSize), sizeof(size_t), &_dataSize);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pData), (*pDataSize), pData);
    pPacket->result = result;
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pDataSize));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pData));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// TODO: Add support for __HOOKED_vkCmdBindShadingRateImageNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdSetViewportShadingRatePaletteNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdSetCoarseSampleOrderNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCreateAccelerationStructureNV: Skipping for now.
// TODO: Add support for __HOOKED_vkDestroyAccelerationStructureNV: Skipping for now.
// TODO: Add support for __HOOKED_vkGetAccelerationStructureMemoryRequirementsNV: Skipping for now.
// TODO: Add support for __HOOKED_vkBindAccelerationStructureMemoryNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdBuildAccelerationStructureNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdCopyAccelerationStructureNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdTraceRaysNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCreateRayTracingPipelinesNV: Skipping for now.
// TODO: Add support for __HOOKED_vkGetRayTracingShaderGroupHandlesNV: Skipping for now.
// TODO: Add support for __HOOKED_vkGetAccelerationStructureHandleNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdWriteAccelerationStructuresPropertiesNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCompileDeferredNV: Skipping for now.
VKTRACER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL __HOOKED_vkGetMemoryHostPointerPropertiesEXT(
    VkDevice device,
    VkExternalMemoryHandleTypeFlagBits handleType,
    const void* pHostPointer,
    VkMemoryHostPointerPropertiesEXT* pMemoryHostPointerProperties) {
    trim::TraceLock<std::mutex> lock(g_mutex_trace);
    VkResult result;
    vktrace_trace_packet_header* pHeader;
    packet_vkGetMemoryHostPointerPropertiesEXT* pPacket = NULL;
    CREATE_TRACE_PACKET(vkGetMemoryHostPointerPropertiesEXT, sizeof(pHostPointer) + sizeof(VkMemoryHostPointerPropertiesEXT));
    result = mdd(device)->devTable.GetMemoryHostPointerPropertiesEXT(device, handleType, pHostPointer, pMemoryHostPointerProperties);
    vktrace_set_packet_entrypoint_end_time(pHeader);
    pPacket = interpret_body_as_vkGetMemoryHostPointerPropertiesEXT(pHeader);
    pPacket->device = device;
    pPacket->handleType = handleType;
    //TODO FIXME vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pHostPointer), sizeof(void), pHostPointer);
    vktrace_add_buffer_to_trace_packet(pHeader, (void**)&(pPacket->pMemoryHostPointerProperties), sizeof(VkMemoryHostPointerPropertiesEXT), pMemoryHostPointerProperties);
    pPacket->result = result;
    //TODO FIXME vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pHostPointer));
    vktrace_finalize_buffer_address(pHeader, (void**)&(pPacket->pMemoryHostPointerProperties));
    if (!g_trimEnabled) {
        FINISH_TRACE_PACKET();
    } else {
        vktrace_finalize_trace_packet(pHeader);
        if (g_trimIsInTrim) {
            trim::write_packet(pHeader);
        } else {
            vktrace_delete_trace_packet(&pHeader);
        }
    }
    return result;
}
// TODO: Add support for __HOOKED_vkCmdWriteBufferMarkerAMD: Skipping for now.
// TODO: Add support for __HOOKED_vkGetPhysicalDeviceCalibrateableTimeDomainsEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkGetCalibratedTimestampsEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdDrawMeshTasksNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdDrawMeshTasksIndirectNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdDrawMeshTasksIndirectCountNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdSetExclusiveScissorNV: Skipping for now.
// TODO: Add support for __HOOKED_vkCmdSetCheckpointNV: Skipping for now.
// TODO: Add support for __HOOKED_vkGetQueueCheckpointDataNV: Skipping for now.
// TODO: Add support for __HOOKED_vkSetLocalDimmingAMD: Skipping for now.
// TODO: Add support for __HOOKED_vkCreateImagePipeSurfaceFUCHSIA: Skipping for now.
// TODO: Add support for __HOOKED_vkCreateMetalSurfaceEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkGetBufferDeviceAddressEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkGetPhysicalDeviceCooperativeMatrixPropertiesNV: Skipping for now.
// TODO: Add support for __HOOKED_vkGetPhysicalDeviceSurfacePresentModes2EXT: Skipping for now.
// TODO: Add support for __HOOKED_vkAcquireFullScreenExclusiveModeEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkReleaseFullScreenExclusiveModeEXT: Skipping for now.
// TODO: Add support for __HOOKED_vkResetQueryPoolEXT: Skipping for now.
PFN_vkVoidFunction layer_intercept_instance_proc(const char* name) {
    if (!name || name[0] != 'v' || name[1] != 'k') return NULL;
    name += 2;
   if (!strcmp(name, "CreateInstance")) return (PFN_vkVoidFunction)__HOOKED_vkCreateInstance;
   if (!strcmp(name, "DestroyInstance")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyInstance;
   if (!strcmp(name, "EnumeratePhysicalDevices")) return (PFN_vkVoidFunction)__HOOKED_vkEnumeratePhysicalDevices;
   if (!strcmp(name, "GetPhysicalDeviceFeatures")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceFeatures;
   if (!strcmp(name, "GetPhysicalDeviceFormatProperties")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceFormatProperties;
   if (!strcmp(name, "GetPhysicalDeviceImageFormatProperties")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceImageFormatProperties;
   if (!strcmp(name, "GetPhysicalDeviceProperties")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceProperties;
   if (!strcmp(name, "GetPhysicalDeviceQueueFamilyProperties")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceQueueFamilyProperties;
   if (!strcmp(name, "GetPhysicalDeviceMemoryProperties")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceMemoryProperties;
   if (!strcmp(name, "GetInstanceProcAddr")) return (PFN_vkVoidFunction)__HOOKED_vkGetInstanceProcAddr;
   if (!strcmp(name, "CreateDevice")) return (PFN_vkVoidFunction)__HOOKED_vkCreateDevice;
   if (!strcmp(name, "EnumerateInstanceExtensionProperties")) return (PFN_vkVoidFunction)__HOOKED_vkEnumerateInstanceExtensionProperties;
   if (!strcmp(name, "EnumerateDeviceExtensionProperties")) return (PFN_vkVoidFunction)__HOOKED_vkEnumerateDeviceExtensionProperties;
   if (!strcmp(name, "EnumerateInstanceLayerProperties")) return (PFN_vkVoidFunction)__HOOKED_vkEnumerateInstanceLayerProperties;
   if (!strcmp(name, "EnumerateDeviceLayerProperties")) return (PFN_vkVoidFunction)__HOOKED_vkEnumerateDeviceLayerProperties;
   if (!strcmp(name, "GetPhysicalDeviceSparseImageFormatProperties")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSparseImageFormatProperties;
   if (!strcmp(name, "EnumeratePhysicalDeviceGroups")) return (PFN_vkVoidFunction)__HOOKED_vkEnumeratePhysicalDeviceGroups;
   if (!strcmp(name, "GetPhysicalDeviceFeatures2")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceFeatures2;
   if (!strcmp(name, "GetPhysicalDeviceProperties2")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceProperties2;
   if (!strcmp(name, "GetPhysicalDeviceFormatProperties2")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceFormatProperties2;
   if (!strcmp(name, "GetPhysicalDeviceImageFormatProperties2")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceImageFormatProperties2;
   if (!strcmp(name, "GetPhysicalDeviceQueueFamilyProperties2")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceQueueFamilyProperties2;
   if (!strcmp(name, "GetPhysicalDeviceMemoryProperties2")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceMemoryProperties2;
   if (!strcmp(name, "GetPhysicalDeviceSparseImageFormatProperties2")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSparseImageFormatProperties2;
   if (!strcmp(name, "GetPhysicalDeviceExternalBufferProperties")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceExternalBufferProperties;
   if (!strcmp(name, "GetPhysicalDeviceExternalFenceProperties")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceExternalFenceProperties;
   if (!strcmp(name, "GetPhysicalDeviceExternalSemaphoreProperties")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceExternalSemaphoreProperties;
   if (!strcmp(name, "DestroySurfaceKHR")) return (PFN_vkVoidFunction)__HOOKED_vkDestroySurfaceKHR;
   if (!strcmp(name, "GetPhysicalDeviceSurfaceSupportKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSurfaceSupportKHR;
   if (!strcmp(name, "GetPhysicalDeviceSurfaceCapabilitiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSurfaceCapabilitiesKHR;
   if (!strcmp(name, "GetPhysicalDeviceSurfaceFormatsKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSurfaceFormatsKHR;
   if (!strcmp(name, "GetPhysicalDeviceSurfacePresentModesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSurfacePresentModesKHR;
   if (!strcmp(name, "GetPhysicalDevicePresentRectanglesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDevicePresentRectanglesKHR;
   if (!strcmp(name, "GetPhysicalDeviceDisplayPropertiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceDisplayPropertiesKHR;
   if (!strcmp(name, "GetPhysicalDeviceDisplayPlanePropertiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceDisplayPlanePropertiesKHR;
   if (!strcmp(name, "GetDisplayPlaneSupportedDisplaysKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetDisplayPlaneSupportedDisplaysKHR;
   if (!strcmp(name, "GetDisplayModePropertiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetDisplayModePropertiesKHR;
   if (!strcmp(name, "CreateDisplayModeKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateDisplayModeKHR;
   if (!strcmp(name, "GetDisplayPlaneCapabilitiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetDisplayPlaneCapabilitiesKHR;
   if (!strcmp(name, "CreateDisplayPlaneSurfaceKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateDisplayPlaneSurfaceKHR;
#ifdef VK_USE_PLATFORM_XLIB_KHR
   if (!strcmp(name, "CreateXlibSurfaceKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateXlibSurfaceKHR;
#endif // VK_USE_PLATFORM_XLIB_KHR
#ifdef VK_USE_PLATFORM_XLIB_KHR
   if (!strcmp(name, "GetPhysicalDeviceXlibPresentationSupportKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceXlibPresentationSupportKHR;
#endif // VK_USE_PLATFORM_XLIB_KHR
#ifdef VK_USE_PLATFORM_XCB_KHR
   if (!strcmp(name, "CreateXcbSurfaceKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateXcbSurfaceKHR;
#endif // VK_USE_PLATFORM_XCB_KHR
#ifdef VK_USE_PLATFORM_XCB_KHR
   if (!strcmp(name, "GetPhysicalDeviceXcbPresentationSupportKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceXcbPresentationSupportKHR;
#endif // VK_USE_PLATFORM_XCB_KHR
#ifdef VK_USE_PLATFORM_WAYLAND_KHR
   if (!strcmp(name, "CreateWaylandSurfaceKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateWaylandSurfaceKHR;
#endif // VK_USE_PLATFORM_WAYLAND_KHR
#ifdef VK_USE_PLATFORM_WAYLAND_KHR
   if (!strcmp(name, "GetPhysicalDeviceWaylandPresentationSupportKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceWaylandPresentationSupportKHR;
#endif // VK_USE_PLATFORM_WAYLAND_KHR
#ifdef VK_USE_PLATFORM_ANDROID_KHR
   if (!strcmp(name, "CreateAndroidSurfaceKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateAndroidSurfaceKHR;
#endif // VK_USE_PLATFORM_ANDROID_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "CreateWin32SurfaceKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateWin32SurfaceKHR;
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "GetPhysicalDeviceWin32PresentationSupportKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceWin32PresentationSupportKHR;
#endif // VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "GetPhysicalDeviceFeatures2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceFeatures2KHR;
   if (!strcmp(name, "GetPhysicalDeviceProperties2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceProperties2KHR;
   if (!strcmp(name, "GetPhysicalDeviceFormatProperties2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceFormatProperties2KHR;
   if (!strcmp(name, "GetPhysicalDeviceImageFormatProperties2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceImageFormatProperties2KHR;
   if (!strcmp(name, "GetPhysicalDeviceQueueFamilyProperties2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceQueueFamilyProperties2KHR;
   if (!strcmp(name, "GetPhysicalDeviceMemoryProperties2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceMemoryProperties2KHR;
   if (!strcmp(name, "GetPhysicalDeviceSparseImageFormatProperties2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSparseImageFormatProperties2KHR;
   if (!strcmp(name, "GetPhysicalDeviceExternalBufferPropertiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceExternalBufferPropertiesKHR;
   if (!strcmp(name, "GetPhysicalDeviceExternalSemaphorePropertiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR;
   if (!strcmp(name, "GetPhysicalDeviceExternalFencePropertiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceExternalFencePropertiesKHR;
   if (!strcmp(name, "GetPhysicalDeviceSurfaceCapabilities2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSurfaceCapabilities2KHR;
   if (!strcmp(name, "GetPhysicalDeviceSurfaceFormats2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSurfaceFormats2KHR;
   if (!strcmp(name, "GetPhysicalDeviceDisplayProperties2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceDisplayProperties2KHR;
   if (!strcmp(name, "GetPhysicalDeviceDisplayPlaneProperties2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceDisplayPlaneProperties2KHR;
   if (!strcmp(name, "GetDisplayModeProperties2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetDisplayModeProperties2KHR;
   if (!strcmp(name, "GetDisplayPlaneCapabilities2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetDisplayPlaneCapabilities2KHR;
   if (!strcmp(name, "CreateDebugReportCallbackEXT")) return (PFN_vkVoidFunction)__HOOKED_vkCreateDebugReportCallbackEXT;
   if (!strcmp(name, "DestroyDebugReportCallbackEXT")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyDebugReportCallbackEXT;
   if (!strcmp(name, "DebugReportMessageEXT")) return (PFN_vkVoidFunction)__HOOKED_vkDebugReportMessageEXT;
   if (!strcmp(name, "GetPhysicalDeviceExternalImageFormatPropertiesNV")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceExternalImageFormatPropertiesNV;
   if (!strcmp(name, "GetPhysicalDeviceGeneratedCommandsPropertiesNVX")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX;
   if (!strcmp(name, "ReleaseDisplayEXT")) return (PFN_vkVoidFunction)__HOOKED_vkReleaseDisplayEXT;
#ifdef VK_USE_PLATFORM_XLIB_XRANDR_EXT
   if (!strcmp(name, "AcquireXlibDisplayEXT")) return (PFN_vkVoidFunction)__HOOKED_vkAcquireXlibDisplayEXT;
#endif // VK_USE_PLATFORM_XLIB_XRANDR_EXT
#ifdef VK_USE_PLATFORM_XLIB_XRANDR_EXT
   if (!strcmp(name, "GetRandROutputDisplayEXT")) return (PFN_vkVoidFunction)__HOOKED_vkGetRandROutputDisplayEXT;
#endif // VK_USE_PLATFORM_XLIB_XRANDR_EXT
   if (!strcmp(name, "GetPhysicalDeviceSurfaceCapabilities2EXT")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceSurfaceCapabilities2EXT;
   if (!strcmp(name, "GetPhysicalDeviceMultisamplePropertiesEXT")) return (PFN_vkVoidFunction)__HOOKED_vkGetPhysicalDeviceMultisamplePropertiesEXT;
    return NULL;
}

PFN_vkVoidFunction layer_intercept_proc(const char* name) {
    if (!name || name[0] != 'v' || name[1] != 'k') return NULL;
    name += 2;
   if (!strcmp(name, "GetDeviceProcAddr")) return (PFN_vkVoidFunction)__HOOKED_vkGetDeviceProcAddr;
   if (!strcmp(name, "DestroyDevice")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyDevice;
   if (!strcmp(name, "GetDeviceQueue")) return (PFN_vkVoidFunction)__HOOKED_vkGetDeviceQueue;
   if (!strcmp(name, "QueueSubmit")) return (PFN_vkVoidFunction)__HOOKED_vkQueueSubmit;
   if (!strcmp(name, "QueueWaitIdle")) return (PFN_vkVoidFunction)__HOOKED_vkQueueWaitIdle;
   if (!strcmp(name, "DeviceWaitIdle")) return (PFN_vkVoidFunction)__HOOKED_vkDeviceWaitIdle;
   if (!strcmp(name, "AllocateMemory")) return (PFN_vkVoidFunction)__HOOKED_vkAllocateMemory;
   if (!strcmp(name, "FreeMemory")) return (PFN_vkVoidFunction)__HOOKED_vkFreeMemory;
   if (!strcmp(name, "MapMemory")) return (PFN_vkVoidFunction)__HOOKED_vkMapMemory;
   if (!strcmp(name, "UnmapMemory")) return (PFN_vkVoidFunction)__HOOKED_vkUnmapMemory;
   if (!strcmp(name, "FlushMappedMemoryRanges")) return (PFN_vkVoidFunction)__HOOKED_vkFlushMappedMemoryRanges;
   if (!strcmp(name, "InvalidateMappedMemoryRanges")) return (PFN_vkVoidFunction)__HOOKED_vkInvalidateMappedMemoryRanges;
   if (!strcmp(name, "GetDeviceMemoryCommitment")) return (PFN_vkVoidFunction)__HOOKED_vkGetDeviceMemoryCommitment;
   if (!strcmp(name, "BindBufferMemory")) return (PFN_vkVoidFunction)__HOOKED_vkBindBufferMemory;
   if (!strcmp(name, "BindImageMemory")) return (PFN_vkVoidFunction)__HOOKED_vkBindImageMemory;
   if (!strcmp(name, "GetBufferMemoryRequirements")) return (PFN_vkVoidFunction)__HOOKED_vkGetBufferMemoryRequirements;
   if (!strcmp(name, "GetImageMemoryRequirements")) return (PFN_vkVoidFunction)__HOOKED_vkGetImageMemoryRequirements;
   if (!strcmp(name, "GetImageSparseMemoryRequirements")) return (PFN_vkVoidFunction)__HOOKED_vkGetImageSparseMemoryRequirements;
   if (!strcmp(name, "QueueBindSparse")) return (PFN_vkVoidFunction)__HOOKED_vkQueueBindSparse;
   if (!strcmp(name, "CreateFence")) return (PFN_vkVoidFunction)__HOOKED_vkCreateFence;
   if (!strcmp(name, "DestroyFence")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyFence;
   if (!strcmp(name, "ResetFences")) return (PFN_vkVoidFunction)__HOOKED_vkResetFences;
   if (!strcmp(name, "GetFenceStatus")) return (PFN_vkVoidFunction)__HOOKED_vkGetFenceStatus;
   if (!strcmp(name, "WaitForFences")) return (PFN_vkVoidFunction)__HOOKED_vkWaitForFences;
   if (!strcmp(name, "CreateSemaphore")) return (PFN_vkVoidFunction)__HOOKED_vkCreateSemaphore;
   if (!strcmp(name, "DestroySemaphore")) return (PFN_vkVoidFunction)__HOOKED_vkDestroySemaphore;
   if (!strcmp(name, "CreateEvent")) return (PFN_vkVoidFunction)__HOOKED_vkCreateEvent;
   if (!strcmp(name, "DestroyEvent")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyEvent;
   if (!strcmp(name, "GetEventStatus")) return (PFN_vkVoidFunction)__HOOKED_vkGetEventStatus;
   if (!strcmp(name, "SetEvent")) return (PFN_vkVoidFunction)__HOOKED_vkSetEvent;
   if (!strcmp(name, "ResetEvent")) return (PFN_vkVoidFunction)__HOOKED_vkResetEvent;
   if (!strcmp(name, "CreateQueryPool")) return (PFN_vkVoidFunction)__HOOKED_vkCreateQueryPool;
   if (!strcmp(name, "DestroyQueryPool")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyQueryPool;
   if (!strcmp(name, "GetQueryPoolResults")) return (PFN_vkVoidFunction)__HOOKED_vkGetQueryPoolResults;
   if (!strcmp(name, "CreateBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkCreateBuffer;
   if (!strcmp(name, "DestroyBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyBuffer;
   if (!strcmp(name, "CreateBufferView")) return (PFN_vkVoidFunction)__HOOKED_vkCreateBufferView;
   if (!strcmp(name, "DestroyBufferView")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyBufferView;
   if (!strcmp(name, "CreateImage")) return (PFN_vkVoidFunction)__HOOKED_vkCreateImage;
   if (!strcmp(name, "DestroyImage")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyImage;
   if (!strcmp(name, "GetImageSubresourceLayout")) return (PFN_vkVoidFunction)__HOOKED_vkGetImageSubresourceLayout;
   if (!strcmp(name, "CreateImageView")) return (PFN_vkVoidFunction)__HOOKED_vkCreateImageView;
   if (!strcmp(name, "DestroyImageView")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyImageView;
   if (!strcmp(name, "CreateShaderModule")) return (PFN_vkVoidFunction)__HOOKED_vkCreateShaderModule;
   if (!strcmp(name, "DestroyShaderModule")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyShaderModule;
   if (!strcmp(name, "CreatePipelineCache")) return (PFN_vkVoidFunction)__HOOKED_vkCreatePipelineCache;
   if (!strcmp(name, "DestroyPipelineCache")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyPipelineCache;
   if (!strcmp(name, "GetPipelineCacheData")) return (PFN_vkVoidFunction)__HOOKED_vkGetPipelineCacheData;
   if (!strcmp(name, "MergePipelineCaches")) return (PFN_vkVoidFunction)__HOOKED_vkMergePipelineCaches;
   if (!strcmp(name, "CreateGraphicsPipelines")) return (PFN_vkVoidFunction)__HOOKED_vkCreateGraphicsPipelines;
   if (!strcmp(name, "CreateComputePipelines")) return (PFN_vkVoidFunction)__HOOKED_vkCreateComputePipelines;
   if (!strcmp(name, "DestroyPipeline")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyPipeline;
   if (!strcmp(name, "CreatePipelineLayout")) return (PFN_vkVoidFunction)__HOOKED_vkCreatePipelineLayout;
   if (!strcmp(name, "DestroyPipelineLayout")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyPipelineLayout;
   if (!strcmp(name, "CreateSampler")) return (PFN_vkVoidFunction)__HOOKED_vkCreateSampler;
   if (!strcmp(name, "DestroySampler")) return (PFN_vkVoidFunction)__HOOKED_vkDestroySampler;
   if (!strcmp(name, "CreateDescriptorSetLayout")) return (PFN_vkVoidFunction)__HOOKED_vkCreateDescriptorSetLayout;
   if (!strcmp(name, "DestroyDescriptorSetLayout")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyDescriptorSetLayout;
   if (!strcmp(name, "CreateDescriptorPool")) return (PFN_vkVoidFunction)__HOOKED_vkCreateDescriptorPool;
   if (!strcmp(name, "DestroyDescriptorPool")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyDescriptorPool;
   if (!strcmp(name, "ResetDescriptorPool")) return (PFN_vkVoidFunction)__HOOKED_vkResetDescriptorPool;
   if (!strcmp(name, "AllocateDescriptorSets")) return (PFN_vkVoidFunction)__HOOKED_vkAllocateDescriptorSets;
   if (!strcmp(name, "FreeDescriptorSets")) return (PFN_vkVoidFunction)__HOOKED_vkFreeDescriptorSets;
   if (!strcmp(name, "UpdateDescriptorSets")) return (PFN_vkVoidFunction)__HOOKED_vkUpdateDescriptorSets;
   if (!strcmp(name, "CreateFramebuffer")) return (PFN_vkVoidFunction)__HOOKED_vkCreateFramebuffer;
   if (!strcmp(name, "DestroyFramebuffer")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyFramebuffer;
   if (!strcmp(name, "CreateRenderPass")) return (PFN_vkVoidFunction)__HOOKED_vkCreateRenderPass;
   if (!strcmp(name, "DestroyRenderPass")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyRenderPass;
   if (!strcmp(name, "GetRenderAreaGranularity")) return (PFN_vkVoidFunction)__HOOKED_vkGetRenderAreaGranularity;
   if (!strcmp(name, "CreateCommandPool")) return (PFN_vkVoidFunction)__HOOKED_vkCreateCommandPool;
   if (!strcmp(name, "DestroyCommandPool")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyCommandPool;
   if (!strcmp(name, "ResetCommandPool")) return (PFN_vkVoidFunction)__HOOKED_vkResetCommandPool;
   if (!strcmp(name, "AllocateCommandBuffers")) return (PFN_vkVoidFunction)__HOOKED_vkAllocateCommandBuffers;
   if (!strcmp(name, "FreeCommandBuffers")) return (PFN_vkVoidFunction)__HOOKED_vkFreeCommandBuffers;
   if (!strcmp(name, "BeginCommandBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkBeginCommandBuffer;
   if (!strcmp(name, "EndCommandBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkEndCommandBuffer;
   if (!strcmp(name, "ResetCommandBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkResetCommandBuffer;
   if (!strcmp(name, "CmdBindPipeline")) return (PFN_vkVoidFunction)__HOOKED_vkCmdBindPipeline;
   if (!strcmp(name, "CmdSetViewport")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetViewport;
   if (!strcmp(name, "CmdSetScissor")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetScissor;
   if (!strcmp(name, "CmdSetLineWidth")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetLineWidth;
   if (!strcmp(name, "CmdSetDepthBias")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetDepthBias;
   if (!strcmp(name, "CmdSetBlendConstants")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetBlendConstants;
   if (!strcmp(name, "CmdSetDepthBounds")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetDepthBounds;
   if (!strcmp(name, "CmdSetStencilCompareMask")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetStencilCompareMask;
   if (!strcmp(name, "CmdSetStencilWriteMask")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetStencilWriteMask;
   if (!strcmp(name, "CmdSetStencilReference")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetStencilReference;
   if (!strcmp(name, "CmdBindDescriptorSets")) return (PFN_vkVoidFunction)__HOOKED_vkCmdBindDescriptorSets;
   if (!strcmp(name, "CmdBindIndexBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkCmdBindIndexBuffer;
   if (!strcmp(name, "CmdBindVertexBuffers")) return (PFN_vkVoidFunction)__HOOKED_vkCmdBindVertexBuffers;
   if (!strcmp(name, "CmdDraw")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDraw;
   if (!strcmp(name, "CmdDrawIndexed")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDrawIndexed;
   if (!strcmp(name, "CmdDrawIndirect")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDrawIndirect;
   if (!strcmp(name, "CmdDrawIndexedIndirect")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDrawIndexedIndirect;
   if (!strcmp(name, "CmdDispatch")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDispatch;
   if (!strcmp(name, "CmdDispatchIndirect")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDispatchIndirect;
   if (!strcmp(name, "CmdCopyBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkCmdCopyBuffer;
   if (!strcmp(name, "CmdCopyImage")) return (PFN_vkVoidFunction)__HOOKED_vkCmdCopyImage;
   if (!strcmp(name, "CmdBlitImage")) return (PFN_vkVoidFunction)__HOOKED_vkCmdBlitImage;
   if (!strcmp(name, "CmdCopyBufferToImage")) return (PFN_vkVoidFunction)__HOOKED_vkCmdCopyBufferToImage;
   if (!strcmp(name, "CmdCopyImageToBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkCmdCopyImageToBuffer;
   if (!strcmp(name, "CmdUpdateBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkCmdUpdateBuffer;
   if (!strcmp(name, "CmdFillBuffer")) return (PFN_vkVoidFunction)__HOOKED_vkCmdFillBuffer;
   if (!strcmp(name, "CmdClearColorImage")) return (PFN_vkVoidFunction)__HOOKED_vkCmdClearColorImage;
   if (!strcmp(name, "CmdClearDepthStencilImage")) return (PFN_vkVoidFunction)__HOOKED_vkCmdClearDepthStencilImage;
   if (!strcmp(name, "CmdClearAttachments")) return (PFN_vkVoidFunction)__HOOKED_vkCmdClearAttachments;
   if (!strcmp(name, "CmdResolveImage")) return (PFN_vkVoidFunction)__HOOKED_vkCmdResolveImage;
   if (!strcmp(name, "CmdSetEvent")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetEvent;
   if (!strcmp(name, "CmdResetEvent")) return (PFN_vkVoidFunction)__HOOKED_vkCmdResetEvent;
   if (!strcmp(name, "CmdWaitEvents")) return (PFN_vkVoidFunction)__HOOKED_vkCmdWaitEvents;
   if (!strcmp(name, "CmdPipelineBarrier")) return (PFN_vkVoidFunction)__HOOKED_vkCmdPipelineBarrier;
   if (!strcmp(name, "CmdBeginQuery")) return (PFN_vkVoidFunction)__HOOKED_vkCmdBeginQuery;
   if (!strcmp(name, "CmdEndQuery")) return (PFN_vkVoidFunction)__HOOKED_vkCmdEndQuery;
   if (!strcmp(name, "CmdResetQueryPool")) return (PFN_vkVoidFunction)__HOOKED_vkCmdResetQueryPool;
   if (!strcmp(name, "CmdWriteTimestamp")) return (PFN_vkVoidFunction)__HOOKED_vkCmdWriteTimestamp;
   if (!strcmp(name, "CmdCopyQueryPoolResults")) return (PFN_vkVoidFunction)__HOOKED_vkCmdCopyQueryPoolResults;
   if (!strcmp(name, "CmdPushConstants")) return (PFN_vkVoidFunction)__HOOKED_vkCmdPushConstants;
   if (!strcmp(name, "CmdBeginRenderPass")) return (PFN_vkVoidFunction)__HOOKED_vkCmdBeginRenderPass;
   if (!strcmp(name, "CmdNextSubpass")) return (PFN_vkVoidFunction)__HOOKED_vkCmdNextSubpass;
   if (!strcmp(name, "CmdEndRenderPass")) return (PFN_vkVoidFunction)__HOOKED_vkCmdEndRenderPass;
   if (!strcmp(name, "CmdExecuteCommands")) return (PFN_vkVoidFunction)__HOOKED_vkCmdExecuteCommands;
   if (!strcmp(name, "BindBufferMemory2")) return (PFN_vkVoidFunction)__HOOKED_vkBindBufferMemory2;
   if (!strcmp(name, "BindImageMemory2")) return (PFN_vkVoidFunction)__HOOKED_vkBindImageMemory2;
   if (!strcmp(name, "GetDeviceGroupPeerMemoryFeatures")) return (PFN_vkVoidFunction)__HOOKED_vkGetDeviceGroupPeerMemoryFeatures;
   if (!strcmp(name, "CmdSetDeviceMask")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetDeviceMask;
   if (!strcmp(name, "CmdDispatchBase")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDispatchBase;
   if (!strcmp(name, "GetImageMemoryRequirements2")) return (PFN_vkVoidFunction)__HOOKED_vkGetImageMemoryRequirements2;
   if (!strcmp(name, "GetBufferMemoryRequirements2")) return (PFN_vkVoidFunction)__HOOKED_vkGetBufferMemoryRequirements2;
   if (!strcmp(name, "GetImageSparseMemoryRequirements2")) return (PFN_vkVoidFunction)__HOOKED_vkGetImageSparseMemoryRequirements2;
   if (!strcmp(name, "TrimCommandPool")) return (PFN_vkVoidFunction)__HOOKED_vkTrimCommandPool;
   if (!strcmp(name, "GetDeviceQueue2")) return (PFN_vkVoidFunction)__HOOKED_vkGetDeviceQueue2;
   if (!strcmp(name, "CreateSamplerYcbcrConversion")) return (PFN_vkVoidFunction)__HOOKED_vkCreateSamplerYcbcrConversion;
   if (!strcmp(name, "DestroySamplerYcbcrConversion")) return (PFN_vkVoidFunction)__HOOKED_vkDestroySamplerYcbcrConversion;
   if (!strcmp(name, "CreateDescriptorUpdateTemplate")) return (PFN_vkVoidFunction)__HOOKED_vkCreateDescriptorUpdateTemplate;
   if (!strcmp(name, "DestroyDescriptorUpdateTemplate")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyDescriptorUpdateTemplate;
   if (!strcmp(name, "UpdateDescriptorSetWithTemplate")) return (PFN_vkVoidFunction)__HOOKED_vkUpdateDescriptorSetWithTemplate;
   if (!strcmp(name, "GetDescriptorSetLayoutSupport")) return (PFN_vkVoidFunction)__HOOKED_vkGetDescriptorSetLayoutSupport;
   if (!strcmp(name, "CreateSwapchainKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateSwapchainKHR;
   if (!strcmp(name, "DestroySwapchainKHR")) return (PFN_vkVoidFunction)__HOOKED_vkDestroySwapchainKHR;
   if (!strcmp(name, "GetSwapchainImagesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetSwapchainImagesKHR;
   if (!strcmp(name, "AcquireNextImageKHR")) return (PFN_vkVoidFunction)__HOOKED_vkAcquireNextImageKHR;
   if (!strcmp(name, "QueuePresentKHR")) return (PFN_vkVoidFunction)__HOOKED_vkQueuePresentKHR;
   if (!strcmp(name, "GetDeviceGroupPresentCapabilitiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetDeviceGroupPresentCapabilitiesKHR;
   if (!strcmp(name, "GetDeviceGroupSurfacePresentModesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetDeviceGroupSurfacePresentModesKHR;
   if (!strcmp(name, "AcquireNextImage2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkAcquireNextImage2KHR;
   if (!strcmp(name, "CreateSharedSwapchainsKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateSharedSwapchainsKHR;
   if (!strcmp(name, "TrimCommandPoolKHR")) return (PFN_vkVoidFunction)__HOOKED_vkTrimCommandPoolKHR;
#ifdef VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "GetMemoryWin32HandleKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetMemoryWin32HandleKHR;
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "GetMemoryWin32HandlePropertiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetMemoryWin32HandlePropertiesKHR;
#endif // VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "GetMemoryFdKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetMemoryFdKHR;
   if (!strcmp(name, "GetMemoryFdPropertiesKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetMemoryFdPropertiesKHR;
#ifdef VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "ImportSemaphoreWin32HandleKHR")) return (PFN_vkVoidFunction)__HOOKED_vkImportSemaphoreWin32HandleKHR;
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "GetSemaphoreWin32HandleKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetSemaphoreWin32HandleKHR;
#endif // VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "ImportSemaphoreFdKHR")) return (PFN_vkVoidFunction)__HOOKED_vkImportSemaphoreFdKHR;
   if (!strcmp(name, "GetSemaphoreFdKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetSemaphoreFdKHR;
   if (!strcmp(name, "CmdPushDescriptorSetKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCmdPushDescriptorSetKHR;
   if (!strcmp(name, "CmdPushDescriptorSetWithTemplateKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCmdPushDescriptorSetWithTemplateKHR;
   if (!strcmp(name, "CreateDescriptorUpdateTemplateKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateDescriptorUpdateTemplateKHR;
   if (!strcmp(name, "DestroyDescriptorUpdateTemplateKHR")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyDescriptorUpdateTemplateKHR;
   if (!strcmp(name, "UpdateDescriptorSetWithTemplateKHR")) return (PFN_vkVoidFunction)__HOOKED_vkUpdateDescriptorSetWithTemplateKHR;
   if (!strcmp(name, "GetSwapchainStatusKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetSwapchainStatusKHR;
#ifdef VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "ImportFenceWin32HandleKHR")) return (PFN_vkVoidFunction)__HOOKED_vkImportFenceWin32HandleKHR;
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "GetFenceWin32HandleKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetFenceWin32HandleKHR;
#endif // VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "ImportFenceFdKHR")) return (PFN_vkVoidFunction)__HOOKED_vkImportFenceFdKHR;
   if (!strcmp(name, "GetFenceFdKHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetFenceFdKHR;
   if (!strcmp(name, "GetImageMemoryRequirements2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetImageMemoryRequirements2KHR;
   if (!strcmp(name, "GetBufferMemoryRequirements2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetBufferMemoryRequirements2KHR;
   if (!strcmp(name, "GetImageSparseMemoryRequirements2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkGetImageSparseMemoryRequirements2KHR;
   if (!strcmp(name, "CreateSamplerYcbcrConversionKHR")) return (PFN_vkVoidFunction)__HOOKED_vkCreateSamplerYcbcrConversionKHR;
   if (!strcmp(name, "DestroySamplerYcbcrConversionKHR")) return (PFN_vkVoidFunction)__HOOKED_vkDestroySamplerYcbcrConversionKHR;
   if (!strcmp(name, "BindBufferMemory2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkBindBufferMemory2KHR;
   if (!strcmp(name, "BindImageMemory2KHR")) return (PFN_vkVoidFunction)__HOOKED_vkBindImageMemory2KHR;
   if (!strcmp(name, "DebugMarkerSetObjectTagEXT")) return (PFN_vkVoidFunction)__HOOKED_vkDebugMarkerSetObjectTagEXT;
   if (!strcmp(name, "DebugMarkerSetObjectNameEXT")) return (PFN_vkVoidFunction)__HOOKED_vkDebugMarkerSetObjectNameEXT;
   if (!strcmp(name, "CmdDebugMarkerBeginEXT")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDebugMarkerBeginEXT;
   if (!strcmp(name, "CmdDebugMarkerEndEXT")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDebugMarkerEndEXT;
   if (!strcmp(name, "CmdDebugMarkerInsertEXT")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDebugMarkerInsertEXT;
   if (!strcmp(name, "CmdDrawIndirectCountAMD")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDrawIndirectCountAMD;
   if (!strcmp(name, "CmdDrawIndexedIndirectCountAMD")) return (PFN_vkVoidFunction)__HOOKED_vkCmdDrawIndexedIndirectCountAMD;
   if (!strcmp(name, "GetShaderInfoAMD")) return (PFN_vkVoidFunction)__HOOKED_vkGetShaderInfoAMD;
#ifdef VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "GetMemoryWin32HandleNV")) return (PFN_vkVoidFunction)__HOOKED_vkGetMemoryWin32HandleNV;
#endif // VK_USE_PLATFORM_WIN32_KHR
   if (!strcmp(name, "CmdProcessCommandsNVX")) return (PFN_vkVoidFunction)__HOOKED_vkCmdProcessCommandsNVX;
   if (!strcmp(name, "CmdReserveSpaceForCommandsNVX")) return (PFN_vkVoidFunction)__HOOKED_vkCmdReserveSpaceForCommandsNVX;
   if (!strcmp(name, "CreateIndirectCommandsLayoutNVX")) return (PFN_vkVoidFunction)__HOOKED_vkCreateIndirectCommandsLayoutNVX;
   if (!strcmp(name, "DestroyIndirectCommandsLayoutNVX")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyIndirectCommandsLayoutNVX;
   if (!strcmp(name, "CreateObjectTableNVX")) return (PFN_vkVoidFunction)__HOOKED_vkCreateObjectTableNVX;
   if (!strcmp(name, "DestroyObjectTableNVX")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyObjectTableNVX;
   if (!strcmp(name, "RegisterObjectsNVX")) return (PFN_vkVoidFunction)__HOOKED_vkRegisterObjectsNVX;
   if (!strcmp(name, "UnregisterObjectsNVX")) return (PFN_vkVoidFunction)__HOOKED_vkUnregisterObjectsNVX;
   if (!strcmp(name, "CmdSetViewportWScalingNV")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetViewportWScalingNV;
   if (!strcmp(name, "DisplayPowerControlEXT")) return (PFN_vkVoidFunction)__HOOKED_vkDisplayPowerControlEXT;
   if (!strcmp(name, "RegisterDeviceEventEXT")) return (PFN_vkVoidFunction)__HOOKED_vkRegisterDeviceEventEXT;
   if (!strcmp(name, "RegisterDisplayEventEXT")) return (PFN_vkVoidFunction)__HOOKED_vkRegisterDisplayEventEXT;
   if (!strcmp(name, "GetSwapchainCounterEXT")) return (PFN_vkVoidFunction)__HOOKED_vkGetSwapchainCounterEXT;
   if (!strcmp(name, "GetRefreshCycleDurationGOOGLE")) return (PFN_vkVoidFunction)__HOOKED_vkGetRefreshCycleDurationGOOGLE;
   if (!strcmp(name, "GetPastPresentationTimingGOOGLE")) return (PFN_vkVoidFunction)__HOOKED_vkGetPastPresentationTimingGOOGLE;
   if (!strcmp(name, "CmdSetDiscardRectangleEXT")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetDiscardRectangleEXT;
   if (!strcmp(name, "SetHdrMetadataEXT")) return (PFN_vkVoidFunction)__HOOKED_vkSetHdrMetadataEXT;
   if (!strcmp(name, "CmdSetSampleLocationsEXT")) return (PFN_vkVoidFunction)__HOOKED_vkCmdSetSampleLocationsEXT;
   if (!strcmp(name, "CreateValidationCacheEXT")) return (PFN_vkVoidFunction)__HOOKED_vkCreateValidationCacheEXT;
   if (!strcmp(name, "DestroyValidationCacheEXT")) return (PFN_vkVoidFunction)__HOOKED_vkDestroyValidationCacheEXT;
   if (!strcmp(name, "MergeValidationCachesEXT")) return (PFN_vkVoidFunction)__HOOKED_vkMergeValidationCachesEXT;
   if (!strcmp(name, "GetValidationCacheDataEXT")) return (PFN_vkVoidFunction)__HOOKED_vkGetValidationCacheDataEXT;
   if (!strcmp(name, "GetMemoryHostPointerPropertiesEXT")) return (PFN_vkVoidFunction)__HOOKED_vkGetMemoryHostPointerPropertiesEXT;
    return NULL;
}

