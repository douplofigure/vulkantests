// *** THIS FILE IS GENERATED - DO NOT EDIT ***
// See vktrace_file_generator.py for modifications


/***************************************************************************
 *
 * Copyright (c) 2015-2017 The Khronos Group Inc.
 * Copyright (c) 2015-2017 Valve Corporation
 * Copyright (c) 2015-2017 LunarG, Inc.
 * Copyright (c) 2015-2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: Mark Lobodzinski <mark@lunarg.com>
 * Author: Jon Ashburn <jon@lunarg.com>
 * Author: Tobin Ehlis <tobin@lunarg.com>
 * Author: Peter Lohrmann <peterl@valvesoftware.com>
 * Author: David Pinedo <david@lunarg.com>
 *
 ****************************************************************************/

#pragma once
#include "vktrace_vk_vk_packets.h"
#include "vktrace_trace_packet_utils.h"
#include "vktrace_trace_packet_identifiers.h"
#include "vktrace_interconnect.h"
#include <inttypes.h>
#include "vk_enum_string_helper.h"
#ifndef _WIN32
#pragma GCC diagnostic ignored "-Wwrite-strings"
#endif
#ifndef _WIN32
#pragma GCC diagnostic warning "-Wwrite-strings"
#endif
#if defined(WIN32)
#define snprintf _snprintf
#endif
#if defined(WIN32)
#define VK_SIZE_T_SPECIFIER "%Iu"
#else
#define VK_SIZE_T_SPECIFIER "%zu"
#endif
#define SEND_ENTRYPOINT_ID(entrypoint) ;
#define SEND_ENTRYPOINT_PARAMS(entrypoint, ...) ;
#define CREATE_TRACE_PACKET(entrypoint, buffer_bytes_needed) \
    pHeader = vktrace_create_trace_packet(VKTRACE_TID_VULKAN, VKTRACE_TPI_VK_##entrypoint, sizeof(packet_##entrypoint), buffer_bytes_needed);

#define FINISH_TRACE_PACKET() \
    vktrace_finalize_trace_packet(pHeader); \
    vktrace_write_trace_packet(pHeader, vktrace_trace_get_trace_file()); \
    vktrace_delete_trace_packet(&pHeader);

// Include trace packet identifier definitions
#include "vktrace_trace_packet_identifiers.h"

static const char *vktrace_vk_packet_id_name(const VKTRACE_TRACE_PACKET_ID_VK id) {
    switch(id) {
        case VKTRACE_TPI_VK_vkApiVersion: {
            return "vkApiVersion";
        }
        case VKTRACE_TPI_VK_vkCreateInstance: {
            return "vkCreateInstance";
        };
        case VKTRACE_TPI_VK_vkDestroyInstance: {
            return "vkDestroyInstance";
        };
        case VKTRACE_TPI_VK_vkEnumeratePhysicalDevices: {
            return "vkEnumeratePhysicalDevices";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures: {
            return "vkGetPhysicalDeviceFeatures";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties: {
            return "vkGetPhysicalDeviceFormatProperties";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties: {
            return "vkGetPhysicalDeviceImageFormatProperties";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties: {
            return "vkGetPhysicalDeviceProperties";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties: {
            return "vkGetPhysicalDeviceQueueFamilyProperties";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties: {
            return "vkGetPhysicalDeviceMemoryProperties";
        };
        case VKTRACE_TPI_VK_vkGetInstanceProcAddr: {
            return "vkGetInstanceProcAddr";
        };
        case VKTRACE_TPI_VK_vkGetDeviceProcAddr: {
            return "vkGetDeviceProcAddr";
        };
        case VKTRACE_TPI_VK_vkCreateDevice: {
            return "vkCreateDevice";
        };
        case VKTRACE_TPI_VK_vkDestroyDevice: {
            return "vkDestroyDevice";
        };
        case VKTRACE_TPI_VK_vkEnumerateInstanceExtensionProperties: {
            return "vkEnumerateInstanceExtensionProperties";
        };
        case VKTRACE_TPI_VK_vkEnumerateDeviceExtensionProperties: {
            return "vkEnumerateDeviceExtensionProperties";
        };
        case VKTRACE_TPI_VK_vkEnumerateInstanceLayerProperties: {
            return "vkEnumerateInstanceLayerProperties";
        };
        case VKTRACE_TPI_VK_vkEnumerateDeviceLayerProperties: {
            return "vkEnumerateDeviceLayerProperties";
        };
        case VKTRACE_TPI_VK_vkGetDeviceQueue: {
            return "vkGetDeviceQueue";
        };
        case VKTRACE_TPI_VK_vkQueueSubmit: {
            return "vkQueueSubmit";
        };
        case VKTRACE_TPI_VK_vkQueueWaitIdle: {
            return "vkQueueWaitIdle";
        };
        case VKTRACE_TPI_VK_vkDeviceWaitIdle: {
            return "vkDeviceWaitIdle";
        };
        case VKTRACE_TPI_VK_vkAllocateMemory: {
            return "vkAllocateMemory";
        };
        case VKTRACE_TPI_VK_vkFreeMemory: {
            return "vkFreeMemory";
        };
        case VKTRACE_TPI_VK_vkMapMemory: {
            return "vkMapMemory";
        };
        case VKTRACE_TPI_VK_vkUnmapMemory: {
            return "vkUnmapMemory";
        };
        case VKTRACE_TPI_VK_vkFlushMappedMemoryRanges: {
            return "vkFlushMappedMemoryRanges";
        };
        case VKTRACE_TPI_VK_vkInvalidateMappedMemoryRanges: {
            return "vkInvalidateMappedMemoryRanges";
        };
        case VKTRACE_TPI_VK_vkGetDeviceMemoryCommitment: {
            return "vkGetDeviceMemoryCommitment";
        };
        case VKTRACE_TPI_VK_vkBindBufferMemory: {
            return "vkBindBufferMemory";
        };
        case VKTRACE_TPI_VK_vkBindImageMemory: {
            return "vkBindImageMemory";
        };
        case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements: {
            return "vkGetBufferMemoryRequirements";
        };
        case VKTRACE_TPI_VK_vkGetImageMemoryRequirements: {
            return "vkGetImageMemoryRequirements";
        };
        case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements: {
            return "vkGetImageSparseMemoryRequirements";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties: {
            return "vkGetPhysicalDeviceSparseImageFormatProperties";
        };
        case VKTRACE_TPI_VK_vkQueueBindSparse: {
            return "vkQueueBindSparse";
        };
        case VKTRACE_TPI_VK_vkCreateFence: {
            return "vkCreateFence";
        };
        case VKTRACE_TPI_VK_vkDestroyFence: {
            return "vkDestroyFence";
        };
        case VKTRACE_TPI_VK_vkResetFences: {
            return "vkResetFences";
        };
        case VKTRACE_TPI_VK_vkGetFenceStatus: {
            return "vkGetFenceStatus";
        };
        case VKTRACE_TPI_VK_vkWaitForFences: {
            return "vkWaitForFences";
        };
        case VKTRACE_TPI_VK_vkCreateSemaphore: {
            return "vkCreateSemaphore";
        };
        case VKTRACE_TPI_VK_vkDestroySemaphore: {
            return "vkDestroySemaphore";
        };
        case VKTRACE_TPI_VK_vkCreateEvent: {
            return "vkCreateEvent";
        };
        case VKTRACE_TPI_VK_vkDestroyEvent: {
            return "vkDestroyEvent";
        };
        case VKTRACE_TPI_VK_vkGetEventStatus: {
            return "vkGetEventStatus";
        };
        case VKTRACE_TPI_VK_vkSetEvent: {
            return "vkSetEvent";
        };
        case VKTRACE_TPI_VK_vkResetEvent: {
            return "vkResetEvent";
        };
        case VKTRACE_TPI_VK_vkCreateQueryPool: {
            return "vkCreateQueryPool";
        };
        case VKTRACE_TPI_VK_vkDestroyQueryPool: {
            return "vkDestroyQueryPool";
        };
        case VKTRACE_TPI_VK_vkGetQueryPoolResults: {
            return "vkGetQueryPoolResults";
        };
        case VKTRACE_TPI_VK_vkCreateBuffer: {
            return "vkCreateBuffer";
        };
        case VKTRACE_TPI_VK_vkDestroyBuffer: {
            return "vkDestroyBuffer";
        };
        case VKTRACE_TPI_VK_vkCreateBufferView: {
            return "vkCreateBufferView";
        };
        case VKTRACE_TPI_VK_vkDestroyBufferView: {
            return "vkDestroyBufferView";
        };
        case VKTRACE_TPI_VK_vkCreateImage: {
            return "vkCreateImage";
        };
        case VKTRACE_TPI_VK_vkDestroyImage: {
            return "vkDestroyImage";
        };
        case VKTRACE_TPI_VK_vkGetImageSubresourceLayout: {
            return "vkGetImageSubresourceLayout";
        };
        case VKTRACE_TPI_VK_vkCreateImageView: {
            return "vkCreateImageView";
        };
        case VKTRACE_TPI_VK_vkDestroyImageView: {
            return "vkDestroyImageView";
        };
        case VKTRACE_TPI_VK_vkCreateShaderModule: {
            return "vkCreateShaderModule";
        };
        case VKTRACE_TPI_VK_vkDestroyShaderModule: {
            return "vkDestroyShaderModule";
        };
        case VKTRACE_TPI_VK_vkCreatePipelineCache: {
            return "vkCreatePipelineCache";
        };
        case VKTRACE_TPI_VK_vkDestroyPipelineCache: {
            return "vkDestroyPipelineCache";
        };
        case VKTRACE_TPI_VK_vkGetPipelineCacheData: {
            return "vkGetPipelineCacheData";
        };
        case VKTRACE_TPI_VK_vkMergePipelineCaches: {
            return "vkMergePipelineCaches";
        };
        case VKTRACE_TPI_VK_vkCreateGraphicsPipelines: {
            return "vkCreateGraphicsPipelines";
        };
        case VKTRACE_TPI_VK_vkCreateComputePipelines: {
            return "vkCreateComputePipelines";
        };
        case VKTRACE_TPI_VK_vkDestroyPipeline: {
            return "vkDestroyPipeline";
        };
        case VKTRACE_TPI_VK_vkCreatePipelineLayout: {
            return "vkCreatePipelineLayout";
        };
        case VKTRACE_TPI_VK_vkDestroyPipelineLayout: {
            return "vkDestroyPipelineLayout";
        };
        case VKTRACE_TPI_VK_vkCreateSampler: {
            return "vkCreateSampler";
        };
        case VKTRACE_TPI_VK_vkDestroySampler: {
            return "vkDestroySampler";
        };
        case VKTRACE_TPI_VK_vkCreateDescriptorSetLayout: {
            return "vkCreateDescriptorSetLayout";
        };
        case VKTRACE_TPI_VK_vkDestroyDescriptorSetLayout: {
            return "vkDestroyDescriptorSetLayout";
        };
        case VKTRACE_TPI_VK_vkCreateDescriptorPool: {
            return "vkCreateDescriptorPool";
        };
        case VKTRACE_TPI_VK_vkDestroyDescriptorPool: {
            return "vkDestroyDescriptorPool";
        };
        case VKTRACE_TPI_VK_vkResetDescriptorPool: {
            return "vkResetDescriptorPool";
        };
        case VKTRACE_TPI_VK_vkAllocateDescriptorSets: {
            return "vkAllocateDescriptorSets";
        };
        case VKTRACE_TPI_VK_vkFreeDescriptorSets: {
            return "vkFreeDescriptorSets";
        };
        case VKTRACE_TPI_VK_vkUpdateDescriptorSets: {
            return "vkUpdateDescriptorSets";
        };
        case VKTRACE_TPI_VK_vkCreateFramebuffer: {
            return "vkCreateFramebuffer";
        };
        case VKTRACE_TPI_VK_vkDestroyFramebuffer: {
            return "vkDestroyFramebuffer";
        };
        case VKTRACE_TPI_VK_vkCreateRenderPass: {
            return "vkCreateRenderPass";
        };
        case VKTRACE_TPI_VK_vkDestroyRenderPass: {
            return "vkDestroyRenderPass";
        };
        case VKTRACE_TPI_VK_vkGetRenderAreaGranularity: {
            return "vkGetRenderAreaGranularity";
        };
        case VKTRACE_TPI_VK_vkCreateCommandPool: {
            return "vkCreateCommandPool";
        };
        case VKTRACE_TPI_VK_vkDestroyCommandPool: {
            return "vkDestroyCommandPool";
        };
        case VKTRACE_TPI_VK_vkResetCommandPool: {
            return "vkResetCommandPool";
        };
        case VKTRACE_TPI_VK_vkAllocateCommandBuffers: {
            return "vkAllocateCommandBuffers";
        };
        case VKTRACE_TPI_VK_vkFreeCommandBuffers: {
            return "vkFreeCommandBuffers";
        };
        case VKTRACE_TPI_VK_vkBeginCommandBuffer: {
            return "vkBeginCommandBuffer";
        };
        case VKTRACE_TPI_VK_vkEndCommandBuffer: {
            return "vkEndCommandBuffer";
        };
        case VKTRACE_TPI_VK_vkResetCommandBuffer: {
            return "vkResetCommandBuffer";
        };
        case VKTRACE_TPI_VK_vkCmdBindPipeline: {
            return "vkCmdBindPipeline";
        };
        case VKTRACE_TPI_VK_vkCmdSetViewport: {
            return "vkCmdSetViewport";
        };
        case VKTRACE_TPI_VK_vkCmdSetScissor: {
            return "vkCmdSetScissor";
        };
        case VKTRACE_TPI_VK_vkCmdSetLineWidth: {
            return "vkCmdSetLineWidth";
        };
        case VKTRACE_TPI_VK_vkCmdSetDepthBias: {
            return "vkCmdSetDepthBias";
        };
        case VKTRACE_TPI_VK_vkCmdSetBlendConstants: {
            return "vkCmdSetBlendConstants";
        };
        case VKTRACE_TPI_VK_vkCmdSetDepthBounds: {
            return "vkCmdSetDepthBounds";
        };
        case VKTRACE_TPI_VK_vkCmdSetStencilCompareMask: {
            return "vkCmdSetStencilCompareMask";
        };
        case VKTRACE_TPI_VK_vkCmdSetStencilWriteMask: {
            return "vkCmdSetStencilWriteMask";
        };
        case VKTRACE_TPI_VK_vkCmdSetStencilReference: {
            return "vkCmdSetStencilReference";
        };
        case VKTRACE_TPI_VK_vkCmdBindDescriptorSets: {
            return "vkCmdBindDescriptorSets";
        };
        case VKTRACE_TPI_VK_vkCmdBindIndexBuffer: {
            return "vkCmdBindIndexBuffer";
        };
        case VKTRACE_TPI_VK_vkCmdBindVertexBuffers: {
            return "vkCmdBindVertexBuffers";
        };
        case VKTRACE_TPI_VK_vkCmdDraw: {
            return "vkCmdDraw";
        };
        case VKTRACE_TPI_VK_vkCmdDrawIndexed: {
            return "vkCmdDrawIndexed";
        };
        case VKTRACE_TPI_VK_vkCmdDrawIndirect: {
            return "vkCmdDrawIndirect";
        };
        case VKTRACE_TPI_VK_vkCmdDrawIndexedIndirect: {
            return "vkCmdDrawIndexedIndirect";
        };
        case VKTRACE_TPI_VK_vkCmdDispatch: {
            return "vkCmdDispatch";
        };
        case VKTRACE_TPI_VK_vkCmdDispatchIndirect: {
            return "vkCmdDispatchIndirect";
        };
        case VKTRACE_TPI_VK_vkCmdCopyBuffer: {
            return "vkCmdCopyBuffer";
        };
        case VKTRACE_TPI_VK_vkCmdCopyImage: {
            return "vkCmdCopyImage";
        };
        case VKTRACE_TPI_VK_vkCmdBlitImage: {
            return "vkCmdBlitImage";
        };
        case VKTRACE_TPI_VK_vkCmdCopyBufferToImage: {
            return "vkCmdCopyBufferToImage";
        };
        case VKTRACE_TPI_VK_vkCmdCopyImageToBuffer: {
            return "vkCmdCopyImageToBuffer";
        };
        case VKTRACE_TPI_VK_vkCmdUpdateBuffer: {
            return "vkCmdUpdateBuffer";
        };
        case VKTRACE_TPI_VK_vkCmdFillBuffer: {
            return "vkCmdFillBuffer";
        };
        case VKTRACE_TPI_VK_vkCmdClearColorImage: {
            return "vkCmdClearColorImage";
        };
        case VKTRACE_TPI_VK_vkCmdClearDepthStencilImage: {
            return "vkCmdClearDepthStencilImage";
        };
        case VKTRACE_TPI_VK_vkCmdClearAttachments: {
            return "vkCmdClearAttachments";
        };
        case VKTRACE_TPI_VK_vkCmdResolveImage: {
            return "vkCmdResolveImage";
        };
        case VKTRACE_TPI_VK_vkCmdSetEvent: {
            return "vkCmdSetEvent";
        };
        case VKTRACE_TPI_VK_vkCmdResetEvent: {
            return "vkCmdResetEvent";
        };
        case VKTRACE_TPI_VK_vkCmdWaitEvents: {
            return "vkCmdWaitEvents";
        };
        case VKTRACE_TPI_VK_vkCmdPipelineBarrier: {
            return "vkCmdPipelineBarrier";
        };
        case VKTRACE_TPI_VK_vkCmdBeginQuery: {
            return "vkCmdBeginQuery";
        };
        case VKTRACE_TPI_VK_vkCmdEndQuery: {
            return "vkCmdEndQuery";
        };
        case VKTRACE_TPI_VK_vkCmdResetQueryPool: {
            return "vkCmdResetQueryPool";
        };
        case VKTRACE_TPI_VK_vkCmdWriteTimestamp: {
            return "vkCmdWriteTimestamp";
        };
        case VKTRACE_TPI_VK_vkCmdCopyQueryPoolResults: {
            return "vkCmdCopyQueryPoolResults";
        };
        case VKTRACE_TPI_VK_vkCmdPushConstants: {
            return "vkCmdPushConstants";
        };
        case VKTRACE_TPI_VK_vkCmdBeginRenderPass: {
            return "vkCmdBeginRenderPass";
        };
        case VKTRACE_TPI_VK_vkCmdNextSubpass: {
            return "vkCmdNextSubpass";
        };
        case VKTRACE_TPI_VK_vkCmdEndRenderPass: {
            return "vkCmdEndRenderPass";
        };
        case VKTRACE_TPI_VK_vkCmdExecuteCommands: {
            return "vkCmdExecuteCommands";
        };
        case VKTRACE_TPI_VK_vkBindBufferMemory2: {
            return "vkBindBufferMemory2";
        };
        case VKTRACE_TPI_VK_vkBindImageMemory2: {
            return "vkBindImageMemory2";
        };
        case VKTRACE_TPI_VK_vkGetDeviceGroupPeerMemoryFeatures: {
            return "vkGetDeviceGroupPeerMemoryFeatures";
        };
        case VKTRACE_TPI_VK_vkCmdSetDeviceMask: {
            return "vkCmdSetDeviceMask";
        };
        case VKTRACE_TPI_VK_vkCmdDispatchBase: {
            return "vkCmdDispatchBase";
        };
        case VKTRACE_TPI_VK_vkEnumeratePhysicalDeviceGroups: {
            return "vkEnumeratePhysicalDeviceGroups";
        };
        case VKTRACE_TPI_VK_vkGetImageMemoryRequirements2: {
            return "vkGetImageMemoryRequirements2";
        };
        case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements2: {
            return "vkGetBufferMemoryRequirements2";
        };
        case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements2: {
            return "vkGetImageSparseMemoryRequirements2";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures2: {
            return "vkGetPhysicalDeviceFeatures2";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties2: {
            return "vkGetPhysicalDeviceProperties2";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties2: {
            return "vkGetPhysicalDeviceFormatProperties2";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties2: {
            return "vkGetPhysicalDeviceImageFormatProperties2";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties2: {
            return "vkGetPhysicalDeviceQueueFamilyProperties2";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties2: {
            return "vkGetPhysicalDeviceMemoryProperties2";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties2: {
            return "vkGetPhysicalDeviceSparseImageFormatProperties2";
        };
        case VKTRACE_TPI_VK_vkTrimCommandPool: {
            return "vkTrimCommandPool";
        };
        case VKTRACE_TPI_VK_vkGetDeviceQueue2: {
            return "vkGetDeviceQueue2";
        };
        case VKTRACE_TPI_VK_vkCreateSamplerYcbcrConversion: {
            return "vkCreateSamplerYcbcrConversion";
        };
        case VKTRACE_TPI_VK_vkDestroySamplerYcbcrConversion: {
            return "vkDestroySamplerYcbcrConversion";
        };
        case VKTRACE_TPI_VK_vkCreateDescriptorUpdateTemplate: {
            return "vkCreateDescriptorUpdateTemplate";
        };
        case VKTRACE_TPI_VK_vkDestroyDescriptorUpdateTemplate: {
            return "vkDestroyDescriptorUpdateTemplate";
        };
        case VKTRACE_TPI_VK_vkUpdateDescriptorSetWithTemplate: {
            return "vkUpdateDescriptorSetWithTemplate";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalBufferProperties: {
            return "vkGetPhysicalDeviceExternalBufferProperties";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalFenceProperties: {
            return "vkGetPhysicalDeviceExternalFenceProperties";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalSemaphoreProperties: {
            return "vkGetPhysicalDeviceExternalSemaphoreProperties";
        };
        case VKTRACE_TPI_VK_vkGetDescriptorSetLayoutSupport: {
            return "vkGetDescriptorSetLayoutSupport";
        };
        case VKTRACE_TPI_VK_vkDestroySurfaceKHR: {
            return "vkDestroySurfaceKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceSupportKHR: {
            return "vkGetPhysicalDeviceSurfaceSupportKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilitiesKHR: {
            return "vkGetPhysicalDeviceSurfaceCapabilitiesKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceFormatsKHR: {
            return "vkGetPhysicalDeviceSurfaceFormatsKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfacePresentModesKHR: {
            return "vkGetPhysicalDeviceSurfacePresentModesKHR";
        };
        case VKTRACE_TPI_VK_vkCreateSwapchainKHR: {
            return "vkCreateSwapchainKHR";
        };
        case VKTRACE_TPI_VK_vkDestroySwapchainKHR: {
            return "vkDestroySwapchainKHR";
        };
        case VKTRACE_TPI_VK_vkGetSwapchainImagesKHR: {
            return "vkGetSwapchainImagesKHR";
        };
        case VKTRACE_TPI_VK_vkAcquireNextImageKHR: {
            return "vkAcquireNextImageKHR";
        };
        case VKTRACE_TPI_VK_vkQueuePresentKHR: {
            return "vkQueuePresentKHR";
        };
        case VKTRACE_TPI_VK_vkGetDeviceGroupPresentCapabilitiesKHR: {
            return "vkGetDeviceGroupPresentCapabilitiesKHR";
        };
        case VKTRACE_TPI_VK_vkGetDeviceGroupSurfacePresentModesKHR: {
            return "vkGetDeviceGroupSurfacePresentModesKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDevicePresentRectanglesKHR: {
            return "vkGetPhysicalDevicePresentRectanglesKHR";
        };
        case VKTRACE_TPI_VK_vkAcquireNextImage2KHR: {
            return "vkAcquireNextImage2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPropertiesKHR: {
            return "vkGetPhysicalDeviceDisplayPropertiesKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPlanePropertiesKHR: {
            return "vkGetPhysicalDeviceDisplayPlanePropertiesKHR";
        };
        case VKTRACE_TPI_VK_vkGetDisplayPlaneSupportedDisplaysKHR: {
            return "vkGetDisplayPlaneSupportedDisplaysKHR";
        };
        case VKTRACE_TPI_VK_vkGetDisplayModePropertiesKHR: {
            return "vkGetDisplayModePropertiesKHR";
        };
        case VKTRACE_TPI_VK_vkCreateDisplayModeKHR: {
            return "vkCreateDisplayModeKHR";
        };
        case VKTRACE_TPI_VK_vkGetDisplayPlaneCapabilitiesKHR: {
            return "vkGetDisplayPlaneCapabilitiesKHR";
        };
        case VKTRACE_TPI_VK_vkCreateDisplayPlaneSurfaceKHR: {
            return "vkCreateDisplayPlaneSurfaceKHR";
        };
        case VKTRACE_TPI_VK_vkCreateSharedSwapchainsKHR: {
            return "vkCreateSharedSwapchainsKHR";
        };
        case VKTRACE_TPI_VK_vkCreateXlibSurfaceKHR: {
            return "vkCreateXlibSurfaceKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceXlibPresentationSupportKHR: {
            return "vkGetPhysicalDeviceXlibPresentationSupportKHR";
        };
        case VKTRACE_TPI_VK_vkCreateXcbSurfaceKHR: {
            return "vkCreateXcbSurfaceKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceXcbPresentationSupportKHR: {
            return "vkGetPhysicalDeviceXcbPresentationSupportKHR";
        };
        case VKTRACE_TPI_VK_vkCreateWaylandSurfaceKHR: {
            return "vkCreateWaylandSurfaceKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceWaylandPresentationSupportKHR: {
            return "vkGetPhysicalDeviceWaylandPresentationSupportKHR";
        };
        case VKTRACE_TPI_VK_vkCreateAndroidSurfaceKHR: {
            return "vkCreateAndroidSurfaceKHR";
        };
        case VKTRACE_TPI_VK_vkCreateWin32SurfaceKHR: {
            return "vkCreateWin32SurfaceKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceWin32PresentationSupportKHR: {
            return "vkGetPhysicalDeviceWin32PresentationSupportKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures2KHR: {
            return "vkGetPhysicalDeviceFeatures2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties2KHR: {
            return "vkGetPhysicalDeviceProperties2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties2KHR: {
            return "vkGetPhysicalDeviceFormatProperties2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties2KHR: {
            return "vkGetPhysicalDeviceImageFormatProperties2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties2KHR: {
            return "vkGetPhysicalDeviceQueueFamilyProperties2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties2KHR: {
            return "vkGetPhysicalDeviceMemoryProperties2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties2KHR: {
            return "vkGetPhysicalDeviceSparseImageFormatProperties2KHR";
        };
        case VKTRACE_TPI_VK_vkTrimCommandPoolKHR: {
            return "vkTrimCommandPoolKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalBufferPropertiesKHR: {
            return "vkGetPhysicalDeviceExternalBufferPropertiesKHR";
        };
        case VKTRACE_TPI_VK_vkGetMemoryWin32HandleKHR: {
            return "vkGetMemoryWin32HandleKHR";
        };
        case VKTRACE_TPI_VK_vkGetMemoryWin32HandlePropertiesKHR: {
            return "vkGetMemoryWin32HandlePropertiesKHR";
        };
        case VKTRACE_TPI_VK_vkGetMemoryFdKHR: {
            return "vkGetMemoryFdKHR";
        };
        case VKTRACE_TPI_VK_vkGetMemoryFdPropertiesKHR: {
            return "vkGetMemoryFdPropertiesKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR: {
            return "vkGetPhysicalDeviceExternalSemaphorePropertiesKHR";
        };
        case VKTRACE_TPI_VK_vkImportSemaphoreWin32HandleKHR: {
            return "vkImportSemaphoreWin32HandleKHR";
        };
        case VKTRACE_TPI_VK_vkGetSemaphoreWin32HandleKHR: {
            return "vkGetSemaphoreWin32HandleKHR";
        };
        case VKTRACE_TPI_VK_vkImportSemaphoreFdKHR: {
            return "vkImportSemaphoreFdKHR";
        };
        case VKTRACE_TPI_VK_vkGetSemaphoreFdKHR: {
            return "vkGetSemaphoreFdKHR";
        };
        case VKTRACE_TPI_VK_vkCmdPushDescriptorSetKHR: {
            return "vkCmdPushDescriptorSetKHR";
        };
        case VKTRACE_TPI_VK_vkCmdPushDescriptorSetWithTemplateKHR: {
            return "vkCmdPushDescriptorSetWithTemplateKHR";
        };
        case VKTRACE_TPI_VK_vkCreateDescriptorUpdateTemplateKHR: {
            return "vkCreateDescriptorUpdateTemplateKHR";
        };
        case VKTRACE_TPI_VK_vkDestroyDescriptorUpdateTemplateKHR: {
            return "vkDestroyDescriptorUpdateTemplateKHR";
        };
        case VKTRACE_TPI_VK_vkUpdateDescriptorSetWithTemplateKHR: {
            return "vkUpdateDescriptorSetWithTemplateKHR";
        };
        case VKTRACE_TPI_VK_vkGetSwapchainStatusKHR: {
            return "vkGetSwapchainStatusKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalFencePropertiesKHR: {
            return "vkGetPhysicalDeviceExternalFencePropertiesKHR";
        };
        case VKTRACE_TPI_VK_vkImportFenceWin32HandleKHR: {
            return "vkImportFenceWin32HandleKHR";
        };
        case VKTRACE_TPI_VK_vkGetFenceWin32HandleKHR: {
            return "vkGetFenceWin32HandleKHR";
        };
        case VKTRACE_TPI_VK_vkImportFenceFdKHR: {
            return "vkImportFenceFdKHR";
        };
        case VKTRACE_TPI_VK_vkGetFenceFdKHR: {
            return "vkGetFenceFdKHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilities2KHR: {
            return "vkGetPhysicalDeviceSurfaceCapabilities2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceFormats2KHR: {
            return "vkGetPhysicalDeviceSurfaceFormats2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayProperties2KHR: {
            return "vkGetPhysicalDeviceDisplayProperties2KHR";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPlaneProperties2KHR: {
            return "vkGetPhysicalDeviceDisplayPlaneProperties2KHR";
        };
        case VKTRACE_TPI_VK_vkGetDisplayModeProperties2KHR: {
            return "vkGetDisplayModeProperties2KHR";
        };
        case VKTRACE_TPI_VK_vkGetDisplayPlaneCapabilities2KHR: {
            return "vkGetDisplayPlaneCapabilities2KHR";
        };
        case VKTRACE_TPI_VK_vkGetImageMemoryRequirements2KHR: {
            return "vkGetImageMemoryRequirements2KHR";
        };
        case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements2KHR: {
            return "vkGetBufferMemoryRequirements2KHR";
        };
        case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements2KHR: {
            return "vkGetImageSparseMemoryRequirements2KHR";
        };
        case VKTRACE_TPI_VK_vkCreateSamplerYcbcrConversionKHR: {
            return "vkCreateSamplerYcbcrConversionKHR";
        };
        case VKTRACE_TPI_VK_vkDestroySamplerYcbcrConversionKHR: {
            return "vkDestroySamplerYcbcrConversionKHR";
        };
        case VKTRACE_TPI_VK_vkBindBufferMemory2KHR: {
            return "vkBindBufferMemory2KHR";
        };
        case VKTRACE_TPI_VK_vkBindImageMemory2KHR: {
            return "vkBindImageMemory2KHR";
        };
        case VKTRACE_TPI_VK_vkCreateDebugReportCallbackEXT: {
            return "vkCreateDebugReportCallbackEXT";
        };
        case VKTRACE_TPI_VK_vkDestroyDebugReportCallbackEXT: {
            return "vkDestroyDebugReportCallbackEXT";
        };
        case VKTRACE_TPI_VK_vkDebugReportMessageEXT: {
            return "vkDebugReportMessageEXT";
        };
        case VKTRACE_TPI_VK_vkDebugMarkerSetObjectTagEXT: {
            return "vkDebugMarkerSetObjectTagEXT";
        };
        case VKTRACE_TPI_VK_vkDebugMarkerSetObjectNameEXT: {
            return "vkDebugMarkerSetObjectNameEXT";
        };
        case VKTRACE_TPI_VK_vkCmdDebugMarkerBeginEXT: {
            return "vkCmdDebugMarkerBeginEXT";
        };
        case VKTRACE_TPI_VK_vkCmdDebugMarkerEndEXT: {
            return "vkCmdDebugMarkerEndEXT";
        };
        case VKTRACE_TPI_VK_vkCmdDebugMarkerInsertEXT: {
            return "vkCmdDebugMarkerInsertEXT";
        };
        case VKTRACE_TPI_VK_vkCmdDrawIndirectCountAMD: {
            return "vkCmdDrawIndirectCountAMD";
        };
        case VKTRACE_TPI_VK_vkCmdDrawIndexedIndirectCountAMD: {
            return "vkCmdDrawIndexedIndirectCountAMD";
        };
        case VKTRACE_TPI_VK_vkGetShaderInfoAMD: {
            return "vkGetShaderInfoAMD";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalImageFormatPropertiesNV: {
            return "vkGetPhysicalDeviceExternalImageFormatPropertiesNV";
        };
        case VKTRACE_TPI_VK_vkGetMemoryWin32HandleNV: {
            return "vkGetMemoryWin32HandleNV";
        };
        case VKTRACE_TPI_VK_vkCmdProcessCommandsNVX: {
            return "vkCmdProcessCommandsNVX";
        };
        case VKTRACE_TPI_VK_vkCmdReserveSpaceForCommandsNVX: {
            return "vkCmdReserveSpaceForCommandsNVX";
        };
        case VKTRACE_TPI_VK_vkCreateIndirectCommandsLayoutNVX: {
            return "vkCreateIndirectCommandsLayoutNVX";
        };
        case VKTRACE_TPI_VK_vkDestroyIndirectCommandsLayoutNVX: {
            return "vkDestroyIndirectCommandsLayoutNVX";
        };
        case VKTRACE_TPI_VK_vkCreateObjectTableNVX: {
            return "vkCreateObjectTableNVX";
        };
        case VKTRACE_TPI_VK_vkDestroyObjectTableNVX: {
            return "vkDestroyObjectTableNVX";
        };
        case VKTRACE_TPI_VK_vkRegisterObjectsNVX: {
            return "vkRegisterObjectsNVX";
        };
        case VKTRACE_TPI_VK_vkUnregisterObjectsNVX: {
            return "vkUnregisterObjectsNVX";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX: {
            return "vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX";
        };
        case VKTRACE_TPI_VK_vkCmdSetViewportWScalingNV: {
            return "vkCmdSetViewportWScalingNV";
        };
        case VKTRACE_TPI_VK_vkReleaseDisplayEXT: {
            return "vkReleaseDisplayEXT";
        };
        case VKTRACE_TPI_VK_vkAcquireXlibDisplayEXT: {
            return "vkAcquireXlibDisplayEXT";
        };
        case VKTRACE_TPI_VK_vkGetRandROutputDisplayEXT: {
            return "vkGetRandROutputDisplayEXT";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilities2EXT: {
            return "vkGetPhysicalDeviceSurfaceCapabilities2EXT";
        };
        case VKTRACE_TPI_VK_vkDisplayPowerControlEXT: {
            return "vkDisplayPowerControlEXT";
        };
        case VKTRACE_TPI_VK_vkRegisterDeviceEventEXT: {
            return "vkRegisterDeviceEventEXT";
        };
        case VKTRACE_TPI_VK_vkRegisterDisplayEventEXT: {
            return "vkRegisterDisplayEventEXT";
        };
        case VKTRACE_TPI_VK_vkGetSwapchainCounterEXT: {
            return "vkGetSwapchainCounterEXT";
        };
        case VKTRACE_TPI_VK_vkGetRefreshCycleDurationGOOGLE: {
            return "vkGetRefreshCycleDurationGOOGLE";
        };
        case VKTRACE_TPI_VK_vkGetPastPresentationTimingGOOGLE: {
            return "vkGetPastPresentationTimingGOOGLE";
        };
        case VKTRACE_TPI_VK_vkCmdSetDiscardRectangleEXT: {
            return "vkCmdSetDiscardRectangleEXT";
        };
        case VKTRACE_TPI_VK_vkSetHdrMetadataEXT: {
            return "vkSetHdrMetadataEXT";
        };
        case VKTRACE_TPI_VK_vkCmdSetSampleLocationsEXT: {
            return "vkCmdSetSampleLocationsEXT";
        };
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMultisamplePropertiesEXT: {
            return "vkGetPhysicalDeviceMultisamplePropertiesEXT";
        };
        case VKTRACE_TPI_VK_vkCreateValidationCacheEXT: {
            return "vkCreateValidationCacheEXT";
        };
        case VKTRACE_TPI_VK_vkDestroyValidationCacheEXT: {
            return "vkDestroyValidationCacheEXT";
        };
        case VKTRACE_TPI_VK_vkMergeValidationCachesEXT: {
            return "vkMergeValidationCachesEXT";
        };
        case VKTRACE_TPI_VK_vkGetValidationCacheDataEXT: {
            return "vkGetValidationCacheDataEXT";
        };
        case VKTRACE_TPI_VK_vkGetMemoryHostPointerPropertiesEXT: {
            return "vkGetMemoryHostPointerPropertiesEXT";
        };
        default:
            return NULL;
    }
}

static const char *vktrace_stringify_vk_packet_id(const VKTRACE_TRACE_PACKET_ID_VK id, const vktrace_trace_packet_header* pHeader) {
    static char str[1024];
    switch(id) {
        case VKTRACE_TPI_VK_vkApiVersion: {
            packet_vkApiVersion* pPacket = (packet_vkApiVersion*)(pHeader->pBody);
            snprintf(str, 1024, "vkApiVersion = 0x%x", pPacket->version);
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateInstance: {
            packet_vkCreateInstance* pPacket = (packet_vkCreateInstance*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateInstance(pCreateInfo = %p, pAllocator = %p, pInstance = %p {%" PRIX64 "})", (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pInstance, (pPacket->pInstance == NULL) ? 0 : (uint64_t)*(pPacket->pInstance));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyInstance: {
            packet_vkDestroyInstance* pPacket = (packet_vkDestroyInstance*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyInstance(instance = %p, pAllocator = %p)", (void*)(pPacket->instance), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkEnumeratePhysicalDevices: {
            packet_vkEnumeratePhysicalDevices* pPacket = (packet_vkEnumeratePhysicalDevices*)(pHeader->pBody);
            snprintf(str, 1024, "vkEnumeratePhysicalDevices(instance = %p, *pPhysicalDeviceCount = %u, pPhysicalDevices = %p)", (void*)(pPacket->instance), (pPacket->pPhysicalDeviceCount == NULL) ? 0 : *(pPacket->pPhysicalDeviceCount), (void*)(pPacket->pPhysicalDevices));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures: {
            packet_vkGetPhysicalDeviceFeatures* pPacket = (packet_vkGetPhysicalDeviceFeatures*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceFeatures(physicalDevice = %p, pFeatures = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pFeatures));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties: {
            packet_vkGetPhysicalDeviceFormatProperties* pPacket = (packet_vkGetPhysicalDeviceFormatProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceFormatProperties(physicalDevice = %p, format = %p, pFormatProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->format), (void*)(pPacket->pFormatProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties: {
            packet_vkGetPhysicalDeviceImageFormatProperties* pPacket = (packet_vkGetPhysicalDeviceImageFormatProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceImageFormatProperties(physicalDevice = %p, format = %p, type = %p, tiling = %p, usage = %i, flags = %i, pImageFormatProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->format), (void*)(pPacket->type), (void*)(pPacket->tiling), pPacket->usage, pPacket->flags, (void*)(pPacket->pImageFormatProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties: {
            packet_vkGetPhysicalDeviceProperties* pPacket = (packet_vkGetPhysicalDeviceProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceProperties(physicalDevice = %p, pProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties: {
            packet_vkGetPhysicalDeviceQueueFamilyProperties* pPacket = (packet_vkGetPhysicalDeviceQueueFamilyProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice = %p, *pQueueFamilyPropertyCount = %u, pQueueFamilyProperties = %p)", (void*)(pPacket->physicalDevice), (pPacket->pQueueFamilyPropertyCount == NULL) ? 0 : *(pPacket->pQueueFamilyPropertyCount), (void*)(pPacket->pQueueFamilyProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties: {
            packet_vkGetPhysicalDeviceMemoryProperties* pPacket = (packet_vkGetPhysicalDeviceMemoryProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceMemoryProperties(physicalDevice = %p, pMemoryProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pMemoryProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetInstanceProcAddr: {
            packet_vkGetInstanceProcAddr* pPacket = (packet_vkGetInstanceProcAddr*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetInstanceProcAddr(instance = %p, pName = %p)", (void*)(pPacket->instance), (void*)(pPacket->pName));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDeviceProcAddr: {
            packet_vkGetDeviceProcAddr* pPacket = (packet_vkGetDeviceProcAddr*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDeviceProcAddr(device = %p, pName = %p)", (void*)(pPacket->device), (void*)(pPacket->pName));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateDevice: {
            packet_vkCreateDevice* pPacket = (packet_vkCreateDevice*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateDevice(physicalDevice = %p, pCreateInfo = %p, pAllocator = %p, pDevice = %p {%" PRIX64 "})", (void*)(pPacket->physicalDevice), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pDevice, (pPacket->pDevice == NULL) ? 0 : (uint64_t)*(pPacket->pDevice));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyDevice: {
            packet_vkDestroyDevice* pPacket = (packet_vkDestroyDevice*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyDevice(device = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkEnumerateInstanceExtensionProperties: {
            packet_vkEnumerateInstanceExtensionProperties* pPacket = (packet_vkEnumerateInstanceExtensionProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkEnumerateInstanceExtensionProperties(pLayerName = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->pLayerName), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkEnumerateDeviceExtensionProperties: {
            packet_vkEnumerateDeviceExtensionProperties* pPacket = (packet_vkEnumerateDeviceExtensionProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkEnumerateDeviceExtensionProperties(physicalDevice = %p, pLayerName = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pLayerName), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkEnumerateInstanceLayerProperties: {
            packet_vkEnumerateInstanceLayerProperties* pPacket = (packet_vkEnumerateInstanceLayerProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkEnumerateInstanceLayerProperties(*pPropertyCount = %u, pProperties = %p)", (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkEnumerateDeviceLayerProperties: {
            packet_vkEnumerateDeviceLayerProperties* pPacket = (packet_vkEnumerateDeviceLayerProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkEnumerateDeviceLayerProperties(physicalDevice = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDeviceQueue: {
            packet_vkGetDeviceQueue* pPacket = (packet_vkGetDeviceQueue*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDeviceQueue(device = %p, queueFamilyIndex = %u, queueIndex = %u, pQueue = %p)", (void*)(pPacket->device), pPacket->queueFamilyIndex, pPacket->queueIndex, (void*)(pPacket->pQueue));
            return str;
        }
    case VKTRACE_TPI_VK_vkQueueSubmit: {
            packet_vkQueueSubmit* pPacket = (packet_vkQueueSubmit*)(pHeader->pBody);
            snprintf(str, 1024, "vkQueueSubmit(queue = %p, submitCount = %u, pSubmits = %p [0]={... waitSemaphoreCount=%u, pWaitSemaphores[0]=%" PRIx64 ", cmdBufferCount=%u, pCmdBuffers[0]=%" PRIx64 ", signalSemaphoreCount=%u, pSignalSemaphores[0]=%" PRIx64 " ...}, fence = %p)", (void*)(pPacket->queue), pPacket->submitCount, pPacket->pSubmits, (pPacket->pSubmits == NULL)?0:pPacket->pSubmits->waitSemaphoreCount, (pPacket->pSubmits == NULL)?0:(pPacket->pSubmits->pWaitSemaphores == NULL)?0:(uint64_t)pPacket->pSubmits->pWaitSemaphores[0], (pPacket->pSubmits == NULL)?0:pPacket->pSubmits->commandBufferCount, (pPacket->pSubmits == NULL)?0:(pPacket->pSubmits->pCommandBuffers == NULL)?0:(uint64_t)pPacket->pSubmits->pCommandBuffers[0], (pPacket->pSubmits == NULL)?0:pPacket->pSubmits->signalSemaphoreCount, (pPacket->pSubmits == NULL)?0:(pPacket->pSubmits->pSignalSemaphores == NULL)?0:(uint64_t)pPacket->pSubmits->pSignalSemaphores[0], (void*)pPacket->fence);
            return str;
        }
    case VKTRACE_TPI_VK_vkQueueWaitIdle: {
            packet_vkQueueWaitIdle* pPacket = (packet_vkQueueWaitIdle*)(pHeader->pBody);
            snprintf(str, 1024, "vkQueueWaitIdle(queue = %p)", (void*)(pPacket->queue));
            return str;
        }
    case VKTRACE_TPI_VK_vkDeviceWaitIdle: {
            packet_vkDeviceWaitIdle* pPacket = (packet_vkDeviceWaitIdle*)(pHeader->pBody);
            snprintf(str, 1024, "vkDeviceWaitIdle(device = %p, ", (void*)(pPacket->device));
            return str;
        }
    case VKTRACE_TPI_VK_vkAllocateMemory: {
            packet_vkAllocateMemory* pPacket = (packet_vkAllocateMemory*)(pHeader->pBody);
            snprintf(str, 1024, "vkAllocateMemory(device = %p, pAllocateInfo = %p, pAllocator = %p, pMemory = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pAllocateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pMemory, (pPacket->pMemory == NULL) ? 0 : (uint64_t)*(pPacket->pMemory));
            return str;
        }
    case VKTRACE_TPI_VK_vkFreeMemory: {
            packet_vkFreeMemory* pPacket = (packet_vkFreeMemory*)(pHeader->pBody);
            snprintf(str, 1024, "vkFreeMemory(device = %p, memory = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->memory), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkMapMemory: {
            packet_vkMapMemory* pPacket = (packet_vkMapMemory*)(pHeader->pBody);
            snprintf(str, 1024, "vkMapMemory(device = %p, memory = %p, offset = %" PRIu64 ", size = %" PRIu64 ", flags = %i, ppData = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->memory), pPacket->offset, pPacket->size, pPacket->flags, (void*)pPacket->ppData, (pPacket->ppData == NULL) ? 0 : (uint64_t)*(pPacket->ppData));
            return str;
        }
    case VKTRACE_TPI_VK_vkUnmapMemory: {
            packet_vkUnmapMemory* pPacket = (packet_vkUnmapMemory*)(pHeader->pBody);
            snprintf(str, 1024, "vkUnmapMemory(device = %p, memory = %p)", (void*)(pPacket->device), (void*)(pPacket->memory));
            return str;
        }
    case VKTRACE_TPI_VK_vkFlushMappedMemoryRanges: {
            packet_vkFlushMappedMemoryRanges* pPacket = (packet_vkFlushMappedMemoryRanges*)(pHeader->pBody);
            snprintf(str, 1024, "vkFlushMappedMemoryRanges(device = %p, memoryRangeCount = %u, pMemoryRanges = %p [0]={memory=%" PRIx64 ", offset=%" PRIu64 ", size=%" PRIu64 "})", (void*)(pPacket->device), pPacket->memoryRangeCount, pPacket->pMemoryRanges, (pPacket->pMemoryRanges == NULL)?0:(uint64_t)(pPacket->pMemoryRanges->memory), (pPacket->pMemoryRanges == NULL)?0:pPacket->pMemoryRanges->offset, (pPacket->pMemoryRanges == NULL)?0:pPacket->pMemoryRanges->size);
            return str;
        }
    case VKTRACE_TPI_VK_vkInvalidateMappedMemoryRanges: {
            packet_vkInvalidateMappedMemoryRanges* pPacket = (packet_vkInvalidateMappedMemoryRanges*)(pHeader->pBody);
            snprintf(str, 1024, "vkInvalidateMappedMemoryRanges(device = %p, memoryRangeCount = %u, pMemoryRanges = %p [0]={memory=%" PRIx64 ", offset=%" PRIu64 ", size=%" PRIu64 "})", (void*)(pPacket->device), pPacket->memoryRangeCount, pPacket->pMemoryRanges, (pPacket->pMemoryRanges == NULL)?0:(uint64_t)(pPacket->pMemoryRanges->memory), (pPacket->pMemoryRanges == NULL)?0:pPacket->pMemoryRanges->offset, (pPacket->pMemoryRanges == NULL)?0:pPacket->pMemoryRanges->size);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDeviceMemoryCommitment: {
            packet_vkGetDeviceMemoryCommitment* pPacket = (packet_vkGetDeviceMemoryCommitment*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDeviceMemoryCommitment(device = %p, memory = %p, *pCommittedMemoryInBytes = %" PRIu64 ")", (void*)(pPacket->device), (void*)(pPacket->memory), (pPacket->pCommittedMemoryInBytes == NULL) ? 0 : *(pPacket->pCommittedMemoryInBytes));
            return str;
        }
    case VKTRACE_TPI_VK_vkBindBufferMemory: {
            packet_vkBindBufferMemory* pPacket = (packet_vkBindBufferMemory*)(pHeader->pBody);
            snprintf(str, 1024, "vkBindBufferMemory(device = %p, buffer = %p, memory = %p, memoryOffset = %" PRIu64 ")", (void*)(pPacket->device), (void*)(pPacket->buffer), (void*)(pPacket->memory), pPacket->memoryOffset);
            return str;
        }
    case VKTRACE_TPI_VK_vkBindImageMemory: {
            packet_vkBindImageMemory* pPacket = (packet_vkBindImageMemory*)(pHeader->pBody);
            snprintf(str, 1024, "vkBindImageMemory(device = %p, image = %p, memory = %p, memoryOffset = %" PRIu64 ")", (void*)(pPacket->device), (void*)(pPacket->image), (void*)(pPacket->memory), pPacket->memoryOffset);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements: {
            packet_vkGetBufferMemoryRequirements* pPacket = (packet_vkGetBufferMemoryRequirements*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetBufferMemoryRequirements(device = %p, buffer = %p, pMemoryRequirements = %p {size=%" PRIu64 ", alignment=%" PRIu64 ", memoryTypeBits=%0x08X})", (void*)(pPacket->device), (void*)(pPacket->buffer), pPacket->pMemoryRequirements, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->size, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->alignment, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryTypeBits);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetImageMemoryRequirements: {
            packet_vkGetImageMemoryRequirements* pPacket = (packet_vkGetImageMemoryRequirements*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetImageMemoryRequirements(device = %p, image = %p, pMemoryRequirements = %p {size=%" PRIu64 ", alignment=%" PRIu64 ", memoryTypeBits=%0x08X})", (void*)(pPacket->device), (void*)(pPacket->image), pPacket->pMemoryRequirements, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->size, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->alignment, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryTypeBits);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements: {
            packet_vkGetImageSparseMemoryRequirements* pPacket = (packet_vkGetImageSparseMemoryRequirements*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetImageSparseMemoryRequirements(device = %p, image = %p, *pSparseMemoryRequirementCount = %u, pSparseMemoryRequirements = %p)", (void*)(pPacket->device), (void*)(pPacket->image), (pPacket->pSparseMemoryRequirementCount == NULL) ? 0 : *(pPacket->pSparseMemoryRequirementCount), (void*)(pPacket->pSparseMemoryRequirements));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties: {
            packet_vkGetPhysicalDeviceSparseImageFormatProperties* pPacket = (packet_vkGetPhysicalDeviceSparseImageFormatProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSparseImageFormatProperties(physicalDevice = %p, format = %p, type = %p, samples = %p, usage = %i, tiling = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->format), (void*)(pPacket->type), (void*)(pPacket->samples), pPacket->usage, (void*)(pPacket->tiling), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkQueueBindSparse: {
            packet_vkQueueBindSparse* pPacket = (packet_vkQueueBindSparse*)(pHeader->pBody);
            snprintf(str, 1024, "vkQueueBindSparse(queue = %p, bindInfoCount = %u, pBindInfo = %p, fence = %p)", (void*)(pPacket->queue), pPacket->bindInfoCount, (void*)(pPacket->pBindInfo), (void*)pPacket->fence);
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateFence: {
            packet_vkCreateFence* pPacket = (packet_vkCreateFence*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateFence(device = %p, pCreateInfo = %p { flags=%s }, pAllocator = %p, *pFence = %p {%" PRIx64 "})", (void*)(pPacket->device), pPacket->pCreateInfo, (pPacket->pCreateInfo == NULL)?"0":(pPacket->pCreateInfo->flags == VK_FENCE_CREATE_SIGNALED_BIT)?"VK_FENCE_CREATE_SIGNALED_BIT":"0", (void*)(pPacket->pAllocator), (void*)pPacket->pFence, (pPacket->pFence == NULL) ? 0 : (uint64_t)*(pPacket->pFence));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyFence: {
            packet_vkDestroyFence* pPacket = (packet_vkDestroyFence*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyFence(device = %p, fence = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)pPacket->fence, (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkResetFences: {
            packet_vkResetFences* pPacket = (packet_vkResetFences*)(pHeader->pBody);
            snprintf(str, 1024, "vkResetFences(device = %p, fenceCount = %u, *pFences = %p {%" PRIx64 "})", (void*)(pPacket->device), pPacket->fenceCount, (void*)pPacket->pFences, (pPacket->pFences == NULL) ? 0 : (uint64_t)*(pPacket->pFences));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetFenceStatus: {
            packet_vkGetFenceStatus* pPacket = (packet_vkGetFenceStatus*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetFenceStatus(device = %p, fence = %p)", (void*)(pPacket->device), (void*)pPacket->fence);
            return str;
        }
    case VKTRACE_TPI_VK_vkWaitForFences: {
            packet_vkWaitForFences* pPacket = (packet_vkWaitForFences*)(pHeader->pBody);
            snprintf(str, 1024, "vkWaitForFences(device = %p, fenceCount = %u, *pFences = %p {%" PRIx64 "}, waitAll = %s, timeout = %" PRIu64 ")", (void*)(pPacket->device), pPacket->fenceCount, (void*)pPacket->pFences, (pPacket->pFences == NULL) ? 0 : (uint64_t)*(pPacket->pFences), (pPacket->waitAll == VK_TRUE) ? "VK_TRUE" : "VK_FALSE", pPacket->timeout);
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateSemaphore: {
            packet_vkCreateSemaphore* pPacket = (packet_vkCreateSemaphore*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateSemaphore(device = %p, pCreateInfo = %p, pAllocator = %p, pSemaphore = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSemaphore, (pPacket->pSemaphore == NULL) ? 0 : (uint64_t)*(pPacket->pSemaphore));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroySemaphore: {
            packet_vkDestroySemaphore* pPacket = (packet_vkDestroySemaphore*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroySemaphore(device = %p, semaphore = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->semaphore), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateEvent: {
            packet_vkCreateEvent* pPacket = (packet_vkCreateEvent*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateEvent(device = %p, pCreateInfo = %p, pAllocator = %p, pEvent = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pEvent, (pPacket->pEvent == NULL) ? 0 : (uint64_t)*(pPacket->pEvent));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyEvent: {
            packet_vkDestroyEvent* pPacket = (packet_vkDestroyEvent*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyEvent(device = %p, event = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->event), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetEventStatus: {
            packet_vkGetEventStatus* pPacket = (packet_vkGetEventStatus*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetEventStatus(device = %p, event = %p)", (void*)(pPacket->device), (void*)(pPacket->event));
            return str;
        }
    case VKTRACE_TPI_VK_vkSetEvent: {
            packet_vkSetEvent* pPacket = (packet_vkSetEvent*)(pHeader->pBody);
            snprintf(str, 1024, "vkSetEvent(device = %p, event = %p)", (void*)(pPacket->device), (void*)(pPacket->event));
            return str;
        }
    case VKTRACE_TPI_VK_vkResetEvent: {
            packet_vkResetEvent* pPacket = (packet_vkResetEvent*)(pHeader->pBody);
            snprintf(str, 1024, "vkResetEvent(device = %p, event = %p)", (void*)(pPacket->device), (void*)(pPacket->event));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateQueryPool: {
            packet_vkCreateQueryPool* pPacket = (packet_vkCreateQueryPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateQueryPool(device = %p, pCreateInfo = %p, pAllocator = %p, pQueryPool = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pQueryPool, (pPacket->pQueryPool == NULL) ? 0 : (uint64_t)*(pPacket->pQueryPool));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyQueryPool: {
            packet_vkDestroyQueryPool* pPacket = (packet_vkDestroyQueryPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyQueryPool(device = %p, queryPool = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->queryPool), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetQueryPoolResults: {
            packet_vkGetQueryPoolResults* pPacket = (packet_vkGetQueryPoolResults*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetQueryPoolResults(device = %p, queryPool = %p, firstQuery = %u, queryCount = %u, dataSize = " VK_SIZE_T_SPECIFIER ", pData = %p, stride = %" PRIu64 ", flags = %i)", (void*)(pPacket->device), (void*)(pPacket->queryPool), pPacket->firstQuery, pPacket->queryCount, pPacket->dataSize, (void*)(pPacket->pData), pPacket->stride, pPacket->flags);
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateBuffer: {
            packet_vkCreateBuffer* pPacket = (packet_vkCreateBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateBuffer(device = %p, pCreateInfo = %p, pAllocator = %p, pBuffer = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pBuffer, (pPacket->pBuffer == NULL) ? 0 : (uint64_t)*(pPacket->pBuffer));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyBuffer: {
            packet_vkDestroyBuffer* pPacket = (packet_vkDestroyBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyBuffer(device = %p, buffer = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->buffer), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateBufferView: {
            packet_vkCreateBufferView* pPacket = (packet_vkCreateBufferView*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateBufferView(device = %p, pCreateInfo = %p, pAllocator = %p, pView = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pView, (pPacket->pView == NULL) ? 0 : (uint64_t)*(pPacket->pView));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyBufferView: {
            packet_vkDestroyBufferView* pPacket = (packet_vkDestroyBufferView*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyBufferView(device = %p, bufferView = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->bufferView), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateImage: {
            packet_vkCreateImage* pPacket = (packet_vkCreateImage*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateImage(device = %p, pCreateInfo = %p, pAllocator = %p, pImage = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pImage, (pPacket->pImage == NULL) ? 0 : (uint64_t)*(pPacket->pImage));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyImage: {
            packet_vkDestroyImage* pPacket = (packet_vkDestroyImage*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyImage(device = %p, image = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->image), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetImageSubresourceLayout: {
            packet_vkGetImageSubresourceLayout* pPacket = (packet_vkGetImageSubresourceLayout*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetImageSubresourceLayout(device = %p, image = %p, pSubresource = %p, pLayout = %p)", (void*)(pPacket->device), (void*)(pPacket->image), (void*)(pPacket->pSubresource), (void*)(pPacket->pLayout));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateImageView: {
            packet_vkCreateImageView* pPacket = (packet_vkCreateImageView*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateImageView(device = %p, pCreateInfo = %p, pAllocator = %p, pView = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pView, (pPacket->pView == NULL) ? 0 : (uint64_t)*(pPacket->pView));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyImageView: {
            packet_vkDestroyImageView* pPacket = (packet_vkDestroyImageView*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyImageView(device = %p, imageView = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->imageView), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateShaderModule: {
            packet_vkCreateShaderModule* pPacket = (packet_vkCreateShaderModule*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateShaderModule(device = %p, pCreateInfo = %p, pAllocator = %p, pShaderModule = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pShaderModule, (pPacket->pShaderModule == NULL) ? 0 : (uint64_t)*(pPacket->pShaderModule));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyShaderModule: {
            packet_vkDestroyShaderModule* pPacket = (packet_vkDestroyShaderModule*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyShaderModule(device = %p, shaderModule = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->shaderModule), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreatePipelineCache: {
            packet_vkCreatePipelineCache* pPacket = (packet_vkCreatePipelineCache*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreatePipelineCache(device = %p, pCreateInfo = %p, pAllocator = %p, pPipelineCache = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pPipelineCache, (pPacket->pPipelineCache == NULL) ? 0 : (uint64_t)*(pPacket->pPipelineCache));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyPipelineCache: {
            packet_vkDestroyPipelineCache* pPacket = (packet_vkDestroyPipelineCache*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyPipelineCache(device = %p, pipelineCache = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->pipelineCache), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPipelineCacheData: {
            packet_vkGetPipelineCacheData* pPacket = (packet_vkGetPipelineCacheData*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPipelineCacheData(device = %p, pipelineCache = %p, *pDataSize = " VK_SIZE_T_SPECIFIER ", pData = %p)", (void*)(pPacket->device), (void*)(pPacket->pipelineCache), (pPacket->pDataSize == NULL) ? 0 : *(pPacket->pDataSize), (void*)(pPacket->pData));
            return str;
        }
    case VKTRACE_TPI_VK_vkMergePipelineCaches: {
            packet_vkMergePipelineCaches* pPacket = (packet_vkMergePipelineCaches*)(pHeader->pBody);
            snprintf(str, 1024, "vkMergePipelineCaches(device = %p, dstCache = %p, srcCacheCount = %u, pSrcCaches = %p)", (void*)(pPacket->device), (void*)(pPacket->dstCache), pPacket->srcCacheCount, (void*)(pPacket->pSrcCaches));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateGraphicsPipelines: {
            packet_vkCreateGraphicsPipelines* pPacket = (packet_vkCreateGraphicsPipelines*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateGraphicsPipelines(device = %p, pipelineCache = %p, createInfoCount = %u, pCreateInfos = %p, pAllocator = %p, pPipelines = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pipelineCache), pPacket->createInfoCount, (void*)(pPacket->pCreateInfos), (void*)(pPacket->pAllocator), (void*)pPacket->pPipelines, (pPacket->pPipelines == NULL) ? 0 : (uint64_t)*(pPacket->pPipelines));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateComputePipelines: {
            packet_vkCreateComputePipelines* pPacket = (packet_vkCreateComputePipelines*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateComputePipelines(device = %p, pipelineCache = %p, createInfoCount = %u, pCreateInfos = %p, pAllocator = %p, pPipelines = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pipelineCache), pPacket->createInfoCount, (void*)(pPacket->pCreateInfos), (void*)(pPacket->pAllocator), (void*)pPacket->pPipelines, (pPacket->pPipelines == NULL) ? 0 : (uint64_t)*(pPacket->pPipelines));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyPipeline: {
            packet_vkDestroyPipeline* pPacket = (packet_vkDestroyPipeline*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyPipeline(device = %p, pipeline = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->pipeline), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreatePipelineLayout: {
            packet_vkCreatePipelineLayout* pPacket = (packet_vkCreatePipelineLayout*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreatePipelineLayout(device = %p, pCreateInfo = %p, pAllocator = %p, pPipelineLayout = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pPipelineLayout, (pPacket->pPipelineLayout == NULL) ? 0 : (uint64_t)*(pPacket->pPipelineLayout));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyPipelineLayout: {
            packet_vkDestroyPipelineLayout* pPacket = (packet_vkDestroyPipelineLayout*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyPipelineLayout(device = %p, pipelineLayout = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->pipelineLayout), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateSampler: {
            packet_vkCreateSampler* pPacket = (packet_vkCreateSampler*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateSampler(device = %p, pCreateInfo = %p, pAllocator = %p, pSampler = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSampler, (pPacket->pSampler == NULL) ? 0 : (uint64_t)*(pPacket->pSampler));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroySampler: {
            packet_vkDestroySampler* pPacket = (packet_vkDestroySampler*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroySampler(device = %p, sampler = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->sampler), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateDescriptorSetLayout: {
            packet_vkCreateDescriptorSetLayout* pPacket = (packet_vkCreateDescriptorSetLayout*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateDescriptorSetLayout(device = %p, pCreateInfo = %p, pAllocator = %p, pSetLayout = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSetLayout, (pPacket->pSetLayout == NULL) ? 0 : (uint64_t)*(pPacket->pSetLayout));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyDescriptorSetLayout: {
            packet_vkDestroyDescriptorSetLayout* pPacket = (packet_vkDestroyDescriptorSetLayout*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyDescriptorSetLayout(device = %p, descriptorSetLayout = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->descriptorSetLayout), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateDescriptorPool: {
            packet_vkCreateDescriptorPool* pPacket = (packet_vkCreateDescriptorPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateDescriptorPool(device = %p, pCreateInfo = %p, pAllocator = %p, pDescriptorPool = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pDescriptorPool, (pPacket->pDescriptorPool == NULL) ? 0 : (uint64_t)*(pPacket->pDescriptorPool));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyDescriptorPool: {
            packet_vkDestroyDescriptorPool* pPacket = (packet_vkDestroyDescriptorPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyDescriptorPool(device = %p, descriptorPool = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->descriptorPool), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkResetDescriptorPool: {
            packet_vkResetDescriptorPool* pPacket = (packet_vkResetDescriptorPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkResetDescriptorPool(device = %p, descriptorPool = %p, flags = %i, ", (void*)(pPacket->device), (void*)(pPacket->descriptorPool), pPacket->flags);
            return str;
        }
    case VKTRACE_TPI_VK_vkAllocateDescriptorSets: {
            packet_vkAllocateDescriptorSets* pPacket = (packet_vkAllocateDescriptorSets*)(pHeader->pBody);
            snprintf(str, 1024, "vkAllocateDescriptorSets(device = %p, pAllocateInfo = %p, pDescriptorSets = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pAllocateInfo), (void*)pPacket->pDescriptorSets, (pPacket->pDescriptorSets == NULL) ? 0 : (uint64_t)*(pPacket->pDescriptorSets));
            return str;
        }
    case VKTRACE_TPI_VK_vkFreeDescriptorSets: {
            packet_vkFreeDescriptorSets* pPacket = (packet_vkFreeDescriptorSets*)(pHeader->pBody);
            snprintf(str, 1024, "vkFreeDescriptorSets(device = %p, descriptorPool = %p, descriptorSetCount = %u, pDescriptorSets = %p)", (void*)(pPacket->device), (void*)(pPacket->descriptorPool), pPacket->descriptorSetCount, (void*)(pPacket->pDescriptorSets));
            return str;
        }
    case VKTRACE_TPI_VK_vkUpdateDescriptorSets: {
            packet_vkUpdateDescriptorSets* pPacket = (packet_vkUpdateDescriptorSets*)(pHeader->pBody);
            snprintf(str, 1024, "vkUpdateDescriptorSets(device = %p, descriptorWriteCount = %u, pDescriptorWrites = %p, descriptorCopyCount = %u, pDescriptorCopies = %p)", (void*)(pPacket->device), pPacket->descriptorWriteCount, (void*)(pPacket->pDescriptorWrites), pPacket->descriptorCopyCount, (void*)(pPacket->pDescriptorCopies));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateFramebuffer: {
            packet_vkCreateFramebuffer* pPacket = (packet_vkCreateFramebuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateFramebuffer(device = %p, pCreateInfo = %p, pAllocator = %p, pFramebuffer = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pFramebuffer, (pPacket->pFramebuffer == NULL) ? 0 : (uint64_t)*(pPacket->pFramebuffer));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyFramebuffer: {
            packet_vkDestroyFramebuffer* pPacket = (packet_vkDestroyFramebuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyFramebuffer(device = %p, framebuffer = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->framebuffer), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateRenderPass: {
            packet_vkCreateRenderPass* pPacket = (packet_vkCreateRenderPass*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateRenderPass(device = %p, pCreateInfo = %p, pAllocator = %p, pRenderPass = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pRenderPass, (pPacket->pRenderPass == NULL) ? 0 : (uint64_t)*(pPacket->pRenderPass));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyRenderPass: {
            packet_vkDestroyRenderPass* pPacket = (packet_vkDestroyRenderPass*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyRenderPass(device = %p, renderPass = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->renderPass), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetRenderAreaGranularity: {
            packet_vkGetRenderAreaGranularity* pPacket = (packet_vkGetRenderAreaGranularity*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetRenderAreaGranularity(device = %p, renderPass = %p, pGranularity = %p)", (void*)(pPacket->device), (void*)(pPacket->renderPass), (void*)(pPacket->pGranularity));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateCommandPool: {
            packet_vkCreateCommandPool* pPacket = (packet_vkCreateCommandPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateCommandPool(device = %p, pCreateInfo = %p, pAllocator = %p, pCommandPool = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pCommandPool, (pPacket->pCommandPool == NULL) ? 0 : (uint64_t)*(pPacket->pCommandPool));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyCommandPool: {
            packet_vkDestroyCommandPool* pPacket = (packet_vkDestroyCommandPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyCommandPool(device = %p, commandPool = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->commandPool), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkResetCommandPool: {
            packet_vkResetCommandPool* pPacket = (packet_vkResetCommandPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkResetCommandPool(device = %p, commandPool = %p, flags = %i)", (void*)(pPacket->device), (void*)(pPacket->commandPool), pPacket->flags);
            return str;
        }
    case VKTRACE_TPI_VK_vkAllocateCommandBuffers: {
            packet_vkAllocateCommandBuffers* pPacket = (packet_vkAllocateCommandBuffers*)(pHeader->pBody);
            snprintf(str, 1024, "vkAllocateCommandBuffers(device = %p, pAllocateInfo = %p, pCommandBuffers = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pAllocateInfo), (void*)pPacket->pCommandBuffers, (pPacket->pCommandBuffers == NULL) ? 0 : (uint64_t)*(pPacket->pCommandBuffers));
            return str;
        }
    case VKTRACE_TPI_VK_vkFreeCommandBuffers: {
            packet_vkFreeCommandBuffers* pPacket = (packet_vkFreeCommandBuffers*)(pHeader->pBody);
            snprintf(str, 1024, "vkFreeCommandBuffers(device = %p, commandPool = %p, commandBufferCount = %u, pCommandBuffers = %p)", (void*)(pPacket->device), (void*)(pPacket->commandPool), pPacket->commandBufferCount, (void*)(pPacket->pCommandBuffers));
            return str;
        }
    case VKTRACE_TPI_VK_vkBeginCommandBuffer: {
            packet_vkBeginCommandBuffer* pPacket = (packet_vkBeginCommandBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkBeginCommandBuffer(commandBuffer = %p, pBeginInfo = %p, ", (void*)(pPacket->commandBuffer), (void*)(pPacket->pBeginInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkEndCommandBuffer: {
            packet_vkEndCommandBuffer* pPacket = (packet_vkEndCommandBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkEndCommandBuffer(commandBuffer = %p, ", (void*)(pPacket->commandBuffer));
            return str;
        }
    case VKTRACE_TPI_VK_vkResetCommandBuffer: {
            packet_vkResetCommandBuffer* pPacket = (packet_vkResetCommandBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkResetCommandBuffer(commandBuffer = %p, flags = %i)", (void*)(pPacket->commandBuffer), pPacket->flags);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdBindPipeline: {
            packet_vkCmdBindPipeline* pPacket = (packet_vkCmdBindPipeline*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdBindPipeline(commandBuffer = %p, pipelineBindPoint = %i, pipeline = %p)", (void*)(pPacket->commandBuffer), pPacket->pipelineBindPoint, (void*)(pPacket->pipeline));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetViewport: {
            packet_vkCmdSetViewport* pPacket = (packet_vkCmdSetViewport*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetViewport(commandBuffer = %p, firstViewport = %u, viewportCount = %u, pViewports = %p)", (void*)(pPacket->commandBuffer), pPacket->firstViewport, pPacket->viewportCount, (void*)(pPacket->pViewports));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetScissor: {
            packet_vkCmdSetScissor* pPacket = (packet_vkCmdSetScissor*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetScissor(commandBuffer = %p, firstScissor = %u, scissorCount = %u, pScissors = %p)", (void*)(pPacket->commandBuffer), pPacket->firstScissor, pPacket->scissorCount, (void*)(pPacket->pScissors));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetLineWidth: {
            packet_vkCmdSetLineWidth* pPacket = (packet_vkCmdSetLineWidth*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetLineWidth(commandBuffer = %p, lineWidth = %f)", (void*)(pPacket->commandBuffer), pPacket->lineWidth);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetDepthBias: {
            packet_vkCmdSetDepthBias* pPacket = (packet_vkCmdSetDepthBias*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetDepthBias(commandBuffer = %p, depthBiasConstantFactor = %f, depthBiasClamp = %f, depthBiasSlopeFactor = %f)", (void*)(pPacket->commandBuffer), pPacket->depthBiasConstantFactor, pPacket->depthBiasClamp, pPacket->depthBiasSlopeFactor);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetBlendConstants: {
            packet_vkCmdSetBlendConstants* pPacket = (packet_vkCmdSetBlendConstants*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetBlendConstants(commandBuffer = %p, blendConstants = [%f, %f, %f, %f])", (void*)(pPacket->commandBuffer), pPacket->blendConstants[0], pPacket->blendConstants[1], pPacket->blendConstants[2], pPacket->blendConstants[3]);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetDepthBounds: {
            packet_vkCmdSetDepthBounds* pPacket = (packet_vkCmdSetDepthBounds*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetDepthBounds(commandBuffer = %p, minDepthBounds = %f, maxDepthBounds = %f)", (void*)(pPacket->commandBuffer), pPacket->minDepthBounds, pPacket->maxDepthBounds);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetStencilCompareMask: {
            packet_vkCmdSetStencilCompareMask* pPacket = (packet_vkCmdSetStencilCompareMask*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetStencilCompareMask(commandBuffer = %p, faceMask = %i, compareMask = %u)", (void*)(pPacket->commandBuffer), pPacket->faceMask, pPacket->compareMask);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetStencilWriteMask: {
            packet_vkCmdSetStencilWriteMask* pPacket = (packet_vkCmdSetStencilWriteMask*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetStencilWriteMask(commandBuffer = %p, faceMask = %i, writeMask = %u)", (void*)(pPacket->commandBuffer), pPacket->faceMask, pPacket->writeMask);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetStencilReference: {
            packet_vkCmdSetStencilReference* pPacket = (packet_vkCmdSetStencilReference*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetStencilReference(commandBuffer = %p, faceMask = %i, reference = %u)", (void*)(pPacket->commandBuffer), pPacket->faceMask, pPacket->reference);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdBindDescriptorSets: {
            packet_vkCmdBindDescriptorSets* pPacket = (packet_vkCmdBindDescriptorSets*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdBindDescriptorSets(commandBuffer = %p, pipelineBindPoint = %i, layout = %p, firstSet = %u, descriptorSetCount = %u, pDescriptorSets = %p, dynamicOffsetCount = %u, *pDynamicOffsets = %u)", (void*)(pPacket->commandBuffer), pPacket->pipelineBindPoint, (void*)(pPacket->layout), pPacket->firstSet, pPacket->descriptorSetCount, (void*)(pPacket->pDescriptorSets), pPacket->dynamicOffsetCount, (pPacket->pDynamicOffsets == NULL) ? 0 : *(pPacket->pDynamicOffsets));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdBindIndexBuffer: {
            packet_vkCmdBindIndexBuffer* pPacket = (packet_vkCmdBindIndexBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdBindIndexBuffer(commandBuffer = %p, buffer = %p, offset = %" PRIu64 ", indexType = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->buffer), pPacket->offset, (void*)(pPacket->indexType));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdBindVertexBuffers: {
            packet_vkCmdBindVertexBuffers* pPacket = (packet_vkCmdBindVertexBuffers*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdBindVertexBuffers(commandBuffer = %p, firstBinding = %u, bindingCount = %u, pBuffers = %p, *pOffsets = %" PRIu64 ")", (void*)(pPacket->commandBuffer), pPacket->firstBinding, pPacket->bindingCount, (void*)(pPacket->pBuffers), (pPacket->pOffsets == NULL) ? 0 : *(pPacket->pOffsets));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDraw: {
            packet_vkCmdDraw* pPacket = (packet_vkCmdDraw*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDraw(commandBuffer = %p, vertexCount = %u, instanceCount = %u, firstVertex = %u, firstInstance = %u)", (void*)(pPacket->commandBuffer), pPacket->vertexCount, pPacket->instanceCount, pPacket->firstVertex, pPacket->firstInstance);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDrawIndexed: {
            packet_vkCmdDrawIndexed* pPacket = (packet_vkCmdDrawIndexed*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDrawIndexed(commandBuffer = %p, indexCount = %u, instanceCount = %u, firstIndex = %u, vertexOffset = %i, firstInstance = %u)", (void*)(pPacket->commandBuffer), pPacket->indexCount, pPacket->instanceCount, pPacket->firstIndex, pPacket->vertexOffset, pPacket->firstInstance);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDrawIndirect: {
            packet_vkCmdDrawIndirect* pPacket = (packet_vkCmdDrawIndirect*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDrawIndirect(commandBuffer = %p, buffer = %p, offset = %" PRIu64 ", drawCount = %u, stride = %u)", (void*)(pPacket->commandBuffer), (void*)(pPacket->buffer), pPacket->offset, pPacket->drawCount, pPacket->stride);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDrawIndexedIndirect: {
            packet_vkCmdDrawIndexedIndirect* pPacket = (packet_vkCmdDrawIndexedIndirect*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDrawIndexedIndirect(commandBuffer = %p, buffer = %p, offset = %" PRIu64 ", drawCount = %u, stride = %u)", (void*)(pPacket->commandBuffer), (void*)(pPacket->buffer), pPacket->offset, pPacket->drawCount, pPacket->stride);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDispatch: {
            packet_vkCmdDispatch* pPacket = (packet_vkCmdDispatch*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDispatch(commandBuffer = %p, groupCountX = %u, groupCountY = %u, groupCountZ = %u)", (void*)(pPacket->commandBuffer), pPacket->groupCountX, pPacket->groupCountY, pPacket->groupCountZ);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDispatchIndirect: {
            packet_vkCmdDispatchIndirect* pPacket = (packet_vkCmdDispatchIndirect*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDispatchIndirect(commandBuffer = %p, buffer = %p, offset = %" PRIu64 ")", (void*)(pPacket->commandBuffer), (void*)(pPacket->buffer), pPacket->offset);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdCopyBuffer: {
            packet_vkCmdCopyBuffer* pPacket = (packet_vkCmdCopyBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdCopyBuffer(commandBuffer = %p, srcBuffer = %p, dstBuffer = %p, regionCount = %u, pRegions = %p [0]={srcOffset=%" PRIu64 ", dstOffset=%" PRIu64 ", size=%" PRIu64 "})", (void*)(pPacket->commandBuffer), (void*)(pPacket->srcBuffer), (void*)(pPacket->dstBuffer), pPacket->regionCount, pPacket->pRegions, (pPacket->pRegions == NULL)?0:pPacket->pRegions->srcOffset, (pPacket->pRegions == NULL)?0:pPacket->pRegions->dstOffset, (pPacket->pRegions == NULL)?0:pPacket->pRegions->size);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdCopyImage: {
            packet_vkCmdCopyImage* pPacket = (packet_vkCmdCopyImage*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdCopyImage(commandBuffer = %p, srcImage = %p, srcImageLayout = %s, dstImage = %p, dstImageLayout = %s, regionCount = %u, pRegions = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->srcImage), string_VkImageLayout(pPacket->srcImageLayout), (void*)(pPacket->dstImage), string_VkImageLayout(pPacket->dstImageLayout), pPacket->regionCount, (void*)(pPacket->pRegions));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdBlitImage: {
            packet_vkCmdBlitImage* pPacket = (packet_vkCmdBlitImage*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdBlitImage(commandBuffer = %p, srcImage = %p, srcImageLayout = %s, dstImage = %p, dstImageLayout = %s, regionCount = %u, pRegions = %p, filter = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->srcImage), string_VkImageLayout(pPacket->srcImageLayout), (void*)(pPacket->dstImage), string_VkImageLayout(pPacket->dstImageLayout), pPacket->regionCount, (void*)(pPacket->pRegions), (void*)(pPacket->filter));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdCopyBufferToImage: {
            packet_vkCmdCopyBufferToImage* pPacket = (packet_vkCmdCopyBufferToImage*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdCopyBufferToImage(commandBuffer = %p, srcBuffer = %p, dstImage = %p, dstImageLayout = %s, regionCount = %u, pRegions = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->srcBuffer), (void*)(pPacket->dstImage), string_VkImageLayout(pPacket->dstImageLayout), pPacket->regionCount, (void*)(pPacket->pRegions));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdCopyImageToBuffer: {
            packet_vkCmdCopyImageToBuffer* pPacket = (packet_vkCmdCopyImageToBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdCopyImageToBuffer(commandBuffer = %p, srcImage = %p, srcImageLayout = %s, dstBuffer = %p, regionCount = %u, pRegions = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->srcImage), string_VkImageLayout(pPacket->srcImageLayout), (void*)(pPacket->dstBuffer), pPacket->regionCount, (void*)(pPacket->pRegions));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdUpdateBuffer: {
            packet_vkCmdUpdateBuffer* pPacket = (packet_vkCmdUpdateBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdUpdateBuffer(commandBuffer = %p, dstBuffer = %p, dstOffset = %" PRIu64 ", dataSize = %" PRIu64 ", pData = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->dstBuffer), pPacket->dstOffset, pPacket->dataSize, (void*)(pPacket->pData));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdFillBuffer: {
            packet_vkCmdFillBuffer* pPacket = (packet_vkCmdFillBuffer*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdFillBuffer(commandBuffer = %p, dstBuffer = %p, dstOffset = %" PRIu64 ", size = %" PRIu64 ", data = %u)", (void*)(pPacket->commandBuffer), (void*)(pPacket->dstBuffer), pPacket->dstOffset, pPacket->size, pPacket->data);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdClearColorImage: {
            packet_vkCmdClearColorImage* pPacket = (packet_vkCmdClearColorImage*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdClearColorImage(commandBuffer = %p, image = %p, imageLayout = %s, pColor = %p, rangeCount = %u, pRanges = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->image), string_VkImageLayout(pPacket->imageLayout), (void*)&pPacket->pColor, pPacket->rangeCount, (void*)(pPacket->pRanges));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdClearDepthStencilImage: {
            packet_vkCmdClearDepthStencilImage* pPacket = (packet_vkCmdClearDepthStencilImage*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdClearDepthStencilImage(commandBuffer = %p, image = %p, imageLayout = %s, pDepthStencil = %p, rangeCount = %u, pRanges = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->image), string_VkImageLayout(pPacket->imageLayout), (void*)(pPacket->pDepthStencil), pPacket->rangeCount, (void*)(pPacket->pRanges));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdClearAttachments: {
            packet_vkCmdClearAttachments* pPacket = (packet_vkCmdClearAttachments*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdClearAttachments(commandBuffer = %p, attachmentCount = %u, pAttachments = %p, rectCount = %u, pRects = %p)", (void*)(pPacket->commandBuffer), pPacket->attachmentCount, (void*)(pPacket->pAttachments), pPacket->rectCount, (void*)(pPacket->pRects));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdResolveImage: {
            packet_vkCmdResolveImage* pPacket = (packet_vkCmdResolveImage*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdResolveImage(commandBuffer = %p, srcImage = %p, srcImageLayout = %s, dstImage = %p, dstImageLayout = %s, regionCount = %u, pRegions = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->srcImage), string_VkImageLayout(pPacket->srcImageLayout), (void*)(pPacket->dstImage), string_VkImageLayout(pPacket->dstImageLayout), pPacket->regionCount, (void*)(pPacket->pRegions));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetEvent: {
            packet_vkCmdSetEvent* pPacket = (packet_vkCmdSetEvent*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetEvent(commandBuffer = %p, event = %p, stageMask = %i)", (void*)(pPacket->commandBuffer), (void*)(pPacket->event), pPacket->stageMask);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdResetEvent: {
            packet_vkCmdResetEvent* pPacket = (packet_vkCmdResetEvent*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdResetEvent(commandBuffer = %p, event = %p, stageMask = %i)", (void*)(pPacket->commandBuffer), (void*)(pPacket->event), pPacket->stageMask);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdWaitEvents: {
            packet_vkCmdWaitEvents* pPacket = (packet_vkCmdWaitEvents*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdWaitEvents(commandBuffer = %p, eventCount = %u, pEvents = %p, srcStageMask = %i, dstStageMask = %i, memoryBarrierCount = %u, pMemoryBarriers = %p, bufferMemoryBarrierCount = %u, pBufferMemoryBarriers = %p [0]={srcAccessMask=%u, dstAccessMask=%u, srcQueueFamilyIndex=%u, dstQueueFamilyIndex=%u, buffer=%" PRIx64 ", offset=%" PRIu64 ", size=%" PRIu64 "}, imageMemoryBarrierCount = %u, pImageMemoryBarriers = %p [0]={srcAccessMask=%u, dstAccessMask=%u, oldLayout=%s, newLayout=%s, srcQueueFamilyIndex=%u, dstQueueFamilyIndex=%u, image=%" PRIx64 ", subresourceRange=%" PRIx64 "})", (void*)(pPacket->commandBuffer), pPacket->eventCount, (void*)(pPacket->pEvents), pPacket->srcStageMask, pPacket->dstStageMask, pPacket->memoryBarrierCount, (void*)(pPacket->pMemoryBarriers), pPacket->bufferMemoryBarrierCount, pPacket->pBufferMemoryBarriers, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->srcAccessMask, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->dstAccessMask, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->srcQueueFamilyIndex, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->dstQueueFamilyIndex, (pPacket->pBufferMemoryBarriers == NULL)?0:(uint64_t)pPacket->pBufferMemoryBarriers->buffer, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->offset, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->size, pPacket->imageMemoryBarrierCount, pPacket->pImageMemoryBarriers, (pPacket->pImageMemoryBarriers == NULL)?0:pPacket->pImageMemoryBarriers->srcAccessMask, (pPacket->pImageMemoryBarriers == NULL)?0:pPacket->pImageMemoryBarriers->dstAccessMask, (pPacket->pImageMemoryBarriers == NULL)?NULL:string_VkImageLayout(pPacket->pImageMemoryBarriers->oldLayout), (pPacket->pImageMemoryBarriers == NULL)?NULL:string_VkImageLayout(pPacket->pImageMemoryBarriers->newLayout), (pPacket->pImageMemoryBarriers == NULL)?0:pPacket->pImageMemoryBarriers->srcQueueFamilyIndex, (pPacket->pImageMemoryBarriers == NULL)?0:pPacket->pImageMemoryBarriers->dstQueueFamilyIndex, (pPacket->pImageMemoryBarriers == NULL)?0:(uint64_t)(pPacket->pImageMemoryBarriers->image), (pPacket->pImageMemoryBarriers == NULL)?0:(uint64_t)&pPacket->pImageMemoryBarriers->subresourceRange);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdPipelineBarrier: {
            packet_vkCmdPipelineBarrier* pPacket = (packet_vkCmdPipelineBarrier*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdPipelineBarrier(commandBuffer = %p, srcStageMask = %i, dstStageMask = %i, dependencyFlags = %i, memoryBarrierCount = %u, pMemoryBarriers = %p, bufferMemoryBarrierCount = %u, pBufferMemoryBarriers = %p [0]={srcAccessMask=%u, dstAccessMask=%u, srcQueueFamilyIndex=%u, dstQueueFamilyIndex=%u, buffer=%" PRIx64 ", offset=%" PRIu64 ", size=%" PRIu64 "}, imageMemoryBarrierCount = %u, pImageMemoryBarriers = %p [0]={srcAccessMask=%u, dstAccessMask=%u, oldLayout=%s, newLayout=%s, srcQueueFamilyIndex=%u, dstQueueFamilyIndex=%u, image=%" PRIx64 ", subresourceRange=%" PRIx64 "})", (void*)(pPacket->commandBuffer), pPacket->srcStageMask, pPacket->dstStageMask, pPacket->dependencyFlags, pPacket->memoryBarrierCount, (void*)(pPacket->pMemoryBarriers), pPacket->bufferMemoryBarrierCount, pPacket->pBufferMemoryBarriers, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->srcAccessMask, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->dstAccessMask, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->srcQueueFamilyIndex, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->dstQueueFamilyIndex, (pPacket->pBufferMemoryBarriers == NULL)?0:(uint64_t)pPacket->pBufferMemoryBarriers->buffer, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->offset, (pPacket->pBufferMemoryBarriers == NULL)?0:pPacket->pBufferMemoryBarriers->size, pPacket->imageMemoryBarrierCount, pPacket->pImageMemoryBarriers, (pPacket->pImageMemoryBarriers == NULL)?0:pPacket->pImageMemoryBarriers->srcAccessMask, (pPacket->pImageMemoryBarriers == NULL)?0:pPacket->pImageMemoryBarriers->dstAccessMask, (pPacket->pImageMemoryBarriers == NULL)?NULL:string_VkImageLayout(pPacket->pImageMemoryBarriers->oldLayout), (pPacket->pImageMemoryBarriers == NULL)?NULL:string_VkImageLayout(pPacket->pImageMemoryBarriers->newLayout), (pPacket->pImageMemoryBarriers == NULL)?0:pPacket->pImageMemoryBarriers->srcQueueFamilyIndex, (pPacket->pImageMemoryBarriers == NULL)?0:pPacket->pImageMemoryBarriers->dstQueueFamilyIndex, (pPacket->pImageMemoryBarriers == NULL)?0:(uint64_t)(pPacket->pImageMemoryBarriers->image), (pPacket->pImageMemoryBarriers == NULL)?0:(uint64_t)&pPacket->pImageMemoryBarriers->subresourceRange);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdBeginQuery: {
            packet_vkCmdBeginQuery* pPacket = (packet_vkCmdBeginQuery*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdBeginQuery(commandBuffer = %p, queryPool = %p, query = %u, flags = %i)", (void*)(pPacket->commandBuffer), (void*)(pPacket->queryPool), pPacket->query, pPacket->flags);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdEndQuery: {
            packet_vkCmdEndQuery* pPacket = (packet_vkCmdEndQuery*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdEndQuery(commandBuffer = %p, queryPool = %p, query = %u)", (void*)(pPacket->commandBuffer), (void*)(pPacket->queryPool), pPacket->query);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdResetQueryPool: {
            packet_vkCmdResetQueryPool* pPacket = (packet_vkCmdResetQueryPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdResetQueryPool(commandBuffer = %p, queryPool = %p, firstQuery = %u, queryCount = %u)", (void*)(pPacket->commandBuffer), (void*)(pPacket->queryPool), pPacket->firstQuery, pPacket->queryCount);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdWriteTimestamp: {
            packet_vkCmdWriteTimestamp* pPacket = (packet_vkCmdWriteTimestamp*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdWriteTimestamp(commandBuffer = %p, pipelineStage = %p, queryPool = %p, query = %u)", (void*)(pPacket->commandBuffer), (void*)(pPacket->pipelineStage), (void*)(pPacket->queryPool), pPacket->query);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdCopyQueryPoolResults: {
            packet_vkCmdCopyQueryPoolResults* pPacket = (packet_vkCmdCopyQueryPoolResults*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdCopyQueryPoolResults(commandBuffer = %p, queryPool = %p, firstQuery = %u, queryCount = %u, dstBuffer = %p, dstOffset = %" PRIu64 ", stride = %" PRIu64 ", flags = %i)", (void*)(pPacket->commandBuffer), (void*)(pPacket->queryPool), pPacket->firstQuery, pPacket->queryCount, (void*)(pPacket->dstBuffer), pPacket->dstOffset, pPacket->stride, pPacket->flags);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdPushConstants: {
            packet_vkCmdPushConstants* pPacket = (packet_vkCmdPushConstants*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdPushConstants(commandBuffer = %p, layout = %p, stageFlags = %i, offset = %u, size = %u, pValues = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->layout), pPacket->stageFlags, pPacket->offset, pPacket->size, (void*)(pPacket->pValues));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdBeginRenderPass: {
            packet_vkCmdBeginRenderPass* pPacket = (packet_vkCmdBeginRenderPass*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdBeginRenderPass(commandBuffer = %p, pRenderPassBegin = %p, contents = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->pRenderPassBegin), (void*)(pPacket->contents));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdNextSubpass: {
            packet_vkCmdNextSubpass* pPacket = (packet_vkCmdNextSubpass*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdNextSubpass(commandBuffer = %p, contents = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->contents));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdEndRenderPass: {
            packet_vkCmdEndRenderPass* pPacket = (packet_vkCmdEndRenderPass*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdEndRenderPass(commandBuffer = %p)", (void*)(pPacket->commandBuffer));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdExecuteCommands: {
            packet_vkCmdExecuteCommands* pPacket = (packet_vkCmdExecuteCommands*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdExecuteCommands(commandBuffer = %p, commandBufferCount = %u, pCommandBuffers = %p)", (void*)(pPacket->commandBuffer), pPacket->commandBufferCount, (void*)(pPacket->pCommandBuffers));
            return str;
        }
    case VKTRACE_TPI_VK_vkBindBufferMemory2: {
            packet_vkBindBufferMemory2* pPacket = (packet_vkBindBufferMemory2*)(pHeader->pBody);
            snprintf(str, 1024, "vkBindBufferMemory2(device = %p, bindInfoCount = %u, pBindInfos = %p)", (void*)(pPacket->device), pPacket->bindInfoCount, (void*)(pPacket->pBindInfos));
            return str;
        }
    case VKTRACE_TPI_VK_vkBindImageMemory2: {
            packet_vkBindImageMemory2* pPacket = (packet_vkBindImageMemory2*)(pHeader->pBody);
            snprintf(str, 1024, "vkBindImageMemory2(device = %p, bindInfoCount = %u, pBindInfos = %p)", (void*)(pPacket->device), pPacket->bindInfoCount, (void*)(pPacket->pBindInfos));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDeviceGroupPeerMemoryFeatures: {
            packet_vkGetDeviceGroupPeerMemoryFeatures* pPacket = (packet_vkGetDeviceGroupPeerMemoryFeatures*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDeviceGroupPeerMemoryFeatures(device = %p, heapIndex = %u, localDeviceIndex = %u, remoteDeviceIndex = %u, *pPeerMemoryFeatures = %i)", (void*)(pPacket->device), pPacket->heapIndex, pPacket->localDeviceIndex, pPacket->remoteDeviceIndex, (pPacket->pPeerMemoryFeatures == NULL) ? 0 : *(pPacket->pPeerMemoryFeatures));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetDeviceMask: {
            packet_vkCmdSetDeviceMask* pPacket = (packet_vkCmdSetDeviceMask*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetDeviceMask(commandBuffer = %p, deviceMask = %u)", (void*)(pPacket->commandBuffer), pPacket->deviceMask);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDispatchBase: {
            packet_vkCmdDispatchBase* pPacket = (packet_vkCmdDispatchBase*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDispatchBase(commandBuffer = %p, baseGroupX = %u, baseGroupY = %u, baseGroupZ = %u, groupCountX = %u, groupCountY = %u, groupCountZ = %u)", (void*)(pPacket->commandBuffer), pPacket->baseGroupX, pPacket->baseGroupY, pPacket->baseGroupZ, pPacket->groupCountX, pPacket->groupCountY, pPacket->groupCountZ);
            return str;
        }
    case VKTRACE_TPI_VK_vkEnumeratePhysicalDeviceGroups: {
            packet_vkEnumeratePhysicalDeviceGroups* pPacket = (packet_vkEnumeratePhysicalDeviceGroups*)(pHeader->pBody);
            snprintf(str, 1024, "vkEnumeratePhysicalDeviceGroups(instance = %p, *pPhysicalDeviceGroupCount = %u, pPhysicalDeviceGroupProperties = %p)", (void*)(pPacket->instance), (pPacket->pPhysicalDeviceGroupCount == NULL) ? 0 : *(pPacket->pPhysicalDeviceGroupCount), (void*)(pPacket->pPhysicalDeviceGroupProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetImageMemoryRequirements2: {
            packet_vkGetImageMemoryRequirements2* pPacket = (packet_vkGetImageMemoryRequirements2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetImageMemoryRequirements2(device = %p, pInfo = %p, pMemoryRequirements = %p {size=%" PRIu64 ", alignment=%" PRIu64 ", memoryTypeBits=%0x08X})", (void*)(pPacket->device), (void*)(pPacket->pInfo), pPacket->pMemoryRequirements, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.size, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.alignment, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.memoryTypeBits);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements2: {
            packet_vkGetBufferMemoryRequirements2* pPacket = (packet_vkGetBufferMemoryRequirements2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetBufferMemoryRequirements2(device = %p, pInfo = %p, pMemoryRequirements = %p {size=%" PRIu64 ", alignment=%" PRIu64 ", memoryTypeBits=%0x08X})", (void*)(pPacket->device), (void*)(pPacket->pInfo), pPacket->pMemoryRequirements, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.size, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.alignment, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.memoryTypeBits);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements2: {
            packet_vkGetImageSparseMemoryRequirements2* pPacket = (packet_vkGetImageSparseMemoryRequirements2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetImageSparseMemoryRequirements2(device = %p, pInfo = %p, *pSparseMemoryRequirementCount = %u, pSparseMemoryRequirements = %p)", (void*)(pPacket->device), (void*)(pPacket->pInfo), (pPacket->pSparseMemoryRequirementCount == NULL) ? 0 : *(pPacket->pSparseMemoryRequirementCount), (void*)(pPacket->pSparseMemoryRequirements));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures2: {
            packet_vkGetPhysicalDeviceFeatures2* pPacket = (packet_vkGetPhysicalDeviceFeatures2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceFeatures2(physicalDevice = %p, pFeatures = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pFeatures));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties2: {
            packet_vkGetPhysicalDeviceProperties2* pPacket = (packet_vkGetPhysicalDeviceProperties2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceProperties2(physicalDevice = %p, pProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties2: {
            packet_vkGetPhysicalDeviceFormatProperties2* pPacket = (packet_vkGetPhysicalDeviceFormatProperties2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceFormatProperties2(physicalDevice = %p, format = %p, pFormatProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->format), (void*)(pPacket->pFormatProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties2: {
            packet_vkGetPhysicalDeviceImageFormatProperties2* pPacket = (packet_vkGetPhysicalDeviceImageFormatProperties2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceImageFormatProperties2(physicalDevice = %p, pImageFormatInfo = %p, pImageFormatProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pImageFormatInfo), (void*)(pPacket->pImageFormatProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties2: {
            packet_vkGetPhysicalDeviceQueueFamilyProperties2* pPacket = (packet_vkGetPhysicalDeviceQueueFamilyProperties2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceQueueFamilyProperties2(physicalDevice = %p, *pQueueFamilyPropertyCount = %u, pQueueFamilyProperties = %p)", (void*)(pPacket->physicalDevice), (pPacket->pQueueFamilyPropertyCount == NULL) ? 0 : *(pPacket->pQueueFamilyPropertyCount), (void*)(pPacket->pQueueFamilyProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties2: {
            packet_vkGetPhysicalDeviceMemoryProperties2* pPacket = (packet_vkGetPhysicalDeviceMemoryProperties2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceMemoryProperties2(physicalDevice = %p, pMemoryProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pMemoryProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties2: {
            packet_vkGetPhysicalDeviceSparseImageFormatProperties2* pPacket = (packet_vkGetPhysicalDeviceSparseImageFormatProperties2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSparseImageFormatProperties2(physicalDevice = %p, pFormatInfo = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pFormatInfo), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkTrimCommandPool: {
            packet_vkTrimCommandPool* pPacket = (packet_vkTrimCommandPool*)(pHeader->pBody);
            snprintf(str, 1024, "vkTrimCommandPool(device = %p, commandPool = %p, flags = %i)", (void*)(pPacket->device), (void*)(pPacket->commandPool), pPacket->flags);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDeviceQueue2: {
            packet_vkGetDeviceQueue2* pPacket = (packet_vkGetDeviceQueue2*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDeviceQueue2(device = %p, pQueueInfo = %p, pQueue = %p)", (void*)(pPacket->device), (void*)(pPacket->pQueueInfo), (void*)(pPacket->pQueue));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateSamplerYcbcrConversion: {
            packet_vkCreateSamplerYcbcrConversion* pPacket = (packet_vkCreateSamplerYcbcrConversion*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateSamplerYcbcrConversion(device = %p, pCreateInfo = %p, pAllocator = %p, pYcbcrConversion = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pYcbcrConversion, (pPacket->pYcbcrConversion == NULL) ? 0 : (uint64_t)*(pPacket->pYcbcrConversion));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroySamplerYcbcrConversion: {
            packet_vkDestroySamplerYcbcrConversion* pPacket = (packet_vkDestroySamplerYcbcrConversion*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroySamplerYcbcrConversion(device = %p, ycbcrConversion = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->ycbcrConversion), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateDescriptorUpdateTemplate: {
            packet_vkCreateDescriptorUpdateTemplate* pPacket = (packet_vkCreateDescriptorUpdateTemplate*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateDescriptorUpdateTemplate(device = %p, pCreateInfo = %p, pAllocator = %p, pDescriptorUpdateTemplate = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pDescriptorUpdateTemplate, (pPacket->pDescriptorUpdateTemplate == NULL) ? 0 : (uint64_t)*(pPacket->pDescriptorUpdateTemplate));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyDescriptorUpdateTemplate: {
            packet_vkDestroyDescriptorUpdateTemplate* pPacket = (packet_vkDestroyDescriptorUpdateTemplate*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyDescriptorUpdateTemplate(device = %p, descriptorUpdateTemplate = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->descriptorUpdateTemplate), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkUpdateDescriptorSetWithTemplate: {
            packet_vkUpdateDescriptorSetWithTemplate* pPacket = (packet_vkUpdateDescriptorSetWithTemplate*)(pHeader->pBody);
            snprintf(str, 1024, "vkUpdateDescriptorSetWithTemplate(device = %p, descriptorSet = %p, descriptorUpdateTemplate = %p, pData = %p)", (void*)(pPacket->device), (void*)(pPacket->descriptorSet), (void*)(pPacket->descriptorUpdateTemplate), (void*)(pPacket->pData));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalBufferProperties: {
            packet_vkGetPhysicalDeviceExternalBufferProperties* pPacket = (packet_vkGetPhysicalDeviceExternalBufferProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceExternalBufferProperties(physicalDevice = %p, pExternalBufferInfo = %p, pExternalBufferProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pExternalBufferInfo), (void*)(pPacket->pExternalBufferProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalFenceProperties: {
            packet_vkGetPhysicalDeviceExternalFenceProperties* pPacket = (packet_vkGetPhysicalDeviceExternalFenceProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceExternalFenceProperties(physicalDevice = %p, pExternalFenceInfo = %p, pExternalFenceProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pExternalFenceInfo), (void*)(pPacket->pExternalFenceProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalSemaphoreProperties: {
            packet_vkGetPhysicalDeviceExternalSemaphoreProperties* pPacket = (packet_vkGetPhysicalDeviceExternalSemaphoreProperties*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceExternalSemaphoreProperties(physicalDevice = %p, pExternalSemaphoreInfo = %p, pExternalSemaphoreProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pExternalSemaphoreInfo), (void*)(pPacket->pExternalSemaphoreProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDescriptorSetLayoutSupport: {
            packet_vkGetDescriptorSetLayoutSupport* pPacket = (packet_vkGetDescriptorSetLayoutSupport*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDescriptorSetLayoutSupport(device = %p, pCreateInfo = %p, pSupport = %p)", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pSupport));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroySurfaceKHR: {
            packet_vkDestroySurfaceKHR* pPacket = (packet_vkDestroySurfaceKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroySurfaceKHR(instance = %p, surface = %p, pAllocator = %p)", (void*)(pPacket->instance), (void*)(pPacket->surface), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceSupportKHR: {
            packet_vkGetPhysicalDeviceSurfaceSupportKHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceSupportKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice = %p, queueFamilyIndex = %u, surface = %p, *pSupported = %s)", (void*)(pPacket->physicalDevice), pPacket->queueFamilyIndex, (void*)(pPacket->surface), (pPacket->pSupported && *pPacket->pSupported == VK_TRUE) ? "VK_TRUE" : "VK_FALSE");
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilitiesKHR: {
            packet_vkGetPhysicalDeviceSurfaceCapabilitiesKHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceCapabilitiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice = %p, surface = %p, pSurfaceCapabilities = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->surface), (void*)(pPacket->pSurfaceCapabilities));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceFormatsKHR: {
            packet_vkGetPhysicalDeviceSurfaceFormatsKHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceFormatsKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice = %p, surface = %p, *pSurfaceFormatCount = %u, pSurfaceFormats = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->surface), (pPacket->pSurfaceFormatCount == NULL) ? 0 : *(pPacket->pSurfaceFormatCount), (void*)(pPacket->pSurfaceFormats));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfacePresentModesKHR: {
            packet_vkGetPhysicalDeviceSurfacePresentModesKHR* pPacket = (packet_vkGetPhysicalDeviceSurfacePresentModesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice = %p, surface = %p, *pPresentModeCount = %u, pPresentModes = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->surface), (pPacket->pPresentModeCount == NULL) ? 0 : *(pPacket->pPresentModeCount), (void*)(pPacket->pPresentModes));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateSwapchainKHR: {
            packet_vkCreateSwapchainKHR* pPacket = (packet_vkCreateSwapchainKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateSwapchainKHR(device = %p, pCreateInfo = %p, pAllocator = %p, pSwapchain = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSwapchain, (pPacket->pSwapchain == NULL) ? 0 : (uint64_t)*(pPacket->pSwapchain));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroySwapchainKHR: {
            packet_vkDestroySwapchainKHR* pPacket = (packet_vkDestroySwapchainKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroySwapchainKHR(device = %p, swapchain = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->swapchain), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetSwapchainImagesKHR: {
            packet_vkGetSwapchainImagesKHR* pPacket = (packet_vkGetSwapchainImagesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetSwapchainImagesKHR(device = %p, swapchain = %p, *pSwapchainImageCount = %u, pSwapchainImages = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->swapchain), (pPacket->pSwapchainImageCount == NULL) ? 0 : *(pPacket->pSwapchainImageCount), (void*)pPacket->pSwapchainImages, (pPacket->pSwapchainImages == NULL) ? 0 : (uint64_t)*(pPacket->pSwapchainImages));
            return str;
        }
    case VKTRACE_TPI_VK_vkAcquireNextImageKHR: {
            packet_vkAcquireNextImageKHR* pPacket = (packet_vkAcquireNextImageKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkAcquireNextImageKHR(device = %p, swapchain = %p, timeout = %" PRIu64 ", semaphore = %p, fence = %p, *pImageIndex = %u)", (void*)(pPacket->device), (void*)(pPacket->swapchain), pPacket->timeout, (void*)(pPacket->semaphore), (void*)pPacket->fence, (pPacket->pImageIndex == NULL) ? 0 : *(pPacket->pImageIndex));
            return str;
        }
    case VKTRACE_TPI_VK_vkQueuePresentKHR: {
            packet_vkQueuePresentKHR* pPacket = (packet_vkQueuePresentKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkQueuePresentKHR(queue = %p, pPresentInfo = %p {... waitSemaphoreCount=%u, pWaitSemaphores[0]=%" PRIx64 ", swapchainCount=%u, pSwapchains[0]=%" PRIx64 ", pImageIndices[0]=%" PRIu64 " ...})", (void*)(pPacket->queue), pPacket->pPresentInfo, (pPacket->pPresentInfo == NULL)?0:pPacket->pPresentInfo->waitSemaphoreCount, (pPacket->pPresentInfo == NULL)?0:(pPacket->pPresentInfo->pWaitSemaphores == NULL)?0:(uint64_t)pPacket->pPresentInfo->pWaitSemaphores[0], (pPacket->pPresentInfo == NULL)?0:pPacket->pPresentInfo->swapchainCount, (pPacket->pPresentInfo == NULL)?0:(pPacket->pPresentInfo->pSwapchains == NULL)?0:(uint64_t)pPacket->pPresentInfo->pSwapchains[0], (pPacket->pPresentInfo == NULL)?0:(pPacket->pPresentInfo->pImageIndices == NULL)?0:(uint64_t)pPacket->pPresentInfo->pImageIndices[0]);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDeviceGroupPresentCapabilitiesKHR: {
            packet_vkGetDeviceGroupPresentCapabilitiesKHR* pPacket = (packet_vkGetDeviceGroupPresentCapabilitiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDeviceGroupPresentCapabilitiesKHR(device = %p, pDeviceGroupPresentCapabilities = %p)", (void*)(pPacket->device), (void*)(pPacket->pDeviceGroupPresentCapabilities));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDeviceGroupSurfacePresentModesKHR: {
            packet_vkGetDeviceGroupSurfacePresentModesKHR* pPacket = (packet_vkGetDeviceGroupSurfacePresentModesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDeviceGroupSurfacePresentModesKHR(device = %p, surface = %p, *pModes = %i)", (void*)(pPacket->device), (void*)(pPacket->surface), (pPacket->pModes == NULL) ? 0 : *(pPacket->pModes));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDevicePresentRectanglesKHR: {
            packet_vkGetPhysicalDevicePresentRectanglesKHR* pPacket = (packet_vkGetPhysicalDevicePresentRectanglesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDevicePresentRectanglesKHR(physicalDevice = %p, surface = %p, *pRectCount = %u, pRects = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->surface), (pPacket->pRectCount == NULL) ? 0 : *(pPacket->pRectCount), (void*)(pPacket->pRects));
            return str;
        }
    case VKTRACE_TPI_VK_vkAcquireNextImage2KHR: {
            packet_vkAcquireNextImage2KHR* pPacket = (packet_vkAcquireNextImage2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkAcquireNextImage2KHR(device = %p, pAcquireInfo = %p, *pImageIndex = %u)", (void*)(pPacket->device), (void*)(pPacket->pAcquireInfo), (pPacket->pImageIndex == NULL) ? 0 : *(pPacket->pImageIndex));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPropertiesKHR: {
            packet_vkGetPhysicalDeviceDisplayPropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceDisplayPropertiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceDisplayPropertiesKHR(physicalDevice = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPlanePropertiesKHR: {
            packet_vkGetPhysicalDeviceDisplayPlanePropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceDisplayPlanePropertiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceDisplayPlanePropertiesKHR(physicalDevice = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDisplayPlaneSupportedDisplaysKHR: {
            packet_vkGetDisplayPlaneSupportedDisplaysKHR* pPacket = (packet_vkGetDisplayPlaneSupportedDisplaysKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDisplayPlaneSupportedDisplaysKHR(physicalDevice = %p, planeIndex = %u, *pDisplayCount = %u, pDisplays = %p)", (void*)(pPacket->physicalDevice), pPacket->planeIndex, (pPacket->pDisplayCount == NULL) ? 0 : *(pPacket->pDisplayCount), (void*)(pPacket->pDisplays));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDisplayModePropertiesKHR: {
            packet_vkGetDisplayModePropertiesKHR* pPacket = (packet_vkGetDisplayModePropertiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDisplayModePropertiesKHR(physicalDevice = %p, display = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->display), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateDisplayModeKHR: {
            packet_vkCreateDisplayModeKHR* pPacket = (packet_vkCreateDisplayModeKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateDisplayModeKHR(physicalDevice = %p, display = %p, pCreateInfo = %p, pAllocator = %p, pMode = %p {%" PRIX64 "})", (void*)(pPacket->physicalDevice), (void*)(pPacket->display), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pMode, (pPacket->pMode == NULL) ? 0 : (uint64_t)*(pPacket->pMode));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDisplayPlaneCapabilitiesKHR: {
            packet_vkGetDisplayPlaneCapabilitiesKHR* pPacket = (packet_vkGetDisplayPlaneCapabilitiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDisplayPlaneCapabilitiesKHR(physicalDevice = %p, mode = %p, planeIndex = %u, pCapabilities = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->mode), pPacket->planeIndex, (void*)(pPacket->pCapabilities));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateDisplayPlaneSurfaceKHR: {
            packet_vkCreateDisplayPlaneSurfaceKHR* pPacket = (packet_vkCreateDisplayPlaneSurfaceKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateDisplayPlaneSurfaceKHR(instance = %p, pCreateInfo = %p, pAllocator = %p, pSurface = %p {%" PRIX64 "})", (void*)(pPacket->instance), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSurface, (pPacket->pSurface == NULL) ? 0 : (uint64_t)*(pPacket->pSurface));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateSharedSwapchainsKHR: {
            packet_vkCreateSharedSwapchainsKHR* pPacket = (packet_vkCreateSharedSwapchainsKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateSharedSwapchainsKHR(device = %p, swapchainCount = %u, pCreateInfos = %p, pAllocator = %p, pSwapchains = %p {%" PRIX64 "})", (void*)(pPacket->device), pPacket->swapchainCount, (void*)(pPacket->pCreateInfos), (void*)(pPacket->pAllocator), (void*)pPacket->pSwapchains, (pPacket->pSwapchains == NULL) ? 0 : (uint64_t)*(pPacket->pSwapchains));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateXlibSurfaceKHR: {
            packet_vkCreateXlibSurfaceKHR* pPacket = (packet_vkCreateXlibSurfaceKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateXlibSurfaceKHR(instance = %p, pCreateInfo = %p, pAllocator = %p, pSurface = %p {%" PRIX64 "})", (void*)(pPacket->instance), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSurface, (pPacket->pSurface == NULL) ? 0 : (uint64_t)*(pPacket->pSurface));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceXlibPresentationSupportKHR: {
            packet_vkGetPhysicalDeviceXlibPresentationSupportKHR* pPacket = (packet_vkGetPhysicalDeviceXlibPresentationSupportKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceXlibPresentationSupportKHR(physicalDevice = %p, queueFamilyIndex = %u, dpy = %p, visualID = %" PRIu64 ")", (void*)(pPacket->physicalDevice), pPacket->queueFamilyIndex, (void*)(pPacket->dpy), (uint64_t)pPacket->visualID);
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateXcbSurfaceKHR: {
            packet_vkCreateXcbSurfaceKHR* pPacket = (packet_vkCreateXcbSurfaceKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateXcbSurfaceKHR(instance = %p, pCreateInfo = %p, pAllocator = %p, pSurface = %p {%" PRIX64 "})", (void*)(pPacket->instance), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSurface, (pPacket->pSurface == NULL) ? 0 : (uint64_t)*(pPacket->pSurface));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceXcbPresentationSupportKHR: {
            packet_vkGetPhysicalDeviceXcbPresentationSupportKHR* pPacket = (packet_vkGetPhysicalDeviceXcbPresentationSupportKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceXcbPresentationSupportKHR(physicalDevice = %p, queueFamilyIndex = %u, connection = %p, visual_id = %u)", (void*)(pPacket->physicalDevice), pPacket->queueFamilyIndex, (void*)(pPacket->connection), pPacket->visual_id);
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateWaylandSurfaceKHR: {
            packet_vkCreateWaylandSurfaceKHR* pPacket = (packet_vkCreateWaylandSurfaceKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateWaylandSurfaceKHR(instance = %p, pCreateInfo = %p, pAllocator = %p, pSurface = %p {%" PRIX64 "})", (void*)(pPacket->instance), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSurface, (pPacket->pSurface == NULL) ? 0 : (uint64_t)*(pPacket->pSurface));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceWaylandPresentationSupportKHR: {
            packet_vkGetPhysicalDeviceWaylandPresentationSupportKHR* pPacket = (packet_vkGetPhysicalDeviceWaylandPresentationSupportKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceWaylandPresentationSupportKHR(physicalDevice = %p, queueFamilyIndex = %u, display = %p)", (void*)(pPacket->physicalDevice), pPacket->queueFamilyIndex, (void*)(pPacket->display));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateAndroidSurfaceKHR: {
            packet_vkCreateAndroidSurfaceKHR* pPacket = (packet_vkCreateAndroidSurfaceKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateAndroidSurfaceKHR(instance = %p, pCreateInfo = %p, pAllocator = %p, pSurface = %p {%" PRIX64 "})", (void*)(pPacket->instance), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSurface, (pPacket->pSurface == NULL) ? 0 : (uint64_t)*(pPacket->pSurface));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateWin32SurfaceKHR: {
            packet_vkCreateWin32SurfaceKHR* pPacket = (packet_vkCreateWin32SurfaceKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateWin32SurfaceKHR(instance = %p, pCreateInfo = %p, pAllocator = %p, pSurface = %p {%" PRIX64 "})", (void*)(pPacket->instance), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pSurface, (pPacket->pSurface == NULL) ? 0 : (uint64_t)*(pPacket->pSurface));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceWin32PresentationSupportKHR: {
            packet_vkGetPhysicalDeviceWin32PresentationSupportKHR* pPacket = (packet_vkGetPhysicalDeviceWin32PresentationSupportKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceWin32PresentationSupportKHR(physicalDevice = %p, queueFamilyIndex = %u)", (void*)(pPacket->physicalDevice), pPacket->queueFamilyIndex);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures2KHR: {
            packet_vkGetPhysicalDeviceFeatures2KHR* pPacket = (packet_vkGetPhysicalDeviceFeatures2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceFeatures2KHR(physicalDevice = %p, pFeatures = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pFeatures));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties2KHR: {
            packet_vkGetPhysicalDeviceProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceProperties2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceProperties2KHR(physicalDevice = %p, pProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties2KHR: {
            packet_vkGetPhysicalDeviceFormatProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceFormatProperties2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceFormatProperties2KHR(physicalDevice = %p, format = %p, pFormatProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->format), (void*)(pPacket->pFormatProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties2KHR: {
            packet_vkGetPhysicalDeviceImageFormatProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceImageFormatProperties2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceImageFormatProperties2KHR(physicalDevice = %p, pImageFormatInfo = %p, pImageFormatProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pImageFormatInfo), (void*)(pPacket->pImageFormatProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties2KHR: {
            packet_vkGetPhysicalDeviceQueueFamilyProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceQueueFamilyProperties2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceQueueFamilyProperties2KHR(physicalDevice = %p, *pQueueFamilyPropertyCount = %u, pQueueFamilyProperties = %p)", (void*)(pPacket->physicalDevice), (pPacket->pQueueFamilyPropertyCount == NULL) ? 0 : *(pPacket->pQueueFamilyPropertyCount), (void*)(pPacket->pQueueFamilyProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties2KHR: {
            packet_vkGetPhysicalDeviceMemoryProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceMemoryProperties2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceMemoryProperties2KHR(physicalDevice = %p, pMemoryProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pMemoryProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties2KHR: {
            packet_vkGetPhysicalDeviceSparseImageFormatProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceSparseImageFormatProperties2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSparseImageFormatProperties2KHR(physicalDevice = %p, pFormatInfo = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pFormatInfo), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkTrimCommandPoolKHR: {
            packet_vkTrimCommandPoolKHR* pPacket = (packet_vkTrimCommandPoolKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkTrimCommandPoolKHR(device = %p, commandPool = %p, flags = %i)", (void*)(pPacket->device), (void*)(pPacket->commandPool), pPacket->flags);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalBufferPropertiesKHR: {
            packet_vkGetPhysicalDeviceExternalBufferPropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceExternalBufferPropertiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceExternalBufferPropertiesKHR(physicalDevice = %p, pExternalBufferInfo = %p, pExternalBufferProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pExternalBufferInfo), (void*)(pPacket->pExternalBufferProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetMemoryWin32HandleKHR: {
            packet_vkGetMemoryWin32HandleKHR* pPacket = (packet_vkGetMemoryWin32HandleKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetMemoryWin32HandleKHR(device = %p, pGetWin32HandleInfo = %p, pHandle = %p)", (void*)(pPacket->device), (void*)(pPacket->pGetWin32HandleInfo), (void*)(pPacket->pHandle));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetMemoryWin32HandlePropertiesKHR: {
            packet_vkGetMemoryWin32HandlePropertiesKHR* pPacket = (packet_vkGetMemoryWin32HandlePropertiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetMemoryWin32HandlePropertiesKHR(device = %p, handleType = %p, handle = %p, pMemoryWin32HandleProperties = %p)", (void*)(pPacket->device), (void*)(pPacket->handleType), (void*)(pPacket->handle), (void*)(pPacket->pMemoryWin32HandleProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetMemoryFdKHR: {
            packet_vkGetMemoryFdKHR* pPacket = (packet_vkGetMemoryFdKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetMemoryFdKHR(device = %p, pGetFdInfo = %p, *pFd = %i)", (void*)(pPacket->device), (void*)(pPacket->pGetFdInfo), (pPacket->pFd == NULL) ? 0 : *(pPacket->pFd));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetMemoryFdPropertiesKHR: {
            packet_vkGetMemoryFdPropertiesKHR* pPacket = (packet_vkGetMemoryFdPropertiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetMemoryFdPropertiesKHR(device = %p, handleType = %p, fd = %i, pMemoryFdProperties = %p)", (void*)(pPacket->device), (void*)(pPacket->handleType), pPacket->fd, (void*)(pPacket->pMemoryFdProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR: {
            packet_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceExternalSemaphorePropertiesKHR(physicalDevice = %p, pExternalSemaphoreInfo = %p, pExternalSemaphoreProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pExternalSemaphoreInfo), (void*)(pPacket->pExternalSemaphoreProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkImportSemaphoreWin32HandleKHR: {
            packet_vkImportSemaphoreWin32HandleKHR* pPacket = (packet_vkImportSemaphoreWin32HandleKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkImportSemaphoreWin32HandleKHR(device = %p, pImportSemaphoreWin32HandleInfo = %p)", (void*)(pPacket->device), (void*)(pPacket->pImportSemaphoreWin32HandleInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetSemaphoreWin32HandleKHR: {
            packet_vkGetSemaphoreWin32HandleKHR* pPacket = (packet_vkGetSemaphoreWin32HandleKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetSemaphoreWin32HandleKHR(device = %p, pGetWin32HandleInfo = %p, pHandle = %p)", (void*)(pPacket->device), (void*)(pPacket->pGetWin32HandleInfo), (void*)(pPacket->pHandle));
            return str;
        }
    case VKTRACE_TPI_VK_vkImportSemaphoreFdKHR: {
            packet_vkImportSemaphoreFdKHR* pPacket = (packet_vkImportSemaphoreFdKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkImportSemaphoreFdKHR(device = %p, pImportSemaphoreFdInfo = %p)", (void*)(pPacket->device), (void*)(pPacket->pImportSemaphoreFdInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetSemaphoreFdKHR: {
            packet_vkGetSemaphoreFdKHR* pPacket = (packet_vkGetSemaphoreFdKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetSemaphoreFdKHR(device = %p, pGetFdInfo = %p, *pFd = %i)", (void*)(pPacket->device), (void*)(pPacket->pGetFdInfo), (pPacket->pFd == NULL) ? 0 : *(pPacket->pFd));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdPushDescriptorSetKHR: {
            packet_vkCmdPushDescriptorSetKHR* pPacket = (packet_vkCmdPushDescriptorSetKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdPushDescriptorSetKHR(commandBuffer = %p, pipelineBindPoint = %i, layout = %p, set = %u, descriptorWriteCount = %u, pDescriptorWrites = %p)", (void*)(pPacket->commandBuffer), pPacket->pipelineBindPoint, (void*)(pPacket->layout), pPacket->set, pPacket->descriptorWriteCount, (void*)(pPacket->pDescriptorWrites));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdPushDescriptorSetWithTemplateKHR: {
            packet_vkCmdPushDescriptorSetWithTemplateKHR* pPacket = (packet_vkCmdPushDescriptorSetWithTemplateKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdPushDescriptorSetWithTemplateKHR(commandBuffer = %p, descriptorUpdateTemplate = %p, layout = %p, set = %u, pData = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->descriptorUpdateTemplate), (void*)(pPacket->layout), pPacket->set, (void*)(pPacket->pData));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateDescriptorUpdateTemplateKHR: {
            packet_vkCreateDescriptorUpdateTemplateKHR* pPacket = (packet_vkCreateDescriptorUpdateTemplateKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateDescriptorUpdateTemplateKHR(device = %p, pCreateInfo = %p, pAllocator = %p, pDescriptorUpdateTemplate = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pDescriptorUpdateTemplate, (pPacket->pDescriptorUpdateTemplate == NULL) ? 0 : (uint64_t)*(pPacket->pDescriptorUpdateTemplate));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyDescriptorUpdateTemplateKHR: {
            packet_vkDestroyDescriptorUpdateTemplateKHR* pPacket = (packet_vkDestroyDescriptorUpdateTemplateKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyDescriptorUpdateTemplateKHR(device = %p, descriptorUpdateTemplate = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->descriptorUpdateTemplate), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkUpdateDescriptorSetWithTemplateKHR: {
            packet_vkUpdateDescriptorSetWithTemplateKHR* pPacket = (packet_vkUpdateDescriptorSetWithTemplateKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkUpdateDescriptorSetWithTemplateKHR(device = %p, descriptorSet = %p, descriptorUpdateTemplate = %p, pData = %p)", (void*)(pPacket->device), (void*)(pPacket->descriptorSet), (void*)(pPacket->descriptorUpdateTemplate), (void*)(pPacket->pData));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetSwapchainStatusKHR: {
            packet_vkGetSwapchainStatusKHR* pPacket = (packet_vkGetSwapchainStatusKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetSwapchainStatusKHR(device = %p, swapchain = %p)", (void*)(pPacket->device), (void*)(pPacket->swapchain));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalFencePropertiesKHR: {
            packet_vkGetPhysicalDeviceExternalFencePropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceExternalFencePropertiesKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceExternalFencePropertiesKHR(physicalDevice = %p, pExternalFenceInfo = %p, pExternalFenceProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pExternalFenceInfo), (void*)(pPacket->pExternalFenceProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkImportFenceWin32HandleKHR: {
            packet_vkImportFenceWin32HandleKHR* pPacket = (packet_vkImportFenceWin32HandleKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkImportFenceWin32HandleKHR(device = %p, pImportFenceWin32HandleInfo = %p)", (void*)(pPacket->device), (void*)(pPacket->pImportFenceWin32HandleInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetFenceWin32HandleKHR: {
            packet_vkGetFenceWin32HandleKHR* pPacket = (packet_vkGetFenceWin32HandleKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetFenceWin32HandleKHR(device = %p, pGetWin32HandleInfo = %p {fence=%" PRIx64 ", handleType=%" PRIx64 "}, pHandle = %p)", (void*)(pPacket->device), pPacket->pGetWin32HandleInfo, (pPacket->pGetWin32HandleInfo == NULL)?0:(uint64_t)pPacket->pGetWin32HandleInfo->fence, (pPacket->pGetWin32HandleInfo == NULL)?0:(uint64_t)pPacket->pGetWin32HandleInfo->handleType, (void*)(pPacket->pHandle));
            return str;
        }
    case VKTRACE_TPI_VK_vkImportFenceFdKHR: {
            packet_vkImportFenceFdKHR* pPacket = (packet_vkImportFenceFdKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkImportFenceFdKHR(device = %p, pImportFenceFdInfo = %p)", (void*)(pPacket->device), (void*)(pPacket->pImportFenceFdInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetFenceFdKHR: {
            packet_vkGetFenceFdKHR* pPacket = (packet_vkGetFenceFdKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetFenceFdKHR(device = %p, pGetFdInfo = %p {fence=%" PRIx64 ", handleType=%" PRIx64 "}, *pFd = %i)", (void*)(pPacket->device), pPacket->pGetFdInfo, (pPacket->pGetFdInfo == NULL)?0:(uint64_t)pPacket->pGetFdInfo->fence, (pPacket->pGetFdInfo == NULL)?0:(uint64_t)pPacket->pGetFdInfo->handleType, (pPacket->pFd == NULL) ? 0 : *(pPacket->pFd));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilities2KHR: {
            packet_vkGetPhysicalDeviceSurfaceCapabilities2KHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceCapabilities2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSurfaceCapabilities2KHR(physicalDevice = %p, pSurfaceInfo = %p, pSurfaceCapabilities = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pSurfaceInfo), (void*)(pPacket->pSurfaceCapabilities));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceFormats2KHR: {
            packet_vkGetPhysicalDeviceSurfaceFormats2KHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceFormats2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSurfaceFormats2KHR(physicalDevice = %p, pSurfaceInfo = %p, *pSurfaceFormatCount = %u, pSurfaceFormats = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pSurfaceInfo), (pPacket->pSurfaceFormatCount == NULL) ? 0 : *(pPacket->pSurfaceFormatCount), (void*)(pPacket->pSurfaceFormats));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayProperties2KHR: {
            packet_vkGetPhysicalDeviceDisplayProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceDisplayProperties2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceDisplayProperties2KHR(physicalDevice = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPlaneProperties2KHR: {
            packet_vkGetPhysicalDeviceDisplayPlaneProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceDisplayPlaneProperties2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceDisplayPlaneProperties2KHR(physicalDevice = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDisplayModeProperties2KHR: {
            packet_vkGetDisplayModeProperties2KHR* pPacket = (packet_vkGetDisplayModeProperties2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDisplayModeProperties2KHR(physicalDevice = %p, display = %p, *pPropertyCount = %u, pProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->display), (pPacket->pPropertyCount == NULL) ? 0 : *(pPacket->pPropertyCount), (void*)(pPacket->pProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetDisplayPlaneCapabilities2KHR: {
            packet_vkGetDisplayPlaneCapabilities2KHR* pPacket = (packet_vkGetDisplayPlaneCapabilities2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetDisplayPlaneCapabilities2KHR(physicalDevice = %p, pDisplayPlaneInfo = %p, pCapabilities = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pDisplayPlaneInfo), (void*)(pPacket->pCapabilities));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetImageMemoryRequirements2KHR: {
            packet_vkGetImageMemoryRequirements2KHR* pPacket = (packet_vkGetImageMemoryRequirements2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetImageMemoryRequirements2KHR(device = %p, pInfo = %p, pMemoryRequirements = %p {size=%" PRIu64 ", alignment=%" PRIu64 ", memoryTypeBits=%0x08X})", (void*)(pPacket->device), (void*)(pPacket->pInfo), pPacket->pMemoryRequirements, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.size, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.alignment, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.memoryTypeBits);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements2KHR: {
            packet_vkGetBufferMemoryRequirements2KHR* pPacket = (packet_vkGetBufferMemoryRequirements2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetBufferMemoryRequirements2KHR(device = %p, pInfo = %p, pMemoryRequirements = %p {size=%" PRIu64 ", alignment=%" PRIu64 ", memoryTypeBits=%0x08X})", (void*)(pPacket->device), (void*)(pPacket->pInfo), pPacket->pMemoryRequirements, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.size, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.alignment, (pPacket->pMemoryRequirements == NULL)?0:pPacket->pMemoryRequirements->memoryRequirements.memoryTypeBits);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements2KHR: {
            packet_vkGetImageSparseMemoryRequirements2KHR* pPacket = (packet_vkGetImageSparseMemoryRequirements2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetImageSparseMemoryRequirements2KHR(device = %p, pInfo = %p, *pSparseMemoryRequirementCount = %u, pSparseMemoryRequirements = %p)", (void*)(pPacket->device), (void*)(pPacket->pInfo), (pPacket->pSparseMemoryRequirementCount == NULL) ? 0 : *(pPacket->pSparseMemoryRequirementCount), (void*)(pPacket->pSparseMemoryRequirements));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateSamplerYcbcrConversionKHR: {
            packet_vkCreateSamplerYcbcrConversionKHR* pPacket = (packet_vkCreateSamplerYcbcrConversionKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateSamplerYcbcrConversionKHR(device = %p, pCreateInfo = %p, pAllocator = %p, pYcbcrConversion = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pYcbcrConversion, (pPacket->pYcbcrConversion == NULL) ? 0 : (uint64_t)*(pPacket->pYcbcrConversion));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroySamplerYcbcrConversionKHR: {
            packet_vkDestroySamplerYcbcrConversionKHR* pPacket = (packet_vkDestroySamplerYcbcrConversionKHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroySamplerYcbcrConversionKHR(device = %p, ycbcrConversion = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->ycbcrConversion), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkBindBufferMemory2KHR: {
            packet_vkBindBufferMemory2KHR* pPacket = (packet_vkBindBufferMemory2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkBindBufferMemory2KHR(device = %p, bindInfoCount = %u, pBindInfos = %p)", (void*)(pPacket->device), pPacket->bindInfoCount, (void*)(pPacket->pBindInfos));
            return str;
        }
    case VKTRACE_TPI_VK_vkBindImageMemory2KHR: {
            packet_vkBindImageMemory2KHR* pPacket = (packet_vkBindImageMemory2KHR*)(pHeader->pBody);
            snprintf(str, 1024, "vkBindImageMemory2KHR(device = %p, bindInfoCount = %u, pBindInfos = %p)", (void*)(pPacket->device), pPacket->bindInfoCount, (void*)(pPacket->pBindInfos));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateDebugReportCallbackEXT: {
            packet_vkCreateDebugReportCallbackEXT* pPacket = (packet_vkCreateDebugReportCallbackEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateDebugReportCallbackEXT(instance = %p, pCreateInfo = %p, pAllocator = %p, pCallback = %p {%" PRIX64 "})", (void*)(pPacket->instance), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pCallback, (pPacket->pCallback == NULL) ? 0 : (uint64_t)*(pPacket->pCallback));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyDebugReportCallbackEXT: {
            packet_vkDestroyDebugReportCallbackEXT* pPacket = (packet_vkDestroyDebugReportCallbackEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyDebugReportCallbackEXT(instance = %p, callback = %p, pAllocator = %p)", (void*)(pPacket->instance), (void*)(pPacket->callback), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkDebugReportMessageEXT: {
            packet_vkDebugReportMessageEXT* pPacket = (packet_vkDebugReportMessageEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkDebugReportMessageEXT(instance = %p, flags = %i, objectType = %p, object = %" PRIu64 ", location = " VK_SIZE_T_SPECIFIER ", messageCode = %i, pLayerPrefix = %p, pMessage = %p)", (void*)(pPacket->instance), pPacket->flags, (void*)(pPacket->objectType), pPacket->object, pPacket->location, pPacket->messageCode, (void*)(pPacket->pLayerPrefix), (void*)(pPacket->pMessage));
            return str;
        }
    case VKTRACE_TPI_VK_vkDebugMarkerSetObjectTagEXT: {
            packet_vkDebugMarkerSetObjectTagEXT* pPacket = (packet_vkDebugMarkerSetObjectTagEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkDebugMarkerSetObjectTagEXT(device = %p, pTagInfo = %p)", (void*)(pPacket->device), (void*)(pPacket->pTagInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkDebugMarkerSetObjectNameEXT: {
            packet_vkDebugMarkerSetObjectNameEXT* pPacket = (packet_vkDebugMarkerSetObjectNameEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkDebugMarkerSetObjectNameEXT(device = %p, pNameInfo = %p)", (void*)(pPacket->device), (void*)(pPacket->pNameInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDebugMarkerBeginEXT: {
            packet_vkCmdDebugMarkerBeginEXT* pPacket = (packet_vkCmdDebugMarkerBeginEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDebugMarkerBeginEXT(commandBuffer = %p, pMarkerInfo = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->pMarkerInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDebugMarkerEndEXT: {
            packet_vkCmdDebugMarkerEndEXT* pPacket = (packet_vkCmdDebugMarkerEndEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDebugMarkerEndEXT(commandBuffer = %p)", (void*)(pPacket->commandBuffer));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDebugMarkerInsertEXT: {
            packet_vkCmdDebugMarkerInsertEXT* pPacket = (packet_vkCmdDebugMarkerInsertEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDebugMarkerInsertEXT(commandBuffer = %p, pMarkerInfo = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->pMarkerInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDrawIndirectCountAMD: {
            packet_vkCmdDrawIndirectCountAMD* pPacket = (packet_vkCmdDrawIndirectCountAMD*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDrawIndirectCountAMD(commandBuffer = %p, buffer = %p, offset = %" PRIu64 ", countBuffer = %p, countBufferOffset = %" PRIu64 ", maxDrawCount = %u, stride = %u)", (void*)(pPacket->commandBuffer), (void*)(pPacket->buffer), pPacket->offset, (void*)(pPacket->countBuffer), pPacket->countBufferOffset, pPacket->maxDrawCount, pPacket->stride);
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdDrawIndexedIndirectCountAMD: {
            packet_vkCmdDrawIndexedIndirectCountAMD* pPacket = (packet_vkCmdDrawIndexedIndirectCountAMD*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdDrawIndexedIndirectCountAMD(commandBuffer = %p, buffer = %p, offset = %" PRIu64 ", countBuffer = %p, countBufferOffset = %" PRIu64 ", maxDrawCount = %u, stride = %u)", (void*)(pPacket->commandBuffer), (void*)(pPacket->buffer), pPacket->offset, (void*)(pPacket->countBuffer), pPacket->countBufferOffset, pPacket->maxDrawCount, pPacket->stride);
            return str;
        }
    case VKTRACE_TPI_VK_vkGetShaderInfoAMD: {
            packet_vkGetShaderInfoAMD* pPacket = (packet_vkGetShaderInfoAMD*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetShaderInfoAMD(device = %p, pipeline = %p, shaderStage = %p, infoType = %p, *pInfoSize = " VK_SIZE_T_SPECIFIER ", pInfo = %p)", (void*)(pPacket->device), (void*)(pPacket->pipeline), (void*)(pPacket->shaderStage), (void*)(pPacket->infoType), (pPacket->pInfoSize == NULL) ? 0 : *(pPacket->pInfoSize), (void*)(pPacket->pInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalImageFormatPropertiesNV: {
            packet_vkGetPhysicalDeviceExternalImageFormatPropertiesNV* pPacket = (packet_vkGetPhysicalDeviceExternalImageFormatPropertiesNV*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceExternalImageFormatPropertiesNV(physicalDevice = %p, format = %p, type = %p, tiling = %p, usage = %i, flags = %i, externalHandleType = %i, pExternalImageFormatProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->format), (void*)(pPacket->type), (void*)(pPacket->tiling), pPacket->usage, pPacket->flags, pPacket->externalHandleType, (void*)(pPacket->pExternalImageFormatProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetMemoryWin32HandleNV: {
            packet_vkGetMemoryWin32HandleNV* pPacket = (packet_vkGetMemoryWin32HandleNV*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetMemoryWin32HandleNV(device = %p, memory = %p, handleType = %i, pHandle = %p)", (void*)(pPacket->device), (void*)(pPacket->memory), pPacket->handleType, (void*)(pPacket->pHandle));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdProcessCommandsNVX: {
            packet_vkCmdProcessCommandsNVX* pPacket = (packet_vkCmdProcessCommandsNVX*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdProcessCommandsNVX(commandBuffer = %p, pProcessCommandsInfo = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->pProcessCommandsInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdReserveSpaceForCommandsNVX: {
            packet_vkCmdReserveSpaceForCommandsNVX* pPacket = (packet_vkCmdReserveSpaceForCommandsNVX*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdReserveSpaceForCommandsNVX(commandBuffer = %p, pReserveSpaceInfo = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->pReserveSpaceInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateIndirectCommandsLayoutNVX: {
            packet_vkCreateIndirectCommandsLayoutNVX* pPacket = (packet_vkCreateIndirectCommandsLayoutNVX*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateIndirectCommandsLayoutNVX(device = %p, pCreateInfo = %p, pAllocator = %p, pIndirectCommandsLayout = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pIndirectCommandsLayout, (pPacket->pIndirectCommandsLayout == NULL) ? 0 : (uint64_t)*(pPacket->pIndirectCommandsLayout));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyIndirectCommandsLayoutNVX: {
            packet_vkDestroyIndirectCommandsLayoutNVX* pPacket = (packet_vkDestroyIndirectCommandsLayoutNVX*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyIndirectCommandsLayoutNVX(device = %p, indirectCommandsLayout = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->indirectCommandsLayout), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateObjectTableNVX: {
            packet_vkCreateObjectTableNVX* pPacket = (packet_vkCreateObjectTableNVX*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateObjectTableNVX(device = %p, pCreateInfo = %p, pAllocator = %p, pObjectTable = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pObjectTable, (pPacket->pObjectTable == NULL) ? 0 : (uint64_t)*(pPacket->pObjectTable));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyObjectTableNVX: {
            packet_vkDestroyObjectTableNVX* pPacket = (packet_vkDestroyObjectTableNVX*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyObjectTableNVX(device = %p, objectTable = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->objectTable), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkRegisterObjectsNVX: {
            packet_vkRegisterObjectsNVX* pPacket = (packet_vkRegisterObjectsNVX*)(pHeader->pBody);
            snprintf(str, 1024, "vkRegisterObjectsNVX(device = %p, objectTable = %p, objectCount = %u, ppObjectTableEntries = %p, *pObjectIndices = %u)", (void*)(pPacket->device), (void*)(pPacket->objectTable), pPacket->objectCount, (void*)(pPacket->ppObjectTableEntries), (pPacket->pObjectIndices == NULL) ? 0 : *(pPacket->pObjectIndices));
            return str;
        }
    case VKTRACE_TPI_VK_vkUnregisterObjectsNVX: {
            packet_vkUnregisterObjectsNVX* pPacket = (packet_vkUnregisterObjectsNVX*)(pHeader->pBody);
            snprintf(str, 1024, "vkUnregisterObjectsNVX(device = %p, objectTable = %p, objectCount = %u, pObjectEntryTypes = %p, *pObjectIndices = %u)", (void*)(pPacket->device), (void*)(pPacket->objectTable), pPacket->objectCount, (void*)(pPacket->pObjectEntryTypes), (pPacket->pObjectIndices == NULL) ? 0 : *(pPacket->pObjectIndices));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX: {
            packet_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX* pPacket = (packet_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX(physicalDevice = %p, pFeatures = %p, pLimits = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->pFeatures), (void*)(pPacket->pLimits));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetViewportWScalingNV: {
            packet_vkCmdSetViewportWScalingNV* pPacket = (packet_vkCmdSetViewportWScalingNV*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetViewportWScalingNV(commandBuffer = %p, firstViewport = %u, viewportCount = %u, pViewportWScalings = %p)", (void*)(pPacket->commandBuffer), pPacket->firstViewport, pPacket->viewportCount, (void*)(pPacket->pViewportWScalings));
            return str;
        }
    case VKTRACE_TPI_VK_vkReleaseDisplayEXT: {
            packet_vkReleaseDisplayEXT* pPacket = (packet_vkReleaseDisplayEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkReleaseDisplayEXT(physicalDevice = %p, display = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->display));
            return str;
        }
    case VKTRACE_TPI_VK_vkAcquireXlibDisplayEXT: {
            packet_vkAcquireXlibDisplayEXT* pPacket = (packet_vkAcquireXlibDisplayEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkAcquireXlibDisplayEXT(physicalDevice = %p, dpy = %p, display = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->dpy), (void*)(pPacket->display));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetRandROutputDisplayEXT: {
            packet_vkGetRandROutputDisplayEXT* pPacket = (packet_vkGetRandROutputDisplayEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetRandROutputDisplayEXT(physicalDevice = %p, dpy = %p, rrOutput = %u, pDisplay = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->dpy), (uint32_t)(pPacket->rrOutput), (void*)(pPacket->pDisplay));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilities2EXT: {
            packet_vkGetPhysicalDeviceSurfaceCapabilities2EXT* pPacket = (packet_vkGetPhysicalDeviceSurfaceCapabilities2EXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceSurfaceCapabilities2EXT(physicalDevice = %p, surface = %p, pSurfaceCapabilities = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->surface), (void*)(pPacket->pSurfaceCapabilities));
            return str;
        }
    case VKTRACE_TPI_VK_vkDisplayPowerControlEXT: {
            packet_vkDisplayPowerControlEXT* pPacket = (packet_vkDisplayPowerControlEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkDisplayPowerControlEXT(device = %p, display = %p, pDisplayPowerInfo = %p)", (void*)(pPacket->device), (void*)(pPacket->display), (void*)(pPacket->pDisplayPowerInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkRegisterDeviceEventEXT: {
            packet_vkRegisterDeviceEventEXT* pPacket = (packet_vkRegisterDeviceEventEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkRegisterDeviceEventEXT(device = %p, pDeviceEventInfo = %p, pAllocator = %p, *pFence = %p {%" PRIx64 "})", (void*)(pPacket->device), (void*)(pPacket->pDeviceEventInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pFence, (pPacket->pFence == NULL) ? 0 : (uint64_t)*(pPacket->pFence));
            return str;
        }
    case VKTRACE_TPI_VK_vkRegisterDisplayEventEXT: {
            packet_vkRegisterDisplayEventEXT* pPacket = (packet_vkRegisterDisplayEventEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkRegisterDisplayEventEXT(device = %p, display = %p, pDisplayEventInfo = %p, pAllocator = %p, *pFence = %p {%" PRIx64 "})", (void*)(pPacket->device), (void*)(pPacket->display), (void*)(pPacket->pDisplayEventInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pFence, (pPacket->pFence == NULL) ? 0 : (uint64_t)*(pPacket->pFence));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetSwapchainCounterEXT: {
            packet_vkGetSwapchainCounterEXT* pPacket = (packet_vkGetSwapchainCounterEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetSwapchainCounterEXT(device = %p, swapchain = %p, counter = %p, *pCounterValue = %" PRIu64 ")", (void*)(pPacket->device), (void*)(pPacket->swapchain), (void*)(pPacket->counter), (pPacket->pCounterValue == NULL) ? 0 : *(pPacket->pCounterValue));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetRefreshCycleDurationGOOGLE: {
            packet_vkGetRefreshCycleDurationGOOGLE* pPacket = (packet_vkGetRefreshCycleDurationGOOGLE*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetRefreshCycleDurationGOOGLE(device = %p, swapchain = %p, pDisplayTimingProperties = %p)", (void*)(pPacket->device), (void*)(pPacket->swapchain), (void*)(pPacket->pDisplayTimingProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPastPresentationTimingGOOGLE: {
            packet_vkGetPastPresentationTimingGOOGLE* pPacket = (packet_vkGetPastPresentationTimingGOOGLE*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPastPresentationTimingGOOGLE(device = %p, swapchain = %p, *pPresentationTimingCount = %u, pPresentationTimings = %p)", (void*)(pPacket->device), (void*)(pPacket->swapchain), (pPacket->pPresentationTimingCount == NULL) ? 0 : *(pPacket->pPresentationTimingCount), (void*)(pPacket->pPresentationTimings));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetDiscardRectangleEXT: {
            packet_vkCmdSetDiscardRectangleEXT* pPacket = (packet_vkCmdSetDiscardRectangleEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetDiscardRectangleEXT(commandBuffer = %p, firstDiscardRectangle = %u, discardRectangleCount = %u, pDiscardRectangles = %p)", (void*)(pPacket->commandBuffer), pPacket->firstDiscardRectangle, pPacket->discardRectangleCount, (void*)(pPacket->pDiscardRectangles));
            return str;
        }
    case VKTRACE_TPI_VK_vkSetHdrMetadataEXT: {
            packet_vkSetHdrMetadataEXT* pPacket = (packet_vkSetHdrMetadataEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkSetHdrMetadataEXT(device = %p, swapchainCount = %u, pSwapchains = %p, pMetadata = %p)", (void*)(pPacket->device), pPacket->swapchainCount, (void*)(pPacket->pSwapchains), (void*)(pPacket->pMetadata));
            return str;
        }
    case VKTRACE_TPI_VK_vkCmdSetSampleLocationsEXT: {
            packet_vkCmdSetSampleLocationsEXT* pPacket = (packet_vkCmdSetSampleLocationsEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkCmdSetSampleLocationsEXT(commandBuffer = %p, pSampleLocationsInfo = %p)", (void*)(pPacket->commandBuffer), (void*)(pPacket->pSampleLocationsInfo));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetPhysicalDeviceMultisamplePropertiesEXT: {
            packet_vkGetPhysicalDeviceMultisamplePropertiesEXT* pPacket = (packet_vkGetPhysicalDeviceMultisamplePropertiesEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetPhysicalDeviceMultisamplePropertiesEXT(physicalDevice = %p, samples = %p, pMultisampleProperties = %p)", (void*)(pPacket->physicalDevice), (void*)(pPacket->samples), (void*)(pPacket->pMultisampleProperties));
            return str;
        }
    case VKTRACE_TPI_VK_vkCreateValidationCacheEXT: {
            packet_vkCreateValidationCacheEXT* pPacket = (packet_vkCreateValidationCacheEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkCreateValidationCacheEXT(device = %p, pCreateInfo = %p, pAllocator = %p, pValidationCache = %p {%" PRIX64 "})", (void*)(pPacket->device), (void*)(pPacket->pCreateInfo), (void*)(pPacket->pAllocator), (void*)pPacket->pValidationCache, (pPacket->pValidationCache == NULL) ? 0 : (uint64_t)*(pPacket->pValidationCache));
            return str;
        }
    case VKTRACE_TPI_VK_vkDestroyValidationCacheEXT: {
            packet_vkDestroyValidationCacheEXT* pPacket = (packet_vkDestroyValidationCacheEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkDestroyValidationCacheEXT(device = %p, validationCache = %p, pAllocator = %p)", (void*)(pPacket->device), (void*)(pPacket->validationCache), (void*)(pPacket->pAllocator));
            return str;
        }
    case VKTRACE_TPI_VK_vkMergeValidationCachesEXT: {
            packet_vkMergeValidationCachesEXT* pPacket = (packet_vkMergeValidationCachesEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkMergeValidationCachesEXT(device = %p, dstCache = %p, srcCacheCount = %u, pSrcCaches = %p)", (void*)(pPacket->device), (void*)(pPacket->dstCache), pPacket->srcCacheCount, (void*)(pPacket->pSrcCaches));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetValidationCacheDataEXT: {
            packet_vkGetValidationCacheDataEXT* pPacket = (packet_vkGetValidationCacheDataEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetValidationCacheDataEXT(device = %p, validationCache = %p, *pDataSize = " VK_SIZE_T_SPECIFIER ", pData = %p)", (void*)(pPacket->device), (void*)(pPacket->validationCache), (pPacket->pDataSize == NULL) ? 0 : *(pPacket->pDataSize), (void*)(pPacket->pData));
            return str;
        }
    case VKTRACE_TPI_VK_vkGetMemoryHostPointerPropertiesEXT: {
            packet_vkGetMemoryHostPointerPropertiesEXT* pPacket = (packet_vkGetMemoryHostPointerPropertiesEXT*)(pHeader->pBody);
            snprintf(str, 1024, "vkGetMemoryHostPointerPropertiesEXT(device = %p, handleType = %p, pHostPointer = %p, pMemoryHostPointerProperties = %p)", (void*)(pPacket->device), (void*)(pPacket->handleType), (void*)(pPacket->pHostPointer), (void*)(pPacket->pMemoryHostPointerProperties));
            return str;
        }
        default:
            return NULL;
    }
}

static vktrace_trace_packet_header* interpret_trace_packet_vk(vktrace_trace_packet_header* pHeader) { 
    if (pHeader == NULL) { 
        return NULL;
    }
    switch (pHeader->packet_id) { 
        case VKTRACE_TPI_VK_vkApiVersion: {
            return interpret_body_as_vkApiVersion(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateInstance: {
            return interpret_body_as_vkCreateInstance(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyInstance: {
            return interpret_body_as_vkDestroyInstance(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkEnumeratePhysicalDevices: {
            return interpret_body_as_vkEnumeratePhysicalDevices(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures: {
            return interpret_body_as_vkGetPhysicalDeviceFeatures(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties: {
            return interpret_body_as_vkGetPhysicalDeviceFormatProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties: {
            return interpret_body_as_vkGetPhysicalDeviceImageFormatProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties: {
            return interpret_body_as_vkGetPhysicalDeviceProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties: {
            return interpret_body_as_vkGetPhysicalDeviceQueueFamilyProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties: {
            return interpret_body_as_vkGetPhysicalDeviceMemoryProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetInstanceProcAddr: {
            return interpret_body_as_vkGetInstanceProcAddr(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDeviceProcAddr: {
            return interpret_body_as_vkGetDeviceProcAddr(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateDevice: {
            return interpret_body_as_vkCreateDevice(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyDevice: {
            return interpret_body_as_vkDestroyDevice(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkEnumerateInstanceExtensionProperties: {
            return interpret_body_as_vkEnumerateInstanceExtensionProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkEnumerateDeviceExtensionProperties: {
            return interpret_body_as_vkEnumerateDeviceExtensionProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkEnumerateInstanceLayerProperties: {
            return interpret_body_as_vkEnumerateInstanceLayerProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkEnumerateDeviceLayerProperties: {
            return interpret_body_as_vkEnumerateDeviceLayerProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDeviceQueue: {
            return interpret_body_as_vkGetDeviceQueue(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkQueueSubmit: {
            return interpret_body_as_vkQueueSubmit(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkQueueWaitIdle: {
            return interpret_body_as_vkQueueWaitIdle(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDeviceWaitIdle: {
            return interpret_body_as_vkDeviceWaitIdle(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkAllocateMemory: {
            return interpret_body_as_vkAllocateMemory(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkFreeMemory: {
            return interpret_body_as_vkFreeMemory(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkMapMemory: {
            return interpret_body_as_vkMapMemory(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkUnmapMemory: {
            return interpret_body_as_vkUnmapMemory(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkFlushMappedMemoryRanges: {
            return interpret_body_as_vkFlushMappedMemoryRanges(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkInvalidateMappedMemoryRanges: {
            return interpret_body_as_vkInvalidateMappedMemoryRanges(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDeviceMemoryCommitment: {
            return interpret_body_as_vkGetDeviceMemoryCommitment(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkBindBufferMemory: {
            return interpret_body_as_vkBindBufferMemory(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkBindImageMemory: {
            return interpret_body_as_vkBindImageMemory(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements: {
            return interpret_body_as_vkGetBufferMemoryRequirements(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetImageMemoryRequirements: {
            return interpret_body_as_vkGetImageMemoryRequirements(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements: {
            return interpret_body_as_vkGetImageSparseMemoryRequirements(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties: {
            return interpret_body_as_vkGetPhysicalDeviceSparseImageFormatProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkQueueBindSparse: {
            return interpret_body_as_vkQueueBindSparse(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateFence: {
            return interpret_body_as_vkCreateFence(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyFence: {
            return interpret_body_as_vkDestroyFence(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkResetFences: {
            return interpret_body_as_vkResetFences(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetFenceStatus: {
            return interpret_body_as_vkGetFenceStatus(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkWaitForFences: {
            return interpret_body_as_vkWaitForFences(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateSemaphore: {
            return interpret_body_as_vkCreateSemaphore(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroySemaphore: {
            return interpret_body_as_vkDestroySemaphore(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateEvent: {
            return interpret_body_as_vkCreateEvent(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyEvent: {
            return interpret_body_as_vkDestroyEvent(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetEventStatus: {
            return interpret_body_as_vkGetEventStatus(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkSetEvent: {
            return interpret_body_as_vkSetEvent(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkResetEvent: {
            return interpret_body_as_vkResetEvent(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateQueryPool: {
            return interpret_body_as_vkCreateQueryPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyQueryPool: {
            return interpret_body_as_vkDestroyQueryPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetQueryPoolResults: {
            return interpret_body_as_vkGetQueryPoolResults(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateBuffer: {
            return interpret_body_as_vkCreateBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyBuffer: {
            return interpret_body_as_vkDestroyBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateBufferView: {
            return interpret_body_as_vkCreateBufferView(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyBufferView: {
            return interpret_body_as_vkDestroyBufferView(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateImage: {
            return interpret_body_as_vkCreateImage(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyImage: {
            return interpret_body_as_vkDestroyImage(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetImageSubresourceLayout: {
            return interpret_body_as_vkGetImageSubresourceLayout(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateImageView: {
            return interpret_body_as_vkCreateImageView(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyImageView: {
            return interpret_body_as_vkDestroyImageView(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateShaderModule: {
            return interpret_body_as_vkCreateShaderModule(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyShaderModule: {
            return interpret_body_as_vkDestroyShaderModule(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreatePipelineCache: {
            return interpret_body_as_vkCreatePipelineCache(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyPipelineCache: {
            return interpret_body_as_vkDestroyPipelineCache(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPipelineCacheData: {
            return interpret_body_as_vkGetPipelineCacheData(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkMergePipelineCaches: {
            return interpret_body_as_vkMergePipelineCaches(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateGraphicsPipelines: {
            return interpret_body_as_vkCreateGraphicsPipelines(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateComputePipelines: {
            return interpret_body_as_vkCreateComputePipelines(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyPipeline: {
            return interpret_body_as_vkDestroyPipeline(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreatePipelineLayout: {
            return interpret_body_as_vkCreatePipelineLayout(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyPipelineLayout: {
            return interpret_body_as_vkDestroyPipelineLayout(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateSampler: {
            return interpret_body_as_vkCreateSampler(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroySampler: {
            return interpret_body_as_vkDestroySampler(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateDescriptorSetLayout: {
            return interpret_body_as_vkCreateDescriptorSetLayout(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyDescriptorSetLayout: {
            return interpret_body_as_vkDestroyDescriptorSetLayout(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateDescriptorPool: {
            return interpret_body_as_vkCreateDescriptorPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyDescriptorPool: {
            return interpret_body_as_vkDestroyDescriptorPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkResetDescriptorPool: {
            return interpret_body_as_vkResetDescriptorPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkAllocateDescriptorSets: {
            return interpret_body_as_vkAllocateDescriptorSets(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkFreeDescriptorSets: {
            return interpret_body_as_vkFreeDescriptorSets(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkUpdateDescriptorSets: {
            return interpret_body_as_vkUpdateDescriptorSets(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateFramebuffer: {
            return interpret_body_as_vkCreateFramebuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyFramebuffer: {
            return interpret_body_as_vkDestroyFramebuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateRenderPass: {
            return interpret_body_as_vkCreateRenderPass(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyRenderPass: {
            return interpret_body_as_vkDestroyRenderPass(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetRenderAreaGranularity: {
            return interpret_body_as_vkGetRenderAreaGranularity(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateCommandPool: {
            return interpret_body_as_vkCreateCommandPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyCommandPool: {
            return interpret_body_as_vkDestroyCommandPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkResetCommandPool: {
            return interpret_body_as_vkResetCommandPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkAllocateCommandBuffers: {
            return interpret_body_as_vkAllocateCommandBuffers(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkFreeCommandBuffers: {
            return interpret_body_as_vkFreeCommandBuffers(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkBeginCommandBuffer: {
            return interpret_body_as_vkBeginCommandBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkEndCommandBuffer: {
            return interpret_body_as_vkEndCommandBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkResetCommandBuffer: {
            return interpret_body_as_vkResetCommandBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdBindPipeline: {
            return interpret_body_as_vkCmdBindPipeline(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetViewport: {
            return interpret_body_as_vkCmdSetViewport(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetScissor: {
            return interpret_body_as_vkCmdSetScissor(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetLineWidth: {
            return interpret_body_as_vkCmdSetLineWidth(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetDepthBias: {
            return interpret_body_as_vkCmdSetDepthBias(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetBlendConstants: {
            return interpret_body_as_vkCmdSetBlendConstants(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetDepthBounds: {
            return interpret_body_as_vkCmdSetDepthBounds(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetStencilCompareMask: {
            return interpret_body_as_vkCmdSetStencilCompareMask(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetStencilWriteMask: {
            return interpret_body_as_vkCmdSetStencilWriteMask(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetStencilReference: {
            return interpret_body_as_vkCmdSetStencilReference(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdBindDescriptorSets: {
            return interpret_body_as_vkCmdBindDescriptorSets(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdBindIndexBuffer: {
            return interpret_body_as_vkCmdBindIndexBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdBindVertexBuffers: {
            return interpret_body_as_vkCmdBindVertexBuffers(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDraw: {
            return interpret_body_as_vkCmdDraw(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndexed: {
            return interpret_body_as_vkCmdDrawIndexed(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndirect: {
            return interpret_body_as_vkCmdDrawIndirect(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndexedIndirect: {
            return interpret_body_as_vkCmdDrawIndexedIndirect(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDispatch: {
            return interpret_body_as_vkCmdDispatch(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDispatchIndirect: {
            return interpret_body_as_vkCmdDispatchIndirect(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdCopyBuffer: {
            return interpret_body_as_vkCmdCopyBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdCopyImage: {
            return interpret_body_as_vkCmdCopyImage(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdBlitImage: {
            return interpret_body_as_vkCmdBlitImage(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdCopyBufferToImage: {
            return interpret_body_as_vkCmdCopyBufferToImage(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdCopyImageToBuffer: {
            return interpret_body_as_vkCmdCopyImageToBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdUpdateBuffer: {
            return interpret_body_as_vkCmdUpdateBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdFillBuffer: {
            return interpret_body_as_vkCmdFillBuffer(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdClearColorImage: {
            return interpret_body_as_vkCmdClearColorImage(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdClearDepthStencilImage: {
            return interpret_body_as_vkCmdClearDepthStencilImage(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdClearAttachments: {
            return interpret_body_as_vkCmdClearAttachments(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdResolveImage: {
            return interpret_body_as_vkCmdResolveImage(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetEvent: {
            return interpret_body_as_vkCmdSetEvent(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdResetEvent: {
            return interpret_body_as_vkCmdResetEvent(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdWaitEvents: {
            return interpret_body_as_vkCmdWaitEvents(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdPipelineBarrier: {
            return interpret_body_as_vkCmdPipelineBarrier(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdBeginQuery: {
            return interpret_body_as_vkCmdBeginQuery(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdEndQuery: {
            return interpret_body_as_vkCmdEndQuery(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdResetQueryPool: {
            return interpret_body_as_vkCmdResetQueryPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdWriteTimestamp: {
            return interpret_body_as_vkCmdWriteTimestamp(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdCopyQueryPoolResults: {
            return interpret_body_as_vkCmdCopyQueryPoolResults(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdPushConstants: {
            return interpret_body_as_vkCmdPushConstants(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdBeginRenderPass: {
            return interpret_body_as_vkCmdBeginRenderPass(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdNextSubpass: {
            return interpret_body_as_vkCmdNextSubpass(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdEndRenderPass: {
            return interpret_body_as_vkCmdEndRenderPass(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdExecuteCommands: {
            return interpret_body_as_vkCmdExecuteCommands(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkBindBufferMemory2: {
            return interpret_body_as_vkBindBufferMemory2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkBindImageMemory2: {
            return interpret_body_as_vkBindImageMemory2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDeviceGroupPeerMemoryFeatures: {
            return interpret_body_as_vkGetDeviceGroupPeerMemoryFeatures(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetDeviceMask: {
            return interpret_body_as_vkCmdSetDeviceMask(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDispatchBase: {
            return interpret_body_as_vkCmdDispatchBase(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkEnumeratePhysicalDeviceGroups: {
            return interpret_body_as_vkEnumeratePhysicalDeviceGroups(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetImageMemoryRequirements2: {
            return interpret_body_as_vkGetImageMemoryRequirements2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements2: {
            return interpret_body_as_vkGetBufferMemoryRequirements2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements2: {
            return interpret_body_as_vkGetImageSparseMemoryRequirements2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures2: {
            return interpret_body_as_vkGetPhysicalDeviceFeatures2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties2: {
            return interpret_body_as_vkGetPhysicalDeviceProperties2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties2: {
            return interpret_body_as_vkGetPhysicalDeviceFormatProperties2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties2: {
            return interpret_body_as_vkGetPhysicalDeviceImageFormatProperties2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties2: {
            return interpret_body_as_vkGetPhysicalDeviceQueueFamilyProperties2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties2: {
            return interpret_body_as_vkGetPhysicalDeviceMemoryProperties2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties2: {
            return interpret_body_as_vkGetPhysicalDeviceSparseImageFormatProperties2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkTrimCommandPool: {
            return interpret_body_as_vkTrimCommandPool(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDeviceQueue2: {
            return interpret_body_as_vkGetDeviceQueue2(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateSamplerYcbcrConversion: {
            return interpret_body_as_vkCreateSamplerYcbcrConversion(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroySamplerYcbcrConversion: {
            return interpret_body_as_vkDestroySamplerYcbcrConversion(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateDescriptorUpdateTemplate: {
            return interpret_body_as_vkCreateDescriptorUpdateTemplate(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyDescriptorUpdateTemplate: {
            return interpret_body_as_vkDestroyDescriptorUpdateTemplate(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkUpdateDescriptorSetWithTemplate: {
            return interpret_body_as_vkUpdateDescriptorSetWithTemplate(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalBufferProperties: {
            return interpret_body_as_vkGetPhysicalDeviceExternalBufferProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalFenceProperties: {
            return interpret_body_as_vkGetPhysicalDeviceExternalFenceProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalSemaphoreProperties: {
            return interpret_body_as_vkGetPhysicalDeviceExternalSemaphoreProperties(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDescriptorSetLayoutSupport: {
            return interpret_body_as_vkGetDescriptorSetLayoutSupport(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroySurfaceKHR: {
            return interpret_body_as_vkDestroySurfaceKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceSupportKHR: {
            return interpret_body_as_vkGetPhysicalDeviceSurfaceSupportKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilitiesKHR: {
            return interpret_body_as_vkGetPhysicalDeviceSurfaceCapabilitiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceFormatsKHR: {
            return interpret_body_as_vkGetPhysicalDeviceSurfaceFormatsKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfacePresentModesKHR: {
            return interpret_body_as_vkGetPhysicalDeviceSurfacePresentModesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateSwapchainKHR: {
            return interpret_body_as_vkCreateSwapchainKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroySwapchainKHR: {
            return interpret_body_as_vkDestroySwapchainKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetSwapchainImagesKHR: {
            return interpret_body_as_vkGetSwapchainImagesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkAcquireNextImageKHR: {
            return interpret_body_as_vkAcquireNextImageKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkQueuePresentKHR: {
            return interpret_body_as_vkQueuePresentKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDeviceGroupPresentCapabilitiesKHR: {
            return interpret_body_as_vkGetDeviceGroupPresentCapabilitiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDeviceGroupSurfacePresentModesKHR: {
            return interpret_body_as_vkGetDeviceGroupSurfacePresentModesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDevicePresentRectanglesKHR: {
            return interpret_body_as_vkGetPhysicalDevicePresentRectanglesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkAcquireNextImage2KHR: {
            return interpret_body_as_vkAcquireNextImage2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPropertiesKHR: {
            return interpret_body_as_vkGetPhysicalDeviceDisplayPropertiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPlanePropertiesKHR: {
            return interpret_body_as_vkGetPhysicalDeviceDisplayPlanePropertiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDisplayPlaneSupportedDisplaysKHR: {
            return interpret_body_as_vkGetDisplayPlaneSupportedDisplaysKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDisplayModePropertiesKHR: {
            return interpret_body_as_vkGetDisplayModePropertiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateDisplayModeKHR: {
            return interpret_body_as_vkCreateDisplayModeKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDisplayPlaneCapabilitiesKHR: {
            return interpret_body_as_vkGetDisplayPlaneCapabilitiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateDisplayPlaneSurfaceKHR: {
            return interpret_body_as_vkCreateDisplayPlaneSurfaceKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateSharedSwapchainsKHR: {
            return interpret_body_as_vkCreateSharedSwapchainsKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateXlibSurfaceKHR: {
            return interpret_body_as_vkCreateXlibSurfaceKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceXlibPresentationSupportKHR: {
            return interpret_body_as_vkGetPhysicalDeviceXlibPresentationSupportKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateXcbSurfaceKHR: {
            return interpret_body_as_vkCreateXcbSurfaceKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceXcbPresentationSupportKHR: {
            return interpret_body_as_vkGetPhysicalDeviceXcbPresentationSupportKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateWaylandSurfaceKHR: {
            return interpret_body_as_vkCreateWaylandSurfaceKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceWaylandPresentationSupportKHR: {
            return interpret_body_as_vkGetPhysicalDeviceWaylandPresentationSupportKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateAndroidSurfaceKHR: {
            return interpret_body_as_vkCreateAndroidSurfaceKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateWin32SurfaceKHR: {
            return interpret_body_as_vkCreateWin32SurfaceKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceWin32PresentationSupportKHR: {
            return interpret_body_as_vkGetPhysicalDeviceWin32PresentationSupportKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceFeatures2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceProperties2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceFormatProperties2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceImageFormatProperties2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceQueueFamilyProperties2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceMemoryProperties2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceSparseImageFormatProperties2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkTrimCommandPoolKHR: {
            return interpret_body_as_vkTrimCommandPoolKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalBufferPropertiesKHR: {
            return interpret_body_as_vkGetPhysicalDeviceExternalBufferPropertiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetMemoryWin32HandleKHR: {
            return interpret_body_as_vkGetMemoryWin32HandleKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetMemoryWin32HandlePropertiesKHR: {
            return interpret_body_as_vkGetMemoryWin32HandlePropertiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetMemoryFdKHR: {
            return interpret_body_as_vkGetMemoryFdKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetMemoryFdPropertiesKHR: {
            return interpret_body_as_vkGetMemoryFdPropertiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR: {
            return interpret_body_as_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkImportSemaphoreWin32HandleKHR: {
            return interpret_body_as_vkImportSemaphoreWin32HandleKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetSemaphoreWin32HandleKHR: {
            return interpret_body_as_vkGetSemaphoreWin32HandleKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkImportSemaphoreFdKHR: {
            return interpret_body_as_vkImportSemaphoreFdKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetSemaphoreFdKHR: {
            return interpret_body_as_vkGetSemaphoreFdKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdPushDescriptorSetKHR: {
            return interpret_body_as_vkCmdPushDescriptorSetKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdPushDescriptorSetWithTemplateKHR: {
            return interpret_body_as_vkCmdPushDescriptorSetWithTemplateKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateDescriptorUpdateTemplateKHR: {
            return interpret_body_as_vkCreateDescriptorUpdateTemplateKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyDescriptorUpdateTemplateKHR: {
            return interpret_body_as_vkDestroyDescriptorUpdateTemplateKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkUpdateDescriptorSetWithTemplateKHR: {
            return interpret_body_as_vkUpdateDescriptorSetWithTemplateKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetSwapchainStatusKHR: {
            return interpret_body_as_vkGetSwapchainStatusKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalFencePropertiesKHR: {
            return interpret_body_as_vkGetPhysicalDeviceExternalFencePropertiesKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkImportFenceWin32HandleKHR: {
            return interpret_body_as_vkImportFenceWin32HandleKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetFenceWin32HandleKHR: {
            return interpret_body_as_vkGetFenceWin32HandleKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkImportFenceFdKHR: {
            return interpret_body_as_vkImportFenceFdKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetFenceFdKHR: {
            return interpret_body_as_vkGetFenceFdKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilities2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceSurfaceCapabilities2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceFormats2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceSurfaceFormats2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayProperties2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceDisplayProperties2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPlaneProperties2KHR: {
            return interpret_body_as_vkGetPhysicalDeviceDisplayPlaneProperties2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDisplayModeProperties2KHR: {
            return interpret_body_as_vkGetDisplayModeProperties2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetDisplayPlaneCapabilities2KHR: {
            return interpret_body_as_vkGetDisplayPlaneCapabilities2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetImageMemoryRequirements2KHR: {
            return interpret_body_as_vkGetImageMemoryRequirements2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements2KHR: {
            return interpret_body_as_vkGetBufferMemoryRequirements2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements2KHR: {
            return interpret_body_as_vkGetImageSparseMemoryRequirements2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateSamplerYcbcrConversionKHR: {
            return interpret_body_as_vkCreateSamplerYcbcrConversionKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroySamplerYcbcrConversionKHR: {
            return interpret_body_as_vkDestroySamplerYcbcrConversionKHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkBindBufferMemory2KHR: {
            return interpret_body_as_vkBindBufferMemory2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkBindImageMemory2KHR: {
            return interpret_body_as_vkBindImageMemory2KHR(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateDebugReportCallbackEXT: {
            return interpret_body_as_vkCreateDebugReportCallbackEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyDebugReportCallbackEXT: {
            return interpret_body_as_vkDestroyDebugReportCallbackEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDebugReportMessageEXT: {
            return interpret_body_as_vkDebugReportMessageEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDebugMarkerSetObjectTagEXT: {
            return interpret_body_as_vkDebugMarkerSetObjectTagEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDebugMarkerSetObjectNameEXT: {
            return interpret_body_as_vkDebugMarkerSetObjectNameEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDebugMarkerBeginEXT: {
            return interpret_body_as_vkCmdDebugMarkerBeginEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDebugMarkerEndEXT: {
            return interpret_body_as_vkCmdDebugMarkerEndEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDebugMarkerInsertEXT: {
            return interpret_body_as_vkCmdDebugMarkerInsertEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndirectCountAMD: {
            return interpret_body_as_vkCmdDrawIndirectCountAMD(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndexedIndirectCountAMD: {
            return interpret_body_as_vkCmdDrawIndexedIndirectCountAMD(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetShaderInfoAMD: {
            return interpret_body_as_vkGetShaderInfoAMD(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalImageFormatPropertiesNV: {
            return interpret_body_as_vkGetPhysicalDeviceExternalImageFormatPropertiesNV(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetMemoryWin32HandleNV: {
            return interpret_body_as_vkGetMemoryWin32HandleNV(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdProcessCommandsNVX: {
            return interpret_body_as_vkCmdProcessCommandsNVX(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdReserveSpaceForCommandsNVX: {
            return interpret_body_as_vkCmdReserveSpaceForCommandsNVX(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateIndirectCommandsLayoutNVX: {
            return interpret_body_as_vkCreateIndirectCommandsLayoutNVX(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyIndirectCommandsLayoutNVX: {
            return interpret_body_as_vkDestroyIndirectCommandsLayoutNVX(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateObjectTableNVX: {
            return interpret_body_as_vkCreateObjectTableNVX(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyObjectTableNVX: {
            return interpret_body_as_vkDestroyObjectTableNVX(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkRegisterObjectsNVX: {
            return interpret_body_as_vkRegisterObjectsNVX(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkUnregisterObjectsNVX: {
            return interpret_body_as_vkUnregisterObjectsNVX(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX: {
            return interpret_body_as_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetViewportWScalingNV: {
            return interpret_body_as_vkCmdSetViewportWScalingNV(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkReleaseDisplayEXT: {
            return interpret_body_as_vkReleaseDisplayEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkAcquireXlibDisplayEXT: {
            return interpret_body_as_vkAcquireXlibDisplayEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetRandROutputDisplayEXT: {
            return interpret_body_as_vkGetRandROutputDisplayEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilities2EXT: {
            return interpret_body_as_vkGetPhysicalDeviceSurfaceCapabilities2EXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDisplayPowerControlEXT: {
            return interpret_body_as_vkDisplayPowerControlEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkRegisterDeviceEventEXT: {
            return interpret_body_as_vkRegisterDeviceEventEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkRegisterDisplayEventEXT: {
            return interpret_body_as_vkRegisterDisplayEventEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetSwapchainCounterEXT: {
            return interpret_body_as_vkGetSwapchainCounterEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetRefreshCycleDurationGOOGLE: {
            return interpret_body_as_vkGetRefreshCycleDurationGOOGLE(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPastPresentationTimingGOOGLE: {
            return interpret_body_as_vkGetPastPresentationTimingGOOGLE(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetDiscardRectangleEXT: {
            return interpret_body_as_vkCmdSetDiscardRectangleEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkSetHdrMetadataEXT: {
            return interpret_body_as_vkSetHdrMetadataEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCmdSetSampleLocationsEXT: {
            return interpret_body_as_vkCmdSetSampleLocationsEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMultisamplePropertiesEXT: {
            return interpret_body_as_vkGetPhysicalDeviceMultisamplePropertiesEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkCreateValidationCacheEXT: {
            return interpret_body_as_vkCreateValidationCacheEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkDestroyValidationCacheEXT: {
            return interpret_body_as_vkDestroyValidationCacheEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkMergeValidationCachesEXT: {
            return interpret_body_as_vkMergeValidationCachesEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetValidationCacheDataEXT: {
            return interpret_body_as_vkGetValidationCacheDataEXT(pHeader)->header;
        }
        case VKTRACE_TPI_VK_vkGetMemoryHostPointerPropertiesEXT: {
            return interpret_body_as_vkGetMemoryHostPointerPropertiesEXT(pHeader)->header;
        }
        default:
            return NULL;
    }
    return NULL;
}
