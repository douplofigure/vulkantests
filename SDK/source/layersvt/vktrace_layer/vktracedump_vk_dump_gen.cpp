// *** THIS FILE IS GENERATED - DO NOT EDIT ***
// See vktrace_file_generator.py for modifications


/***************************************************************************
 *
 * Copyright (c) 2015-2017 The Khronos Group Inc.
 * Copyright (c) 2015-2017 Valve Corporation
 * Copyright (c) 2015-2017 LunarG, Inc.
 * Copyright (c) 2015-2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: Mark Lobodzinski <mark@lunarg.com>
 * Author: Jon Ashburn <jon@lunarg.com>
 * Author: Tobin Ehlis <tobin@lunarg.com>
 * Author: Peter Lohrmann <peterl@valvesoftware.com>
 * Author: David Pinedo <david@lunarg.com>
 *
 ****************************************************************************/


#include "vktrace_vk_packet_id.h"

#undef NOMINMAX
#include "api_dump_text.h"
#include "api_dump_html.h"
#include "vktracedump_main.h"
void dump_packet(const vktrace_trace_packet_header* packet) {
    switch (packet->packet_id) {
        case VKTRACE_TPI_VK_vkCreateInstance: { 
            packet_vkCreateInstance* pPacket = (packet_vkCreateInstance*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateInstance(dump_inst, pPacket->result, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pInstance);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateInstance(dump_inst, pPacket->result, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pInstance);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyInstance: { 
            packet_vkDestroyInstance* pPacket = (packet_vkDestroyInstance*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyInstance(dump_inst, pPacket->instance, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyInstance(dump_inst, pPacket->instance, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkEnumeratePhysicalDevices: { 
            packet_vkEnumeratePhysicalDevices* pPacket = (packet_vkEnumeratePhysicalDevices*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkEnumeratePhysicalDevices(dump_inst, pPacket->result, pPacket->instance, pPacket->pPhysicalDeviceCount, pPacket->pPhysicalDevices);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkEnumeratePhysicalDevices(dump_inst, pPacket->result, pPacket->instance, pPacket->pPhysicalDeviceCount, pPacket->pPhysicalDevices);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures: { 
            packet_vkGetPhysicalDeviceFeatures* pPacket = (packet_vkGetPhysicalDeviceFeatures*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceFeatures(dump_inst, pPacket->physicalDevice, pPacket->pFeatures);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceFeatures(dump_inst, pPacket->physicalDevice, pPacket->pFeatures);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties: { 
            packet_vkGetPhysicalDeviceFormatProperties* pPacket = (packet_vkGetPhysicalDeviceFormatProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceFormatProperties(dump_inst, pPacket->physicalDevice, pPacket->format, pPacket->pFormatProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceFormatProperties(dump_inst, pPacket->physicalDevice, pPacket->format, pPacket->pFormatProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties: { 
            packet_vkGetPhysicalDeviceImageFormatProperties* pPacket = (packet_vkGetPhysicalDeviceImageFormatProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceImageFormatProperties(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->format, pPacket->type, pPacket->tiling, pPacket->usage, pPacket->flags, pPacket->pImageFormatProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceImageFormatProperties(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->format, pPacket->type, pPacket->tiling, pPacket->usage, pPacket->flags, pPacket->pImageFormatProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties: { 
            packet_vkGetPhysicalDeviceProperties* pPacket = (packet_vkGetPhysicalDeviceProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceProperties(dump_inst, pPacket->physicalDevice, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceProperties(dump_inst, pPacket->physicalDevice, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties: { 
            packet_vkGetPhysicalDeviceQueueFamilyProperties* pPacket = (packet_vkGetPhysicalDeviceQueueFamilyProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceQueueFamilyProperties(dump_inst, pPacket->physicalDevice, pPacket->pQueueFamilyPropertyCount, pPacket->pQueueFamilyProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceQueueFamilyProperties(dump_inst, pPacket->physicalDevice, pPacket->pQueueFamilyPropertyCount, pPacket->pQueueFamilyProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties: { 
            packet_vkGetPhysicalDeviceMemoryProperties* pPacket = (packet_vkGetPhysicalDeviceMemoryProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceMemoryProperties(dump_inst, pPacket->physicalDevice, pPacket->pMemoryProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceMemoryProperties(dump_inst, pPacket->physicalDevice, pPacket->pMemoryProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetInstanceProcAddr: { 
            break;
        }
        case VKTRACE_TPI_VK_vkGetDeviceProcAddr: { 
            break;
        }
        case VKTRACE_TPI_VK_vkCreateDevice: { 
            packet_vkCreateDevice* pPacket = (packet_vkCreateDevice*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateDevice(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pDevice);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateDevice(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pDevice);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyDevice: { 
            packet_vkDestroyDevice* pPacket = (packet_vkDestroyDevice*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyDevice(dump_inst, pPacket->device, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyDevice(dump_inst, pPacket->device, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkEnumerateInstanceExtensionProperties: { 
            packet_vkEnumerateInstanceExtensionProperties* pPacket = (packet_vkEnumerateInstanceExtensionProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkEnumerateInstanceExtensionProperties(dump_inst, pPacket->result, pPacket->pLayerName, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkEnumerateInstanceExtensionProperties(dump_inst, pPacket->result, pPacket->pLayerName, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkEnumerateDeviceExtensionProperties: { 
            packet_vkEnumerateDeviceExtensionProperties* pPacket = (packet_vkEnumerateDeviceExtensionProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkEnumerateDeviceExtensionProperties(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pLayerName, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkEnumerateDeviceExtensionProperties(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pLayerName, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkEnumerateInstanceLayerProperties: { 
            packet_vkEnumerateInstanceLayerProperties* pPacket = (packet_vkEnumerateInstanceLayerProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkEnumerateInstanceLayerProperties(dump_inst, pPacket->result, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkEnumerateInstanceLayerProperties(dump_inst, pPacket->result, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkEnumerateDeviceLayerProperties: { 
            packet_vkEnumerateDeviceLayerProperties* pPacket = (packet_vkEnumerateDeviceLayerProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkEnumerateDeviceLayerProperties(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkEnumerateDeviceLayerProperties(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDeviceQueue: { 
            packet_vkGetDeviceQueue* pPacket = (packet_vkGetDeviceQueue*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDeviceQueue(dump_inst, pPacket->device, pPacket->queueFamilyIndex, pPacket->queueIndex, pPacket->pQueue);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDeviceQueue(dump_inst, pPacket->device, pPacket->queueFamilyIndex, pPacket->queueIndex, pPacket->pQueue);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkQueueSubmit: { 
            packet_vkQueueSubmit* pPacket = (packet_vkQueueSubmit*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkQueueSubmit(dump_inst, pPacket->result, pPacket->queue, pPacket->submitCount, pPacket->pSubmits, pPacket->fence);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkQueueSubmit(dump_inst, pPacket->result, pPacket->queue, pPacket->submitCount, pPacket->pSubmits, pPacket->fence);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkQueueWaitIdle: { 
            packet_vkQueueWaitIdle* pPacket = (packet_vkQueueWaitIdle*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkQueueWaitIdle(dump_inst, pPacket->result, pPacket->queue);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkQueueWaitIdle(dump_inst, pPacket->result, pPacket->queue);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDeviceWaitIdle: { 
            packet_vkDeviceWaitIdle* pPacket = (packet_vkDeviceWaitIdle*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDeviceWaitIdle(dump_inst, pPacket->result, pPacket->device);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDeviceWaitIdle(dump_inst, pPacket->result, pPacket->device);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkAllocateMemory: { 
            packet_vkAllocateMemory* pPacket = (packet_vkAllocateMemory*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkAllocateMemory(dump_inst, pPacket->result, pPacket->device, pPacket->pAllocateInfo, pPacket->pAllocator, pPacket->pMemory);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkAllocateMemory(dump_inst, pPacket->result, pPacket->device, pPacket->pAllocateInfo, pPacket->pAllocator, pPacket->pMemory);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkFreeMemory: { 
            packet_vkFreeMemory* pPacket = (packet_vkFreeMemory*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkFreeMemory(dump_inst, pPacket->device, pPacket->memory, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkFreeMemory(dump_inst, pPacket->device, pPacket->memory, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkMapMemory: { 
            packet_vkMapMemory* pPacket = (packet_vkMapMemory*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkMapMemory(dump_inst, pPacket->result, pPacket->device, pPacket->memory, pPacket->offset, pPacket->size, pPacket->flags, pPacket->ppData);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkMapMemory(dump_inst, pPacket->result, pPacket->device, pPacket->memory, pPacket->offset, pPacket->size, pPacket->flags, pPacket->ppData);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkUnmapMemory: { 
            packet_vkUnmapMemory* pPacket = (packet_vkUnmapMemory*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkUnmapMemory(dump_inst, pPacket->device, pPacket->memory);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkUnmapMemory(dump_inst, pPacket->device, pPacket->memory);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkFlushMappedMemoryRanges: { 
            packet_vkFlushMappedMemoryRanges* pPacket = (packet_vkFlushMappedMemoryRanges*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkFlushMappedMemoryRanges(dump_inst, pPacket->result, pPacket->device, pPacket->memoryRangeCount, pPacket->pMemoryRanges);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkFlushMappedMemoryRanges(dump_inst, pPacket->result, pPacket->device, pPacket->memoryRangeCount, pPacket->pMemoryRanges);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkInvalidateMappedMemoryRanges: { 
            packet_vkInvalidateMappedMemoryRanges* pPacket = (packet_vkInvalidateMappedMemoryRanges*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkInvalidateMappedMemoryRanges(dump_inst, pPacket->result, pPacket->device, pPacket->memoryRangeCount, pPacket->pMemoryRanges);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkInvalidateMappedMemoryRanges(dump_inst, pPacket->result, pPacket->device, pPacket->memoryRangeCount, pPacket->pMemoryRanges);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDeviceMemoryCommitment: { 
            packet_vkGetDeviceMemoryCommitment* pPacket = (packet_vkGetDeviceMemoryCommitment*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDeviceMemoryCommitment(dump_inst, pPacket->device, pPacket->memory, pPacket->pCommittedMemoryInBytes);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDeviceMemoryCommitment(dump_inst, pPacket->device, pPacket->memory, pPacket->pCommittedMemoryInBytes);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkBindBufferMemory: { 
            packet_vkBindBufferMemory* pPacket = (packet_vkBindBufferMemory*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkBindBufferMemory(dump_inst, pPacket->result, pPacket->device, pPacket->buffer, pPacket->memory, pPacket->memoryOffset);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkBindBufferMemory(dump_inst, pPacket->result, pPacket->device, pPacket->buffer, pPacket->memory, pPacket->memoryOffset);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkBindImageMemory: { 
            packet_vkBindImageMemory* pPacket = (packet_vkBindImageMemory*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkBindImageMemory(dump_inst, pPacket->result, pPacket->device, pPacket->image, pPacket->memory, pPacket->memoryOffset);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkBindImageMemory(dump_inst, pPacket->result, pPacket->device, pPacket->image, pPacket->memory, pPacket->memoryOffset);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements: { 
            packet_vkGetBufferMemoryRequirements* pPacket = (packet_vkGetBufferMemoryRequirements*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetBufferMemoryRequirements(dump_inst, pPacket->device, pPacket->buffer, pPacket->pMemoryRequirements);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetBufferMemoryRequirements(dump_inst, pPacket->device, pPacket->buffer, pPacket->pMemoryRequirements);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetImageMemoryRequirements: { 
            packet_vkGetImageMemoryRequirements* pPacket = (packet_vkGetImageMemoryRequirements*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetImageMemoryRequirements(dump_inst, pPacket->device, pPacket->image, pPacket->pMemoryRequirements);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetImageMemoryRequirements(dump_inst, pPacket->device, pPacket->image, pPacket->pMemoryRequirements);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements: { 
            packet_vkGetImageSparseMemoryRequirements* pPacket = (packet_vkGetImageSparseMemoryRequirements*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetImageSparseMemoryRequirements(dump_inst, pPacket->device, pPacket->image, pPacket->pSparseMemoryRequirementCount, pPacket->pSparseMemoryRequirements);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetImageSparseMemoryRequirements(dump_inst, pPacket->device, pPacket->image, pPacket->pSparseMemoryRequirementCount, pPacket->pSparseMemoryRequirements);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties: { 
            packet_vkGetPhysicalDeviceSparseImageFormatProperties* pPacket = (packet_vkGetPhysicalDeviceSparseImageFormatProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSparseImageFormatProperties(dump_inst, pPacket->physicalDevice, pPacket->format, pPacket->type, pPacket->samples, pPacket->usage, pPacket->tiling, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSparseImageFormatProperties(dump_inst, pPacket->physicalDevice, pPacket->format, pPacket->type, pPacket->samples, pPacket->usage, pPacket->tiling, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkQueueBindSparse: { 
            packet_vkQueueBindSparse* pPacket = (packet_vkQueueBindSparse*)(packet->pBody);
            VkSparseImageMemoryBindInfo *sIMBinf = NULL;
            VkSparseBufferMemoryBindInfo *sBMBinf = NULL;
            VkSparseImageOpaqueMemoryBindInfo *sIMOBinf = NULL;
            VkSparseImageMemoryBind *pLocalIMs = NULL;
            VkSparseMemoryBind *pLocalBMs = NULL;
            VkSparseMemoryBind *pLocalIOMs = NULL;
            VkBindSparseInfo *pLocalBIs = VKTRACE_NEW_ARRAY(VkBindSparseInfo, pPacket->bindInfoCount);
            memcpy((void *)pLocalBIs, (void *)(pPacket->pBindInfo), sizeof(VkBindSparseInfo) * pPacket->bindInfoCount);
            for (uint32_t i = 0; i < pPacket->bindInfoCount; i++) {
                vkreplay_process_pnext_structs(pPacket->header, (void *)&pLocalBIs[i]);
                if (pLocalBIs[i].pBufferBinds) {
                    sBMBinf = VKTRACE_NEW_ARRAY(VkSparseBufferMemoryBindInfo, pLocalBIs[i].bufferBindCount);
                    pLocalBIs[i].pBufferBinds =
                        (const VkSparseBufferMemoryBindInfo *)(vktrace_trace_packet_interpret_buffer_pointer(
                            pPacket->header, (intptr_t)pLocalBIs[i].pBufferBinds));
                    memcpy((void *)sBMBinf, (void *)pLocalBIs[i].pBufferBinds,
                        sizeof(VkSparseBufferMemoryBindInfo) * pLocalBIs[i].bufferBindCount);
                    if (pLocalBIs[i].pBufferBinds->bindCount > 0 && pLocalBIs[i].pBufferBinds->pBinds) {
                        pLocalBMs  = (VkSparseMemoryBind *)(vktrace_trace_packet_interpret_buffer_pointer(
                            pPacket->header, (intptr_t)pLocalBIs[i].pBufferBinds->pBinds));
                    }
                    sBMBinf->pBinds = pLocalBMs;
                    pLocalBIs[i].pBufferBinds = sBMBinf;
                }
                if (pLocalBIs[i].pImageBinds) {
                    sIMBinf = VKTRACE_NEW_ARRAY(VkSparseImageMemoryBindInfo, pLocalBIs[i].imageBindCount);
                    pLocalBIs[i].pImageBinds =
                        (const VkSparseImageMemoryBindInfo *)(vktrace_trace_packet_interpret_buffer_pointer(
                            pPacket->header, (intptr_t)pLocalBIs[i].pImageBinds));
                    memcpy((void *)sIMBinf, (void *)pLocalBIs[i].pImageBinds,
                        sizeof(VkSparseImageMemoryBindInfo) * pLocalBIs[i].imageBindCount);
                    if (pLocalBIs[i].pImageBinds->bindCount > 0 && pLocalBIs[i].pImageBinds->pBinds) {
                        pLocalIMs  = (VkSparseImageMemoryBind *)(vktrace_trace_packet_interpret_buffer_pointer(
                            pPacket->header, (intptr_t)pLocalBIs[i].pImageBinds->pBinds));
                    }
                    sIMBinf->pBinds = pLocalIMs;
                    pLocalBIs[i].pImageBinds = sIMBinf;
                }
                if (pLocalBIs[i].pImageOpaqueBinds) {
                    sIMOBinf = VKTRACE_NEW_ARRAY(VkSparseImageOpaqueMemoryBindInfo, pLocalBIs[i].imageOpaqueBindCount);
                    pLocalBIs[i].pImageOpaqueBinds =
                        (const VkSparseImageOpaqueMemoryBindInfo *)(vktrace_trace_packet_interpret_buffer_pointer(
                            pPacket->header, (intptr_t)pLocalBIs[i].pImageOpaqueBinds));
                    memcpy((void *)sIMOBinf, (void *)pLocalBIs[i].pImageOpaqueBinds,
                        sizeof(VkSparseImageOpaqueMemoryBindInfo) * pLocalBIs[i].imageOpaqueBindCount);
                    if (pLocalBIs[i].pImageOpaqueBinds->bindCount > 0 && pLocalBIs[i].pImageOpaqueBinds->pBinds) {
                        pLocalIOMs = (VkSparseMemoryBind *)(vktrace_trace_packet_interpret_buffer_pointer(
                            pPacket->header, (intptr_t)pLocalBIs[i].pImageOpaqueBinds->pBinds));
                    }
                    sIMOBinf->pBinds = pLocalIOMs;
                    pLocalBIs[i].pImageOpaqueBinds = sIMOBinf;
                }
            }
            pPacket->pBindInfo = pLocalBIs;
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkQueueBindSparse(dump_inst, pPacket->result, pPacket->queue, pPacket->bindInfoCount, pPacket->pBindInfo, pPacket->fence);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkQueueBindSparse(dump_inst, pPacket->result, pPacket->queue, pPacket->bindInfoCount, pPacket->pBindInfo, pPacket->fence);
                break;
            }
            VKTRACE_DELETE(pLocalBIs);
            VKTRACE_DELETE(sIMBinf);
            VKTRACE_DELETE(sBMBinf);
            VKTRACE_DELETE(sIMOBinf);
            break;
        }
        case VKTRACE_TPI_VK_vkCreateFence: { 
            packet_vkCreateFence* pPacket = (packet_vkCreateFence*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateFence(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pFence);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateFence(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pFence);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyFence: { 
            packet_vkDestroyFence* pPacket = (packet_vkDestroyFence*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyFence(dump_inst, pPacket->device, pPacket->fence, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyFence(dump_inst, pPacket->device, pPacket->fence, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkResetFences: { 
            packet_vkResetFences* pPacket = (packet_vkResetFences*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkResetFences(dump_inst, pPacket->result, pPacket->device, pPacket->fenceCount, pPacket->pFences);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkResetFences(dump_inst, pPacket->result, pPacket->device, pPacket->fenceCount, pPacket->pFences);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetFenceStatus: { 
            packet_vkGetFenceStatus* pPacket = (packet_vkGetFenceStatus*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetFenceStatus(dump_inst, pPacket->result, pPacket->device, pPacket->fence);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetFenceStatus(dump_inst, pPacket->result, pPacket->device, pPacket->fence);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkWaitForFences: { 
            packet_vkWaitForFences* pPacket = (packet_vkWaitForFences*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkWaitForFences(dump_inst, pPacket->result, pPacket->device, pPacket->fenceCount, pPacket->pFences, pPacket->waitAll, pPacket->timeout);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkWaitForFences(dump_inst, pPacket->result, pPacket->device, pPacket->fenceCount, pPacket->pFences, pPacket->waitAll, pPacket->timeout);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateSemaphore: { 
            packet_vkCreateSemaphore* pPacket = (packet_vkCreateSemaphore*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateSemaphore(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSemaphore);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateSemaphore(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSemaphore);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroySemaphore: { 
            packet_vkDestroySemaphore* pPacket = (packet_vkDestroySemaphore*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroySemaphore(dump_inst, pPacket->device, pPacket->semaphore, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroySemaphore(dump_inst, pPacket->device, pPacket->semaphore, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateEvent: { 
            packet_vkCreateEvent* pPacket = (packet_vkCreateEvent*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateEvent(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pEvent);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateEvent(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pEvent);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyEvent: { 
            packet_vkDestroyEvent* pPacket = (packet_vkDestroyEvent*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyEvent(dump_inst, pPacket->device, pPacket->event, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyEvent(dump_inst, pPacket->device, pPacket->event, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetEventStatus: { 
            packet_vkGetEventStatus* pPacket = (packet_vkGetEventStatus*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetEventStatus(dump_inst, pPacket->result, pPacket->device, pPacket->event);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetEventStatus(dump_inst, pPacket->result, pPacket->device, pPacket->event);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkSetEvent: { 
            packet_vkSetEvent* pPacket = (packet_vkSetEvent*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkSetEvent(dump_inst, pPacket->result, pPacket->device, pPacket->event);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkSetEvent(dump_inst, pPacket->result, pPacket->device, pPacket->event);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkResetEvent: { 
            packet_vkResetEvent* pPacket = (packet_vkResetEvent*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkResetEvent(dump_inst, pPacket->result, pPacket->device, pPacket->event);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkResetEvent(dump_inst, pPacket->result, pPacket->device, pPacket->event);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateQueryPool: { 
            packet_vkCreateQueryPool* pPacket = (packet_vkCreateQueryPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateQueryPool(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pQueryPool);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateQueryPool(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pQueryPool);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyQueryPool: { 
            packet_vkDestroyQueryPool* pPacket = (packet_vkDestroyQueryPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyQueryPool(dump_inst, pPacket->device, pPacket->queryPool, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyQueryPool(dump_inst, pPacket->device, pPacket->queryPool, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetQueryPoolResults: { 
            packet_vkGetQueryPoolResults* pPacket = (packet_vkGetQueryPoolResults*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetQueryPoolResults(dump_inst, pPacket->result, pPacket->device, pPacket->queryPool, pPacket->firstQuery, pPacket->queryCount, pPacket->dataSize, pPacket->pData, pPacket->stride, pPacket->flags);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetQueryPoolResults(dump_inst, pPacket->result, pPacket->device, pPacket->queryPool, pPacket->firstQuery, pPacket->queryCount, pPacket->dataSize, pPacket->pData, pPacket->stride, pPacket->flags);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateBuffer: { 
            packet_vkCreateBuffer* pPacket = (packet_vkCreateBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateBuffer(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pBuffer);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateBuffer(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pBuffer);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyBuffer: { 
            packet_vkDestroyBuffer* pPacket = (packet_vkDestroyBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyBuffer(dump_inst, pPacket->device, pPacket->buffer, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyBuffer(dump_inst, pPacket->device, pPacket->buffer, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateBufferView: { 
            packet_vkCreateBufferView* pPacket = (packet_vkCreateBufferView*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateBufferView(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pView);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateBufferView(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pView);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyBufferView: { 
            packet_vkDestroyBufferView* pPacket = (packet_vkDestroyBufferView*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyBufferView(dump_inst, pPacket->device, pPacket->bufferView, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyBufferView(dump_inst, pPacket->device, pPacket->bufferView, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateImage: { 
            packet_vkCreateImage* pPacket = (packet_vkCreateImage*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateImage(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pImage);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateImage(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pImage);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyImage: { 
            packet_vkDestroyImage* pPacket = (packet_vkDestroyImage*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyImage(dump_inst, pPacket->device, pPacket->image, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyImage(dump_inst, pPacket->device, pPacket->image, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetImageSubresourceLayout: { 
            packet_vkGetImageSubresourceLayout* pPacket = (packet_vkGetImageSubresourceLayout*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetImageSubresourceLayout(dump_inst, pPacket->device, pPacket->image, pPacket->pSubresource, pPacket->pLayout);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetImageSubresourceLayout(dump_inst, pPacket->device, pPacket->image, pPacket->pSubresource, pPacket->pLayout);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateImageView: { 
            packet_vkCreateImageView* pPacket = (packet_vkCreateImageView*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateImageView(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pView);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateImageView(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pView);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyImageView: { 
            packet_vkDestroyImageView* pPacket = (packet_vkDestroyImageView*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyImageView(dump_inst, pPacket->device, pPacket->imageView, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyImageView(dump_inst, pPacket->device, pPacket->imageView, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateShaderModule: { 
            packet_vkCreateShaderModule* pPacket = (packet_vkCreateShaderModule*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateShaderModule(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pShaderModule);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateShaderModule(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pShaderModule);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyShaderModule: { 
            packet_vkDestroyShaderModule* pPacket = (packet_vkDestroyShaderModule*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyShaderModule(dump_inst, pPacket->device, pPacket->shaderModule, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyShaderModule(dump_inst, pPacket->device, pPacket->shaderModule, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreatePipelineCache: { 
            packet_vkCreatePipelineCache* pPacket = (packet_vkCreatePipelineCache*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreatePipelineCache(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pPipelineCache);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreatePipelineCache(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pPipelineCache);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyPipelineCache: { 
            packet_vkDestroyPipelineCache* pPacket = (packet_vkDestroyPipelineCache*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyPipelineCache(dump_inst, pPacket->device, pPacket->pipelineCache, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyPipelineCache(dump_inst, pPacket->device, pPacket->pipelineCache, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPipelineCacheData: { 
            packet_vkGetPipelineCacheData* pPacket = (packet_vkGetPipelineCacheData*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPipelineCacheData(dump_inst, pPacket->result, pPacket->device, pPacket->pipelineCache, pPacket->pDataSize, pPacket->pData);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPipelineCacheData(dump_inst, pPacket->result, pPacket->device, pPacket->pipelineCache, pPacket->pDataSize, pPacket->pData);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkMergePipelineCaches: { 
            packet_vkMergePipelineCaches* pPacket = (packet_vkMergePipelineCaches*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkMergePipelineCaches(dump_inst, pPacket->result, pPacket->device, pPacket->dstCache, pPacket->srcCacheCount, pPacket->pSrcCaches);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkMergePipelineCaches(dump_inst, pPacket->result, pPacket->device, pPacket->dstCache, pPacket->srcCacheCount, pPacket->pSrcCaches);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateGraphicsPipelines: { 
            packet_vkCreateGraphicsPipelines* pPacket = (packet_vkCreateGraphicsPipelines*)(packet->pBody);
            VkGraphicsPipelineCreateInfo *pLocalCIs = VKTRACE_NEW_ARRAY(VkGraphicsPipelineCreateInfo, pPacket->createInfoCount);
            for (uint32_t i = 0; i < pPacket->createInfoCount; i++) {
                memcpy((void *)&(pLocalCIs[i]), (void *)&(pPacket->pCreateInfos[i]), sizeof(VkGraphicsPipelineCreateInfo));
                vkreplay_process_pnext_structs(pPacket->header, (void *)&pLocalCIs[i]);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pStages);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pVertexInputState);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pInputAssemblyState);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pTessellationState);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pViewportState);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pRasterizationState);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pMultisampleState);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pDepthStencilState);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pColorBlendState);
                vkreplay_process_pnext_structs(pPacket->header, (void *)pLocalCIs[i].pDynamicState);
                ((VkPipelineViewportStateCreateInfo *)pLocalCIs[i].pViewportState)->pViewports =
                    (VkViewport *)vktrace_trace_packet_interpret_buffer_pointer(
                        pPacket->header, (intptr_t)pPacket->pCreateInfos[i].pViewportState->pViewports);
                ((VkPipelineViewportStateCreateInfo *)pLocalCIs[i].pViewportState)->pScissors =
                    (VkRect2D *)vktrace_trace_packet_interpret_buffer_pointer(pPacket->header,
                        (intptr_t)pPacket->pCreateInfos[i].pViewportState->pScissors);
                ((VkPipelineMultisampleStateCreateInfo *)pLocalCIs[i].pMultisampleState)->pSampleMask =
                    (VkSampleMask *)vktrace_trace_packet_interpret_buffer_pointer(
                        pPacket->header, (intptr_t)pPacket->pCreateInfos[i].pMultisampleState->pSampleMask);
            }
            pPacket->pCreateInfos = pLocalCIs;
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateGraphicsPipelines(dump_inst, pPacket->result, pPacket->device, pPacket->pipelineCache, pPacket->createInfoCount, pPacket->pCreateInfos, pPacket->pAllocator, pPacket->pPipelines);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateGraphicsPipelines(dump_inst, pPacket->result, pPacket->device, pPacket->pipelineCache, pPacket->createInfoCount, pPacket->pCreateInfos, pPacket->pAllocator, pPacket->pPipelines);
                break;
            }
            VKTRACE_DELETE(pLocalCIs);
            break;
        }
        case VKTRACE_TPI_VK_vkCreateComputePipelines: { 
            packet_vkCreateComputePipelines* pPacket = (packet_vkCreateComputePipelines*)(packet->pBody);
            VkComputePipelineCreateInfo *pLocalCIs = VKTRACE_NEW_ARRAY(VkComputePipelineCreateInfo, pPacket->createInfoCount);
            memcpy((void *)pLocalCIs, (void *)(pPacket->pCreateInfos), sizeof(VkComputePipelineCreateInfo) * pPacket->createInfoCount);
            for (uint32_t i = 0; i < pPacket->createInfoCount; i++) {
                vkreplay_process_pnext_structs(pPacket->header, (void *)&pLocalCIs[i]);
                if (pLocalCIs[i].stage.pName) {
                    pLocalCIs[i].stage.pName =
                        (const char *)(vktrace_trace_packet_interpret_buffer_pointer(pPacket->header, (intptr_t)pLocalCIs[i].stage.pName));
                }
                if (pLocalCIs[i].stage.pSpecializationInfo) {
                    pLocalCIs[i].stage.pSpecializationInfo = 
                        (const VkSpecializationInfo  *)(vktrace_trace_packet_interpret_buffer_pointer(
                            pPacket->header, (intptr_t)pLocalCIs[i].stage.pSpecializationInfo));
                    if (pLocalCIs[i].stage.pSpecializationInfo->mapEntryCount > 0 && pLocalCIs[i].stage.pSpecializationInfo->pMapEntries) {
                        ((VkSpecializationInfo *)(pLocalCIs[i]).stage.pSpecializationInfo)->pMapEntries =
                            (const VkSpecializationMapEntry *)(vktrace_trace_packet_interpret_buffer_pointer(
                                pPacket->header, (intptr_t)pLocalCIs[i].stage.pSpecializationInfo->pMapEntries));
                    }
                    if (pLocalCIs[i].stage.pSpecializationInfo->dataSize > 0 && pLocalCIs[i].stage.pSpecializationInfo->pData) {
                        ((VkSpecializationInfo *)(pLocalCIs[i]).stage.pSpecializationInfo)->pData =
                            (const void *)(vktrace_trace_packet_interpret_buffer_pointer(
                                pPacket->header, (intptr_t)pLocalCIs[i].stage.pSpecializationInfo->pData));
                    }
                }
            }
            pPacket->pCreateInfos = pLocalCIs;
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateComputePipelines(dump_inst, pPacket->result, pPacket->device, pPacket->pipelineCache, pPacket->createInfoCount, pPacket->pCreateInfos, pPacket->pAllocator, pPacket->pPipelines);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateComputePipelines(dump_inst, pPacket->result, pPacket->device, pPacket->pipelineCache, pPacket->createInfoCount, pPacket->pCreateInfos, pPacket->pAllocator, pPacket->pPipelines);
                break;
            }
            VKTRACE_DELETE(pLocalCIs);
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyPipeline: { 
            packet_vkDestroyPipeline* pPacket = (packet_vkDestroyPipeline*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyPipeline(dump_inst, pPacket->device, pPacket->pipeline, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyPipeline(dump_inst, pPacket->device, pPacket->pipeline, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreatePipelineLayout: { 
            packet_vkCreatePipelineLayout* pPacket = (packet_vkCreatePipelineLayout*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreatePipelineLayout(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pPipelineLayout);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreatePipelineLayout(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pPipelineLayout);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyPipelineLayout: { 
            packet_vkDestroyPipelineLayout* pPacket = (packet_vkDestroyPipelineLayout*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyPipelineLayout(dump_inst, pPacket->device, pPacket->pipelineLayout, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyPipelineLayout(dump_inst, pPacket->device, pPacket->pipelineLayout, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateSampler: { 
            packet_vkCreateSampler* pPacket = (packet_vkCreateSampler*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateSampler(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSampler);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateSampler(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSampler);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroySampler: { 
            packet_vkDestroySampler* pPacket = (packet_vkDestroySampler*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroySampler(dump_inst, pPacket->device, pPacket->sampler, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroySampler(dump_inst, pPacket->device, pPacket->sampler, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateDescriptorSetLayout: { 
            packet_vkCreateDescriptorSetLayout* pPacket = (packet_vkCreateDescriptorSetLayout*)(packet->pBody);
            VkDescriptorSetLayoutCreateInfo *pInfo = (VkDescriptorSetLayoutCreateInfo *)pPacket->pCreateInfo;
            if (pInfo != NULL) {
                if (pInfo->pBindings != NULL) {
                    pInfo->pBindings = (VkDescriptorSetLayoutBinding *)vktrace_trace_packet_interpret_buffer_pointer(
                        pPacket->header, (intptr_t)pInfo->pBindings);
                    for (unsigned int i = 0; i < pInfo->bindingCount; i++) {
                        VkDescriptorSetLayoutBinding *pBindings = (VkDescriptorSetLayoutBinding *)&pInfo->pBindings[i];
                        if (pBindings->pImmutableSamplers != NULL) {
                            pBindings->pImmutableSamplers = (const VkSampler *)vktrace_trace_packet_interpret_buffer_pointer(
                                pPacket->header, (intptr_t)pBindings->pImmutableSamplers);
                        }
                    }
                }
            }
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateDescriptorSetLayout(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSetLayout);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateDescriptorSetLayout(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSetLayout);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyDescriptorSetLayout: { 
            packet_vkDestroyDescriptorSetLayout* pPacket = (packet_vkDestroyDescriptorSetLayout*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyDescriptorSetLayout(dump_inst, pPacket->device, pPacket->descriptorSetLayout, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyDescriptorSetLayout(dump_inst, pPacket->device, pPacket->descriptorSetLayout, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateDescriptorPool: { 
            packet_vkCreateDescriptorPool* pPacket = (packet_vkCreateDescriptorPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateDescriptorPool(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pDescriptorPool);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateDescriptorPool(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pDescriptorPool);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyDescriptorPool: { 
            packet_vkDestroyDescriptorPool* pPacket = (packet_vkDestroyDescriptorPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyDescriptorPool(dump_inst, pPacket->device, pPacket->descriptorPool, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyDescriptorPool(dump_inst, pPacket->device, pPacket->descriptorPool, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkResetDescriptorPool: { 
            packet_vkResetDescriptorPool* pPacket = (packet_vkResetDescriptorPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkResetDescriptorPool(dump_inst, pPacket->result, pPacket->device, pPacket->descriptorPool, pPacket->flags);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkResetDescriptorPool(dump_inst, pPacket->result, pPacket->device, pPacket->descriptorPool, pPacket->flags);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkAllocateDescriptorSets: { 
            packet_vkAllocateDescriptorSets* pPacket = (packet_vkAllocateDescriptorSets*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkAllocateDescriptorSets(dump_inst, pPacket->result, pPacket->device, pPacket->pAllocateInfo, pPacket->pDescriptorSets);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkAllocateDescriptorSets(dump_inst, pPacket->result, pPacket->device, pPacket->pAllocateInfo, pPacket->pDescriptorSets);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkFreeDescriptorSets: { 
            packet_vkFreeDescriptorSets* pPacket = (packet_vkFreeDescriptorSets*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkFreeDescriptorSets(dump_inst, pPacket->result, pPacket->device, pPacket->descriptorPool, pPacket->descriptorSetCount, pPacket->pDescriptorSets);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkFreeDescriptorSets(dump_inst, pPacket->result, pPacket->device, pPacket->descriptorPool, pPacket->descriptorSetCount, pPacket->pDescriptorSets);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkUpdateDescriptorSets: { 
            packet_vkUpdateDescriptorSets* pPacket = (packet_vkUpdateDescriptorSets*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkUpdateDescriptorSets(dump_inst, pPacket->device, pPacket->descriptorWriteCount, pPacket->pDescriptorWrites, pPacket->descriptorCopyCount, pPacket->pDescriptorCopies);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkUpdateDescriptorSets(dump_inst, pPacket->device, pPacket->descriptorWriteCount, pPacket->pDescriptorWrites, pPacket->descriptorCopyCount, pPacket->pDescriptorCopies);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateFramebuffer: { 
            packet_vkCreateFramebuffer* pPacket = (packet_vkCreateFramebuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateFramebuffer(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pFramebuffer);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateFramebuffer(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pFramebuffer);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyFramebuffer: { 
            packet_vkDestroyFramebuffer* pPacket = (packet_vkDestroyFramebuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyFramebuffer(dump_inst, pPacket->device, pPacket->framebuffer, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyFramebuffer(dump_inst, pPacket->device, pPacket->framebuffer, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateRenderPass: { 
            packet_vkCreateRenderPass* pPacket = (packet_vkCreateRenderPass*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateRenderPass(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pRenderPass);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateRenderPass(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pRenderPass);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyRenderPass: { 
            packet_vkDestroyRenderPass* pPacket = (packet_vkDestroyRenderPass*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyRenderPass(dump_inst, pPacket->device, pPacket->renderPass, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyRenderPass(dump_inst, pPacket->device, pPacket->renderPass, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetRenderAreaGranularity: { 
            packet_vkGetRenderAreaGranularity* pPacket = (packet_vkGetRenderAreaGranularity*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetRenderAreaGranularity(dump_inst, pPacket->device, pPacket->renderPass, pPacket->pGranularity);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetRenderAreaGranularity(dump_inst, pPacket->device, pPacket->renderPass, pPacket->pGranularity);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateCommandPool: { 
            packet_vkCreateCommandPool* pPacket = (packet_vkCreateCommandPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateCommandPool(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pCommandPool);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateCommandPool(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pCommandPool);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyCommandPool: { 
            packet_vkDestroyCommandPool* pPacket = (packet_vkDestroyCommandPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            dump_inst.eraseCmdBufferPool(pPacket->device, pPacket->commandPool);
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyCommandPool(dump_inst, pPacket->device, pPacket->commandPool, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyCommandPool(dump_inst, pPacket->device, pPacket->commandPool, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkResetCommandPool: { 
            packet_vkResetCommandPool* pPacket = (packet_vkResetCommandPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkResetCommandPool(dump_inst, pPacket->result, pPacket->device, pPacket->commandPool, pPacket->flags);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkResetCommandPool(dump_inst, pPacket->result, pPacket->device, pPacket->commandPool, pPacket->flags);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkAllocateCommandBuffers: { 
            packet_vkAllocateCommandBuffers* pPacket = (packet_vkAllocateCommandBuffers*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            dump_inst.addCmdBuffers(pPacket->device,
                                    pPacket->pAllocateInfo->commandPool,
                                    std::vector<VkCommandBuffer>(pPacket->pCommandBuffers, pPacket->pCommandBuffers + pPacket->pAllocateInfo->commandBufferCount),
                                    pPacket->pAllocateInfo->level);
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkAllocateCommandBuffers(dump_inst, pPacket->result, pPacket->device, pPacket->pAllocateInfo, pPacket->pCommandBuffers);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkAllocateCommandBuffers(dump_inst, pPacket->result, pPacket->device, pPacket->pAllocateInfo, pPacket->pCommandBuffers);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkFreeCommandBuffers: { 
            packet_vkFreeCommandBuffers* pPacket = (packet_vkFreeCommandBuffers*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            dump_inst.eraseCmdBuffers(pPacket->device,
                                      pPacket->commandPool,
                                      std::vector<VkCommandBuffer>(pPacket->pCommandBuffers, pPacket->pCommandBuffers + pPacket->commandBufferCount));
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkFreeCommandBuffers(dump_inst, pPacket->device, pPacket->commandPool, pPacket->commandBufferCount, pPacket->pCommandBuffers);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkFreeCommandBuffers(dump_inst, pPacket->device, pPacket->commandPool, pPacket->commandBufferCount, pPacket->pCommandBuffers);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkBeginCommandBuffer: { 
            packet_vkBeginCommandBuffer* pPacket = (packet_vkBeginCommandBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkBeginCommandBuffer(dump_inst, pPacket->result, pPacket->commandBuffer, pPacket->pBeginInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkBeginCommandBuffer(dump_inst, pPacket->result, pPacket->commandBuffer, pPacket->pBeginInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkEndCommandBuffer: { 
            packet_vkEndCommandBuffer* pPacket = (packet_vkEndCommandBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkEndCommandBuffer(dump_inst, pPacket->result, pPacket->commandBuffer);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkEndCommandBuffer(dump_inst, pPacket->result, pPacket->commandBuffer);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkResetCommandBuffer: { 
            packet_vkResetCommandBuffer* pPacket = (packet_vkResetCommandBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkResetCommandBuffer(dump_inst, pPacket->result, pPacket->commandBuffer, pPacket->flags);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkResetCommandBuffer(dump_inst, pPacket->result, pPacket->commandBuffer, pPacket->flags);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdBindPipeline: { 
            packet_vkCmdBindPipeline* pPacket = (packet_vkCmdBindPipeline*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdBindPipeline(dump_inst, pPacket->commandBuffer, pPacket->pipelineBindPoint, pPacket->pipeline);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdBindPipeline(dump_inst, pPacket->commandBuffer, pPacket->pipelineBindPoint, pPacket->pipeline);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetViewport: { 
            packet_vkCmdSetViewport* pPacket = (packet_vkCmdSetViewport*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetViewport(dump_inst, pPacket->commandBuffer, pPacket->firstViewport, pPacket->viewportCount, pPacket->pViewports);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetViewport(dump_inst, pPacket->commandBuffer, pPacket->firstViewport, pPacket->viewportCount, pPacket->pViewports);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetScissor: { 
            packet_vkCmdSetScissor* pPacket = (packet_vkCmdSetScissor*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetScissor(dump_inst, pPacket->commandBuffer, pPacket->firstScissor, pPacket->scissorCount, pPacket->pScissors);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetScissor(dump_inst, pPacket->commandBuffer, pPacket->firstScissor, pPacket->scissorCount, pPacket->pScissors);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetLineWidth: { 
            packet_vkCmdSetLineWidth* pPacket = (packet_vkCmdSetLineWidth*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetLineWidth(dump_inst, pPacket->commandBuffer, pPacket->lineWidth);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetLineWidth(dump_inst, pPacket->commandBuffer, pPacket->lineWidth);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetDepthBias: { 
            packet_vkCmdSetDepthBias* pPacket = (packet_vkCmdSetDepthBias*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetDepthBias(dump_inst, pPacket->commandBuffer, pPacket->depthBiasConstantFactor, pPacket->depthBiasClamp, pPacket->depthBiasSlopeFactor);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetDepthBias(dump_inst, pPacket->commandBuffer, pPacket->depthBiasConstantFactor, pPacket->depthBiasClamp, pPacket->depthBiasSlopeFactor);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetBlendConstants: { 
            packet_vkCmdSetBlendConstants* pPacket = (packet_vkCmdSetBlendConstants*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetBlendConstants(dump_inst, pPacket->commandBuffer, pPacket->blendConstants);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetBlendConstants(dump_inst, pPacket->commandBuffer, pPacket->blendConstants);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetDepthBounds: { 
            packet_vkCmdSetDepthBounds* pPacket = (packet_vkCmdSetDepthBounds*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetDepthBounds(dump_inst, pPacket->commandBuffer, pPacket->minDepthBounds, pPacket->maxDepthBounds);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetDepthBounds(dump_inst, pPacket->commandBuffer, pPacket->minDepthBounds, pPacket->maxDepthBounds);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetStencilCompareMask: { 
            packet_vkCmdSetStencilCompareMask* pPacket = (packet_vkCmdSetStencilCompareMask*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetStencilCompareMask(dump_inst, pPacket->commandBuffer, pPacket->faceMask, pPacket->compareMask);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetStencilCompareMask(dump_inst, pPacket->commandBuffer, pPacket->faceMask, pPacket->compareMask);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetStencilWriteMask: { 
            packet_vkCmdSetStencilWriteMask* pPacket = (packet_vkCmdSetStencilWriteMask*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetStencilWriteMask(dump_inst, pPacket->commandBuffer, pPacket->faceMask, pPacket->writeMask);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetStencilWriteMask(dump_inst, pPacket->commandBuffer, pPacket->faceMask, pPacket->writeMask);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetStencilReference: { 
            packet_vkCmdSetStencilReference* pPacket = (packet_vkCmdSetStencilReference*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetStencilReference(dump_inst, pPacket->commandBuffer, pPacket->faceMask, pPacket->reference);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetStencilReference(dump_inst, pPacket->commandBuffer, pPacket->faceMask, pPacket->reference);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdBindDescriptorSets: { 
            packet_vkCmdBindDescriptorSets* pPacket = (packet_vkCmdBindDescriptorSets*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdBindDescriptorSets(dump_inst, pPacket->commandBuffer, pPacket->pipelineBindPoint, pPacket->layout, pPacket->firstSet, pPacket->descriptorSetCount, pPacket->pDescriptorSets, pPacket->dynamicOffsetCount, pPacket->pDynamicOffsets);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdBindDescriptorSets(dump_inst, pPacket->commandBuffer, pPacket->pipelineBindPoint, pPacket->layout, pPacket->firstSet, pPacket->descriptorSetCount, pPacket->pDescriptorSets, pPacket->dynamicOffsetCount, pPacket->pDynamicOffsets);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdBindIndexBuffer: { 
            packet_vkCmdBindIndexBuffer* pPacket = (packet_vkCmdBindIndexBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdBindIndexBuffer(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->indexType);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdBindIndexBuffer(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->indexType);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdBindVertexBuffers: { 
            packet_vkCmdBindVertexBuffers* pPacket = (packet_vkCmdBindVertexBuffers*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdBindVertexBuffers(dump_inst, pPacket->commandBuffer, pPacket->firstBinding, pPacket->bindingCount, pPacket->pBuffers, pPacket->pOffsets);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdBindVertexBuffers(dump_inst, pPacket->commandBuffer, pPacket->firstBinding, pPacket->bindingCount, pPacket->pBuffers, pPacket->pOffsets);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDraw: { 
            packet_vkCmdDraw* pPacket = (packet_vkCmdDraw*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDraw(dump_inst, pPacket->commandBuffer, pPacket->vertexCount, pPacket->instanceCount, pPacket->firstVertex, pPacket->firstInstance);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDraw(dump_inst, pPacket->commandBuffer, pPacket->vertexCount, pPacket->instanceCount, pPacket->firstVertex, pPacket->firstInstance);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndexed: { 
            packet_vkCmdDrawIndexed* pPacket = (packet_vkCmdDrawIndexed*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDrawIndexed(dump_inst, pPacket->commandBuffer, pPacket->indexCount, pPacket->instanceCount, pPacket->firstIndex, pPacket->vertexOffset, pPacket->firstInstance);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDrawIndexed(dump_inst, pPacket->commandBuffer, pPacket->indexCount, pPacket->instanceCount, pPacket->firstIndex, pPacket->vertexOffset, pPacket->firstInstance);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndirect: { 
            packet_vkCmdDrawIndirect* pPacket = (packet_vkCmdDrawIndirect*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDrawIndirect(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->drawCount, pPacket->stride);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDrawIndirect(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->drawCount, pPacket->stride);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndexedIndirect: { 
            packet_vkCmdDrawIndexedIndirect* pPacket = (packet_vkCmdDrawIndexedIndirect*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDrawIndexedIndirect(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->drawCount, pPacket->stride);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDrawIndexedIndirect(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->drawCount, pPacket->stride);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDispatch: { 
            packet_vkCmdDispatch* pPacket = (packet_vkCmdDispatch*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDispatch(dump_inst, pPacket->commandBuffer, pPacket->groupCountX, pPacket->groupCountY, pPacket->groupCountZ);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDispatch(dump_inst, pPacket->commandBuffer, pPacket->groupCountX, pPacket->groupCountY, pPacket->groupCountZ);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDispatchIndirect: { 
            packet_vkCmdDispatchIndirect* pPacket = (packet_vkCmdDispatchIndirect*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDispatchIndirect(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDispatchIndirect(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdCopyBuffer: { 
            packet_vkCmdCopyBuffer* pPacket = (packet_vkCmdCopyBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdCopyBuffer(dump_inst, pPacket->commandBuffer, pPacket->srcBuffer, pPacket->dstBuffer, pPacket->regionCount, pPacket->pRegions);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdCopyBuffer(dump_inst, pPacket->commandBuffer, pPacket->srcBuffer, pPacket->dstBuffer, pPacket->regionCount, pPacket->pRegions);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdCopyImage: { 
            packet_vkCmdCopyImage* pPacket = (packet_vkCmdCopyImage*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdCopyImage(dump_inst, pPacket->commandBuffer, pPacket->srcImage, pPacket->srcImageLayout, pPacket->dstImage, pPacket->dstImageLayout, pPacket->regionCount, pPacket->pRegions);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdCopyImage(dump_inst, pPacket->commandBuffer, pPacket->srcImage, pPacket->srcImageLayout, pPacket->dstImage, pPacket->dstImageLayout, pPacket->regionCount, pPacket->pRegions);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdBlitImage: { 
            packet_vkCmdBlitImage* pPacket = (packet_vkCmdBlitImage*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdBlitImage(dump_inst, pPacket->commandBuffer, pPacket->srcImage, pPacket->srcImageLayout, pPacket->dstImage, pPacket->dstImageLayout, pPacket->regionCount, pPacket->pRegions, pPacket->filter);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdBlitImage(dump_inst, pPacket->commandBuffer, pPacket->srcImage, pPacket->srcImageLayout, pPacket->dstImage, pPacket->dstImageLayout, pPacket->regionCount, pPacket->pRegions, pPacket->filter);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdCopyBufferToImage: { 
            packet_vkCmdCopyBufferToImage* pPacket = (packet_vkCmdCopyBufferToImage*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdCopyBufferToImage(dump_inst, pPacket->commandBuffer, pPacket->srcBuffer, pPacket->dstImage, pPacket->dstImageLayout, pPacket->regionCount, pPacket->pRegions);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdCopyBufferToImage(dump_inst, pPacket->commandBuffer, pPacket->srcBuffer, pPacket->dstImage, pPacket->dstImageLayout, pPacket->regionCount, pPacket->pRegions);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdCopyImageToBuffer: { 
            packet_vkCmdCopyImageToBuffer* pPacket = (packet_vkCmdCopyImageToBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdCopyImageToBuffer(dump_inst, pPacket->commandBuffer, pPacket->srcImage, pPacket->srcImageLayout, pPacket->dstBuffer, pPacket->regionCount, pPacket->pRegions);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdCopyImageToBuffer(dump_inst, pPacket->commandBuffer, pPacket->srcImage, pPacket->srcImageLayout, pPacket->dstBuffer, pPacket->regionCount, pPacket->pRegions);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdUpdateBuffer: { 
            packet_vkCmdUpdateBuffer* pPacket = (packet_vkCmdUpdateBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdUpdateBuffer(dump_inst, pPacket->commandBuffer, pPacket->dstBuffer, pPacket->dstOffset, pPacket->dataSize, pPacket->pData);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdUpdateBuffer(dump_inst, pPacket->commandBuffer, pPacket->dstBuffer, pPacket->dstOffset, pPacket->dataSize, pPacket->pData);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdFillBuffer: { 
            packet_vkCmdFillBuffer* pPacket = (packet_vkCmdFillBuffer*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdFillBuffer(dump_inst, pPacket->commandBuffer, pPacket->dstBuffer, pPacket->dstOffset, pPacket->size, pPacket->data);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdFillBuffer(dump_inst, pPacket->commandBuffer, pPacket->dstBuffer, pPacket->dstOffset, pPacket->size, pPacket->data);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdClearColorImage: { 
            packet_vkCmdClearColorImage* pPacket = (packet_vkCmdClearColorImage*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdClearColorImage(dump_inst, pPacket->commandBuffer, pPacket->image, pPacket->imageLayout, pPacket->pColor, pPacket->rangeCount, pPacket->pRanges);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdClearColorImage(dump_inst, pPacket->commandBuffer, pPacket->image, pPacket->imageLayout, pPacket->pColor, pPacket->rangeCount, pPacket->pRanges);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdClearDepthStencilImage: { 
            packet_vkCmdClearDepthStencilImage* pPacket = (packet_vkCmdClearDepthStencilImage*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdClearDepthStencilImage(dump_inst, pPacket->commandBuffer, pPacket->image, pPacket->imageLayout, pPacket->pDepthStencil, pPacket->rangeCount, pPacket->pRanges);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdClearDepthStencilImage(dump_inst, pPacket->commandBuffer, pPacket->image, pPacket->imageLayout, pPacket->pDepthStencil, pPacket->rangeCount, pPacket->pRanges);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdClearAttachments: { 
            packet_vkCmdClearAttachments* pPacket = (packet_vkCmdClearAttachments*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdClearAttachments(dump_inst, pPacket->commandBuffer, pPacket->attachmentCount, pPacket->pAttachments, pPacket->rectCount, pPacket->pRects);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdClearAttachments(dump_inst, pPacket->commandBuffer, pPacket->attachmentCount, pPacket->pAttachments, pPacket->rectCount, pPacket->pRects);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdResolveImage: { 
            packet_vkCmdResolveImage* pPacket = (packet_vkCmdResolveImage*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdResolveImage(dump_inst, pPacket->commandBuffer, pPacket->srcImage, pPacket->srcImageLayout, pPacket->dstImage, pPacket->dstImageLayout, pPacket->regionCount, pPacket->pRegions);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdResolveImage(dump_inst, pPacket->commandBuffer, pPacket->srcImage, pPacket->srcImageLayout, pPacket->dstImage, pPacket->dstImageLayout, pPacket->regionCount, pPacket->pRegions);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetEvent: { 
            packet_vkCmdSetEvent* pPacket = (packet_vkCmdSetEvent*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetEvent(dump_inst, pPacket->commandBuffer, pPacket->event, pPacket->stageMask);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetEvent(dump_inst, pPacket->commandBuffer, pPacket->event, pPacket->stageMask);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdResetEvent: { 
            packet_vkCmdResetEvent* pPacket = (packet_vkCmdResetEvent*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdResetEvent(dump_inst, pPacket->commandBuffer, pPacket->event, pPacket->stageMask);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdResetEvent(dump_inst, pPacket->commandBuffer, pPacket->event, pPacket->stageMask);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdWaitEvents: { 
            packet_vkCmdWaitEvents* pPacket = (packet_vkCmdWaitEvents*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdWaitEvents(dump_inst, pPacket->commandBuffer, pPacket->eventCount, pPacket->pEvents, pPacket->srcStageMask, pPacket->dstStageMask, pPacket->memoryBarrierCount, pPacket->pMemoryBarriers, pPacket->bufferMemoryBarrierCount, pPacket->pBufferMemoryBarriers, pPacket->imageMemoryBarrierCount, pPacket->pImageMemoryBarriers);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdWaitEvents(dump_inst, pPacket->commandBuffer, pPacket->eventCount, pPacket->pEvents, pPacket->srcStageMask, pPacket->dstStageMask, pPacket->memoryBarrierCount, pPacket->pMemoryBarriers, pPacket->bufferMemoryBarrierCount, pPacket->pBufferMemoryBarriers, pPacket->imageMemoryBarrierCount, pPacket->pImageMemoryBarriers);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdPipelineBarrier: { 
            packet_vkCmdPipelineBarrier* pPacket = (packet_vkCmdPipelineBarrier*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdPipelineBarrier(dump_inst, pPacket->commandBuffer, pPacket->srcStageMask, pPacket->dstStageMask, pPacket->dependencyFlags, pPacket->memoryBarrierCount, pPacket->pMemoryBarriers, pPacket->bufferMemoryBarrierCount, pPacket->pBufferMemoryBarriers, pPacket->imageMemoryBarrierCount, pPacket->pImageMemoryBarriers);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdPipelineBarrier(dump_inst, pPacket->commandBuffer, pPacket->srcStageMask, pPacket->dstStageMask, pPacket->dependencyFlags, pPacket->memoryBarrierCount, pPacket->pMemoryBarriers, pPacket->bufferMemoryBarrierCount, pPacket->pBufferMemoryBarriers, pPacket->imageMemoryBarrierCount, pPacket->pImageMemoryBarriers);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdBeginQuery: { 
            packet_vkCmdBeginQuery* pPacket = (packet_vkCmdBeginQuery*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdBeginQuery(dump_inst, pPacket->commandBuffer, pPacket->queryPool, pPacket->query, pPacket->flags);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdBeginQuery(dump_inst, pPacket->commandBuffer, pPacket->queryPool, pPacket->query, pPacket->flags);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdEndQuery: { 
            packet_vkCmdEndQuery* pPacket = (packet_vkCmdEndQuery*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdEndQuery(dump_inst, pPacket->commandBuffer, pPacket->queryPool, pPacket->query);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdEndQuery(dump_inst, pPacket->commandBuffer, pPacket->queryPool, pPacket->query);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdResetQueryPool: { 
            packet_vkCmdResetQueryPool* pPacket = (packet_vkCmdResetQueryPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdResetQueryPool(dump_inst, pPacket->commandBuffer, pPacket->queryPool, pPacket->firstQuery, pPacket->queryCount);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdResetQueryPool(dump_inst, pPacket->commandBuffer, pPacket->queryPool, pPacket->firstQuery, pPacket->queryCount);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdWriteTimestamp: { 
            packet_vkCmdWriteTimestamp* pPacket = (packet_vkCmdWriteTimestamp*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdWriteTimestamp(dump_inst, pPacket->commandBuffer, pPacket->pipelineStage, pPacket->queryPool, pPacket->query);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdWriteTimestamp(dump_inst, pPacket->commandBuffer, pPacket->pipelineStage, pPacket->queryPool, pPacket->query);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdCopyQueryPoolResults: { 
            packet_vkCmdCopyQueryPoolResults* pPacket = (packet_vkCmdCopyQueryPoolResults*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdCopyQueryPoolResults(dump_inst, pPacket->commandBuffer, pPacket->queryPool, pPacket->firstQuery, pPacket->queryCount, pPacket->dstBuffer, pPacket->dstOffset, pPacket->stride, pPacket->flags);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdCopyQueryPoolResults(dump_inst, pPacket->commandBuffer, pPacket->queryPool, pPacket->firstQuery, pPacket->queryCount, pPacket->dstBuffer, pPacket->dstOffset, pPacket->stride, pPacket->flags);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdPushConstants: { 
            packet_vkCmdPushConstants* pPacket = (packet_vkCmdPushConstants*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdPushConstants(dump_inst, pPacket->commandBuffer, pPacket->layout, pPacket->stageFlags, pPacket->offset, pPacket->size, pPacket->pValues);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdPushConstants(dump_inst, pPacket->commandBuffer, pPacket->layout, pPacket->stageFlags, pPacket->offset, pPacket->size, pPacket->pValues);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdBeginRenderPass: { 
            packet_vkCmdBeginRenderPass* pPacket = (packet_vkCmdBeginRenderPass*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdBeginRenderPass(dump_inst, pPacket->commandBuffer, pPacket->pRenderPassBegin, pPacket->contents);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdBeginRenderPass(dump_inst, pPacket->commandBuffer, pPacket->pRenderPassBegin, pPacket->contents);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdNextSubpass: { 
            packet_vkCmdNextSubpass* pPacket = (packet_vkCmdNextSubpass*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdNextSubpass(dump_inst, pPacket->commandBuffer, pPacket->contents);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdNextSubpass(dump_inst, pPacket->commandBuffer, pPacket->contents);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdEndRenderPass: { 
            packet_vkCmdEndRenderPass* pPacket = (packet_vkCmdEndRenderPass*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdEndRenderPass(dump_inst, pPacket->commandBuffer);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdEndRenderPass(dump_inst, pPacket->commandBuffer);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdExecuteCommands: { 
            packet_vkCmdExecuteCommands* pPacket = (packet_vkCmdExecuteCommands*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdExecuteCommands(dump_inst, pPacket->commandBuffer, pPacket->commandBufferCount, pPacket->pCommandBuffers);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdExecuteCommands(dump_inst, pPacket->commandBuffer, pPacket->commandBufferCount, pPacket->pCommandBuffers);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkBindBufferMemory2: { 
            packet_vkBindBufferMemory2* pPacket = (packet_vkBindBufferMemory2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkBindBufferMemory2(dump_inst, pPacket->result, pPacket->device, pPacket->bindInfoCount, pPacket->pBindInfos);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkBindBufferMemory2(dump_inst, pPacket->result, pPacket->device, pPacket->bindInfoCount, pPacket->pBindInfos);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkBindImageMemory2: { 
            packet_vkBindImageMemory2* pPacket = (packet_vkBindImageMemory2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkBindImageMemory2(dump_inst, pPacket->result, pPacket->device, pPacket->bindInfoCount, pPacket->pBindInfos);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkBindImageMemory2(dump_inst, pPacket->result, pPacket->device, pPacket->bindInfoCount, pPacket->pBindInfos);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDeviceGroupPeerMemoryFeatures: { 
            packet_vkGetDeviceGroupPeerMemoryFeatures* pPacket = (packet_vkGetDeviceGroupPeerMemoryFeatures*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDeviceGroupPeerMemoryFeatures(dump_inst, pPacket->device, pPacket->heapIndex, pPacket->localDeviceIndex, pPacket->remoteDeviceIndex, pPacket->pPeerMemoryFeatures);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDeviceGroupPeerMemoryFeatures(dump_inst, pPacket->device, pPacket->heapIndex, pPacket->localDeviceIndex, pPacket->remoteDeviceIndex, pPacket->pPeerMemoryFeatures);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetDeviceMask: { 
            packet_vkCmdSetDeviceMask* pPacket = (packet_vkCmdSetDeviceMask*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetDeviceMask(dump_inst, pPacket->commandBuffer, pPacket->deviceMask);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetDeviceMask(dump_inst, pPacket->commandBuffer, pPacket->deviceMask);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDispatchBase: { 
            packet_vkCmdDispatchBase* pPacket = (packet_vkCmdDispatchBase*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDispatchBase(dump_inst, pPacket->commandBuffer, pPacket->baseGroupX, pPacket->baseGroupY, pPacket->baseGroupZ, pPacket->groupCountX, pPacket->groupCountY, pPacket->groupCountZ);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDispatchBase(dump_inst, pPacket->commandBuffer, pPacket->baseGroupX, pPacket->baseGroupY, pPacket->baseGroupZ, pPacket->groupCountX, pPacket->groupCountY, pPacket->groupCountZ);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkEnumeratePhysicalDeviceGroups: { 
            packet_vkEnumeratePhysicalDeviceGroups* pPacket = (packet_vkEnumeratePhysicalDeviceGroups*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkEnumeratePhysicalDeviceGroups(dump_inst, pPacket->result, pPacket->instance, pPacket->pPhysicalDeviceGroupCount, pPacket->pPhysicalDeviceGroupProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkEnumeratePhysicalDeviceGroups(dump_inst, pPacket->result, pPacket->instance, pPacket->pPhysicalDeviceGroupCount, pPacket->pPhysicalDeviceGroupProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetImageMemoryRequirements2: { 
            packet_vkGetImageMemoryRequirements2* pPacket = (packet_vkGetImageMemoryRequirements2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetImageMemoryRequirements2(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pMemoryRequirements);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetImageMemoryRequirements2(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pMemoryRequirements);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements2: { 
            packet_vkGetBufferMemoryRequirements2* pPacket = (packet_vkGetBufferMemoryRequirements2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetBufferMemoryRequirements2(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pMemoryRequirements);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetBufferMemoryRequirements2(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pMemoryRequirements);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements2: { 
            packet_vkGetImageSparseMemoryRequirements2* pPacket = (packet_vkGetImageSparseMemoryRequirements2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetImageSparseMemoryRequirements2(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pSparseMemoryRequirementCount, pPacket->pSparseMemoryRequirements);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetImageSparseMemoryRequirements2(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pSparseMemoryRequirementCount, pPacket->pSparseMemoryRequirements);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures2: { 
            packet_vkGetPhysicalDeviceFeatures2* pPacket = (packet_vkGetPhysicalDeviceFeatures2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceFeatures2(dump_inst, pPacket->physicalDevice, pPacket->pFeatures);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceFeatures2(dump_inst, pPacket->physicalDevice, pPacket->pFeatures);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties2: { 
            packet_vkGetPhysicalDeviceProperties2* pPacket = (packet_vkGetPhysicalDeviceProperties2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceProperties2(dump_inst, pPacket->physicalDevice, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceProperties2(dump_inst, pPacket->physicalDevice, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties2: { 
            packet_vkGetPhysicalDeviceFormatProperties2* pPacket = (packet_vkGetPhysicalDeviceFormatProperties2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceFormatProperties2(dump_inst, pPacket->physicalDevice, pPacket->format, pPacket->pFormatProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceFormatProperties2(dump_inst, pPacket->physicalDevice, pPacket->format, pPacket->pFormatProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties2: { 
            packet_vkGetPhysicalDeviceImageFormatProperties2* pPacket = (packet_vkGetPhysicalDeviceImageFormatProperties2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceImageFormatProperties2(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pImageFormatInfo, pPacket->pImageFormatProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceImageFormatProperties2(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pImageFormatInfo, pPacket->pImageFormatProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties2: { 
            packet_vkGetPhysicalDeviceQueueFamilyProperties2* pPacket = (packet_vkGetPhysicalDeviceQueueFamilyProperties2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceQueueFamilyProperties2(dump_inst, pPacket->physicalDevice, pPacket->pQueueFamilyPropertyCount, pPacket->pQueueFamilyProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceQueueFamilyProperties2(dump_inst, pPacket->physicalDevice, pPacket->pQueueFamilyPropertyCount, pPacket->pQueueFamilyProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties2: { 
            packet_vkGetPhysicalDeviceMemoryProperties2* pPacket = (packet_vkGetPhysicalDeviceMemoryProperties2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceMemoryProperties2(dump_inst, pPacket->physicalDevice, pPacket->pMemoryProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceMemoryProperties2(dump_inst, pPacket->physicalDevice, pPacket->pMemoryProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties2: { 
            packet_vkGetPhysicalDeviceSparseImageFormatProperties2* pPacket = (packet_vkGetPhysicalDeviceSparseImageFormatProperties2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSparseImageFormatProperties2(dump_inst, pPacket->physicalDevice, pPacket->pFormatInfo, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSparseImageFormatProperties2(dump_inst, pPacket->physicalDevice, pPacket->pFormatInfo, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkTrimCommandPool: { 
            packet_vkTrimCommandPool* pPacket = (packet_vkTrimCommandPool*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkTrimCommandPool(dump_inst, pPacket->device, pPacket->commandPool, pPacket->flags);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkTrimCommandPool(dump_inst, pPacket->device, pPacket->commandPool, pPacket->flags);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDeviceQueue2: { 
            packet_vkGetDeviceQueue2* pPacket = (packet_vkGetDeviceQueue2*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDeviceQueue2(dump_inst, pPacket->device, pPacket->pQueueInfo, pPacket->pQueue);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDeviceQueue2(dump_inst, pPacket->device, pPacket->pQueueInfo, pPacket->pQueue);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateSamplerYcbcrConversion: { 
            packet_vkCreateSamplerYcbcrConversion* pPacket = (packet_vkCreateSamplerYcbcrConversion*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateSamplerYcbcrConversion(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pYcbcrConversion);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateSamplerYcbcrConversion(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pYcbcrConversion);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroySamplerYcbcrConversion: { 
            packet_vkDestroySamplerYcbcrConversion* pPacket = (packet_vkDestroySamplerYcbcrConversion*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroySamplerYcbcrConversion(dump_inst, pPacket->device, pPacket->ycbcrConversion, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroySamplerYcbcrConversion(dump_inst, pPacket->device, pPacket->ycbcrConversion, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateDescriptorUpdateTemplate: { 
            packet_vkCreateDescriptorUpdateTemplate* pPacket = (packet_vkCreateDescriptorUpdateTemplate*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateDescriptorUpdateTemplate(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pDescriptorUpdateTemplate);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateDescriptorUpdateTemplate(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pDescriptorUpdateTemplate);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyDescriptorUpdateTemplate: { 
            packet_vkDestroyDescriptorUpdateTemplate* pPacket = (packet_vkDestroyDescriptorUpdateTemplate*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyDescriptorUpdateTemplate(dump_inst, pPacket->device, pPacket->descriptorUpdateTemplate, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyDescriptorUpdateTemplate(dump_inst, pPacket->device, pPacket->descriptorUpdateTemplate, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkUpdateDescriptorSetWithTemplate: { 
            packet_vkUpdateDescriptorSetWithTemplate* pPacket = (packet_vkUpdateDescriptorSetWithTemplate*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkUpdateDescriptorSetWithTemplate(dump_inst, pPacket->device, pPacket->descriptorSet, pPacket->descriptorUpdateTemplate, pPacket->pData);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkUpdateDescriptorSetWithTemplate(dump_inst, pPacket->device, pPacket->descriptorSet, pPacket->descriptorUpdateTemplate, pPacket->pData);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalBufferProperties: { 
            packet_vkGetPhysicalDeviceExternalBufferProperties* pPacket = (packet_vkGetPhysicalDeviceExternalBufferProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceExternalBufferProperties(dump_inst, pPacket->physicalDevice, pPacket->pExternalBufferInfo, pPacket->pExternalBufferProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceExternalBufferProperties(dump_inst, pPacket->physicalDevice, pPacket->pExternalBufferInfo, pPacket->pExternalBufferProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalFenceProperties: { 
            packet_vkGetPhysicalDeviceExternalFenceProperties* pPacket = (packet_vkGetPhysicalDeviceExternalFenceProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceExternalFenceProperties(dump_inst, pPacket->physicalDevice, pPacket->pExternalFenceInfo, pPacket->pExternalFenceProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceExternalFenceProperties(dump_inst, pPacket->physicalDevice, pPacket->pExternalFenceInfo, pPacket->pExternalFenceProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalSemaphoreProperties: { 
            packet_vkGetPhysicalDeviceExternalSemaphoreProperties* pPacket = (packet_vkGetPhysicalDeviceExternalSemaphoreProperties*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceExternalSemaphoreProperties(dump_inst, pPacket->physicalDevice, pPacket->pExternalSemaphoreInfo, pPacket->pExternalSemaphoreProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceExternalSemaphoreProperties(dump_inst, pPacket->physicalDevice, pPacket->pExternalSemaphoreInfo, pPacket->pExternalSemaphoreProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDescriptorSetLayoutSupport: { 
            packet_vkGetDescriptorSetLayoutSupport* pPacket = (packet_vkGetDescriptorSetLayoutSupport*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDescriptorSetLayoutSupport(dump_inst, pPacket->device, pPacket->pCreateInfo, pPacket->pSupport);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDescriptorSetLayoutSupport(dump_inst, pPacket->device, pPacket->pCreateInfo, pPacket->pSupport);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroySurfaceKHR: { 
            packet_vkDestroySurfaceKHR* pPacket = (packet_vkDestroySurfaceKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroySurfaceKHR(dump_inst, pPacket->instance, pPacket->surface, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroySurfaceKHR(dump_inst, pPacket->instance, pPacket->surface, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceSupportKHR: { 
            packet_vkGetPhysicalDeviceSurfaceSupportKHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceSupportKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSurfaceSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex, pPacket->surface, pPacket->pSupported);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSurfaceSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex, pPacket->surface, pPacket->pSupported);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilitiesKHR: { 
            packet_vkGetPhysicalDeviceSurfaceCapabilitiesKHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceCapabilitiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSurfaceCapabilitiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pSurfaceCapabilities);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSurfaceCapabilitiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pSurfaceCapabilities);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceFormatsKHR: { 
            packet_vkGetPhysicalDeviceSurfaceFormatsKHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceFormatsKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSurfaceFormatsKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pSurfaceFormatCount, pPacket->pSurfaceFormats);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSurfaceFormatsKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pSurfaceFormatCount, pPacket->pSurfaceFormats);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfacePresentModesKHR: { 
            packet_vkGetPhysicalDeviceSurfacePresentModesKHR* pPacket = (packet_vkGetPhysicalDeviceSurfacePresentModesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSurfacePresentModesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pPresentModeCount, pPacket->pPresentModes);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSurfacePresentModesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pPresentModeCount, pPacket->pPresentModes);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateSwapchainKHR: { 
            packet_vkCreateSwapchainKHR* pPacket = (packet_vkCreateSwapchainKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateSwapchainKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSwapchain);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateSwapchainKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSwapchain);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroySwapchainKHR: { 
            packet_vkDestroySwapchainKHR* pPacket = (packet_vkDestroySwapchainKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroySwapchainKHR(dump_inst, pPacket->device, pPacket->swapchain, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroySwapchainKHR(dump_inst, pPacket->device, pPacket->swapchain, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetSwapchainImagesKHR: { 
            packet_vkGetSwapchainImagesKHR* pPacket = (packet_vkGetSwapchainImagesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetSwapchainImagesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->pSwapchainImageCount, pPacket->pSwapchainImages);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetSwapchainImagesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->pSwapchainImageCount, pPacket->pSwapchainImages);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkAcquireNextImageKHR: { 
            packet_vkAcquireNextImageKHR* pPacket = (packet_vkAcquireNextImageKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkAcquireNextImageKHR(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->timeout, pPacket->semaphore, pPacket->fence, pPacket->pImageIndex);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkAcquireNextImageKHR(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->timeout, pPacket->semaphore, pPacket->fence, pPacket->pImageIndex);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkQueuePresentKHR: { 
            packet_vkQueuePresentKHR* pPacket = (packet_vkQueuePresentKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkQueuePresentKHR(dump_inst, pPacket->result, pPacket->queue, pPacket->pPresentInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkQueuePresentKHR(dump_inst, pPacket->result, pPacket->queue, pPacket->pPresentInfo);
                break;
            }
            dump_inst.nextFrame();
            break;
        }
        case VKTRACE_TPI_VK_vkGetDeviceGroupPresentCapabilitiesKHR: { 
            packet_vkGetDeviceGroupPresentCapabilitiesKHR* pPacket = (packet_vkGetDeviceGroupPresentCapabilitiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDeviceGroupPresentCapabilitiesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pDeviceGroupPresentCapabilities);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDeviceGroupPresentCapabilitiesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pDeviceGroupPresentCapabilities);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDeviceGroupSurfacePresentModesKHR: { 
            packet_vkGetDeviceGroupSurfacePresentModesKHR* pPacket = (packet_vkGetDeviceGroupSurfacePresentModesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDeviceGroupSurfacePresentModesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->surface, pPacket->pModes);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDeviceGroupSurfacePresentModesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->surface, pPacket->pModes);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDevicePresentRectanglesKHR: { 
            packet_vkGetPhysicalDevicePresentRectanglesKHR* pPacket = (packet_vkGetPhysicalDevicePresentRectanglesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDevicePresentRectanglesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pRectCount, pPacket->pRects);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDevicePresentRectanglesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pRectCount, pPacket->pRects);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkAcquireNextImage2KHR: { 
            packet_vkAcquireNextImage2KHR* pPacket = (packet_vkAcquireNextImage2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkAcquireNextImage2KHR(dump_inst, pPacket->result, pPacket->device, pPacket->pAcquireInfo, pPacket->pImageIndex);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkAcquireNextImage2KHR(dump_inst, pPacket->result, pPacket->device, pPacket->pAcquireInfo, pPacket->pImageIndex);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPropertiesKHR: { 
            packet_vkGetPhysicalDeviceDisplayPropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceDisplayPropertiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceDisplayPropertiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceDisplayPropertiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPlanePropertiesKHR: { 
            packet_vkGetPhysicalDeviceDisplayPlanePropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceDisplayPlanePropertiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceDisplayPlanePropertiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceDisplayPlanePropertiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDisplayPlaneSupportedDisplaysKHR: { 
            packet_vkGetDisplayPlaneSupportedDisplaysKHR* pPacket = (packet_vkGetDisplayPlaneSupportedDisplaysKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDisplayPlaneSupportedDisplaysKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->planeIndex, pPacket->pDisplayCount, pPacket->pDisplays);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDisplayPlaneSupportedDisplaysKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->planeIndex, pPacket->pDisplayCount, pPacket->pDisplays);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDisplayModePropertiesKHR: { 
            packet_vkGetDisplayModePropertiesKHR* pPacket = (packet_vkGetDisplayModePropertiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDisplayModePropertiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->display, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDisplayModePropertiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->display, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateDisplayModeKHR: { 
            packet_vkCreateDisplayModeKHR* pPacket = (packet_vkCreateDisplayModeKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateDisplayModeKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->display, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pMode);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateDisplayModeKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->display, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pMode);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDisplayPlaneCapabilitiesKHR: { 
            packet_vkGetDisplayPlaneCapabilitiesKHR* pPacket = (packet_vkGetDisplayPlaneCapabilitiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDisplayPlaneCapabilitiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->mode, pPacket->planeIndex, pPacket->pCapabilities);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDisplayPlaneCapabilitiesKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->mode, pPacket->planeIndex, pPacket->pCapabilities);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateDisplayPlaneSurfaceKHR: { 
            packet_vkCreateDisplayPlaneSurfaceKHR* pPacket = (packet_vkCreateDisplayPlaneSurfaceKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateDisplayPlaneSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateDisplayPlaneSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateSharedSwapchainsKHR: { 
            packet_vkCreateSharedSwapchainsKHR* pPacket = (packet_vkCreateSharedSwapchainsKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateSharedSwapchainsKHR(dump_inst, pPacket->result, pPacket->device, pPacket->swapchainCount, pPacket->pCreateInfos, pPacket->pAllocator, pPacket->pSwapchains);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateSharedSwapchainsKHR(dump_inst, pPacket->result, pPacket->device, pPacket->swapchainCount, pPacket->pCreateInfos, pPacket->pAllocator, pPacket->pSwapchains);
                break;
            }
            break;
        }
#ifdef VK_USE_PLATFORM_XLIB_KHR
        case VKTRACE_TPI_VK_vkCreateXlibSurfaceKHR: { 
            packet_vkCreateXlibSurfaceKHR* pPacket = (packet_vkCreateXlibSurfaceKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateXlibSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateXlibSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_XLIB_KHR
#ifdef VK_USE_PLATFORM_XLIB_KHR
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceXlibPresentationSupportKHR: { 
            packet_vkGetPhysicalDeviceXlibPresentationSupportKHR* pPacket = (packet_vkGetPhysicalDeviceXlibPresentationSupportKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceXlibPresentationSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex, pPacket->dpy, pPacket->visualID);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceXlibPresentationSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex, pPacket->dpy, pPacket->visualID);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_XLIB_KHR
#ifdef VK_USE_PLATFORM_XCB_KHR
        case VKTRACE_TPI_VK_vkCreateXcbSurfaceKHR: { 
            packet_vkCreateXcbSurfaceKHR* pPacket = (packet_vkCreateXcbSurfaceKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateXcbSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateXcbSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_XCB_KHR
#ifdef VK_USE_PLATFORM_XCB_KHR
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceXcbPresentationSupportKHR: { 
            packet_vkGetPhysicalDeviceXcbPresentationSupportKHR* pPacket = (packet_vkGetPhysicalDeviceXcbPresentationSupportKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceXcbPresentationSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex, pPacket->connection, pPacket->visual_id);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceXcbPresentationSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex, pPacket->connection, pPacket->visual_id);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_XCB_KHR
#ifdef VK_USE_PLATFORM_WAYLAND_KHR
        case VKTRACE_TPI_VK_vkCreateWaylandSurfaceKHR: { 
            packet_vkCreateWaylandSurfaceKHR* pPacket = (packet_vkCreateWaylandSurfaceKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateWaylandSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateWaylandSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WAYLAND_KHR
#ifdef VK_USE_PLATFORM_WAYLAND_KHR
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceWaylandPresentationSupportKHR: { 
            packet_vkGetPhysicalDeviceWaylandPresentationSupportKHR* pPacket = (packet_vkGetPhysicalDeviceWaylandPresentationSupportKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceWaylandPresentationSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex, pPacket->display);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceWaylandPresentationSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex, pPacket->display);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WAYLAND_KHR
#ifdef VK_USE_PLATFORM_ANDROID_KHR
        case VKTRACE_TPI_VK_vkCreateAndroidSurfaceKHR: { 
            packet_vkCreateAndroidSurfaceKHR* pPacket = (packet_vkCreateAndroidSurfaceKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateAndroidSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateAndroidSurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_ANDROID_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkCreateWin32SurfaceKHR: { 
            packet_vkCreateWin32SurfaceKHR* pPacket = (packet_vkCreateWin32SurfaceKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateWin32SurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateWin32SurfaceKHR(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pSurface);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceWin32PresentationSupportKHR: { 
            packet_vkGetPhysicalDeviceWin32PresentationSupportKHR* pPacket = (packet_vkGetPhysicalDeviceWin32PresentationSupportKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceWin32PresentationSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceWin32PresentationSupportKHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->queueFamilyIndex);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFeatures2KHR: { 
            packet_vkGetPhysicalDeviceFeatures2KHR* pPacket = (packet_vkGetPhysicalDeviceFeatures2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceFeatures2KHR(dump_inst, pPacket->physicalDevice, pPacket->pFeatures);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceFeatures2KHR(dump_inst, pPacket->physicalDevice, pPacket->pFeatures);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceProperties2KHR: { 
            packet_vkGetPhysicalDeviceProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceProperties2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceFormatProperties2KHR: { 
            packet_vkGetPhysicalDeviceFormatProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceFormatProperties2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceFormatProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->format, pPacket->pFormatProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceFormatProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->format, pPacket->pFormatProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceImageFormatProperties2KHR: { 
            packet_vkGetPhysicalDeviceImageFormatProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceImageFormatProperties2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceImageFormatProperties2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pImageFormatInfo, pPacket->pImageFormatProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceImageFormatProperties2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pImageFormatInfo, pPacket->pImageFormatProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceQueueFamilyProperties2KHR: { 
            packet_vkGetPhysicalDeviceQueueFamilyProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceQueueFamilyProperties2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceQueueFamilyProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->pQueueFamilyPropertyCount, pPacket->pQueueFamilyProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceQueueFamilyProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->pQueueFamilyPropertyCount, pPacket->pQueueFamilyProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMemoryProperties2KHR: { 
            packet_vkGetPhysicalDeviceMemoryProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceMemoryProperties2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceMemoryProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->pMemoryProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceMemoryProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->pMemoryProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSparseImageFormatProperties2KHR: { 
            packet_vkGetPhysicalDeviceSparseImageFormatProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceSparseImageFormatProperties2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSparseImageFormatProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->pFormatInfo, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSparseImageFormatProperties2KHR(dump_inst, pPacket->physicalDevice, pPacket->pFormatInfo, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkTrimCommandPoolKHR: { 
            packet_vkTrimCommandPoolKHR* pPacket = (packet_vkTrimCommandPoolKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkTrimCommandPoolKHR(dump_inst, pPacket->device, pPacket->commandPool, pPacket->flags);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkTrimCommandPoolKHR(dump_inst, pPacket->device, pPacket->commandPool, pPacket->flags);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalBufferPropertiesKHR: { 
            packet_vkGetPhysicalDeviceExternalBufferPropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceExternalBufferPropertiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceExternalBufferPropertiesKHR(dump_inst, pPacket->physicalDevice, pPacket->pExternalBufferInfo, pPacket->pExternalBufferProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceExternalBufferPropertiesKHR(dump_inst, pPacket->physicalDevice, pPacket->pExternalBufferInfo, pPacket->pExternalBufferProperties);
                break;
            }
            break;
        }
#ifdef VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkGetMemoryWin32HandleKHR: { 
            packet_vkGetMemoryWin32HandleKHR* pPacket = (packet_vkGetMemoryWin32HandleKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetMemoryWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetWin32HandleInfo, pPacket->pHandle);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetMemoryWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetWin32HandleInfo, pPacket->pHandle);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkGetMemoryWin32HandlePropertiesKHR: { 
            packet_vkGetMemoryWin32HandlePropertiesKHR* pPacket = (packet_vkGetMemoryWin32HandlePropertiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetMemoryWin32HandlePropertiesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->handleType, pPacket->handle, pPacket->pMemoryWin32HandleProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetMemoryWin32HandlePropertiesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->handleType, pPacket->handle, pPacket->pMemoryWin32HandleProperties);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkGetMemoryFdKHR: { 
            packet_vkGetMemoryFdKHR* pPacket = (packet_vkGetMemoryFdKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetMemoryFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetFdInfo, pPacket->pFd);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetMemoryFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetFdInfo, pPacket->pFd);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetMemoryFdPropertiesKHR: { 
            packet_vkGetMemoryFdPropertiesKHR* pPacket = (packet_vkGetMemoryFdPropertiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetMemoryFdPropertiesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->handleType, pPacket->fd, pPacket->pMemoryFdProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetMemoryFdPropertiesKHR(dump_inst, pPacket->result, pPacket->device, pPacket->handleType, pPacket->fd, pPacket->pMemoryFdProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR: { 
            packet_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR(dump_inst, pPacket->physicalDevice, pPacket->pExternalSemaphoreInfo, pPacket->pExternalSemaphoreProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceExternalSemaphorePropertiesKHR(dump_inst, pPacket->physicalDevice, pPacket->pExternalSemaphoreInfo, pPacket->pExternalSemaphoreProperties);
                break;
            }
            break;
        }
#ifdef VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkImportSemaphoreWin32HandleKHR: { 
            packet_vkImportSemaphoreWin32HandleKHR* pPacket = (packet_vkImportSemaphoreWin32HandleKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkImportSemaphoreWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pImportSemaphoreWin32HandleInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkImportSemaphoreWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pImportSemaphoreWin32HandleInfo);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkGetSemaphoreWin32HandleKHR: { 
            packet_vkGetSemaphoreWin32HandleKHR* pPacket = (packet_vkGetSemaphoreWin32HandleKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetSemaphoreWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetWin32HandleInfo, pPacket->pHandle);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetSemaphoreWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetWin32HandleInfo, pPacket->pHandle);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkImportSemaphoreFdKHR: { 
            packet_vkImportSemaphoreFdKHR* pPacket = (packet_vkImportSemaphoreFdKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkImportSemaphoreFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pImportSemaphoreFdInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkImportSemaphoreFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pImportSemaphoreFdInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetSemaphoreFdKHR: { 
            packet_vkGetSemaphoreFdKHR* pPacket = (packet_vkGetSemaphoreFdKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetSemaphoreFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetFdInfo, pPacket->pFd);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetSemaphoreFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetFdInfo, pPacket->pFd);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdPushDescriptorSetKHR: { 
            packet_vkCmdPushDescriptorSetKHR* pPacket = (packet_vkCmdPushDescriptorSetKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdPushDescriptorSetKHR(dump_inst, pPacket->commandBuffer, pPacket->pipelineBindPoint, pPacket->layout, pPacket->set, pPacket->descriptorWriteCount, pPacket->pDescriptorWrites);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdPushDescriptorSetKHR(dump_inst, pPacket->commandBuffer, pPacket->pipelineBindPoint, pPacket->layout, pPacket->set, pPacket->descriptorWriteCount, pPacket->pDescriptorWrites);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdPushDescriptorSetWithTemplateKHR: { 
            packet_vkCmdPushDescriptorSetWithTemplateKHR* pPacket = (packet_vkCmdPushDescriptorSetWithTemplateKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdPushDescriptorSetWithTemplateKHR(dump_inst, pPacket->commandBuffer, pPacket->descriptorUpdateTemplate, pPacket->layout, pPacket->set, pPacket->pData);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdPushDescriptorSetWithTemplateKHR(dump_inst, pPacket->commandBuffer, pPacket->descriptorUpdateTemplate, pPacket->layout, pPacket->set, pPacket->pData);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateDescriptorUpdateTemplateKHR: { 
            packet_vkCreateDescriptorUpdateTemplateKHR* pPacket = (packet_vkCreateDescriptorUpdateTemplateKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateDescriptorUpdateTemplateKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pDescriptorUpdateTemplate);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateDescriptorUpdateTemplateKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pDescriptorUpdateTemplate);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyDescriptorUpdateTemplateKHR: { 
            packet_vkDestroyDescriptorUpdateTemplateKHR* pPacket = (packet_vkDestroyDescriptorUpdateTemplateKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyDescriptorUpdateTemplateKHR(dump_inst, pPacket->device, pPacket->descriptorUpdateTemplate, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyDescriptorUpdateTemplateKHR(dump_inst, pPacket->device, pPacket->descriptorUpdateTemplate, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkUpdateDescriptorSetWithTemplateKHR: { 
            packet_vkUpdateDescriptorSetWithTemplateKHR* pPacket = (packet_vkUpdateDescriptorSetWithTemplateKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkUpdateDescriptorSetWithTemplateKHR(dump_inst, pPacket->device, pPacket->descriptorSet, pPacket->descriptorUpdateTemplate, pPacket->pData);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkUpdateDescriptorSetWithTemplateKHR(dump_inst, pPacket->device, pPacket->descriptorSet, pPacket->descriptorUpdateTemplate, pPacket->pData);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetSwapchainStatusKHR: { 
            packet_vkGetSwapchainStatusKHR* pPacket = (packet_vkGetSwapchainStatusKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetSwapchainStatusKHR(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetSwapchainStatusKHR(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalFencePropertiesKHR: { 
            packet_vkGetPhysicalDeviceExternalFencePropertiesKHR* pPacket = (packet_vkGetPhysicalDeviceExternalFencePropertiesKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceExternalFencePropertiesKHR(dump_inst, pPacket->physicalDevice, pPacket->pExternalFenceInfo, pPacket->pExternalFenceProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceExternalFencePropertiesKHR(dump_inst, pPacket->physicalDevice, pPacket->pExternalFenceInfo, pPacket->pExternalFenceProperties);
                break;
            }
            break;
        }
#ifdef VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkImportFenceWin32HandleKHR: { 
            packet_vkImportFenceWin32HandleKHR* pPacket = (packet_vkImportFenceWin32HandleKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkImportFenceWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pImportFenceWin32HandleInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkImportFenceWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pImportFenceWin32HandleInfo);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WIN32_KHR
#ifdef VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkGetFenceWin32HandleKHR: { 
            packet_vkGetFenceWin32HandleKHR* pPacket = (packet_vkGetFenceWin32HandleKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetFenceWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetWin32HandleInfo, pPacket->pHandle);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetFenceWin32HandleKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetWin32HandleInfo, pPacket->pHandle);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkImportFenceFdKHR: { 
            packet_vkImportFenceFdKHR* pPacket = (packet_vkImportFenceFdKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkImportFenceFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pImportFenceFdInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkImportFenceFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pImportFenceFdInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetFenceFdKHR: { 
            packet_vkGetFenceFdKHR* pPacket = (packet_vkGetFenceFdKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetFenceFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetFdInfo, pPacket->pFd);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetFenceFdKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pGetFdInfo, pPacket->pFd);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilities2KHR: { 
            packet_vkGetPhysicalDeviceSurfaceCapabilities2KHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceCapabilities2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSurfaceCapabilities2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pSurfaceInfo, pPacket->pSurfaceCapabilities);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSurfaceCapabilities2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pSurfaceInfo, pPacket->pSurfaceCapabilities);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceFormats2KHR: { 
            packet_vkGetPhysicalDeviceSurfaceFormats2KHR* pPacket = (packet_vkGetPhysicalDeviceSurfaceFormats2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSurfaceFormats2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pSurfaceInfo, pPacket->pSurfaceFormatCount, pPacket->pSurfaceFormats);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSurfaceFormats2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pSurfaceInfo, pPacket->pSurfaceFormatCount, pPacket->pSurfaceFormats);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayProperties2KHR: { 
            packet_vkGetPhysicalDeviceDisplayProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceDisplayProperties2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceDisplayProperties2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceDisplayProperties2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceDisplayPlaneProperties2KHR: { 
            packet_vkGetPhysicalDeviceDisplayPlaneProperties2KHR* pPacket = (packet_vkGetPhysicalDeviceDisplayPlaneProperties2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceDisplayPlaneProperties2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceDisplayPlaneProperties2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDisplayModeProperties2KHR: { 
            packet_vkGetDisplayModeProperties2KHR* pPacket = (packet_vkGetDisplayModeProperties2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDisplayModeProperties2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->display, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDisplayModeProperties2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->display, pPacket->pPropertyCount, pPacket->pProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetDisplayPlaneCapabilities2KHR: { 
            packet_vkGetDisplayPlaneCapabilities2KHR* pPacket = (packet_vkGetDisplayPlaneCapabilities2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetDisplayPlaneCapabilities2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pDisplayPlaneInfo, pPacket->pCapabilities);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetDisplayPlaneCapabilities2KHR(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->pDisplayPlaneInfo, pPacket->pCapabilities);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetImageMemoryRequirements2KHR: { 
            packet_vkGetImageMemoryRequirements2KHR* pPacket = (packet_vkGetImageMemoryRequirements2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetImageMemoryRequirements2KHR(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pMemoryRequirements);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetImageMemoryRequirements2KHR(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pMemoryRequirements);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetBufferMemoryRequirements2KHR: { 
            packet_vkGetBufferMemoryRequirements2KHR* pPacket = (packet_vkGetBufferMemoryRequirements2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetBufferMemoryRequirements2KHR(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pMemoryRequirements);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetBufferMemoryRequirements2KHR(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pMemoryRequirements);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetImageSparseMemoryRequirements2KHR: { 
            packet_vkGetImageSparseMemoryRequirements2KHR* pPacket = (packet_vkGetImageSparseMemoryRequirements2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetImageSparseMemoryRequirements2KHR(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pSparseMemoryRequirementCount, pPacket->pSparseMemoryRequirements);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetImageSparseMemoryRequirements2KHR(dump_inst, pPacket->device, pPacket->pInfo, pPacket->pSparseMemoryRequirementCount, pPacket->pSparseMemoryRequirements);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateSamplerYcbcrConversionKHR: { 
            packet_vkCreateSamplerYcbcrConversionKHR* pPacket = (packet_vkCreateSamplerYcbcrConversionKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateSamplerYcbcrConversionKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pYcbcrConversion);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateSamplerYcbcrConversionKHR(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pYcbcrConversion);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroySamplerYcbcrConversionKHR: { 
            packet_vkDestroySamplerYcbcrConversionKHR* pPacket = (packet_vkDestroySamplerYcbcrConversionKHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroySamplerYcbcrConversionKHR(dump_inst, pPacket->device, pPacket->ycbcrConversion, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroySamplerYcbcrConversionKHR(dump_inst, pPacket->device, pPacket->ycbcrConversion, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkBindBufferMemory2KHR: { 
            packet_vkBindBufferMemory2KHR* pPacket = (packet_vkBindBufferMemory2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkBindBufferMemory2KHR(dump_inst, pPacket->result, pPacket->device, pPacket->bindInfoCount, pPacket->pBindInfos);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkBindBufferMemory2KHR(dump_inst, pPacket->result, pPacket->device, pPacket->bindInfoCount, pPacket->pBindInfos);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkBindImageMemory2KHR: { 
            packet_vkBindImageMemory2KHR* pPacket = (packet_vkBindImageMemory2KHR*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkBindImageMemory2KHR(dump_inst, pPacket->result, pPacket->device, pPacket->bindInfoCount, pPacket->pBindInfos);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkBindImageMemory2KHR(dump_inst, pPacket->result, pPacket->device, pPacket->bindInfoCount, pPacket->pBindInfos);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateDebugReportCallbackEXT: { 
            packet_vkCreateDebugReportCallbackEXT* pPacket = (packet_vkCreateDebugReportCallbackEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateDebugReportCallbackEXT(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pCallback);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateDebugReportCallbackEXT(dump_inst, pPacket->result, pPacket->instance, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pCallback);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyDebugReportCallbackEXT: { 
            packet_vkDestroyDebugReportCallbackEXT* pPacket = (packet_vkDestroyDebugReportCallbackEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyDebugReportCallbackEXT(dump_inst, pPacket->instance, pPacket->callback, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyDebugReportCallbackEXT(dump_inst, pPacket->instance, pPacket->callback, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDebugReportMessageEXT: { 
            packet_vkDebugReportMessageEXT* pPacket = (packet_vkDebugReportMessageEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDebugReportMessageEXT(dump_inst, pPacket->instance, pPacket->flags, pPacket->objectType, pPacket->object, pPacket->location, pPacket->messageCode, pPacket->pLayerPrefix, pPacket->pMessage);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDebugReportMessageEXT(dump_inst, pPacket->instance, pPacket->flags, pPacket->objectType, pPacket->object, pPacket->location, pPacket->messageCode, pPacket->pLayerPrefix, pPacket->pMessage);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDebugMarkerSetObjectTagEXT: { 
            packet_vkDebugMarkerSetObjectTagEXT* pPacket = (packet_vkDebugMarkerSetObjectTagEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDebugMarkerSetObjectTagEXT(dump_inst, pPacket->result, pPacket->device, pPacket->pTagInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDebugMarkerSetObjectTagEXT(dump_inst, pPacket->result, pPacket->device, pPacket->pTagInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDebugMarkerSetObjectNameEXT: { 
            packet_vkDebugMarkerSetObjectNameEXT* pPacket = (packet_vkDebugMarkerSetObjectNameEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDebugMarkerSetObjectNameEXT(dump_inst, pPacket->result, pPacket->device, pPacket->pNameInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDebugMarkerSetObjectNameEXT(dump_inst, pPacket->result, pPacket->device, pPacket->pNameInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDebugMarkerBeginEXT: { 
            packet_vkCmdDebugMarkerBeginEXT* pPacket = (packet_vkCmdDebugMarkerBeginEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDebugMarkerBeginEXT(dump_inst, pPacket->commandBuffer, pPacket->pMarkerInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDebugMarkerBeginEXT(dump_inst, pPacket->commandBuffer, pPacket->pMarkerInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDebugMarkerEndEXT: { 
            packet_vkCmdDebugMarkerEndEXT* pPacket = (packet_vkCmdDebugMarkerEndEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDebugMarkerEndEXT(dump_inst, pPacket->commandBuffer);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDebugMarkerEndEXT(dump_inst, pPacket->commandBuffer);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDebugMarkerInsertEXT: { 
            packet_vkCmdDebugMarkerInsertEXT* pPacket = (packet_vkCmdDebugMarkerInsertEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDebugMarkerInsertEXT(dump_inst, pPacket->commandBuffer, pPacket->pMarkerInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDebugMarkerInsertEXT(dump_inst, pPacket->commandBuffer, pPacket->pMarkerInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndirectCountAMD: { 
            packet_vkCmdDrawIndirectCountAMD* pPacket = (packet_vkCmdDrawIndirectCountAMD*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDrawIndirectCountAMD(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->countBuffer, pPacket->countBufferOffset, pPacket->maxDrawCount, pPacket->stride);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDrawIndirectCountAMD(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->countBuffer, pPacket->countBufferOffset, pPacket->maxDrawCount, pPacket->stride);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdDrawIndexedIndirectCountAMD: { 
            packet_vkCmdDrawIndexedIndirectCountAMD* pPacket = (packet_vkCmdDrawIndexedIndirectCountAMD*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdDrawIndexedIndirectCountAMD(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->countBuffer, pPacket->countBufferOffset, pPacket->maxDrawCount, pPacket->stride);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdDrawIndexedIndirectCountAMD(dump_inst, pPacket->commandBuffer, pPacket->buffer, pPacket->offset, pPacket->countBuffer, pPacket->countBufferOffset, pPacket->maxDrawCount, pPacket->stride);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetShaderInfoAMD: { 
            packet_vkGetShaderInfoAMD* pPacket = (packet_vkGetShaderInfoAMD*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetShaderInfoAMD(dump_inst, pPacket->result, pPacket->device, pPacket->pipeline, pPacket->shaderStage, pPacket->infoType, pPacket->pInfoSize, pPacket->pInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetShaderInfoAMD(dump_inst, pPacket->result, pPacket->device, pPacket->pipeline, pPacket->shaderStage, pPacket->infoType, pPacket->pInfoSize, pPacket->pInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceExternalImageFormatPropertiesNV: { 
            packet_vkGetPhysicalDeviceExternalImageFormatPropertiesNV* pPacket = (packet_vkGetPhysicalDeviceExternalImageFormatPropertiesNV*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceExternalImageFormatPropertiesNV(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->format, pPacket->type, pPacket->tiling, pPacket->usage, pPacket->flags, pPacket->externalHandleType, pPacket->pExternalImageFormatProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceExternalImageFormatPropertiesNV(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->format, pPacket->type, pPacket->tiling, pPacket->usage, pPacket->flags, pPacket->externalHandleType, pPacket->pExternalImageFormatProperties);
                break;
            }
            break;
        }
#ifdef VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkGetMemoryWin32HandleNV: { 
            packet_vkGetMemoryWin32HandleNV* pPacket = (packet_vkGetMemoryWin32HandleNV*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetMemoryWin32HandleNV(dump_inst, pPacket->result, pPacket->device, pPacket->memory, pPacket->handleType, pPacket->pHandle);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetMemoryWin32HandleNV(dump_inst, pPacket->result, pPacket->device, pPacket->memory, pPacket->handleType, pPacket->pHandle);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_WIN32_KHR
        case VKTRACE_TPI_VK_vkCmdProcessCommandsNVX: { 
            packet_vkCmdProcessCommandsNVX* pPacket = (packet_vkCmdProcessCommandsNVX*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdProcessCommandsNVX(dump_inst, pPacket->commandBuffer, pPacket->pProcessCommandsInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdProcessCommandsNVX(dump_inst, pPacket->commandBuffer, pPacket->pProcessCommandsInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdReserveSpaceForCommandsNVX: { 
            packet_vkCmdReserveSpaceForCommandsNVX* pPacket = (packet_vkCmdReserveSpaceForCommandsNVX*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdReserveSpaceForCommandsNVX(dump_inst, pPacket->commandBuffer, pPacket->pReserveSpaceInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdReserveSpaceForCommandsNVX(dump_inst, pPacket->commandBuffer, pPacket->pReserveSpaceInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateIndirectCommandsLayoutNVX: { 
            packet_vkCreateIndirectCommandsLayoutNVX* pPacket = (packet_vkCreateIndirectCommandsLayoutNVX*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateIndirectCommandsLayoutNVX(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pIndirectCommandsLayout);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateIndirectCommandsLayoutNVX(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pIndirectCommandsLayout);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyIndirectCommandsLayoutNVX: { 
            packet_vkDestroyIndirectCommandsLayoutNVX* pPacket = (packet_vkDestroyIndirectCommandsLayoutNVX*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyIndirectCommandsLayoutNVX(dump_inst, pPacket->device, pPacket->indirectCommandsLayout, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyIndirectCommandsLayoutNVX(dump_inst, pPacket->device, pPacket->indirectCommandsLayout, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateObjectTableNVX: { 
            packet_vkCreateObjectTableNVX* pPacket = (packet_vkCreateObjectTableNVX*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateObjectTableNVX(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pObjectTable);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateObjectTableNVX(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pObjectTable);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyObjectTableNVX: { 
            packet_vkDestroyObjectTableNVX* pPacket = (packet_vkDestroyObjectTableNVX*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyObjectTableNVX(dump_inst, pPacket->device, pPacket->objectTable, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyObjectTableNVX(dump_inst, pPacket->device, pPacket->objectTable, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkRegisterObjectsNVX: { 
            packet_vkRegisterObjectsNVX* pPacket = (packet_vkRegisterObjectsNVX*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkRegisterObjectsNVX(dump_inst, pPacket->result, pPacket->device, pPacket->objectTable, pPacket->objectCount, pPacket->ppObjectTableEntries, pPacket->pObjectIndices);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkRegisterObjectsNVX(dump_inst, pPacket->result, pPacket->device, pPacket->objectTable, pPacket->objectCount, pPacket->ppObjectTableEntries, pPacket->pObjectIndices);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkUnregisterObjectsNVX: { 
            packet_vkUnregisterObjectsNVX* pPacket = (packet_vkUnregisterObjectsNVX*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkUnregisterObjectsNVX(dump_inst, pPacket->result, pPacket->device, pPacket->objectTable, pPacket->objectCount, pPacket->pObjectEntryTypes, pPacket->pObjectIndices);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkUnregisterObjectsNVX(dump_inst, pPacket->result, pPacket->device, pPacket->objectTable, pPacket->objectCount, pPacket->pObjectEntryTypes, pPacket->pObjectIndices);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX: { 
            packet_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX* pPacket = (packet_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX(dump_inst, pPacket->physicalDevice, pPacket->pFeatures, pPacket->pLimits);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX(dump_inst, pPacket->physicalDevice, pPacket->pFeatures, pPacket->pLimits);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetViewportWScalingNV: { 
            packet_vkCmdSetViewportWScalingNV* pPacket = (packet_vkCmdSetViewportWScalingNV*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetViewportWScalingNV(dump_inst, pPacket->commandBuffer, pPacket->firstViewport, pPacket->viewportCount, pPacket->pViewportWScalings);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetViewportWScalingNV(dump_inst, pPacket->commandBuffer, pPacket->firstViewport, pPacket->viewportCount, pPacket->pViewportWScalings);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkReleaseDisplayEXT: { 
            packet_vkReleaseDisplayEXT* pPacket = (packet_vkReleaseDisplayEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkReleaseDisplayEXT(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->display);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkReleaseDisplayEXT(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->display);
                break;
            }
            break;
        }
#ifdef VK_USE_PLATFORM_XLIB_XRANDR_EXT
        case VKTRACE_TPI_VK_vkAcquireXlibDisplayEXT: { 
            packet_vkAcquireXlibDisplayEXT* pPacket = (packet_vkAcquireXlibDisplayEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkAcquireXlibDisplayEXT(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->dpy, pPacket->display);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkAcquireXlibDisplayEXT(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->dpy, pPacket->display);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_XLIB_XRANDR_EXT
#ifdef VK_USE_PLATFORM_XLIB_XRANDR_EXT
        case VKTRACE_TPI_VK_vkGetRandROutputDisplayEXT: { 
            packet_vkGetRandROutputDisplayEXT* pPacket = (packet_vkGetRandROutputDisplayEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetRandROutputDisplayEXT(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->dpy, pPacket->rrOutput, pPacket->pDisplay);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetRandROutputDisplayEXT(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->dpy, pPacket->rrOutput, pPacket->pDisplay);
                break;
            }
            break;
        }
#endif // VK_USE_PLATFORM_XLIB_XRANDR_EXT
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceSurfaceCapabilities2EXT: { 
            packet_vkGetPhysicalDeviceSurfaceCapabilities2EXT* pPacket = (packet_vkGetPhysicalDeviceSurfaceCapabilities2EXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceSurfaceCapabilities2EXT(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pSurfaceCapabilities);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceSurfaceCapabilities2EXT(dump_inst, pPacket->result, pPacket->physicalDevice, pPacket->surface, pPacket->pSurfaceCapabilities);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDisplayPowerControlEXT: { 
            packet_vkDisplayPowerControlEXT* pPacket = (packet_vkDisplayPowerControlEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDisplayPowerControlEXT(dump_inst, pPacket->result, pPacket->device, pPacket->display, pPacket->pDisplayPowerInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDisplayPowerControlEXT(dump_inst, pPacket->result, pPacket->device, pPacket->display, pPacket->pDisplayPowerInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkRegisterDeviceEventEXT: { 
            packet_vkRegisterDeviceEventEXT* pPacket = (packet_vkRegisterDeviceEventEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkRegisterDeviceEventEXT(dump_inst, pPacket->result, pPacket->device, pPacket->pDeviceEventInfo, pPacket->pAllocator, pPacket->pFence);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkRegisterDeviceEventEXT(dump_inst, pPacket->result, pPacket->device, pPacket->pDeviceEventInfo, pPacket->pAllocator, pPacket->pFence);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkRegisterDisplayEventEXT: { 
            packet_vkRegisterDisplayEventEXT* pPacket = (packet_vkRegisterDisplayEventEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkRegisterDisplayEventEXT(dump_inst, pPacket->result, pPacket->device, pPacket->display, pPacket->pDisplayEventInfo, pPacket->pAllocator, pPacket->pFence);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkRegisterDisplayEventEXT(dump_inst, pPacket->result, pPacket->device, pPacket->display, pPacket->pDisplayEventInfo, pPacket->pAllocator, pPacket->pFence);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetSwapchainCounterEXT: { 
            packet_vkGetSwapchainCounterEXT* pPacket = (packet_vkGetSwapchainCounterEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetSwapchainCounterEXT(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->counter, pPacket->pCounterValue);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetSwapchainCounterEXT(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->counter, pPacket->pCounterValue);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetRefreshCycleDurationGOOGLE: { 
            packet_vkGetRefreshCycleDurationGOOGLE* pPacket = (packet_vkGetRefreshCycleDurationGOOGLE*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetRefreshCycleDurationGOOGLE(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->pDisplayTimingProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetRefreshCycleDurationGOOGLE(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->pDisplayTimingProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPastPresentationTimingGOOGLE: { 
            packet_vkGetPastPresentationTimingGOOGLE* pPacket = (packet_vkGetPastPresentationTimingGOOGLE*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPastPresentationTimingGOOGLE(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->pPresentationTimingCount, pPacket->pPresentationTimings);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPastPresentationTimingGOOGLE(dump_inst, pPacket->result, pPacket->device, pPacket->swapchain, pPacket->pPresentationTimingCount, pPacket->pPresentationTimings);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetDiscardRectangleEXT: { 
            packet_vkCmdSetDiscardRectangleEXT* pPacket = (packet_vkCmdSetDiscardRectangleEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetDiscardRectangleEXT(dump_inst, pPacket->commandBuffer, pPacket->firstDiscardRectangle, pPacket->discardRectangleCount, pPacket->pDiscardRectangles);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetDiscardRectangleEXT(dump_inst, pPacket->commandBuffer, pPacket->firstDiscardRectangle, pPacket->discardRectangleCount, pPacket->pDiscardRectangles);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkSetHdrMetadataEXT: { 
            packet_vkSetHdrMetadataEXT* pPacket = (packet_vkSetHdrMetadataEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkSetHdrMetadataEXT(dump_inst, pPacket->device, pPacket->swapchainCount, pPacket->pSwapchains, pPacket->pMetadata);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkSetHdrMetadataEXT(dump_inst, pPacket->device, pPacket->swapchainCount, pPacket->pSwapchains, pPacket->pMetadata);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCmdSetSampleLocationsEXT: { 
            packet_vkCmdSetSampleLocationsEXT* pPacket = (packet_vkCmdSetSampleLocationsEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCmdSetSampleLocationsEXT(dump_inst, pPacket->commandBuffer, pPacket->pSampleLocationsInfo);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCmdSetSampleLocationsEXT(dump_inst, pPacket->commandBuffer, pPacket->pSampleLocationsInfo);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetPhysicalDeviceMultisamplePropertiesEXT: { 
            packet_vkGetPhysicalDeviceMultisamplePropertiesEXT* pPacket = (packet_vkGetPhysicalDeviceMultisamplePropertiesEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetPhysicalDeviceMultisamplePropertiesEXT(dump_inst, pPacket->physicalDevice, pPacket->samples, pPacket->pMultisampleProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetPhysicalDeviceMultisamplePropertiesEXT(dump_inst, pPacket->physicalDevice, pPacket->samples, pPacket->pMultisampleProperties);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkCreateValidationCacheEXT: { 
            packet_vkCreateValidationCacheEXT* pPacket = (packet_vkCreateValidationCacheEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkCreateValidationCacheEXT(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pValidationCache);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkCreateValidationCacheEXT(dump_inst, pPacket->result, pPacket->device, pPacket->pCreateInfo, pPacket->pAllocator, pPacket->pValidationCache);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkDestroyValidationCacheEXT: { 
            packet_vkDestroyValidationCacheEXT* pPacket = (packet_vkDestroyValidationCacheEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkDestroyValidationCacheEXT(dump_inst, pPacket->device, pPacket->validationCache, pPacket->pAllocator);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkDestroyValidationCacheEXT(dump_inst, pPacket->device, pPacket->validationCache, pPacket->pAllocator);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkMergeValidationCachesEXT: { 
            packet_vkMergeValidationCachesEXT* pPacket = (packet_vkMergeValidationCachesEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkMergeValidationCachesEXT(dump_inst, pPacket->result, pPacket->device, pPacket->dstCache, pPacket->srcCacheCount, pPacket->pSrcCaches);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkMergeValidationCachesEXT(dump_inst, pPacket->result, pPacket->device, pPacket->dstCache, pPacket->srcCacheCount, pPacket->pSrcCaches);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetValidationCacheDataEXT: { 
            packet_vkGetValidationCacheDataEXT* pPacket = (packet_vkGetValidationCacheDataEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetValidationCacheDataEXT(dump_inst, pPacket->result, pPacket->device, pPacket->validationCache, pPacket->pDataSize, pPacket->pData);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetValidationCacheDataEXT(dump_inst, pPacket->result, pPacket->device, pPacket->validationCache, pPacket->pDataSize, pPacket->pData);
                break;
            }
            break;
        }
        case VKTRACE_TPI_VK_vkGetMemoryHostPointerPropertiesEXT: { 
            packet_vkGetMemoryHostPointerPropertiesEXT* pPacket = (packet_vkGetMemoryHostPointerPropertiesEXT*)(packet->pBody);
            ApiDumpInstance& dump_inst = ApiDumpInstance::current();
            const ApiDumpSettings& settings(dump_inst.settings());
            dump_inst.setThreadID(packet->thread_id);
            if (dump_inst.settings().format() == ApiDumpFormat::Text) {
                settings.stream() << "GlobalPacketIndex " << packet->global_packet_index << ", ";
            }
            switch(dump_inst.settings().format()) {
            case ApiDumpFormat::Text:
                dump_text_vkGetMemoryHostPointerPropertiesEXT(dump_inst, pPacket->result, pPacket->device, pPacket->handleType, pPacket->pHostPointer, pPacket->pMemoryHostPointerProperties);
                break;
            case ApiDumpFormat::Html:
                dump_html_vkGetMemoryHostPointerPropertiesEXT(dump_inst, pPacket->result, pPacket->device, pPacket->handleType, pPacket->pHostPointer, pPacket->pMemoryHostPointerProperties);
                break;
            }
            break;
        }
        default:
            vktrace_LogWarning("Unrecognized packet_id %u, skipping.", packet->packet_id);
            break;
    }
    return;
}
