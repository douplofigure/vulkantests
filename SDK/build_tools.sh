#!/bin/bash
set -e

SDKDIR="$( dirname "$( readlink -f "${BASH_SOURCE:-$_}" )" )"
ARCHDIR=${SDKDIR}/x86_64
BINDIR="${ARCHDIR}"/bin
SHAREDDIR="${ARCHDIR}"/shared
LIBDIR="${ARCHDIR}"/lib
INCLUDEDIR="${ARCHDIR}"/include

MF=""
if [ -z ${MAKEFLAGS+xyz} ]; then
    if [ -x "$(command -v nproc)" ]; then
	MF=-j`nproc`
    fi
fi


buildVia() {
    pushd "${SDKDIR}"/source/via
    [ -d build ] || mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release -DJSONCPP_SOURCE_DIR=./ -DJSONCPP_INCLUDE_DIR=./ ..
    make $MF
    cp vkvia "${BINDIR}"
    popd
}

buildShaderc() {
    pushd "${SDKDIR}"/source/shaderc
    python2 update_shaderc_sources.py
    cd src
    [ -d build ] || mkdir build
    cd build
    cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=RelWithDebInfo \
	  -DSHADERC_SKIP_INSTALL=OFF -DSHADERC_SKIP_TESTS=ON ..
    sed -i -E 's:\/(.*?)build:..:g' libshaderc/shaderc_combined.ar
    make $MF
    cp glslc/glslc "${BINDIR}"
    mkdir -p "${LIBDIR}"/libshaderc
    ln -sf "$PWD"/libshaderc/libshaderc_combined.a "${LIBDIR}"/libshaderc
    mkdir -p "${INCLUDEDIR}"/libshaderc
    ln -sf "$PWD"/../libshaderc/include/shaderc/shaderc.h "${INCLUDEDIR}"/libshaderc/
    ln -sf "$PWD"/../libshaderc/include/shaderc/shaderc.hpp "${INCLUDEDIR}"/libshaderc/
    popd
}

buildSpirvTools() {
    pushd "${SDKDIR}"/source/spirv-tools
    cp tools/emacs/50spirv-tools.el "${SHAREDDIR}"
    [ -d build ] || mkdir build
    cd build
    cmake -DSPIRV_WERROR=OFF -DCMAKE_BUILD_TYPE=Release -DSPIRV_SKIP_TESTS=ON ..
    make $MF spirv-as spirv-cfg spirv-dis spirv-opt spirv-val
    cp tools/spirv-as "${BINDIR}"
    cp tools/spirv-cfg "${BINDIR}"
    cp tools/spirv-dis "${BINDIR}"
    cp tools/spirv-opt "${BINDIR}"
    cp tools/spirv-val "${BINDIR}"
    cp ../tools/lesspipe/spirv-lesspipe.sh "${BINDIR}"
    chmod 755 "${BINDIR}"/spirv-lesspipe.sh
    popd
}

buildSpirvCross() {
    pushd "${SDKDIR}"/source/spirv-cross
    [ -d build ] || mkdir build
    cd build
    cmake ..
    make $MF
    cp spirv-cross "${BINDIR}"
    popd
}

buildVLF() {
    pushd "${SDKDIR}"/source/layer_factory
    [ -d build ] || mkdir build
    cd build
    cmake ..
    make $MF
    popd
}

buildVkconfig() {
    pushd "${SDKDIR}"/source/vkconfig
    [ -d build ] || mkdir build
    cd build
    cmake -DVulkan_INCLUDE_DIR="${INCLUDEDIR}" -DCMAKE_INSTALL_BINDIR="${BINDIR}" ..
    make $MF
    [ -e vkconfig ] && [ -x vkconfig ] && mv vkconfig "${BINDIR}"
    popd
}

buildValidationLayers() {
    [ ! -d $SDKDIR/source/glslang/build/install ] && echo "Please run the build_samples.sh script first" && exit
    pushd "${SDKDIR}"/source/layers
    [ -d build ] || mkdir build
    cd build
    cmake -DVulkan-Headers_SOURCE_DIR=$ARCHDIR -DCMAKE_MODULE_PATH=$SDKDIR/source/cmake \
	  -DGLSLANG_INSTALL_DIR=$SDKDIR/source/glslang/build/install \
	  -DSPIRV_TOOLS_LIB=$LIBDIR/spirv-tools/libSPIRV-Tools.a \
	  -DSPIRV_TOOLS_OPT_LIB=$LIBDIR/spirv-tools/libSPIRV-Tools-opt.a ..
    make $MF
    cp -fv *.so $LIBDIR/
    popd
}

buildLunargLayers() {
    pushd "${SDKDIR}"/source/layersvt
    [ -d build ] || mkdir build
    cd build
    cmake -DJSONCPP_SOURCE_DIR=.. ..
    make $MF
    cp -fv *.so $LIBDIR/
    popd
    pushd "${SDKDIR}"/source/layersvt/vktrace_layer
    [ -d build ] || mkdir build
    cd build
    cmake ..
    make $MF
    cp -fv ../libVkLayer_vktrace_layer.so $LIBDIR
    popd
}

checkPkgs() {
    if [ dpkg-query -l python2.7 git &>/dev/null ]; then
	echo "Please check that git and python2 is installed to run the build_tools script"
	exit
    fi
}

usage() {
    echo "Build tools script"
    echo "Usage: $CMDNAME [--via] [--shaderc] [--spirvtools] [--sprivcross] [--vlf] [--vkconfig] [--layers]"
    echo ""
    echo "Omitting parameters will build every tool"
}

if [[ $# == 0 ]]; then
    buildVia
    buildShaderc
    buildSpirvTools
    buildSpirvCross
    buildVLF
    buildVkconfig
fi

# parse the arguments
while test $# -gt 0; do
    case "$1" in
	--via)
	    shift
	    buildVia
	    ;;
	--shaderc)
	    shift
	    buildShaderc
	    ;;
	--spirvtools)
	    shift
	    buildSpirvTools
	    ;;
	--spirvcross)
	    shift
	    buildSpirvCross
	    ;;
	--vlf)
	    shift
	    buildVLF
	    ;;
	--vkconfig)
	    shift
	    buildVkconfig
	    ;;
	--layers)
	    shift
	    buildValidationLayers
	    buildLunargLayers
	    ;;
	--help|-h)
	    shift
	    usage
	    ;;
	*)
	    shift
	    echo "error: unknown option"
	    usage
	    ;;
    esac
done


