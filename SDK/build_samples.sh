#!/bin/bash
set -e

SDKDIR="$( dirname "$( readlink -f "${BASH_SOURCE:-$_}" )" )"
ARCHDIR=${SDKDIR}/x86_64
BINDIR="${ARCHDIR}"/bin
SHAREDDIR="${ARCHDIR}"/shared
LIBDIR="${ARCHDIR}"/lib
INCLUDEDIR="${ARCHDIR}"/include

# Build sample dependencies.
pushd source/glslang
[ -d build ] || mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=install -DCMAKE_INSTALL_LIBDIR=lib .. ..
make -j`nproc` install
mkdir -p $LIBDIR/glslang
mkdir -p $LIBDIR/spirv-tools
cp SPIRV/libSPIRV.a $LIBDIR/glslang
cp SPIRV/libSPVRemapper.a $LIBDIR/glslang
cp glslang/libglslang.a $LIBDIR/glslang
cp glslang/OSDependent/Unix/libOSDependent.a $LIBDIR/glslang
cp OGLCompilersDLL/libOGLCompiler.a $LIBDIR/glslang
cp hlsl/libHLSL.a $LIBDIR/glslang
cp External/spirv-tools/source/libSPIRV-Tools.a $LIBDIR/spirv-tools
cp External/spirv-tools/source/opt/libSPIRV-Tools-opt.a $LIBDIR/spirv-tools
cp StandAlone/spirv-remap $BINDIR
cp StandAlone/glslangValidator $BINDIR
popd

# Build the samples.
pushd samples
[ -d build ] || mkdir build
cd build
cmake ..
make -j`nproc`
popd
