#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (input_attachment_index = 0, binding = 0) uniform subpassInput inputPosition;
layout (input_attachment_index = 1, binding = 1) uniform subpassInput inputNormal;
layout (input_attachment_index = 2, binding = 2) uniform subpassInput inputAlbedo;

layout (binding = 3) uniform LightData {

    vec4 position[32];
    vec4 color[32];

} inLights;

layout (binding = 4) uniform CameraData {

    vec3 position;

} inCamera;

layout (location = 0) out vec4 ppResult;


void main() {

    vec3 light = vec3(0.1);

    vec3 n = normalize(subpassLoad(inputNormal).xyz);
    vec3 p = subpassLoad(inputPosition).xyz;
    vec3 c = inCamera.position - p;

    for (int i = 0; i < 32; ++i) {

        vec3 r = reflect(-c , n);
        vec3 l = inLights.position[i].xyz - p;

        if (inLights.color[i].w >= 1.0) {

            light += clamp(dot(normalize(inLights.position[i].xyz), n), 0, 1) * inLights.color[i].xyz;

        } else {
            light += clamp(dot(normalize(l), n), 0, 1) * inLights.color[i].xyz / pow(length(l), inLights.position[i].w);
        }

    }

    ppResult = vec4(light, 1) * subpassLoad(inputAlbedo);
    //ppResult = subpassLoad(inputAlbedo);

}
