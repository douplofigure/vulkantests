VULKAN_SDK_PATH := ./SDK/x86_64
CFLAGS = -std=c++17 -I$(VULKAN_SDK_PATH)/include -I./GLFW/include
LDFLAGS = -L$(VULKAN_SDK_PATH)/lib -L./GLFW -lglfw -lvulkan

test: main.cpp
	g++ -o test main.cpp $(CFLAGS) $(LDFLAGS)

tutorial: tutorial.cpp
	g++ -o tutorial tutorial.cpp $(CFLAGS) $(LDFLAGS)
